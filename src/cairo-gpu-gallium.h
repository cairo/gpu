/* Cairo - a vector graphics library with display and print output
 *
 * Copyright © 2009 Eric Anholt
 * Copyright © 2009 Chris Wilson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation
 * (the "LGPL") or, at your option, under the terms of the Mozilla
 * Public License Version 1.1 (the "MPL"). If you do not alter this
 * notice, a recipient may use your version of this file under either
 * the MPL or the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-2.1; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * You should have received a copy of the MPL along with this library
 * in the file COPYING-MPL-1.1
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL or the MPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the cairo graphics library.
 *
 * The Initial Developer of the Original Code is Eric Anholt.
 */

#ifndef CAIRO_GPU_GALLIUM_H
#define CAIRO_GPU_GALLIUM_H

#include "cairo.h"

#if CAIRO_HAS_GPU_GALLIUM_SURFACE

CAIRO_BEGIN_DECLS

struct drm_api;
struct pipe_screen;
struct pipe_context;
struct pipe_texture;

#define CAIRO_GPU_GALLIUM_TRACE 1
#define CAIRO_GPU_GALLIUM_DISABLE_TEXTURE_NON_POWER_OF_TWO 2

cairo_public cairo_space_t*
cairo_gallium_drm_space_wrap(struct drm_api* drm_api, struct pipe_screen* pipe_screen, unsigned flags);

cairo_public cairo_space_t*
cairo_gallium_drm_space_create (int card, unsigned flags);

cairo_public cairo_space_t*
cairo_gallium_x11_space_wrap (Display* dpy, int screen, cairo_bool_t owns_dpy, unsigned flags);

cairo_public cairo_space_t*
cairo_gallium_x11_space_create (const char* name, unsigned flags);

cairo_public cairo_space_t*
cairo_gallium_hardpipe_space_create(unsigned flags);

cairo_space_t *
cairo_gallium_softpipe_space_create(unsigned flags);

cairo_public cairo_space_t*
cairo_gallium_space_create (unsigned flags);

cairo_public cairo_space_t *
cairo_gallium_space_wrap(struct pipe_screen* screen, struct pipe_context* (*context_create)(void*, struct pipe_screen*), void* cookie, unsigned flags);

cairo_public struct pipe_screen*
cairo_gallium_space_get_screen(cairo_space_t* space);

cairo_public struct pipe_context*
cairo_gallium_space_create_context(cairo_space_t* space);

cairo_public struct pipe_texture*
cairo_gallium_surface_get_texture(cairo_surface_t* surface);

CAIRO_END_DECLS

#else  /* CAIRO_HAS_GL_SURFACE */
# error Cairo was not compiled with support for the GPU OpenGL backend
#endif /* CAIRO_HAS_GL_SURFACE */

#endif /* CAIRO_GPU_GALLIUM_H */

