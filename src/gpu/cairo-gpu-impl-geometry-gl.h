/* the geometry context must always be bound before calling geometry_ functions */

/*
	_cairo_gpu_geometry_begin
	// write vertex data
	_cairo_gpu_geometry_end
	// draw

	//optionally
	{
		// do something else
		_cairo_gpu_bind
		// draw again
	}
	_cairo_gpu_geometry_put
*/

static inline cairo_gpu_geometry_t*
_cairo_gpu_geometry_init(cairo_gpu_geometry_t * geometry)
{
	geometry->flags = 0;
	geometry->vbo = 0;
	geometry->vao = 0;
	geometry->vbo_size = 0;
	geometry->data = 0;
	geometry->data_size = 0;
	return geometry;
}

static inline cairo_gpu_geometry_t *
_cairo_gpu_geometry_fini_vbo(cairo_gpu_context_t * ctx, cairo_gpu_geometry_t * geometry)
{
	if(geometry->vbo)
	{
		if(geometry->flags & GEOM_VBO_MAPPED)
			ctx->gl.UnmapBufferARB(GL_ARRAY_BUFFER);

		assert(ctx);
		ctx->gl.DeleteBuffersARB(1, &geometry->vbo);
		geometry->vbo = 0;
		geometry->vbo_size = 0;
	}

	if(geometry->vao)
	{
		assert(ctx);
		ctx->gl.DeleteVertexArrays(1, &geometry->vao);
		geometry->vao = 0;
	}

	geometry->flags = 0;
	return geometry;
}


static inline cairo_gpu_geometry_t *
_cairo_gpu_geometry_fini(cairo_gpu_context_t * ctx, cairo_gpu_geometry_t * geometry)
{
	if(geometry->data)
	{
		free(geometry->data);
		geometry->data = 0;
		geometry->data_size = 0;
	}

	return _cairo_gpu_geometry_fini_vbo(ctx, geometry);
}

static void *
_cairo_gpu_geometry_begin(cairo_gpu_context_t * ctx, cairo_gpu_geometry_t * geometry, unsigned mode, int count, int vert, int color, int tex)
{
	unsigned size = count * ((vert + !!color + tex) * sizeof(float));

	_cairo_gpu_geometry_fini_vbo(ctx, geometry);

	geometry->id = _cairo_id();
	geometry->mode = mode;
	geometry->vert = vert;
	geometry->color = color;
	geometry->tex = tex;

	geometry->flags &=~ GEOM_SETUP;

	assert(!(geometry->flags & GEOM_VBO_MAPPED));

	// it seems VBOs are a win for large buffers, but a loss for small ones...
	// TODO: maybe apply some threshold to use VBOs
	if(ctx->space->use_vbo)
	{
		void *p = 0;

		if(!geometry->vbo)
			ctx->gl.GenBuffersARB(1, &geometry->vbo);

		if(geometry->vbo)
		{
			if(geometry->id != ctx->vbo_id)
			{
				ctx->gl.BindBufferARB(GL_ARRAY_BUFFER, geometry->vbo);
				ctx->vbo_id = geometry->id;
			}

			if(geometry->vbo_size < size)
			{
				if(!geometry->vbo_size)
					geometry->vbo_size = 1;
				while(geometry->vbo_size < size)
					geometry->vbo_size <<= 1;
				ctx->gl.BufferDataARB(GL_ARRAY_BUFFER, geometry->vbo_size, 0, GL_STREAM_DRAW);
			}

#ifdef GLEW_ARB_map_buffer_range
			if(GLEW_ARB_map_buffer_range)
				p = ctx->gl.MapBufferRange(GL_ARRAY_BUFFER, 0, size, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
			else
#endif
				p = ctx->gl.MapBufferARB(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
		}

		if(p)
		{
			geometry->flags |= GEOM_VBO_MAPPED;
			return p;
		}
		else
		{
			fprintf(stderr, "vbo create/map failed for %i! fallback!\n", size);
		}
	}

	if(geometry->data_size < size)
	{
		if(!geometry->data_size)
			geometry->data_size = 1;
		while(geometry->data_size < size)
			geometry->data_size <<= 1;
		if(geometry->data)
			free(geometry->data);

		geometry->data = malloc(geometry->data_size);
	}

	return geometry->data;
}

static void
_cairo_gpu_geometry_end(cairo_gpu_context_t * ctx, cairo_gpu_geometry_t * geometry, int count)
{
	int size = count * ((geometry->vert + geometry->tex + !!geometry->color) * sizeof(float));

	geometry->count = count;
	if(geometry->vbo)
	{
		if(geometry->id != ctx->vbo_id)
		{
			ctx->gl.BindBufferARB(GL_ARRAY_BUFFER, (unsigned)geometry->vbo);
			ctx->vbo_id = geometry->id;
		}

		if(geometry->flags & GEOM_VBO_MAPPED)
		{
			ctx->gl.UnmapBufferARB(GL_ARRAY_BUFFER);
			geometry->flags &= ~GEOM_VBO_MAPPED;
		}
		else if(size)
			ctx->gl.BufferDataARB(GL_ARRAY_BUFFER, size, geometry->data, GL_STREAM_DRAW);
	}
}

static inline void
_cairo_gpu_geometry__do_bind(cairo_gpu_context_t * ctx, cairo_gpu_geometry_t* geometry)
{
	char* base;
	GLsizei stride;
	int vert = geometry->vert;
	int tex = geometry->tex;
	int color = geometry->color;

	// XXX: fix and reenable VAO support
	//if(0 && !geometry->vao && GLEW_ARB_vertex_array_object)
	if(0)
	{
		ctx->gl.GenVertexArrays(1, &geometry->vao);
		if(geometry->vao)
		{
			ctx->gl.BindVertexArray(geometry->vao);
			ctx->vao_id = geometry->id;
		}
	}

	if(geometry->id != ctx->vbo_id)
	{
		ctx->gl.BindBufferARB(GL_ARRAY_BUFFER, (unsigned)geometry->vbo);
		ctx->vbo_id = geometry->id;
	}

	if(geometry->vbo)
		base = 0;
	else
		base = (char *)geometry->data;

	stride = (vert + !!color + tex) * sizeof(float);
	if(vert)
	{
		ctx->gl.VertexPointer(vert, GL_FLOAT, stride, base);
		if(!ctx->vertex_array)
		{
			ctx->gl.EnableClientState(GL_VERTEX_ARRAY);
			ctx->vertex_array = 1;
		}
	}
	else
	{
		if(ctx->vertex_array)
		{
			ctx->gl.DisableClientState(GL_VERTEX_ARRAY);
			ctx->vertex_array = 0;
		}
	}

	if(color)
	{
		ctx->gl.ColorPointer(color, GL_UNSIGNED_BYTE, stride, base + vert * sizeof(float));
		if(!ctx->color_array)
		{
			ctx->gl.EnableClientState(GL_COLOR_ARRAY);
			ctx->color_array = 1;
		}
	}
	else
	{
		if(ctx->color_array)
		{
			ctx->gl.DisableClientState(GL_COLOR_ARRAY);
			ctx->color_array = 0;
		}
	}

	// TODO: this is an hack for show_glyphs, we should maybe do it more generally?
	if(tex)
	{
		ctx->gl.ClientActiveTextureARB(GL_TEXTURE0);
		ctx->gl.TexCoordPointer(tex, GL_FLOAT, stride, base + (vert + !!color) * sizeof(float));
		if(!ctx->tex_array)
			ctx->gl.EnableClientState(GL_TEXTURE_COORD_ARRAY);
		ctx->gl.ClientActiveTextureARB(GL_TEXTURE1);
		ctx->gl.TexCoordPointer(tex, GL_FLOAT, stride, base + (vert + !!color) * sizeof(float));
		if(!ctx->tex_array)
		{
			ctx->gl.EnableClientState(GL_TEXTURE_COORD_ARRAY);
			ctx->tex_array = 1;
		}
	}
	else
	{
		if(ctx->tex_array)
		{
			ctx->gl.ClientActiveTextureARB(GL_TEXTURE0);
			ctx->gl.DisableClientState(GL_TEXTURE_COORD_ARRAY);
			ctx->gl.ClientActiveTextureARB(GL_TEXTURE1);
			ctx->gl.DisableClientState(GL_TEXTURE_COORD_ARRAY);
			ctx->tex_array = 0;
		}
	}
}

static inline void
_cairo_gpu_geometry_bind(cairo_gpu_context_t * ctx, cairo_gpu_geometry_t * geometry)
{
	if(geometry->vao)
	{
		if(ctx->vao_id != geometry->id)
		{
			ctx->gl.BindVertexArray(geometry->vao);
			ctx->vao_id = geometry->id;
		}
	}
	else
	{
		if(ctx->vao_id)
		{
			ctx->gl.BindVertexArray(0);
			ctx->vao_id = 0;
		}

		if(ctx->vao0_id != geometry->id)
		{
			geometry->flags &= ~GEOM_SETUP;
			ctx->vao0_id = geometry->id;
		}
	}

	ctx->mode = geometry->mode;
	ctx->count = geometry->count;

	if(geometry->flags & GEOM_SETUP)
		return;

	_cairo_gpu_geometry__do_bind(ctx, geometry);
}

#if 0
static inline void
_cairo_gpu_geometry_put(cairo_gpu_context_t * ctx, cairo_gpu_geometry_t * geometry)
{
	if(geometry->vbo && (geometry->vbo_size > 256 * 1024))
	{
		if(geometry->vbo != ctx->vbo)
		{
			ctx->gl.BindBufferARB(GL_ARRAY_BUFFER, (unsigned)geometry->vbo);
			ctx->vbo = geometry->vbo;
		}

		// this seems to sometimes fail on nVidia (why?!)
		ctx->gl.BufferDataARB(GL_ARRAY_BUFFER, 0, 0, GL_STREAM_DRAW);
		while(ctx->gl.GetError())
		{}

		geometry->vbo_size = 0;
	}
}
#endif

/*
static inline cairo_geometry_t*
_cairo_gpu_space_create_geometry(cairo_gpu_space_t* space)
{
	cairo_gpu_user_geometry_t* geometry = (cairo_gpu_geometry_t*)malloc(sizeof(cairo_gpu_user_geometry_t));
	geometry->space = space;
	_cairo_gpu_geometry_init(&geometry->geometry, space);
	return &geometry->base;
}

static inline void
_cairo_gpu_geometry_destroy(cairo_gpu_user_geometry_t* geometry)
{
	cairo_gpu_context_t* ctx = cairo_space_bind(geometry->space);
	_cairo_gpu_geometry_fini(ctx, &geometry->geometry);
	free(geometry);
}
*/
