static inline int
_cairo_gpu_surface_destination_drawable(cairo_gpu_surface_t * surface)
{
	return -1;
}

static inline int
_cairo_gpu_surface_destination_msaa(cairo_gpu_surface_t * surface, int want_msaa)
{
	return SURF_TEX;
}

static inline int
_cairo_gpu_surface_destination(cairo_gpu_surface_t * surface)
{
	return _cairo_gpu_surface_destination_msaa(surface, surface->want_msaa);
}

static inline cairo_bool_t
_cairo_gpu_surface_has_physical_alpha(cairo_gpu_surface_t* surface)
{
	// We assume that the unused alpha channel is set to 1
	// TODO: due to this, we don't try to use the alpha channels of drawables; check whether we can do something about this
	return TRUE;
}

static inline int
_cairo_gpu_surface_use_msaa(cairo_gpu_surface_t * surface)
{
	return 0;
}


static void
_cairo_gpu_surface__create_tex(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface)
{
	// NO: unsigned format = (surface->base.content == CAIRO_CONTENT_COLOR) ? GL_RGB : GL_RGBA;
	// we must have an alpha channel to support EXTEND_NONE (and we also assume it exists to draw trapezoid masks)
	_cairo_gpu_texture_realize(ctx, &surface->texture);
}

static void
_cairo_gpu_surface__set(cairo_gpu_context_t* ctx, int mask, cairo_gpu_surface_t * surface, int i)
{
      if(i == SURF_TEX)
       {
               if(!surface->texture.texture)
                       _cairo_gpu_surface__create_tex(ctx, surface);

               if(mask & FB_READ)
               {
               	      ctx->read_surface = surface->texture.surface;
               	      ctx->read_texture = &surface->texture;
               }

               if(mask & FB_DRAW)
        	       _cairo_gallium_context_set_framebuffer(ctx, surface->texture.surface, surface->width, surface->height);
       }
       else
	       assert(0);
}


static void
_cairo_gpu_blit_image(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, cairo_gpu_texture_t* texture, int dst_x, int dst_y, int src_x, int src_y, int width, int height, int zoom_x, int zoom_y);

static __attribute__((unused)) void
_cairo_gpu_fill_rect_diff(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface, cairo_rectangle_int_t * out, cairo_rectangle_int_t * in, float r, float g, float b, float a);

// may clobber read fbo and draw state
static void
_cairo_gpu_surface__update(cairo_gpu_context_t* ctx, cairo_gpu_surface_t * surface, int j)
{
	assert(!(surface->valid_mask & (1 << j)));

	if(!ctx)
		ctx = _cairo_gpu_space_bind(surface->space);

	if(j == SURF_TEX)
	{
		if(!surface->texture.texture)
			_cairo_gpu_surface__create_tex(ctx, surface);
	}

	{
		cairo_rectangle_int_t *box = &surface->bbox[j];

		if(surface->base.content == CAIRO_CONTENT_COLOR)
			_cairo_gpu_context_fill_rect_unbound(ctx, surface, box->x, box->y, box->width, box->height, 0, 0, 0, 1);
		else
			_cairo_gpu_context_fill_rect_unbound(ctx, surface, box->x, box->y, box->width, box->height, 0, 0, 0, 0);

		box->width = 0;
		box->height = 0;
	}

	surface->valid_mask |= 1 << j;
}

static inline cairo_gpu_context_t*
_cairo_gpu_surface__lookup_context(cairo_gpu_surface_t * surface, unsigned mask, int idx)
{
	return _cairo_gpu_space_tls_lookup_context(_cairo_gpu_space_get_tls(surface->space));
}

static inline cairo_gpu_context_t*
_cairo_gpu_surface_lookup_context(cairo_gpu_surface_t * surface, unsigned mask)
{
	return _cairo_gpu_surface__lookup_context(surface, mask, _cairo_gpu_surface_destination(surface));
}

static inline void
_cairo_gpu_surface__bind_to(cairo_gpu_surface_t * surface, cairo_gpu_context_t* ctx, unsigned mask, int idx)
{
	_cairo_gpu_context_bind(ctx);

	_cairo_gpu_surface__set(ctx, mask, surface, idx);
	if(!(surface->valid_mask & (1 << idx)))
		_cairo_gpu_surface__update(ctx, surface, idx);
}

static inline void
_cairo_gpu_surface_bind_to(cairo_gpu_surface_t * surface, cairo_gpu_context_t* ctx, unsigned mask)
{
	return _cairo_gpu_surface__bind_to(surface, ctx, mask, _cairo_gpu_surface_destination(surface));
}

static inline cairo_gpu_context_t*
_cairo_gpu_surface__bind(cairo_gpu_surface_t * surface, unsigned mask, int idx)
{
	cairo_gpu_context_t* ctx;
	ctx = _cairo_gpu_space_bind(surface->space);

	_cairo_gpu_surface__set(ctx, mask, surface, idx);
	if(!(surface->valid_mask & (1 << idx)))
		_cairo_gpu_surface__update(ctx, surface, idx);
	return ctx;
}

static inline cairo_gpu_context_t*
_cairo_gpu_surface_bind(cairo_gpu_surface_t * surface, unsigned mask)
{
	return _cairo_gpu_surface__bind(surface, mask, _cairo_gpu_surface_destination(surface));
}

static inline cairo_gpu_context_t*
_cairo_gpu_surface_bind_drawable(cairo_gpu_surface_t * surface, unsigned mask)
{
	return _cairo_gpu_surface__bind(surface, mask, _cairo_gpu_surface_destination_drawable(surface));
}

// TODO: maybe we want to generate mipmaps?
// may clobber read fb, blend/color mask
// this binds the texture
static inline cairo_gpu_texture_t*
_cairo_gpu_surface_begin_texture(cairo_gpu_surface_t * surface, cairo_gpu_context_t* ctx, int idx)
{
	if(!(surface->valid_mask & (1 << SURF_TEX)))
	{
		if(!ctx)
			ctx = _cairo_gpu_space_bind(surface->space);

		_cairo_gpu_surface__update(ctx, surface, SURF_TEX);
	}

#if 0
	if(!(surface->valid_mask & (1 << SURF_PSEUDO_MIPMAPS)) && !surface->tex_rectangle)
	{
		assert(0); // TODO
	}
#endif
	return &surface->texture;
}

static void
_cairo_gpu_surface_end_texture(cairo_gpu_context_t * ctx, cairo_gpu_surface_t* surface, cairo_gpu_texture_t* texture)
{
}

static int _cairo_gpu_aa_formats[] = {
	16, 4,
	16, 8,
	16, 16,
	8, 4,
	8, 8,
	4, 4, // 4x MSAA
	0, 0
};

static cairo_int_status_t
_cairo_gpu_surface_set_samples(void* abstract_surface, unsigned coverage_samples, unsigned color_samples)
{
	// TODO: implement this
	return CAIRO_STATUS_NO_MEMORY;
}

static cairo_surface_t *
_cairo_gpu_surface_create(void * abstract_space, void* abstract_surface, cairo_content_t content, unsigned long drawable, unsigned long visualid, cairo_bool_t double_buffer, double width, double height, int color_mantissa_bits, int alpha_mantissa_bits, int exponent_bits, int shared_exponent_bits)
{
	cairo_gpu_space_t* space = abstract_space;
	cairo_gpu_surface_t* surface = abstract_surface;

	if(surface && surface->base.backend != &_cairo_gpu_surface_backend)
		return _cairo_surface_create_in_error(CAIRO_STATUS_SURFACE_TYPE_MISMATCH);

	if(!space)
	{
		if(!surface)
			return _cairo_surface_create_in_error(CAIRO_STATUS_SURFACE_FINISHED);

		space = surface->space;
	}

	if(width < 0)
	{
		if(!surface)
			return _cairo_surface_create_in_error(CAIRO_STATUS_SURFACE_FINISHED);
		width = surface->width;
	}

	if(height < 0)
	{
		if(!surface)
			return _cairo_surface_create_in_error(CAIRO_STATUS_SURFACE_FINISHED);
		height = surface->height;
	}

	if(drawable)
		return _cairo_surface_create_in_error(CAIRO_STATUS_SURFACE_TYPE_MISMATCH);

	if(surface)
		_cairo_gpu_texture_fini(0, &surface->texture);
	else
	{
		surface = calloc(1, sizeof(cairo_gpu_surface_t));
		if(unlikely(surface == NULL))
			return (cairo_surface_t*)_cairo_surface_create_in_error(_cairo_error(CAIRO_STATUS_NO_MEMORY));

		_cairo_surface_init(&surface->base, &_cairo_gpu_surface_backend, 0);
	}

	if(width < 1)
		width = 1;
	if(height < 1)
		height = 1;

	surface->width = width;
	surface->height = height;
	surface->base.content = content;
	surface->space = (cairo_gpu_space_t*)cairo_space_reference(&space->base);

	_cairo_gpu_texture_init(space, &surface->texture, width, height);

	// uninitialized, so set fully dirty
	surface->bbox[SURF_TEX].x = 0;
	surface->bbox[SURF_TEX].y = 0;
	surface->bbox[SURF_TEX].width = surface->width;
	surface->bbox[SURF_TEX].height = surface->height;
	surface->valid_mask = 0;
	return &surface->base;
}

static cairo_int_status_t
_cairo_gpu_surface_copy_page(void *abstract_surface)
{
	return CAIRO_STATUS_SUCCESS;
}

static cairo_status_t
_cairo_gpu_surface_get_image(cairo_gpu_surface_t * surface, cairo_rectangle_int_t * interest, cairo_image_surface_t ** image_out, cairo_rectangle_int_t * rect_out, void** extra)
{
	struct pipe_transfer* transfer;
	unsigned char* data;
	cairo_image_surface_t* image;
	cairo_gpu_context_t* ctx;
	pixman_format_code_t pixman_format;

	ctx = _cairo_gpu_space_bind(surface->space);

	if(!(surface->valid_mask & (1 << SURF_TEX)))
		_cairo_gpu_surface__update(ctx, surface, SURF_TEX);

	if(ctx->pipe->is_texture_referenced(ctx->pipe, surface->texture.texture, 0, 0) & PIPE_REFERENCED_FOR_WRITE)
	{
		struct pipe_fence_handle* fence;
		ctx->pipe->flush(ctx->pipe, PIPE_FLUSH_RENDER_CACHE, &fence);

		if(fence)
		{
			ctx->space->screen->fence_finish(ctx->space->screen, fence, 0);
			ctx->space->screen->fence_reference(ctx->space->screen, &fence, 0);
		}
	}

	transfer = surface->space->screen->get_tex_transfer(surface->space->screen, surface->texture.texture, 0, 0, 0, PIPE_TRANSFER_WRITE, interest ? interest->x : 0, interest ? interest->y : 0, interest ? interest->width : (int)surface->width, interest ? interest->height : (int)surface->height);
	data = surface->space->screen->transfer_map(surface->space->screen, transfer);

	if(transfer->format == PIPE_FORMAT_A8R8G8B8_UNORM)
	{
		if(surface->base.content == CAIRO_CONTENT_COLOR)
			pixman_format = PIXMAN_x8r8g8b8;
		else
			pixman_format = PIXMAN_a8r8g8b8;
	}
	else
		abort();

	image = (cairo_image_surface_t*)_cairo_image_surface_create_with_pixman_format (data, pixman_format, interest ? interest->width : (int)surface->width, interest ? interest->height : (int)surface->height, transfer->stride);
	if(image->base.status)
		return image->base.status;

	if(rect_out)
	{
		if(interest)
			*rect_out = *interest;
		else
		{
			rect_out->x = 0;
			rect_out->y = 0;
			rect_out->width = surface->width;
			rect_out->width = surface->height;
		}
	}
	*image_out = image;
	*extra = transfer;

	return CAIRO_STATUS_SUCCESS;
}

static cairo_status_t
_cairo_gpu_surface_put_image(cairo_gpu_surface_t* dst, cairo_image_surface_t * src, int src_x, int src_y, int width, int height, int dst_x, int dst_y, int is_write, void* extra)
{
	struct pipe_transfer* transfer = (struct pipe_transfer*)extra;
	dst->space->screen->transfer_unmap(dst->space->screen, transfer);
	dst->space->screen->tex_transfer_destroy(transfer);
	return CAIRO_STATUS_SUCCESS;
}

static inline void
_cairo_gpu_surface_modified_(cairo_gpu_surface_t* surface, int idx, int x, int y, int width, int height);

static cairo_status_t
_cairo_gpu_surface_draw_image(cairo_gpu_surface_t* dst, cairo_image_surface_t * src, int src_x, int src_y, int width, int height, int dst_x, int dst_y)
{
	cairo_gpu_context_t* ctx = _cairo_gpu_space_bind(dst->space);
	if(!(dst->valid_mask & (1 << SURF_TEX)))
	{
		if(dst_x == 0 && dst_y == 0 && width == (int)dst->width && height == (int)dst->height)
		{
			if(!dst->texture.texture)
				_cairo_gpu_surface__create_tex(ctx, dst);
		}
		else
			_cairo_gpu_surface__update(ctx, dst, SURF_TEX);
	}

	_cairo_gpu_context_upload_pixels(ctx, dst, -1, &dst->texture, src, src_x, src_y, width, height, dst_x, dst_y);
	_cairo_gpu_surface_modified_(dst, SURF_TEX, dst_x, dst_y, width, height);
	return CAIRO_STATUS_SUCCESS;
}

static cairo_status_t
_cairo_gpu_surface_finish(void *abstract_surface)
{
	cairo_gpu_surface_t *surface = abstract_surface;
	cairo_gpu_space_t* space = surface->space;

	_cairo_gpu_surface_set_samples(surface, 0, 0);

	_cairo_gpu_texture_fini(0, &surface->texture);

	cairo_space_destroy(&space->base);

	return CAIRO_STATUS_SUCCESS;
}

struct pipe_texture*
cairo_gallium_surface_get_texture(cairo_surface_t * abstract_surface)
{
	cairo_gpu_surface_t* surface = (cairo_gpu_surface_t*)abstract_surface;
	if(abstract_surface->backend != &_cairo_gpu_surface_backend)
		return 0;

	if(!surface->texture.texture)
		_cairo_gpu_surface__create_tex(0, surface);

	return surface->texture.texture;
}

static inline void
_cairo_gpu_surface_orient_drawable_like_texture(cairo_gpu_surface_t* draw_surf, cairo_gpu_surface_t* tex_surf)
{
}
