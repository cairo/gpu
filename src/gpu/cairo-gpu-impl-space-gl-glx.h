static __thread XErrorEvent* _cairo_gpu_x11_error;

static int _cairo_gpu_x11_error_handler(Display * dpy, XErrorEvent * error)
{
	_cairo_gpu_x11_error = error;
	return 0;
}

static void
_cairo_gpu_glx_init(cairo_glx_t* glx, PFNGLGETPROCADDRESSPROC GetProcAddress)
{
#define DO(x) glx->x = GetProcAddress("glX" #x)
	DO(BindTexImageEXT);
	DO(ChooseFBConfigSGIX);
	DO(CreateNewContext);
	DO(CreatePbuffer);
	DO(CreatePixmap);
	DO(DestroyContext);
	DO(DestroyGLXPixmap);
	DO(DestroyPbuffer);
	DO(DestroyPixmap);
	DO(GetCurrentContext);
	DO(GetCurrentDisplay);
	DO(GetCurrentDrawable);
	DO(GetCurrentReadDrawable);
	DO(GetFBConfigAttribSGIX);
	DO(GetFBConfigs);
	DO(GetVisualFromFBConfigSGIX);
	DO(IsDirect);
	DO(MakeContextCurrent);
	DO(MakeCurrent);
	DO(QueryDrawable);
	DO(ReleaseTexImageEXT);
	DO(SwapBuffers);
	DO(WaitGL);
	DO(WaitX);
#undef DO
}

// we don't try to support floating point pbuffers, because if they are supported, FBOs should be too
static GLXFBConfig
_cairo_glxfbconfig_for_pbuffer(cairo_gpu_space_t* space, Display* dpy, int color_bits, int alpha_bits)
{
	int nfbc;
	GLXFBConfig fbc = 0;
	GLXFBConfig* fbcs;
	int targets[4];
	int pow2_target;
	unsigned long long score = ~0ULL;
	int i;
	targets[0] = targets[1] = targets[2] = color_bits;
	targets[3] = alpha_bits;
	pow2_target = higher_pow2(targets[0] + targets[1] + targets[2] + targets[3] - 1);

	{
		int fbc_attribs[] = {GLX_DRAWABLE_TYPE, GLX_PBUFFER_BIT,
				GLX_RENDER_TYPE, GLX_RGBA_BIT,
				GLX_RED_SIZE, targets[0],
				GLX_GREEN_SIZE, targets[1],
				GLX_BLUE_SIZE, targets[2],
				GLX_ALPHA_SIZE, targets[3],
				0, 0};

		fbcs = space->glx.ChooseFBConfigSGIX(dpy, DefaultScreen(dpy), fbc_attribs, &nfbc);
	}
	if(!fbcs)
	{
retry:
		fbcs = space->glx.GetFBConfigs(dpy, DefaultScreen(dpy), &nfbc);
		if(!fbcs)
			return 0;
	}

	// ordered by (missing_components, sum deficit[i]^2, is_less_than_pow2_target, diff_from_pow2_target, sum size_i^2, junk)
	for(i = 0; i < nfbc; ++i)
	{
		int j;
		int size = 0;
		unsigned long long cscore = 0;

		for(j = 0; j < 4; ++j)
		{
			int v;
			space->glx.GetFBConfigAttribSGIX(dpy, fbcs[i], GLX_RED_SIZE + j, &v);
			if(!v && targets[j])
				cscore += (1ULL << 61);
			size += v;
			if(v < targets[j])
				cscore += (unsigned long long)((targets[j] - v) * (targets[j] - v)) << 45;
			else
				cscore += (unsigned long long)(v * v) << 20;
		}

		for(j = GLX_DEPTH_SIZE; j <= GLX_ACCUM_ALPHA_SIZE; ++j)
		{
			int v;
			space->glx.GetFBConfigAttribSGIX(dpy, fbcs[i], j, &v);
			cscore += v << 11;
		}

		{
			int v;
			space->glx.GetFBConfigAttribSGIX(dpy, fbcs[i], GLX_DOUBLEBUFFER, &v);
			cscore += 1 << 19;
		}

		if(size > pow2_target)
			cscore += (1ULL << 44) + ((unsigned long long)(size - pow2_target) << 36);
		else
			cscore += ((unsigned long long)(pow2_target - size) << 36);

		if(cscore < score)
		{
			fbc = fbcs[i];
			score = cscore;
		}
	}

	XFree(fbcs);

	if(!fbc)
		goto retry;
	return fbc;
}

static GLXFBConfig
_cairo_glxfbconfig_for_visualid(cairo_gpu_space_t* space, Display* dpy, VisualID visualid)
{
	int nfbc;
	GLXFBConfig fbc = 0;
	GLXFBConfig* fbcs;
	int fbc_attribs[] = {GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT | GLX_PIXMAP_BIT, GLX_X_RENDERABLE, 1, GLX_RENDER_TYPE, GLX_RGBA_BIT, 0, 0};
	int i;

	fbcs = space->glx.ChooseFBConfigSGIX(dpy, DefaultScreen(dpy), fbc_attribs, &nfbc);
	if(!fbcs)
		fbcs = space->glx.GetFBConfigs(dpy, DefaultScreen(dpy), &nfbc);

	for(i = 0; i < nfbc; ++i)
	{
		VisualID vivisualid;
		XVisualInfo* vi;
		vi = space->glx.GetVisualFromFBConfigSGIX(dpy, fbcs[i]);
		if(!vi)
			continue;
		vivisualid = vi->visualid;
		XFree(vi);
		if(vivisualid == visualid)
		{
			fbc = fbcs[i];
			break;
		}
	}

	XFree(fbcs);
	return fbc;
}

static inline void
_cairo_glx_space_make_current(cairo_gpu_space_t* space, cairo_gpu_gl_state_t* user)
{
	space->glx.MakeContextCurrent(user->glx.dpy, user->glx.draw_drawable, user->glx.read_drawable, user->glx.ctx);
}

static cairo_gpu_context_t*
_cairo_glx_space_create_context(cairo_gpu_space_t* space, cairo_gpu_subspace_t* sub)
{
	cairo_gpu_context_t* ctx = _cairo_gpu_context__create(space, sub);
	if(!ctx)
		return 0;

	ctx->glx.ctx = ctx->space->glx.CreateNewContext(space->glx.dpy, sub->glx.fbconfig, GLX_RGBA_TYPE, space->glx.share, space->glx.direct);

	if(!ctx->glx.ctx)
	{
		free(ctx);
		return 0;
	}

	return ctx;
}

static void
_cairo_glx_context_do_destroy(cairo_gpu_context_t * ctx)
{
	ctx->space->glx.DestroyContext(ctx->space->glx.dpy, ctx->glx.ctx);
}

static void _cairo_glx_space_do_destroy(cairo_gpu_space_t* space)
{
	space->glx.DestroyContext(space->glx.dpy, space->glx.share);
	if(space->glx.owns_dpy)
		XCloseDisplay(space->glx.dpy);
}

static void _cairo_glx_space_destroy_subspace(cairo_gpu_space_t* space, cairo_gpu_subspace_t* sub)
{
	if(sub->glx.dummy_drawable)
	{
		if(sub->glx.dummy_drawable_is_window)
			XDestroyWindow(space->glx.dpy, (Window) sub->glx.dummy_drawable);
		else
			space->glx.DestroyPbuffer(space->glx.dpy, sub->glx.dummy_drawable);
	}
}

static void
_cairo_glx_space_create_dummy_drawable(cairo_gpu_space_t* space, cairo_gpu_subspace_t* subspace, GLXContext context)
{
	GLXDrawable dummy_drawable = 0;
	int attribs[] = {0, 0};
	int (*handler)(Display *, XErrorEvent *);
	int is_window;
	Display* dpy = space->glx.dpy;

	handler = XSetErrorHandler(_cairo_gpu_x11_error_handler);
	dummy_drawable = space->glx.CreatePbuffer(dpy, subspace->glx.fbconfig, attribs);
	// Gallium Nouveau fails on this for some reason
	space->glx.MakeContextCurrent(dpy, dummy_drawable, dummy_drawable, context);
	XSync(dpy, 0);
	XSetErrorHandler(handler);

	if(_cairo_gpu_x11_error)
	{
		_cairo_gpu_x11_error = 0;
		if(dummy_drawable)
			space->glx.DestroyPbuffer(dpy, dummy_drawable);
		dummy_drawable = 0;
	}

	if(dummy_drawable)
		is_window = 0;
	else
	{
		XVisualInfo* vi = space->glx.GetVisualFromFBConfigSGIX(dpy, subspace->glx.fbconfig);

		XSetWindowAttributes swa;
		Colormap cmap = XCreateColormap (dpy,
						RootWindow (dpy, vi->screen),
						vi->visual,
						AllocNone);
		swa.colormap = cmap;
		dummy_drawable = XCreateWindow (dpy, RootWindow (dpy, vi->screen),
					    0, 0,
					    1, 1,
					    0,
					    vi->depth,
					    CopyFromParent,
					    vi->visual,
					    CWColormap, &swa);
		XFree(vi);
		is_window = 1;
	}

	if(subspace->glx.dummy_drawable || !__sync_bool_compare_and_swap(&subspace->glx.dummy_drawable, 0, dummy_drawable))
	{
		if(is_window)
			XDestroyWindow(dpy, dummy_drawable);
		else
			glXDestroyPbuffer(dpy, dummy_drawable);
	}
	else
		subspace->glx.dummy_drawable_is_window = is_window;
}

static void
_cairo_glx_space_init_font_options(cairo_gpu_space_t* space)
{
	_cairo_gpu_init_font_options_for_display (&space->font_options, space->glx.dpy);
}

static cairo_bool_t
_cairo_glx_space_has_offscreen_drawables(cairo_gpu_space_t* space)
{
	return 1;
}

static void
_cairo_glx_space_set_surface_subspace(cairo_gpu_space_t* space, cairo_gpu_surface_t* surface)
{
	cairo_gpu_subspace_t* sub = 0;
	cairo_gpu_subspace_t* sub_cur;
	int locked = 0;

	if(!surface->glx.fbconfig)
		return;

	retry:
	for(sub_cur = &space->subspace; sub_cur; sub_cur = sub_cur->next)
	{
		if(surface->glx.fbconfig != sub_cur->glx.fbconfig)
			continue;

		sub = sub_cur;
		break;
	}

	if(sub)
	{
		if(locked)
			CAIRO_MUTEX_UNLOCK(space->mutex);
		surface->subspace = sub;
		return;
	}
	else if(!locked)
	{
		CAIRO_MUTEX_LOCK(space->mutex);
		locked = 1;
		goto retry;
	}

	sub = (cairo_gpu_subspace_t*)calloc(sizeof(cairo_gpu_subspace_t), 1);

	if(surface && surface->glx.fbconfig)
		sub->glx.fbconfig = surface->glx.fbconfig;
	else
		sub->glx.fbconfig = space->subspace.glx.fbconfig;

	pthread_key_create(&sub->context_tls, 0);

	sub->next = space->subspace.next;
	space->subspace.next = sub;

	surface->subspace = sub;
	CAIRO_MUTEX_UNLOCK(space->mutex);
}

static cairo_bool_t
_cairo_glx_context_is_surface_bound(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface)
{
	return ctx->glx.current_drawable == surface->glx.glxdrawable;
}

static void
_cairo_glx_context_update_state(cairo_gpu_context_t* ctx, cairo_gpu_gl_state_t* user)
{
	cairo_gpu_context_t* new_ctx = 0;
	GLXContext gl_ctx = ctx->space->glx.GetCurrentContext();
	if(gl_ctx)
	{
		if(_cairo_gl_tls.ctx && gl_ctx == _cairo_gl_tls.ctx->glx.ctx)
			new_ctx = _cairo_gl_tls.ctx;
		else
		{
			user->api = CAIRO_GL_API_GLX;
			user->glx.dpy = ctx->space->glx.GetCurrentDisplay();
			user->glx.ctx = gl_ctx;
			user->glx.read_drawable = ctx->space->glx.GetCurrentReadDrawable();
			user->glx.draw_drawable = ctx->space->glx.GetCurrentDrawable();
		}
	}

	_cairo_gl_tls.ctx = new_ctx;

	if(new_ctx && new_ctx->space->api == CAIRO_GL_API_GLX && new_ctx->space->glx.waitx)
		ctx->space->glx.WaitX();
}

static void
_cairo_glx_context_do_bind(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface)
{
	if(surface && surface->has_drawable)
	{
		if(!surface->glx.glxdrawable)
		{
			int attribs[] = {GLX_PBUFFER_WIDTH, surface->width, GLX_PBUFFER_HEIGHT, surface->height, GLX_PRESERVED_CONTENTS, 1, 0, 0};
			surface->glx.glxdrawable = ctx->space->glx.CreatePbuffer(surface->space->glx.dpy, surface->glx.fbconfig, attribs);

			// XXX: what if we fail here?!
			assert(surface->glx.glxdrawable);

			surface->glx.owns_pbuffer = 1;
		}

		ctx->glx.current_drawable = surface->glx.glxdrawable;
	}
	else
	{
		if(!ctx->subspace->glx.dummy_drawable)
			_cairo_glx_space_create_dummy_drawable(ctx->space, ctx->subspace, ctx->glx.ctx);
		ctx->glx.current_drawable = ctx->subspace->glx.dummy_drawable;
	}

	//printf("MakeCurrent %p %lx\n", ctx, ctx->glx.current_drawable);
	ctx->space->glx.MakeContextCurrent(ctx->space->glx.dpy, ctx->glx.current_drawable, ctx->glx.current_drawable, ctx->glx.ctx);

	if(!_cairo_gl_tls.ctx && ctx->space->glx.waitx)
		ctx->space->glx.WaitX();
}

static void
_cairo_glx_space_unbind_context(cairo_gpu_space_t* space)
{
	space->glx.MakeContextCurrent(space->glx.dpy, 0, 0, 0);
}

static void
_cairo_glx_context_flush(cairo_gpu_context_t* ctx)
{
	if(ctx->space->glx.waitgl)
		ctx->space->glx.WaitGL();
	else
		ctx->gl.Flush();
}

static void*
_cairo_glx_space_create_gl_context(cairo_gpu_space_t * space, cairo_surface_t* surface)
{
	cairo_gpu_subspace_t* sub = surface ? ((cairo_gpu_surface_t*)surface)->subspace : 0;
	if(!sub)
		sub = &space->subspace;
	return space->glx.CreateNewContext(space->glx.dpy, sub->glx.fbconfig, GLX_RGBA_TYPE, space->glx.share, space->glx.direct);
}

static void
_cairo_glx_space_destroy_gl_context(cairo_gpu_space_t * space, void* context)
{
	space->glx.DestroyContext(space->glx.dpy, context);
}

static cairo_bool_t
_cairo_glx_space_make_gl_context_current(cairo_gpu_space_t * space, void* context, cairo_gpu_surface_t* draw_surface, cairo_gpu_surface_t* read_surface)
{
	Display* dpy = 0;
	GLXFBConfig fbconfig = 0;
	GLXDrawable read_drawable = 0;
	GLXDrawable draw_drawable = 0;
	cairo_gpu_subspace_t* subspace = 0;

	if(read_surface)
	{
		dpy = read_surface->space->glx.dpy;
		if(read_surface->subspace)
			fbconfig = read_surface->subspace->glx.fbconfig;
	}

	if(draw_surface)
	{
		if(dpy && draw_surface->space->glx.dpy != dpy)
			return 0;

		dpy = draw_surface->space->glx.dpy;
		if(draw_surface->subspace)
		{
			if(fbconfig && fbconfig != draw_surface->subspace->glx.fbconfig)
				return 0;

			fbconfig = draw_surface->subspace->glx.fbconfig;
		}
	}

	if(fbconfig)
	{
		if(draw_surface && draw_surface->subspace)
		{
			subspace = draw_surface->subspace;
			space = draw_surface->space;
		}
		else if(read_surface && read_surface->subspace)
		{
			subspace = read_surface->subspace;
			space = read_surface->space;
		}
	}
	else
	{
		if(draw_surface)
			space = draw_surface->space;
		else if(read_surface)
			space = read_surface->space;

		if(!space)
			return 0;
		if(space->base.backend != &_cairo_gpu_space_backend)
			return 0;

		subspace = &space->subspace;
	}

	if(draw_surface && draw_surface->glx.glxdrawable)
	{
		if(read_surface && read_surface->glx.glxdrawable)
		{
			draw_drawable = draw_surface->glx.glxdrawable;
			read_drawable = read_surface->glx.glxdrawable;
		}
		else
			draw_drawable = read_drawable = draw_surface->glx.glxdrawable;
	}
	else if(read_surface && read_surface->glx.glxdrawable)
		draw_drawable = read_drawable = read_surface->glx.glxdrawable;
	else
	{
		if(!subspace->glx.dummy_drawable)
			_cairo_glx_space_create_dummy_drawable(space, subspace, (GLXContext)context);
		read_drawable = draw_drawable = subspace->glx.dummy_drawable;
	}

	space->glx.MakeContextCurrent(dpy, draw_drawable, read_drawable, (GLXContext)context);
	return 1;
}

cairo_space_t *
cairo_glx_space_wrap(void* libgl, Display* dpy, int owns_dpy, int direct, GLXContext share_context, unsigned flags)
{
	cairo_gpu_space_t *space;
	int owns_libgl = 0;
	PFNGLGETPROCADDRESSPROC GetProcAddress;

	if(!XOpenDisplay)
		return 0;

	GetProcAddress = (PFNGLGETPROCADDRESSPROC)dlsym(libgl, "glXGetProcAddressARB");
	if(!GetProcAddress)
	{
		if(!libgl)
		{
			libgl = dlopen("libGL.so.1", RTLD_LAZY);
			if(!libgl)
				return 0;
			owns_libgl = 1;
			GetProcAddress = (PFNGLGETPROCADDRESSPROC)dlsym(libgl, "glXGetProcAddressARB");
			if(!GetProcAddress)
				goto fail;
		}
		else
			return 0;
	}

	space = _cairo_gpu_space__begin_create();
	if(!space)
		return 0;

	space->api = CAIRO_GL_API_GLX;
	space->glx.dpy = dpy;
	space->glx.owns_dpy = owns_dpy;
	space->glx.direct = direct;

	space->libgl = libgl;
	space->owns_libgl = owns_libgl;
	space->GetProcAddress = GetProcAddress;
	_cairo_gpu_glx_init(&space->glx, space->GetProcAddress);

	{
		GLXFBConfig fbc = _cairo_glxfbconfig_for_visualid(space, dpy, XVisualIDFromVisual(DefaultVisual(dpy, DefaultScreen(dpy))));

		if(!fbc)
		{
			int fbc_attribs[] = {GLX_RENDER_TYPE, GLX_RGBA_BIT, 0, 0};
			int nfbcs = 0;
			GLXFBConfig* fbcs = space->glx.ChooseFBConfigSGIX(dpy, DefaultScreen(dpy), fbc_attribs, &nfbcs);
			if(fbcs && nfbcs)
			{
				fbc = fbcs[0];
				XFree(fbcs);
			}
			else
			{
				fbcs = space->glx.GetFBConfigs(dpy, 0, &nfbcs);
				if(fbcs && nfbcs)
				{
					fbc = fbcs[0];
					XFree(fbcs);
				}
			}
		}

		assert(fbc);

		space->subspace.glx.fbconfig = fbc;
	}

	space->glx.share = space->glx.CreateNewContext(dpy, space->subspace.glx.fbconfig, GLX_RGBA_TYPE, share_context, space->glx.direct);

	return _cairo_gpu_space__finish_create(space, flags);
fail:
	if(owns_libgl)
		dlclose(libgl);
	return 0;
}

cairo_space_t *
cairo_glx_space_create(void* libgl, const char* name, int direct, GLXContext share_context, unsigned flags)
{
	Display* dpy = XOpenDisplay(NULL);

	if(!dpy)
		return 0;

	return cairo_glx_space_wrap(libgl, dpy, 1, direct, share_context, flags);
}

void
cairo_glx_space_use_waitgl(cairo_space_t* abstract_space, int value)
{
	cairo_gpu_space_t* space = (cairo_gpu_space_t*)abstract_space;

	if(space->base.backend == &_cairo_gpu_space_backend && space->api == CAIRO_GL_API_GLX)
	{
		space->glx.waitgl = value;
	}
}

void
cairo_glx_space_use_waitx(cairo_space_t* abstract_space, int value)
{
	cairo_gpu_space_t* space = (cairo_gpu_space_t*)abstract_space;

	if(space->base.backend == &_cairo_gpu_space_backend && space->api == CAIRO_GL_API_GLX)
	{
		space->glx.waitx = value;
	}
}

cairo_space_t *
cairo_glx_space_create_from_current_context(void* libgl, unsigned flags)
{
	cairo_space_t *space;
	PFNGLGETPROCADDRESSPROC GetProcAddress;

	if((GetProcAddress = dlsym(libgl, "glXGetProcAddress")))
	{
		PFNGLXGETCURRENTCONTEXTPROC GetCurrentContext = GetProcAddress("glXGetCurrentContext");
		PFNGLXGETCURRENTDISPLAYPROC GetCurrentDisplay = GetProcAddress("glXGetCurrentDisplay");
		PFNGLXISDIRECTPROC IsDirect = GetProcAddress("glXIsDirect");

		if(GetCurrentContext && GetCurrentDisplay)
		{
			GLXContext ctx = GetCurrentContext();
			Display* dpy = GetCurrentDisplay();
			if(dpy && ctx)
			{
				space = cairo_glx_space_wrap(libgl, dpy, 0, IsDirect(dpy, ctx), ctx, flags);
				if(space)
					return space;
			}
		}
	}

	return 0;
}
