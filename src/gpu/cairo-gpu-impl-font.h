// TODO: this code is a modified version of the glitz code; this version is actually backend-independent and could be moved to a generic file

#define CAIRO_GPU_AREA_AVAILABLE 0
#define CAIRO_GPU_AREA_DIVIDED   1
#define CAIRO_GPU_AREA_OCCUPIED  2

typedef struct _cairo_gpu_root_area cairo_gpu_root_area_t;

typedef struct _cairo_gpu_area {
    int			     state;
    int			     level;
    int			     x, y;
    int			     width, height;
    struct _cairo_gpu_area *area[4];
    cairo_gpu_root_area_t  *root;
    void		     *closure;
} cairo_gpu_area_t;

static cairo_gpu_area_t _empty_area = {
    0, 0, 0, 0, 0, 0,
    { NULL, NULL, NULL, NULL },
    NULL,
    NULL
};

typedef struct _cairo_gpu_area_funcs {
    cairo_status_t (*move_in)	    (cairo_gpu_area_t *area,
				     void		*closure);

    void	   (*move_out)	    (cairo_gpu_area_t *area,
				     void		*closure);

    int		   (*compare_score) (cairo_gpu_area_t *area,
				     void		*closure1,
				     void		*closure2);
} cairo_gpu_area_funcs_t;

struct _cairo_gpu_root_area {
    int				   max_level;
    int				   width, height;
    cairo_gpu_area_t		   *area;
    const cairo_gpu_area_funcs_t *funcs;
};

static cairo_status_t
_cairo_gpu_area_move_in (cairo_gpu_area_t *area,
			   void		      *closure)
{
    area->closure = closure;
    area->state   = CAIRO_GPU_AREA_OCCUPIED;

    return (*area->root->funcs->move_in) (area, area->closure);
}

static void
_cairo_gpu_area_move_out (cairo_gpu_area_t *area)
{
    if (area->root)
    {
	(*area->root->funcs->move_out) (area, area->closure);

	area->closure = NULL;
	area->state   = CAIRO_GPU_AREA_AVAILABLE;
    }
}

static cairo_gpu_area_t *
_cairo_gpu_area_create (cairo_gpu_root_area_t *root,
			  int			  level,
			  int			  x,
			  int			  y,
			  int			  width,
			  int			  height)
{
    cairo_gpu_area_t *area;
    int			n = 4;

    area = malloc (sizeof (cairo_gpu_area_t));
    if (!area) {
	_cairo_error_throw (CAIRO_STATUS_NO_MEMORY);
	return NULL;
    }

    area->level   = level;
    area->x	  = x;
    area->y	  = y;
    area->width   = width;
    area->height  = height;
    area->root    = root;
    area->closure = NULL;
    area->state   = CAIRO_GPU_AREA_AVAILABLE;

    while (n--)
	area->area[n] = NULL;

    return area;
}

static void
_cairo_gpu_area_destroy (cairo_gpu_area_t *area)
{
    if (area == NULL)
	return;

    if (area->state == CAIRO_GPU_AREA_OCCUPIED)
    {
	_cairo_gpu_area_move_out (area);
    }
    else
    {
	int n = 4;

	while (n--)
	    _cairo_gpu_area_destroy (area->area[n]);
    }

    free (area);
}

static cairo_gpu_area_t *
_cairo_gpu_area_get_top_scored_sub_area (cairo_gpu_area_t *area)
{
    if (!area)
	return NULL;

    switch (area->state) {
    case CAIRO_GPU_AREA_OCCUPIED:
	return area;
    case CAIRO_GPU_AREA_AVAILABLE:
	break;
    case CAIRO_GPU_AREA_DIVIDED: {
	cairo_gpu_area_t *tmp, *top = NULL;
	int		   i;

	for (i = 0; i < 4; i++)
	{
	    tmp = _cairo_gpu_area_get_top_scored_sub_area (area->area[i]);
	    if (tmp && top)
	    {
		if ((*area->root->funcs->compare_score) (tmp,
							 tmp->closure,
							 top->closure) > 0)
		    top = tmp;
	    }
	    else if (tmp)
	    {
		top = tmp;
	    }
	}
	return top;
    }
    }

    return NULL;
}

static cairo_int_status_t
_cairo_gpu_area_find (cairo_gpu_area_t *area,
			int		   width,
			int		   height,
			cairo_bool_t	   kick_out,
			void		   *closure)
{
    cairo_status_t status;

    if (area->width < width || area->height < height)
	return CAIRO_INT_STATUS_UNSUPPORTED;

    switch (area->state) {
    case CAIRO_GPU_AREA_OCCUPIED:
	if (kick_out)
	{
	    if ((*area->root->funcs->compare_score) (area,
						     area->closure,
						     closure) >= 0)
		return CAIRO_INT_STATUS_UNSUPPORTED;

	    _cairo_gpu_area_move_out (area);
	} else {
	    return CAIRO_INT_STATUS_UNSUPPORTED;
	}

    /* fall-through */
    case CAIRO_GPU_AREA_AVAILABLE: {
	if (area->level == area->root->max_level ||
	    (area->width == width && area->height == height))
	{
	    return _cairo_gpu_area_move_in (area, closure);
	}
	else
	{
	    int dx[4], dy[4], w[4], h[4], i;

	    dx[0] = dx[2] = dy[0] = dy[1] = 0;

	    w[0] = w[2] = dx[1] = dx[3] = width;
	    h[0] = h[1] = dy[2] = dy[3] = height;

	    w[1] = w[3] = area->width - width;
	    h[2] = h[3] = area->height - height;

	    for (i = 0; i < 2; i++)
	    {
		if (w[i])
		    area->area[i] =
			_cairo_gpu_area_create (area->root,
						  area->level + 1,
						  area->x + dx[i],
						  area->y + dy[i],
						  w[i], h[i]);
	    }

	    for (; i < 4; i++)
	    {
		if (w[i] && h[i])
		    area->area[i] =
			_cairo_gpu_area_create (area->root,
						  area->level + 1,
						  area->x + dx[i],
						  area->y + dy[i],
						  w[i], h[i]);
	    }

	    area->state = CAIRO_GPU_AREA_DIVIDED;

	    status = _cairo_gpu_area_find (area->area[0],
					     width, height,
					     kick_out, closure);
	    if (status == CAIRO_STATUS_SUCCESS)
		return CAIRO_STATUS_SUCCESS;
	}
    } break;
    case CAIRO_GPU_AREA_DIVIDED: {
	cairo_gpu_area_t *to_area;
	int		   i, rejected = FALSE;

	for (i = 0; i < 4; i++)
	{
	    if (area->area[i])
	    {
		if (area->area[i]->width >= width &&
		    area->area[i]->height >= height)
		{
		    status = _cairo_gpu_area_find (area->area[i],
						     width, height,
						     kick_out, closure);
		    if (status == CAIRO_STATUS_SUCCESS)
			return CAIRO_STATUS_SUCCESS;

		    rejected = TRUE;
		}
	    }
	}

	if (rejected)
	    return CAIRO_INT_STATUS_UNSUPPORTED;

	to_area = _cairo_gpu_area_get_top_scored_sub_area (area);
	if (to_area)
	{
	    if (kick_out)
	    {
		if ((*area->root->funcs->compare_score) (to_area,
							 to_area->closure,
							 closure) >= 0)
		    return CAIRO_INT_STATUS_UNSUPPORTED;
	    } else {
		return CAIRO_INT_STATUS_UNSUPPORTED;
	    }
	}

	for (i = 0; i < 4; i++)
	{
	    _cairo_gpu_area_destroy (area->area[i]);
	    area->area[i] = NULL;
	}

	area->closure = NULL;
	area->state   = CAIRO_GPU_AREA_AVAILABLE;

	status = _cairo_gpu_area_find (area, width, height,
					 TRUE, closure);
	if (status == CAIRO_STATUS_SUCCESS)
	    return CAIRO_STATUS_SUCCESS;

    } break;
    }

    return CAIRO_INT_STATUS_UNSUPPORTED;
}

static cairo_status_t
_cairo_gpu_root_area_init (cairo_gpu_root_area_t	    *root,
			     int			    max_level,
			     int			    width,
			     int			    height,
			     const cairo_gpu_area_funcs_t *funcs)
{
    root->max_level = max_level;
    root->funcs     = funcs;

    root->area = _cairo_gpu_area_create (root, 0, 0, 0, width, height);
    if (!root->area)
	return _cairo_error (CAIRO_STATUS_NO_MEMORY);

    return CAIRO_STATUS_SUCCESS;
}

static void
_cairo_gpu_root_area_fini (cairo_gpu_root_area_t *root)
{
    _cairo_gpu_area_destroy (root->area);
}

typedef struct _cairo_gpu_surface_font_private {
    cairo_gpu_root_area_t root;
    cairo_surface_t	    *surface;
    cairo_pattern_t* pattern;
} cairo_gpu_surface_font_private_t;

typedef struct _cairo_gpu_surface_glyph_private {
    cairo_gpu_area_t	 *area;
    struct _cairo_gpu_surface_glyph_private* next_locked;
} cairo_gpu_surface_glyph_private_t;

static cairo_status_t
_cairo_glyph_move_in (cairo_gpu_area_t *area,
			    void		*closure)
{
    cairo_gpu_surface_glyph_private_t *glyph_private = closure;

    glyph_private->area = area;

    return CAIRO_STATUS_SUCCESS;
}

static void
_cairo_glyph_move_out (cairo_gpu_area_t	*area,
			     void		 *closure)
{
    cairo_gpu_surface_glyph_private_t *glyph_private = closure;

    glyph_private->area = NULL;
}

static int
_cairo_glyph_compare (cairo_gpu_area_t *area,
			    void		*closure1,
			    void		*closure2)
{
    cairo_gpu_surface_glyph_private_t *glyph_private = closure1;

    if (glyph_private->next_locked)
	return 1;

    return -1;
}

static const cairo_gpu_area_funcs_t _cairo_gpu_area_funcs = {
    _cairo_glyph_move_in,
    _cairo_glyph_move_out,
    _cairo_glyph_compare
};

#define GLYPH_CACHE_TEXTURE_SIZE 512
#define GLYPH_CACHE_MAX_LEVEL     64
#define GLYPH_CACHE_MAX_HEIGHT    96
#define GLYPH_CACHE_MAX_WIDTH     96

#define WRITE_VEC2(ptr, _x, _y) \
    *(ptr)++ = (_x);		\
    *(ptr)++ = (_y)

#define WRITE_BOX(ptr, _vx1, _vy1, _vx2, _vy2, p1, p2) \
    WRITE_VEC2 (ptr, _vx1, _vy1);			\
    WRITE_VEC2 (ptr, (p1)->x, (p2)->y);			\
    WRITE_VEC2 (ptr, _vx2, _vy1);			\
    WRITE_VEC2 (ptr, (p2)->x, (p2)->y);			\
    WRITE_VEC2 (ptr, _vx2, _vy2);			\
    WRITE_VEC2 (ptr, (p2)->x, (p1)->y);			\
    WRITE_VEC2 (ptr, _vx1, _vy2);			\
    WRITE_VEC2 (ptr, (p1)->x, (p1)->y)

static cairo_status_t
_cairo_gpu_surface_font_init (cairo_surface_t *surface,
				cairo_scaled_font_t   *scaled_font,
				cairo_content_t	      content)
{
    cairo_gpu_surface_font_private_t *font_private;
    cairo_int_status_t			status;

    font_private = malloc (sizeof (cairo_gpu_surface_font_private_t));
    if (!font_private)
	return _cairo_error (CAIRO_STATUS_NO_MEMORY);

    font_private->surface = cairo_surface_create_similar (surface, content,
						  GLYPH_CACHE_TEXTURE_SIZE,
						  GLYPH_CACHE_TEXTURE_SIZE);
    if (font_private->surface == NULL)
	    goto fail1;

    font_private->pattern = cairo_pattern_create_for_surface(font_private->surface);
    if (font_private->pattern == NULL)
	    goto fail2;

    cairo_pattern_set_component_alpha(font_private->pattern, TRUE);

    status = _cairo_gpu_root_area_init (&font_private->root,
					  GLYPH_CACHE_MAX_LEVEL,
					  GLYPH_CACHE_TEXTURE_SIZE,
					  GLYPH_CACHE_TEXTURE_SIZE,
					  &_cairo_gpu_area_funcs);
    if (status != CAIRO_STATUS_SUCCESS)
	    goto fail3;

    scaled_font->surface_private = font_private;
    scaled_font->surface_backend = surface->backend;

    return CAIRO_STATUS_SUCCESS;

fail3:
	cairo_pattern_destroy (font_private->pattern);
fail2:
	cairo_surface_destroy (font_private->surface);
fail1:
	free (font_private);
	return CAIRO_INT_STATUS_UNSUPPORTED;
}

static void
_cairo_gpu_surface_scaled_font_fini (cairo_scaled_font_t *scaled_font)
{
    cairo_gpu_surface_font_private_t *font_private;

    font_private = scaled_font->surface_private;
    if (font_private)
    {
	_cairo_gpu_root_area_fini (&font_private->root);
	cairo_pattern_destroy (font_private->pattern);
	cairo_surface_destroy (font_private->surface);
	free (font_private);
	scaled_font->surface_private = 0;
    }
}

static void
_cairo_gpu_surface_scaled_glyph_fini (cairo_scaled_glyph_t *scaled_glyph,
					cairo_scaled_font_t  *scaled_font)
{
    cairo_gpu_surface_glyph_private_t *glyph_private;

    glyph_private = scaled_glyph->surface_private;
    if (glyph_private)
    {
	if (glyph_private->area)
	    _cairo_gpu_area_move_out (glyph_private->area);

	free (glyph_private);
    }
}

#define FIXED_TO_FLOAT(f) (((gpu_float_t) (f)) / 65536)

static cairo_status_t
_cairo_gpu_surface_add_glyph (cairo_surface_t *surface,
				cairo_scaled_font_t   *scaled_font,
				cairo_scaled_glyph_t  *scaled_glyph)
{
    cairo_image_surface_t		*glyph_surface = scaled_glyph->surface;
    cairo_pattern_t* pattern;
    cairo_gpu_surface_font_private_t  *font_private;
    cairo_gpu_surface_glyph_private_t *glyph_private;
    cairo_int_status_t			status;

    glyph_private = scaled_glyph->surface_private;
    if (glyph_private == NULL)
    {
	glyph_private = malloc (sizeof (cairo_gpu_surface_glyph_private_t));
	if (!glyph_private)
	    return _cairo_error (CAIRO_STATUS_NO_MEMORY);

	glyph_private->area   = NULL;
	glyph_private->next_locked = NULL;

	scaled_glyph->surface_private = (void *) glyph_private;
    }

    if (glyph_surface->width  > GLYPH_CACHE_MAX_WIDTH ||
	glyph_surface->height > GLYPH_CACHE_MAX_HEIGHT)
	return CAIRO_INT_STATUS_UNSUPPORTED;

    if (scaled_font->surface_private == NULL)
    {
	status = _cairo_gpu_surface_font_init (surface, scaled_font,
						 glyph_surface->base.content);
	if (status)
	    return status;
    }

    font_private = scaled_font->surface_private;

    if (glyph_surface->width == 0 || glyph_surface->height == 0)
    {
	glyph_private->area = &_empty_area;
	return CAIRO_STATUS_SUCCESS;
    }

    if (_cairo_gpu_area_find (font_private->root.area,
				glyph_surface->width,
				glyph_surface->height,
				FALSE, glyph_private))
    {
	if (_cairo_gpu_area_find (font_private->root.area,
				    glyph_surface->width,
				    glyph_surface->height,
				    TRUE, glyph_private))
	    return CAIRO_STATUS_SUCCESS;
    }

    assert(glyph_surface->base.content == font_private->surface->content);

    // XXX: greyscale alpha is currently not MAX or average of component alpha values. This should be fixed somehow!

    pattern = cairo_pattern_create_for_surface(&glyph_surface->base);
    status = _cairo_surface_composite(CAIRO_OPERATOR_SOURCE, pattern, 0, font_private->surface, 0, 0, 0, 0, glyph_private->area->x, glyph_private->area->y, glyph_surface->width, glyph_surface->height);
    cairo_pattern_destroy(pattern);
    return status;
}

