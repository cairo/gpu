#include <dlfcn.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <drm/drm.h>
#include <errno.h>
#include <stdio.h>

#define extern extern __attribute__((weak))
#include <X11/Xlib.h>
#include <X11/Xlibint.h>
#include <X11/extensions/dri2tokens.h>
#include <X11/extensions/dri2proto.h>
#include <X11/extensions/extutil.h>
#undef extern

static void *driOpenDriver(const char *driverName)
{
   void *glhandle, *handle;
   const char *libPaths, *p, *next;
   char realDriverName[200];
   int len;

   /* Attempt to make sure libGL symbols will be visible to the driver */
   glhandle = dlopen("libGL.so.1", RTLD_NOW | RTLD_GLOBAL);

   libPaths = NULL;
   if (geteuid() == getuid()) {
      /* don't allow setuid apps to use LIBGL_DRIVERS_PATH */
      libPaths = getenv("LIBGL_DRIVERS_PATH");
      if (!libPaths)
         libPaths = getenv("LIBGL_DRIVERS_DIR"); /* deprecated */
   }

   if (libPaths == NULL)
       libPaths = "/usr/lib/dri";

   handle = NULL;
   for (p = libPaths; *p; p = next) {
       next = strchr(p, ':');
       if (next == NULL) {
           len = strlen(p);
           next = p + len;
       } else {
           len = next - p;
           next++;
       }

      snprintf(realDriverName, sizeof realDriverName,
               "%.*s/tls/%s_dri.so", len, p, driverName);

      handle = dlopen(realDriverName, RTLD_NOW | RTLD_GLOBAL);

      if ( handle == NULL ) {
         snprintf(realDriverName, sizeof realDriverName,
                  "%.*s/%s_dri.so", len, p, driverName);
         handle = dlopen(realDriverName, RTLD_NOW | RTLD_GLOBAL);
      }

      if ( handle != NULL )
          break;
   }

   if (glhandle)
      dlclose(glhandle);

   return handle;
}

static void*
drm_open_driver(const char* driver_name, struct drm_api** papi)
{
	void* driver;
	struct drm_api * (*p_drm_api_create)(void);
	struct drm_api* api;

	driver = driOpenDriver(driver_name);
	if(!driver)
		return 0;

	// TODO: this is not necessarily public. We may want to use _egl Main instead; note that we need to check that the name is "DRM/Gallium/Win" since it may be non-Gallium
	p_drm_api_create = dlsym(driver, "drm_api_create");
	if(!p_drm_api_create) // non-Gallium driver
		goto fail;

	api = p_drm_api_create();
	if(!api)
		goto fail;

	*papi = api;
	return driver;

fail:
	dlclose(driver);
	return 0;
}

extern __attribute__((weak)) ssize_t getline(char **lineptr, size_t *n, FILE *stream);

static char*
drm_get_driver_name(int card)
{
	char *driver;
	size_t driver_len;
	char path[PATH_MAX];
	FILE *f;
	int len;

	if(!getline)
		return 0;

	snprintf(path, sizeof(path), "/sys/class/drm/card%d/dri_library_name", card);

	f = fopen(path, "r");
	if (!f)
		return 0;

	driver = malloc(sizeof(PATH_MAX));
	driver = 0;
	driver_len = 0;
	len = getline(&driver, &driver_len, f); /* this is Linux-only so we must have glibc */
	fclose(f);

	if (len >= 1)
	{
		/* remove the trailing newline from sysfs */
		driver[len - 1] = '\0';
		return driver;
	}

	return 0;
}

static int
drmIoctl(int fd, unsigned long request, void *arg)
{
    int ret;

    do {
        ret = ioctl(fd, request, arg);
    } while (ret == -1 && (errno == EINTR || errno == EAGAIN));
    return ret;
}

static int
drm_open(int card)
{
	char path[ NAME_MAX ];
	int fd;
	drm_set_version_t sv;

	sprintf(path, "/dev/dri/card%d", card);
	fd = open(path, O_RDWR, 0);
	if(fd < 0)
		return 0;

	/* Set the interface version, asking for 1.2 */
	sv.drm_di_major = 1;
	sv.drm_di_minor = 2;
	sv.drm_dd_major = -1;
	sv.drm_dd_minor = -1;

	if (drmIoctl(fd, DRM_IOCTL_SET_VERSION, &sv))
		goto fail;

	return fd;

fail:
	close(fd);
	return 0;
}

static cairo_space_t*
_cairo_gallium_drm_space_create (int fd, void* driver, struct drm_api* api, unsigned flags)
{
	cairo_gpu_space_t* space;
	struct pipe_screen* screen;
	struct drm_create_screen_arg arg;

	arg.mode = DRM_CREATE_NORMAL;
	screen = api->create_screen(api, fd, &arg);

	space = _cairo_gpu_space__begin_create();
	if(!space)
	{
		screen->destroy(screen);
		return 0;
	}

	space->api = API_DRM;
	space->drm.driver = driver;
	space->drm.fd = fd;
	space->drm.api = api;
	space->screen = screen;
	space->owns_screen = 1;

	_cairo_gpu_space__finish_create(space, flags);
	return &space->base;
}

cairo_space_t*
cairo_gallium_drm_space_wrap(struct drm_api* api, struct pipe_screen* screen, unsigned flags)
{
	cairo_gpu_space_t* space = _cairo_gpu_space__begin_create();
	if(!space)
		return 0;

	space->api = API_DRM;
	space->drm.api = api;
	space->drm.fd = -1;
	space->screen = screen;
	space->owns_screen = 1;

	_cairo_gpu_space__finish_create(space, flags);
	return &space->base;
}

cairo_space_t*
cairo_gallium_drm_space_create (int card, unsigned flags)
{
	cairo_space_t* space;
	void* driver;
	char* driver_name;
	int fd;

	struct drm_api* api;
	drm_auth_t auth;

	driver_name = drm_get_driver_name(0);
	if(!driver_name)
		return 0;

	driver = drm_open_driver(driver_name, &api);
	free(driver_name);
	if(!driver)
		return 0;

	fd = drm_open(card);
	if(fd < 0)
		goto fail;

	if (drmIoctl(fd, DRM_IOCTL_GET_MAGIC, &auth))
		goto fail2;

	if (drmIoctl(fd, DRM_IOCTL_AUTH_MAGIC, &auth))
		goto fail2;

	space = _cairo_gallium_drm_space_create(fd, driver, api, flags);
	if(space)
		return space;

fail2:
	close(fd);
fail:
	dlclose(driver);
	return 0;
}

static char dri2ExtensionName[] = DRI2_NAME;
static XExtensionInfo *dri2Info;
static XEXT_GENERATE_CLOSE_DISPLAY (DRI2CloseDisplay, dri2Info);
static /* const */ XExtensionHooks dri2ExtensionHooks = {
    NULL,                               /* create_gc */
    NULL,                               /* copy_gc */
    NULL,                               /* flush_gc */
    NULL,                               /* free_gc */
    NULL,                               /* create_font */
    NULL,                               /* free_font */
    DRI2CloseDisplay,                   /* close_display */
    NULL,                               /* wire_to_event */
    NULL,                               /* event_to_wire */
    NULL,                               /* error */
    NULL,                               /* error_string */
};

static XEXT_GENERATE_FIND_DISPLAY (DRI2FindDisplay, dri2Info,
                                   dri2ExtensionName,
                                   &dri2ExtensionHooks,
                                   0, NULL);

static Bool DRI2Connect(Display *dpy, XID window,
		 char **driverName, char **deviceName)
{
    XExtDisplayInfo *info = DRI2FindDisplay(dpy);
    xDRI2ConnectReply rep;
    xDRI2ConnectReq *req;

    XextCheckExtension (dpy, info, DRI2_NAME, False);

    LockDisplay(dpy);
    GetReq(DRI2Connect, req);
    req->reqType = info->codes->major_opcode;
    req->dri2ReqType = X_DRI2Connect;
    req->window = window;
    req->driverType = DRI2DriverDRI;
    if (!_XReply(dpy, (xReply *)&rep, 0, xFalse)) {
	UnlockDisplay(dpy);
	SyncHandle();
	return False;
    }

    if (rep.driverNameLength == 0 && rep.deviceNameLength == 0) {
	UnlockDisplay(dpy);
	SyncHandle();
	return False;
    }

    *driverName = Xmalloc(rep.driverNameLength + 1);
    if (*driverName == NULL) {
	_XEatData(dpy,
		  ((rep.driverNameLength + 3) & ~3) +
		  ((rep.deviceNameLength + 3) & ~3));
	UnlockDisplay(dpy);
	SyncHandle();
	return False;
    }
    _XReadPad(dpy, *driverName, rep.driverNameLength);
    (*driverName)[rep.driverNameLength] = '\0';

    *deviceName = Xmalloc(rep.deviceNameLength + 1);
    if (*deviceName == NULL) {
	Xfree(*driverName);
	_XEatData(dpy, ((rep.deviceNameLength + 3) & ~3));
	UnlockDisplay(dpy);
	SyncHandle();
	return False;
    }
    _XReadPad(dpy, *deviceName, rep.deviceNameLength);
    (*deviceName)[rep.deviceNameLength] = '\0';

    UnlockDisplay(dpy);
    SyncHandle();

    return True;
}

static Bool DRI2Authenticate(Display *dpy, XID window, drm_magic_t magic)
{
    XExtDisplayInfo *info = DRI2FindDisplay(dpy);
    xDRI2AuthenticateReq *req;
    xDRI2AuthenticateReply rep;

    XextCheckExtension (dpy, info, DRI2_NAME, False);

    LockDisplay(dpy);
    GetReq(DRI2Authenticate, req);
    req->reqType = info->codes->major_opcode;
    req->dri2ReqType = X_DRI2Authenticate;
    req->window = window;
    req->magic = magic;

    if (!_XReply(dpy, (xReply *)&rep, 0, xFalse)) {
	UnlockDisplay(dpy);
	SyncHandle();
	return False;
    }

    UnlockDisplay(dpy);
    SyncHandle();

    return rep.authenticated;
}

cairo_space_t*
cairo_gallium_x11_space_wrap (Display* dpy, int scr, cairo_bool_t owns_dpy, unsigned flags)
{
	cairo_space_t* space;
	char *driver_name, *device_name;
	int fd;
	void* driver;
	struct drm_api* api;
	drm_auth_t auth;

	if (!DRI2Connect(dpy, RootWindow(dpy, scr), &driver_name, &device_name))
		return 0;

	driver = drm_open_driver(driver_name, &api);
	XFree(driver_name);
	if(!driver)
	{
		XFree(device_name);
		return 0;
	}

	fd = open(device_name, O_RDWR);
	XFree(device_name);
	if(fd < 0)
		goto fail;

	if (drmIoctl(fd, DRM_IOCTL_GET_MAGIC, &auth))
		goto fail2;

	if(!DRI2Authenticate(dpy, RootWindow(dpy, scr), auth.magic))
	        goto fail2;

	space = _cairo_gallium_drm_space_create(fd, driver, api, flags);
	if(!space)
		goto fail2;

	if(owns_dpy)
		((cairo_gpu_space_t*)space)->drm.dpy = dpy;
	return space;

fail2:
	close(fd);
fail:
	dlclose(driver);
	return 0;
}

cairo_space_t*
cairo_gallium_x11_space_create (const char* name, unsigned flags)
{
	cairo_space_t* space;
	Display* dpy;
	if(!XOpenDisplay)
		return 0;
	dpy = XOpenDisplay(name);
	space = cairo_gallium_x11_space_wrap(dpy, DefaultScreen(dpy), 1, flags);
	if(!space)
		XCloseDisplay(dpy);
	return space;
}

void
_cairo_gpu_drm_space_destroy(cairo_gpu_space_t* space)
{
	if(space->drm.driver)
		dlclose(space->drm.driver);
	if(space->drm.fd >= 0)
		close(space->drm.fd);
	if(space->drm.dpy)
		XCloseDisplay(space->drm.dpy);
}
