#define CHAN_C 1

// means that A is not "uniform"
#define CHAN_A 2

#define CHAN_KA 4

typedef struct
{
	cairo_pattern_t *src;
	int chan;
	int src_x;
	int src_y;
	cairo_gpu_surface_t *surface;
	cairo_gpu_texture_t* texture;
	unsigned char owns_texture;
	unsigned char owns_surface;
	unsigned char unpremultiplied : 1;
	unsigned char gradient_discontinuous : 1;
	unsigned char has_coords : 1;
	cairo_surface_attributes_t attributes;
	unsigned gradient_denominator;
	unsigned gradient_width;
} cairo_gpu_composite_operand_t;

#define BLEND_COLOR_C_DIV_A 1
#define BLEND_COLOR_A 2
#define BLEND_COLOR_1_MINUS_A 3

typedef struct
{
	unsigned frag;
	unsigned vert;
	cairo_gpu_blend_t blend;
	unsigned char blend_color;
	unsigned char chan;
	unsigned char component;
	unsigned char unpremultiplied : 1;
	unsigned char unpremultiplied_one_alpha : 1;
	unsigned char draw_geometry : 1;
	char operands[MAX_OPERANDS];
} cairo_gpu_composite_pass_t;

#define BLEND_ALPHA 2

typedef struct
{
	cairo_gpu_surface_t *dst;
	int obj_x;
	int obj_y;

	int dst_x;
	int dst_y;
	int width;
	int height;

	cairo_gpu_color_t c;
	cairo_gpu_color_t a;
	float ka;

	unsigned char blend_dst;
	unsigned char blend_src;
	unsigned char smooth;
	unsigned char saturate : 1;
	unsigned char dst_alpha_mask : 1;
	unsigned char unpremultiplied : 1;

	int primary_chan;
	int constant_chan;
	int zero_chan;
	int operands_chan;
	cairo_gpu_composite_operand_t operands[MAX_OPERANDS];
	char cur_operands[MAX_OPERANDS];

	int npasses;
	cairo_gpu_composite_pass_t passes[4];
} cairo_gpu_composite_setup_t;

#if 0
#if CAIRO_NO_MUTEX
#define CURRENT_THREAD() ((void*)1)
#elif HAVE_PTHREAD_H
#define CURRENT_THREAD() ((void*)pthread_self())
#elif defined(HAVE_WINDOWS_H) || defined(_MSC_VER)
#define CURRENT_THREAD() ((void*)GetCurrentThreadId())
#elif defined __OS2__
#error Implement CURRENT_THREAD for OS/2
#elif CAIRO_HAS_BEOS_SURFACE
#define CURRENT_THREAD() ((void*)find_thread(0))
#else
#error Implement CURRENT_THREAD for your platform
#endif

static inline void
_cairo_gpu_context__lock(cairo_gpu_context_t* ctx)
{
	if(ctx->owner_thread != CURRENT_THREAD())
	{
		CAIRO_MUTEX_LOCK(ctx->mutex);
		ctx->owner_thread = CURRENT_THREAD();
	}
	++ctx->lock_depth;
}

static inline void
_cairo_gpu_context__unlock(cairo_gpu_context_t* ctx)
{
	if(!--ctx->lock_depth)
	{
		ctx->owner_thread = 0;
		CAIRO_MUTEX_UNLOCK(ctx->mutex);
	}
}
#endif

#define ARRAY_SIZE(array) (sizeof(array) / sizeof(array[0]))

static inline float
int_as_float(uint32_t val)
{
	union fi
	{
		float f;
		uint32_t u;
	} fi;

	fi.u = val;
	return fi.f;
}

/* Some loss of precision possible */
static inline float
_cairo_fixed_to_float(cairo_fixed_t f)
{
	return ((float)f) * (1.0f / (float)(1 << CAIRO_FIXED_FRAC_BITS));
}

#ifdef _XLIB_H_
static int
parse_boolean (const char *v)
{
    char c0, c1;

    c0 = *v;
    if (c0 == 't' || c0 == 'T' || c0 == 'y' || c0 == 'Y' || c0 == '1')
	return 1;
    if (c0 == 'f' || c0 == 'F' || c0 == 'n' || c0 == 'N' || c0 == '0')
	return 0;
    if (c0 == 'o')
    {
	c1 = v[1];
	if (c1 == 'n' || c1 == 'N')
	    return 1;
	if (c1 == 'f' || c1 == 'F')
	    return 0;
    }

    return -1;
}

static cairo_bool_t
get_boolean_default (Display	*dpy,
		     const char    *option,
		     cairo_bool_t  *value)
{
    char *v;
    int i;

    v = XGetDefault (dpy, "Xft", option);
    if (v) {
	i = parse_boolean (v);
	if (i >= 0) {
	    *value = i;
	    return TRUE;
	}
    }

    return FALSE;
}

#if CAIRO_HAS_FT_FONT
int __attribute__((weak)) FcNameConstant (unsigned char* string, int *result);
#endif

static cairo_bool_t
get_integer_default (Display    *dpy,
		     const char *option,
		     int	 *value)
{
    char *v, *e;

    v = XGetDefault (dpy, "Xft", option);
    if (v) {
#if CAIRO_HAS_FT_FONT
	if (FcNameConstant ((unsigned char*) v, value))
	    return TRUE;
#endif

	*value = strtol (v, &e, 0);
	if (e != v)
	    return TRUE;
    }

    return FALSE;
}

#ifndef FC_RGBA_UNKNOWN
#define FC_RGBA_UNKNOWN     0
#define FC_RGBA_RGB	  1
#define FC_RGBA_BGR	  2
#define FC_RGBA_VRGB	 3
#define FC_RGBA_VBGR	 4
#define FC_RGBA_NONE	 5
#endif

/* Old versions of fontconfig didn't have these options */
#ifndef FC_HINT_NONE
#define FC_HINT_NONE	 0
#define FC_HINT_SLIGHT      1
#define FC_HINT_MEDIUM      2
#define FC_HINT_FULL	 3
#endif

/* Fontconfig version older than 2.6 didn't have these options */
#ifndef FC_LCD_FILTER
#define FC_LCD_FILTER	"lcdfilter"
#endif
/* Some Ubuntu versions defined FC_LCD_FILTER without defining the following */
#ifndef FC_LCD_NONE
#define FC_LCD_NONE	0
#define FC_LCD_DEFAULT	1
#define FC_LCD_LIGHT	2
#define FC_LCD_LEGACY	3
#endif

static void
_cairo_gpu_init_font_options_for_display (cairo_font_options_t * font_options, Display* dpy)
{
    cairo_bool_t xft_hinting;
    cairo_bool_t xft_antialias;
    int xft_hintstyle;
    int xft_rgba;
    cairo_antialias_t antialias;
    cairo_subpixel_order_t subpixel_order;
    cairo_hint_style_t hint_style;

    if (!get_boolean_default (dpy, "antialias", &xft_antialias))
	xft_antialias = TRUE;

    if (!get_boolean_default (dpy, "hinting", &xft_hinting))
	xft_hinting = TRUE;

    if (!get_integer_default (dpy, "hintstyle", &xft_hintstyle))
	xft_hintstyle = FC_HINT_FULL;

    if (!get_integer_default (dpy, "rgba", &xft_rgba))
    {
	int event_base, error_base, has_render;
	xft_rgba = FC_RGBA_UNKNOWN;



	has_render = (XRenderQueryExtension (dpy, &event_base, &error_base) &&
			(XRenderFindVisualFormat (dpy, DefaultVisual (dpy, DefaultScreen (dpy))) != 0));

#if RENDER_MAJOR > 0 || RENDER_MINOR >= 6
	if (has_render)
	{
	    int render_order = XRenderQuerySubpixelOrder (dpy, DefaultScreen(dpy));

	    switch (render_order)
	    {
	    default:
	    case SubPixelUnknown:
		xft_rgba = FC_RGBA_UNKNOWN;
		break;
	    case SubPixelHorizontalRGB:
		xft_rgba = FC_RGBA_RGB;
		break;
	    case SubPixelHorizontalBGR:
		xft_rgba = FC_RGBA_BGR;
		break;
	    case SubPixelVerticalRGB:
		xft_rgba = FC_RGBA_VRGB;
		break;
	    case SubPixelVerticalBGR:
		xft_rgba = FC_RGBA_VBGR;
		break;
	    case SubPixelNone:
		xft_rgba = FC_RGBA_NONE;
		break;
	    }
	}
#endif
    }

    if (xft_hinting) {
	switch (xft_hintstyle) {
	case FC_HINT_NONE:
	    hint_style = CAIRO_HINT_STYLE_NONE;
	    break;
	case FC_HINT_SLIGHT:
	    hint_style = CAIRO_HINT_STYLE_SLIGHT;
	    break;
	case FC_HINT_MEDIUM:
	    hint_style = CAIRO_HINT_STYLE_MEDIUM;
	    break;
	case FC_HINT_FULL:
	    hint_style = CAIRO_HINT_STYLE_FULL;
	    break;
	default:
	    hint_style = CAIRO_HINT_STYLE_DEFAULT;
	}
    } else {
	hint_style = CAIRO_HINT_STYLE_NONE;
    }

    switch (xft_rgba) {
    case FC_RGBA_RGB:
	subpixel_order = CAIRO_SUBPIXEL_ORDER_RGB;
	break;
    case FC_RGBA_BGR:
	subpixel_order = CAIRO_SUBPIXEL_ORDER_BGR;
	break;
    case FC_RGBA_VRGB:
	subpixel_order = CAIRO_SUBPIXEL_ORDER_VRGB;
	break;
    case FC_RGBA_VBGR:
	subpixel_order = CAIRO_SUBPIXEL_ORDER_VBGR;
	break;
    case FC_RGBA_UNKNOWN:
    case FC_RGBA_NONE:
    default:
	subpixel_order = CAIRO_SUBPIXEL_ORDER_DEFAULT;
    }

    if (xft_antialias) {
	if (subpixel_order == CAIRO_SUBPIXEL_ORDER_DEFAULT)
	    antialias = CAIRO_ANTIALIAS_GRAY;
	else
	    antialias = CAIRO_ANTIALIAS_SUBPIXEL;
    } else {
	antialias = CAIRO_ANTIALIAS_NONE;
    }

    cairo_font_options_set_hint_style (font_options, hint_style);
    cairo_font_options_set_antialias (font_options, antialias);
    cairo_font_options_set_subpixel_order (font_options, subpixel_order);
    cairo_font_options_set_hint_metrics (font_options, CAIRO_HINT_METRICS_ON);
}
#endif

static inline cairo_gpu_space_tls_t*
_cairo_gpu_space_alloc_tls(cairo_gpu_space_t* space);

static cairo_gpu_space_tls_t*
_cairo_gpu_space_create_tls(cairo_gpu_space_t* space)
{
	cairo_gpu_space_tls_t* tls = _cairo_gpu_space_alloc_tls(space);
	tls->space = space;

	CAIRO_MUTEX_LOCK(space->mutex);
	insert_after(&space->tls_list, &tls->node);
	CAIRO_MUTEX_UNLOCK(space->mutex);

	pthread_setspecific(space->tls, tls);
	return tls;
}

static cairo_gpu_space_tls_t*
_cairo_gpu_space_get_tls(cairo_gpu_space_t* space)
{
	cairo_gpu_space_tls_t* tls = pthread_getspecific(space->tls);
	if(tls)
		return tls;

	return _cairo_gpu_space_create_tls(space);
}

static inline cairo_gpu_context_t *
_cairo_gpu_space_tls_lookup_context(cairo_gpu_space_tls_t* gpu_tls);

static inline void
_cairo_gpu_context_bind(cairo_gpu_context_t* ctx);

static inline cairo_gpu_context_t *
_cairo_gpu_space_bind(cairo_gpu_space_t* space)
{
	cairo_gpu_context_t* ctx = _cairo_gpu_space_tls_lookup_context(_cairo_gpu_space_get_tls(space));
	_cairo_gpu_context_bind(ctx);
	return ctx;
}

static inline void
_cairo_gpu_space_tls_destroy_contexts(cairo_gpu_space_tls_t* gpu_tls);

static void
_cairo_gpu_space_tls_destroy(cairo_gpu_space_tls_t* tls)
{
	int i;
	cairo_gpu_context_t* ctx = _cairo_gpu_space_tls_lookup_context(tls);
	_cairo_gpu_context_bind(ctx);

	for(i = 0; i < MAX_OPERANDS; ++i)
	{
		int j;
		for(j = 0; j < MAX_SMALL_WIDTH; ++j)
		{
			if(tls->small_texture_pools[i].texture[j])
			{
				_cairo_gpu_texture_fini(ctx, tls->small_texture_pools[i].texture[j]);
				free(tls->small_texture_pools[i].texture[j]);
				tls->small_texture_pools[i].texture[j] = 0;
			}
		}
	}

	_cairo_gpu_space_tls_destroy_contexts(tls);

	free(tls);
}

static void
_cairo_gpu_space_tls_dtor(void* abstract)
{
	cairo_gpu_space_tls_t* tls = abstract;

	if(tls)
	{
		CAIRO_MUTEX_LOCK(tls->space->mutex);
		unlink_node(&tls->node);
		CAIRO_MUTEX_UNLOCK(tls->space->mutex);

		_cairo_gpu_space_tls_destroy(tls);
	}
}

#define _cairo_gpu_context_geometry(ctx, unused) _cairo_gpu_geometry_init((cairo_gpu_geometry_t*)alloca(sizeof(cairo_gpu_geometry_t)))
#define _cairo_gpu_geometry_put(ctx, geometry) _cairo_gpu_geometry_fini(ctx, geometry)

#define _cairo_gpu_context_geometry_long(ctx, unused) _cairo_gpu_geometry_init((cairo_gpu_geometry_t*)malloc(sizeof(cairo_gpu_geometry_t)))
#define _cairo_gpu_geometry_put_long(ctx, geometry) free(_cairo_gpu_geometry_fini(ctx, geometry))
