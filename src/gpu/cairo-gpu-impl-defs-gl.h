// XXX: we rely on __attribute__((weak)), which is probably non-portable to Windows

//#ifdef HAVE_X11
#define extern extern __attribute__((weak))
#include <X11/extensions/Xrender.h>
#undef extern
//#endif

#include <GL/gl.h>
#include <GL/glext.h>

#define API_CTX(f) ((ctx->space->api == CAIRO_GL_API_OSMESA) ? _cairo_osmesa_context_##f : _cairo_glx_context_##f)
#define API_SPACE(f) ((space->api == CAIRO_GL_API_OSMESA) ? _cairo_osmesa_space_##f : _cairo_glx_space_##f)
#define API_SURFACE_(r, f) if(surface->space->api == CAIRO_GL_API_GLX) r = _cairo_glx_surface_##f
#define API_SURFACE(f) if(surface->space->api == CAIRO_GL_API_GLX) _cairo_glx_surface_##f

#include "cairo-gpu-impl-defs-gl-glx.h"
#include "cairo-gpu-impl-defs-gl-osmesa.h"

struct _cairo_gpu_texture
{
	CAIRO_GPU_BASE_TEXTURE_T;

	unsigned tex;
	long long id;
	unsigned unsized_format; // unsized internal format
	unsigned filter;
	unsigned wrap_s;
	unsigned wrap_t;
	float anisotropy;
	char non_upside_down;
};

struct _cairo_gpu_subspace;
typedef struct _cairo_gpu_subspace cairo_gpu_subspace_t;

#define MAX_SURFS 4

 struct _cairo_gpu_surface
{
	CAIRO_GPU_BASE_SURFACE_T;
	cairo_rectangle_int_t bbox[MAX_SURFS];

	cairo_gpu_subspace_t* subspace;

	union
	{
		struct
		{
			GLXFBConfig fbconfig;
			Drawable xdrawable;
			GLXDrawable glxdrawable;
			char fbconfig_alpha_size;
			char fbconfig_color_size;
			char owns_glx_pixmap;
			char owns_pbuffer;
			char fbconfig_double_buffered;
			cairo_gpu_texture_t bound_texture;
			int bound_image_ref;
		} glx;
	};

	cairo_gpu_texture_t texture;
	GLuint fb;

	GLuint msaa_rb;
	GLuint msaa_fb;
	long long msaa_id;

	char clear_rgba; // clear all channels even if not CONTENT_COLOR_ALPHA
	char buffer_non_upside_down;
	char draw_to_back_buffer;
	unsigned char has_drawable;
};

 typedef struct _cairo_gpu_context cairo_gpu_context_t;

#define GEOM_SETUP 1
#define GEOM_VBO_MAPPED 2

struct _cairo_gpu_geometry
{
	unsigned mode;
	unsigned char flags;
	unsigned char vert;
	unsigned char tex;
	unsigned char color;
	unsigned vao;
	unsigned vbo;
	long long id;
	unsigned vbo_size;
	unsigned data_size;
	unsigned count;
	void *data;
};

struct _cairo_gpu_subspace
{
	cairo_gpu_subspace_t* next;
	pthread_key_t context_tls;

	union
	{
		struct
		{
			GLXFBConfig fbconfig;
			GLXDrawable dummy_drawable;
			char dummy_drawable_is_window;
		} glx;
	};
};

#define TABLE_FRAG (0 << 30)
#define TABLE_VERT (1 << 30)
#define TABLE_FBCONFIG (2 << 30)
#define TABLE_MASK (3 << 30)

typedef struct
{
	cairo_hash_entry_t base;
	unsigned v;
} cairo_gpu_int_entry_t;

typedef struct
{
	cairo_hash_entry_t base;
	void* v;
} cairo_gpu_ptr_entry_t;;

typedef struct
{
	char api;
	union
	{
		cairo_glx_state_t glx;
		cairo_osmesa_state_t osmesa;
	};
} cairo_gpu_gl_state_t;

#define MAX_TEXTURE_UNITS 4

typedef void* (*PFNGLGETPROCADDRESSPROC) (const char*);

typedef void (APIENTRYP PFNGLCULLFACEPROC) (GLenum mode);
typedef void (APIENTRYP PFNGLFRONTFACEPROC) (GLenum mode);
typedef void (APIENTRYP PFNGLHINTPROC) (GLenum target, GLenum mode);
typedef void (APIENTRYP PFNGLLINEWIDTHPROC) (GLfloat width);
typedef void (APIENTRYP PFNGLPOINTSIZEPROC) (GLfloat size);
typedef void (APIENTRYP PFNGLPOLYGONMODEPROC) (GLenum face, GLenum mode);
typedef void (APIENTRYP PFNGLSCISSORPROC) (GLint x, GLint y, GLsizei width, GLsizei height);
typedef void (APIENTRYP PFNGLTEXPARAMETERFPROC) (GLenum target, GLenum pname, GLfloat param);
typedef void (APIENTRYP PFNGLTEXPARAMETERFVPROC) (GLenum target, GLenum pname, const GLfloat *params);
typedef void (APIENTRYP PFNGLTEXPARAMETERIPROC) (GLenum target, GLenum pname, GLint param);
typedef void (APIENTRYP PFNGLTEXPARAMETERIVPROC) (GLenum target, GLenum pname, const GLint *params);
typedef void (APIENTRYP PFNGLTEXIMAGE1DPROC) (GLenum target, GLint level, GLint internalformat, GLsizei width, GLint border, GLenum format, GLenum type, const GLvoid *pixels);
typedef void (APIENTRYP PFNGLTEXIMAGE2DPROC) (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid *pixels);
typedef void (APIENTRYP PFNGLDRAWBUFFERPROC) (GLenum mode);
typedef void (APIENTRYP PFNGLCLEARPROC) (GLbitfield mask);
typedef void (APIENTRYP PFNGLCLEARCOLORPROC) (GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha);
typedef void (APIENTRYP PFNGLCLEARSTENCILPROC) (GLint s);
typedef void (APIENTRYP PFNGLCLEARDEPTHPROC) (GLclampd depth);
typedef void (APIENTRYP PFNGLSTENCILMASKPROC) (GLuint mask);
typedef void (APIENTRYP PFNGLCOLORMASKPROC) (GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha);
typedef void (APIENTRYP PFNGLDEPTHMASKPROC) (GLboolean flag);
typedef void (APIENTRYP PFNGLDISABLEPROC) (GLenum cap);
typedef void (APIENTRYP PFNGLENABLEPROC) (GLenum cap);
typedef void (APIENTRYP PFNGLFINISHPROC) (void);
typedef void (APIENTRYP PFNGLFLUSHPROC) (void);
typedef void (APIENTRYP PFNGLBLENDFUNCPROC) (GLenum sfactor, GLenum dfactor);
typedef void (APIENTRYP PFNGLLOGICOPPROC) (GLenum opcode);
typedef void (APIENTRYP PFNGLSTENCILFUNCPROC) (GLenum func, GLint ref, GLuint mask);
typedef void (APIENTRYP PFNGLSTENCILOPPROC) (GLenum fail, GLenum zfail, GLenum zpass);
typedef void (APIENTRYP PFNGLDEPTHFUNCPROC) (GLenum func);
typedef void (APIENTRYP PFNGLPIXELSTOREFPROC) (GLenum pname, GLfloat param);
typedef void (APIENTRYP PFNGLPIXELSTOREIPROC) (GLenum pname, GLint param);
typedef void (APIENTRYP PFNGLREADBUFFERPROC) (GLenum mode);
typedef void (APIENTRYP PFNGLREADPIXELSPROC) (GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid *pixels);
typedef void (APIENTRYP PFNGLGETBOOLEANVPROC) (GLenum pname, GLboolean *params);
typedef void (APIENTRYP PFNGLGETDOUBLEVPROC) (GLenum pname, GLdouble *params);
typedef GLenum (APIENTRYP PFNGLGETERRORPROC) (void);
typedef void (APIENTRYP PFNGLGETFLOATVPROC) (GLenum pname, GLfloat *params);
typedef void (APIENTRYP PFNGLGETINTEGERVPROC) (GLenum pname, GLint *params);
typedef const GLubyte * (APIENTRYP PFNGLGETSTRINGPROC) (GLenum name);
typedef void (APIENTRYP PFNGLGETTEXIMAGEPROC) (GLenum target, GLint level, GLenum format, GLenum type, GLvoid *pixels);
typedef void (APIENTRYP PFNGLGETTEXPARAMETERFVPROC) (GLenum target, GLenum pname, GLfloat *params);
typedef void (APIENTRYP PFNGLGETTEXPARAMETERIVPROC) (GLenum target, GLenum pname, GLint *params);
typedef void (APIENTRYP PFNGLGETTEXLEVELPARAMETERFVPROC) (GLenum target, GLint level, GLenum pname, GLfloat *params);
typedef void (APIENTRYP PFNGLGETTEXLEVELPARAMETERIVPROC) (GLenum target, GLint level, GLenum pname, GLint *params);
typedef GLboolean (APIENTRYP PFNGLISENABLEDPROC) (GLenum cap);
typedef void (APIENTRYP PFNGLDEPTHRANGEPROC) (GLclampd near, GLclampd far);
typedef void (APIENTRYP PFNGLVIEWPORTPROC) (GLint x, GLint y, GLsizei width, GLsizei height);

typedef void (APIENTRYP PFNGLCOLORPOINTERPROC) (GLint size, GLenum type, GLsizei stride, const GLvoid *pointer);
typedef void (APIENTRYP PFNGLTEXCOORDPOINTERPROC) (GLint size, GLenum type, GLsizei stride, const GLvoid *pointer);
typedef void (APIENTRYP PFNGLVERTEXPOINTERPROC) (GLint size, GLenum type, GLsizei stride, const GLvoid *pointer);

typedef void (APIENTRYP PFNGLCOLOR4FVPROC)(const GLfloat *v);
typedef void (APIENTRYP PFNGLCOPYPIXELSPROC)(GLint x, GLint y, GLsizei width, GLsizei height, GLenum type);
typedef void (APIENTRYP PFNGLENABLECLIENTSTATEPROC)(GLenum array);
typedef void (APIENTRYP PFNGLDISABLECLIENTSTATEPROC)(GLenum array);
typedef void (APIENTRYP PFNGLDRAWPIXELSPROC)(GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid *pixels);
typedef void (APIENTRYP PFNGLLOADIDENTITYPROC)(void);
typedef void (APIENTRYP PFNGLLOADMATRIXFPROC)(const GLfloat* m);
typedef void (APIENTRYP PFNGLLOADMATRIXDPROC)(const GLdouble* m);
typedef void (APIENTRYP PFNGLMATRIXMODEPROC)(GLenum mode);
typedef void (APIENTRYP PFNGLORTHOPROC)(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble zNear, GLdouble zFar);
typedef void (APIENTRYP PFNGLPIXELMAPFVPROC) (GLenum map, GLsizei mapsize, const GLfloat *values);
typedef void (APIENTRYP PFNGLPIXELTRANSFERFPROC) (GLenum pname, GLfloat param);
typedef void (APIENTRYP PFNGLPIXELZOOMPROC) (GLfloat xfactor, GLfloat yfactor);
typedef void (APIENTRYP PFNGLRASTERPOS2IPROC) (GLint x, GLint y);
typedef void (APIENTRYP PFNGLRECTIPROC) (GLint x1, GLint y1, GLint x2, GLint y2);
typedef void (APIENTRYP PFNGLTEXENVFVPROC) (GLenum target, GLenum pname, const GLfloat *params);
typedef void (APIENTRYP PFNGLTEXENVIPROC) (GLenum target, GLenum pname, GLint param);
typedef void (APIENTRYP PFNGLTEXGENFVPROC) (GLenum coord, GLenum pname, const GLfloat *params);
typedef void (APIENTRYP PFNGLTEXGENIPROC) (GLenum coord, GLenum pname, GLint param);
typedef void (APIENTRYP PFNGLTEXSUBIMAGE2DPROC) (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid *pixels);
typedef void (APIENTRYP PFNGLTRANSLATEDPROC) (GLdouble x, GLdouble y, GLdouble z);
typedef void (APIENTRYP PFNGLTRANSLATEFPROC) (GLfloat x, GLfloat y, GLfloat z);
typedef void (APIENTRYP PFNGLCOPYTEXSUBIMAGE2DPROC) (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height);

typedef struct
{
	PFNGLACTIVETEXTUREARBPROC ActiveTextureARB;
	PFNGLBINDBUFFERARBPROC BindBufferARB;
	PFNGLBINDFRAMEBUFFEREXTPROC BindFramebufferEXT;
	PFNGLBINDPROGRAMARBPROC BindProgramARB;
	PFNGLBINDRENDERBUFFEREXTPROC BindRenderbufferEXT;
	PFNGLBINDTEXTUREEXTPROC BindTextureEXT;
	PFNGLBINDVERTEXARRAYPROC BindVertexArray;
	PFNGLBLENDCOLOREXTPROC BlendColorEXT;
	PFNGLBLENDEQUATIONPROC BlendEquation;
	PFNGLBLENDFUNCPROC BlendFunc;
	PFNGLBLENDFUNCSEPARATEEXTPROC BlendFuncSeparateEXT;
	PFNGLBLITFRAMEBUFFEREXTPROC BlitFramebufferEXT;
	PFNGLBUFFERDATAARBPROC BufferDataARB;
	PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC CheckFramebufferStatusEXT;
	PFNGLCLEARCOLORPROC ClearColor;
	PFNGLCLEARPROC Clear;
	PFNGLCLIENTACTIVETEXTUREARBPROC ClientActiveTextureARB;
	PFNGLCOLOR4FVPROC Color4fv;
	PFNGLCOLORMASKPROC ColorMask;
	PFNGLCOLORPOINTERPROC ColorPointer;
	PFNGLCOPYPIXELSPROC CopyPixels;
	PFNGLCOPYTEXSUBIMAGE2DPROC CopyTexSubImage2D;
	PFNGLDELETEBUFFERSARBPROC DeleteBuffersARB;
	PFNGLDELETEFRAMEBUFFERSEXTPROC DeleteFramebuffersEXT;
	PFNGLDELETEPROGRAMSARBPROC DeleteProgramsARB;
	PFNGLDELETERENDERBUFFERSEXTPROC DeleteRenderbuffersEXT;
	PFNGLDELETETEXTURESEXTPROC DeleteTexturesEXT;
	PFNGLDELETEVERTEXARRAYSPROC DeleteVertexArrays;
	PFNGLDISABLECLIENTSTATEPROC DisableClientState;
	PFNGLDISABLEPROC Disable;
	PFNGLDRAWARRAYSEXTPROC DrawArraysEXT;
	PFNGLDRAWBUFFERPROC DrawBuffer;
	PFNGLDRAWPIXELSPROC DrawPixels;
	PFNGLENABLECLIENTSTATEPROC EnableClientState;
	PFNGLENABLEPROC Enable;
	PFNGLENABLEVERTEXATTRIBARRAYARBPROC EnableVertexAttribArrayARB;
	PFNGLFINISHPROC Finish;
	PFNGLFLUSHPROC Flush;
	PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC FramebufferRenderbufferEXT;
	PFNGLFRAMEBUFFERTEXTURE2DEXTPROC FramebufferTexture2DEXT;
	PFNGLGENBUFFERSARBPROC GenBuffersARB;
	PFNGLGENERATEMIPMAPEXTPROC GenerateMipmapEXT;
	PFNGLGENFRAMEBUFFERSEXTPROC GenFramebuffersEXT;
	PFNGLGENPROGRAMSARBPROC GenProgramsARB;
	PFNGLGENRENDERBUFFERSEXTPROC GenRenderbuffersEXT;
	PFNGLGENTEXTURESEXTPROC GenTexturesEXT;
	PFNGLGENVERTEXARRAYSPROC GenVertexArrays;
	PFNGLGETERRORPROC GetError;
	PFNGLGETFLOATVPROC GetFloatv;
	PFNGLGETINTEGERVPROC GetIntegerv;
	PFNGLGETSTRINGPROC GetString;
	PFNGLGETTEXIMAGEPROC GetTexImage;
	PFNGLHINTPROC Hint;
	PFNGLLOADIDENTITYPROC LoadIdentity;
	PFNGLLOADMATRIXDPROC LoadMatrixd;
	PFNGLLOADMATRIXDPROC LoadMatrixf;
	PFNGLMAPBUFFERARBPROC MapBufferARB;
	PFNGLMAPBUFFERRANGEPROC MapBufferRange;
	PFNGLMATRIXMODEPROC MatrixMode;
	PFNGLORTHOPROC Ortho;
	PFNGLPIXELMAPFVPROC PixelMapfv;
	PFNGLPIXELSTOREIPROC PixelStorei;
	PFNGLPIXELTRANSFERFPROC PixelTransferf;
	PFNGLPIXELZOOMPROC PixelZoom;
	PFNGLPROGRAMENVPARAMETER4FVARBPROC ProgramEnvParameter4fvARB;
	PFNGLPROGRAMSTRINGARBPROC ProgramStringARB;
	PFNGLRASTERPOS2IPROC RasterPos2i;
	PFNGLREADBUFFERPROC ReadBuffer;
	PFNGLREADPIXELSPROC ReadPixels;
	PFNGLRECTIPROC Recti;
	PFNGLRENDERBUFFERSTORAGEMULTISAMPLECOVERAGENVPROC RenderbufferStorageMultisampleCoverageNV;
	PFNGLRENDERBUFFERSTORAGEMULTISAMPLEEXTPROC RenderbufferStorageMultisampleEXT;
	PFNGLSCISSORPROC Scissor;
	PFNGLTEXCOORDPOINTERPROC TexCoordPointer;
	PFNGLTEXENVFVPROC TexEnvfv;
	PFNGLTEXENVIPROC TexEnvi;
	PFNGLTEXGENFVPROC TexGenfv;
	PFNGLTEXGENIPROC TexGeni;
	PFNGLTEXIMAGE2DPROC TexImage2D;
	PFNGLTEXPARAMETERIPROC TexParameteri;
	PFNGLTEXSUBIMAGE2DPROC TexSubImage2D;
	PFNGLTRANSLATEDPROC Translated;
	PFNGLTRANSLATEFPROC Translatef;
	PFNGLUNMAPBUFFERARBPROC UnmapBufferARB;
	PFNGLVERTEXPOINTERPROC VertexPointer;
	PFNGLVIEWPORTPROC Viewport;
	PFNGLWINDOWPOS2IPROC WindowPos2i;
} cairo_gpu_gl_t;

struct _cairo_gpu_space
{
	CAIRO_GPU_BASE_SPACE_T;

	void* libgl;
	PFNGLGETPROCADDRESSPROC GetProcAddress;

	cairo_gpu_subspace_t subspace;

	unsigned char api;
	unsigned char fb_blit : 1;
	unsigned char use_frag_prog : 1;
	unsigned char use_vert_prog : 1;
	unsigned char use_vbo : 1;
	unsigned char has_window_pos : 1;
	unsigned char has_framebuffer_multisample : 1;
	unsigned char has_framebuffer_multisample_coverage : 1;
	unsigned char has_fragment_program2 : 1;
	unsigned char has_gpu_program4 : 1;
	unsigned char has_combine : 1;
	unsigned char use_intensity : 1;
	unsigned char crossbar : 1;
	unsigned char destroy_on_unbind : 1;
	unsigned char alpha_fbo_supported : 1;
	unsigned char alpha_fbo_unsupported : 1;
	unsigned char owns_libgl : 1;
	unsigned char tex_units;

	union
	{
		cairo_osmesa_t osmesa;
		cairo_glx_t glx;
	};

	cairo_hash_table_t* table;
};

struct _cairo_gpu_space_tls
{
	CAIRO_GPU_BASE_SPACE_TLS_T;
	cairo_gpu_context_t* last_context;
	cairo_gpu_context_t* contexts;
};

// subspace_tls
struct _cairo_gpu_context
{
	cairo_gpu_space_t* space;
	cairo_gpu_subspace_t* subspace;
	cairo_gpu_space_tls_t* tls;
	cairo_gpu_context_t* next; /* in per-thread contexts list */

	cairo_gpu_gl_t gl;

	union
	{
		struct
		{
			OSMesaContext ctx;
		} osmesa;
		struct
		{
			GLXContext ctx;
			GLXDrawable current_drawable;
		} glx;
	};

	// copy of GL state
	struct
	{
		struct
		{
			unsigned tex;
			long long id;
			cairo_gpu_texture_t* texture;
		} targets[2];
		unsigned active_target_idx;
		unsigned enabled_target;
		unsigned nv_texture_shader;
	} textures[MAX_TEXTURE_UNITS];
	char constant_unit;
	unsigned char active_texture;
	unsigned char preferred_texture;

	unsigned blend_func;
	unsigned char smooth;
	unsigned char smooth_hint; // unknown / nicest / fastest
	unsigned char blend_eq;
	unsigned char color_mask;

	unsigned char vertex_array : 1;
	unsigned char color_array : 1;
	unsigned char tex_array : 1;
	unsigned char blend : 1;
	unsigned char init_frag_modulate : 1;
	unsigned char init_frag_combine : 1;
	unsigned char init_vert_fixed : 1;
	unsigned char init_pixel_map : 1;
	unsigned char fragp_enabled : 1;
	unsigned char vertp_enabled : 1;
	unsigned char destroy_on_unbind : 1;
	unsigned char component : 2;
	unsigned char tex2_src0_rgb_constant : 1;
	unsigned char nv_texture_shader : 1;
	unsigned char gl_inited : 1;

	unsigned frag;
	unsigned vert;
	unsigned fragp;
	unsigned vertp;

	cairo_gpu_color4_t color;

	int draw_flip_height;
	int read_flip_height;
	GLuint draw_fb;
	GLuint read_fb;
	long long draw_fb_id;
	long long read_fb_id;
	unsigned draw_buffer;
	unsigned read_buffer;
	int viewport_x;
	int viewport_y;
	int viewport_width;
	int viewport_height;
	int x_offset;
	int y_offset;
	int proj_sign;

	long long vao0_id; // the geometry that was last setup for VAO 0
	long long vbo_id; // bound VBO
	long long vao_id; // bound VAO

	unsigned mode;
	unsigned count;
};

static inline void*
_cairo_gpu_space__lookup(cairo_gpu_space_t* space, unsigned hash)
{
	cairo_hash_entry_t entry;
	void* p;
	entry.hash = hash;

	CAIRO_MUTEX_LOCK(space->mutex);
	p = _cairo_hash_table_lookup(space->table, &entry);
	CAIRO_MUTEX_UNLOCK(space->mutex);
	return p;
}

// OpenGL windows have "upside-down" coordinaties: the projection matrix normally handles this, but here it is not used. So we flip y manually.
static inline int
_cairo_gpu_flip_draw_y(cairo_gpu_context_t* ctx, int* y, int height)
{
	if(ctx->draw_flip_height >= 0)
	{
		*y = ctx->draw_flip_height - *y - height;
		return -height;
	}
	return height;
}

static inline int
_cairo_gpu_flip_read_y(cairo_gpu_context_t* ctx, int* y, int height)
{
	if(ctx->read_flip_height >= 0)
	{
		*y = ctx->read_flip_height - *y - height;
		return -height;
	}
	return height;
}

static inline unsigned
_cairo_gl_target(int idx)
{
	return idx ? GL_TEXTURE_RECTANGLE_ARB : GL_TEXTURE_2D;
}

static inline void _cairo_gpu_texture_fini(cairo_gpu_context_t* ctx, cairo_gpu_texture_t* texture)
{
	if(texture->tex)
		ctx->gl.DeleteTexturesEXT(1, &texture->tex);
	texture->tex = 0;
	texture->id = 0;
}

// you must fill target_idx
static inline void _cairo_gpu_texture_init(cairo_gpu_space_t* space, cairo_gpu_texture_t* texture, int width, int height)
{
	texture->unsized_format = 0;
	texture->tex = 0;
	texture->filter = 0;
	texture->wrap_s = texture->wrap_t = GL_REPEAT;
	texture->anisotropy = 1.0;
	texture->non_upside_down = 0;

	if(space->tex_npot || (is_pow2(width) && is_pow2(height)))
		texture->target_idx = TARGET_2D;
	else if(space->tex_rectangle)
		texture->target_idx = TARGET_RECTANGLE;
	else
	{
		texture->target_idx = TARGET_2D;
		width = higher_pow2(width - 1);
		height = higher_pow2(height - 1);
	}

	texture->width = width;
	texture->height = height;
}

static inline void _cairo_gpu_texture_realize(cairo_gpu_context_t* ctx, cairo_gpu_texture_t* texture)
{
	texture->id = _cairo_id();
	ctx->gl.GenTexturesEXT(1, &texture->tex);
}

// you must fill target_idx
static inline void _cairo_gpu_texture_create(cairo_gpu_context_t* ctx, cairo_gpu_texture_t* texture, int width, int height)
{
	_cairo_gpu_texture_init(ctx->space, texture, width, height);
	_cairo_gpu_texture_realize(ctx, texture);
}


static inline void
_cairo_gpu_texture_adjust_matrix(cairo_gpu_texture_t* texture, cairo_matrix_t* matrix)
{
	if(texture->non_upside_down)
	{
		matrix->xx = -matrix->xx;
		matrix->xy = -matrix->xy;
		matrix->x0 = texture->width - matrix->x0;

		matrix->yx = -matrix->yx;
		matrix->yy = -matrix->yy;
		matrix->y0 = texture->height - matrix->y0;
	}

	if(texture->target_idx == TARGET_2D)
	{
		double xm = 1.0 / texture->width;
		double ym = 1.0 / texture->height;

		matrix->xx *= xm;
		matrix->xy *= xm;
		matrix->x0 *= xm;

		matrix->yx *= ym;
		matrix->yy *= ym;
		matrix->y0 *= ym;
	}
}

static inline int
_cairo_gpu_surface_use_msaa(cairo_gpu_surface_t * surface)
{
	return surface->want_msaa && surface->msaa_fb;
}
