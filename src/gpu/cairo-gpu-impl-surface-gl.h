#include "cairo-gpu-impl-surface-gl-glx.h"

static inline int
_cairo_gpu_surface_destination_drawable(cairo_gpu_surface_t * surface)
{
	int idx = -1;
	API_SURFACE_(idx, get_destination_idx_drawable)(surface);
	return idx;
}

static inline cairo_bool_t
_cairo_gpu_surface_has_physical_alpha(cairo_gpu_surface_t* surface)
{
	cairo_bool_t v = TRUE;
	// We assume that the unused alpha channel is set to 1
	// TODO: due to this, we don't try to use the alpha channels of drawables; check whether we can do something about this
	API_SURFACE_(v, has_physical_alpha)(surface);
	return v;
}

static inline int
_cairo_gpu_surface_destination_msaa(cairo_gpu_surface_t * surface, int want_msaa)
{
	/* Currently, if we have a drawable, we always draw on that.
	 * This reduces surprises for the user.
	 */

	if(surface->msaa_fb && want_msaa)
		return SURF_MSAA;
	else if(surface->has_drawable)
		return _cairo_gpu_surface_destination_drawable(surface);
	else
		return SURF_TEX;

	/*
	else if(surface->autoflush && surface->buffer)
		return _cairo_gpu_surface_destination_drawable(surface);
	else if(surface->valid_mask & (1 << SURF_TEX))
		return SURF_TEX;
	else if(surface->buffer == GL_BACK && surface->valid_mask & (1 << SURF_BACK))
		return SURF_BACK;
	else if(surface->buffer == GL_FRONT && surface->valid_mask & (1 << SURF_FRONT))
		return SURF_FRONT;
	//else if(surface->buffer)
	//	return _cairo_gpu_surface_destination_drawable(surface);
	else
		return SURF_TEX; // draw to the texture by default so that nothing will be drawn if the user forgets to either flush or enable autoflushing
	*/
}

static inline int
_cairo_gpu_surface_destination(cairo_gpu_surface_t * surface)
{
	return _cairo_gpu_surface_destination_msaa(surface, surface->want_msaa);
}

static unsigned
_cairo_gpu_surface__create_fb(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface)
{
	ctx->gl.GenFramebuffersEXT(1, &surface->fb);
	_cairo_gl_context_set_framebuffer(ctx, FB_DRAW, surface->fb, surface->texture.id, GL_COLOR_ATTACHMENT0, -1);
	ctx->gl.FramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, _cairo_gl_target(surface->texture.target_idx), surface->texture.tex, 0);

	return ctx->gl.CheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
}

static void
_cairo_gpu_surface__create_tex(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface)
{
	// NO: unsigned format = (surface->base.content == CAIRO_CONTENT_COLOR) ? GL_RGB : GL_RGBA;
	// we must have an alpha channel to support EXTEND_NONE (and we also assume it exists to draw trapezoid masks)
	unsigned format;

	// TODO: only do this if, after checking, render-to-alpha turns out to be not supported
	if(ctx->space->use_fbo && ctx->space->alpha_fbo_unsupported)
		format = GL_RGBA;
	else
		format = (surface->base.content == CAIRO_CONTENT_ALPHA) ? (ctx->space->use_intensity ?  GL_INTENSITY : GL_ALPHA) : GL_RGBA;

	_cairo_gpu_texture_realize(ctx, &surface->texture);
	_cairo_gl_context_set_active_texture(ctx, _cairo_gl_context_set_texture(ctx, -1, &surface->texture));

	_cairo_gpu_context__set_mipmap_hint(ctx);
retry:
	surface->texture.unsized_format = format;
	GL_ERROR(ctx->gl.TexImage2D(_cairo_gl_context_active_target(ctx), 0, format, surface->texture.width, surface->texture.height, 0, format == GL_INTENSITY ? GL_ALPHA : format, GL_UNSIGNED_BYTE, NULL));

	// TODO: generalize to support checking non-8-bit formats
	if(ctx->space->use_fbo && format != GL_RGBA && !ctx->space->alpha_fbo_supported)
	{
		unsigned status = _cairo_gpu_surface__create_fb(ctx, surface);
		if(status && status != GL_FRAMEBUFFER_COMPLETE_EXT)
		{
			ctx->space->alpha_fbo_unsupported = 1;
			format = GL_RGBA;
			goto retry;
		}
		else
			ctx->space->alpha_fbo_supported = 1;
	}

	// uninitialized, so set fully dirty
	surface->bbox[SURF_TEX].x = 0;
	surface->bbox[SURF_TEX].y = 0;
	surface->bbox[SURF_TEX].width = surface->width;
	surface->bbox[SURF_TEX].height = surface->height;
}

// guaranteed to set the surface
static int
_cairo_gpu_surface__create_tex_fb(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface)
{
	assert(surface->space->use_fbo);

	// this is disallowed because FBOs are currently always treated as upside-down and if there is a drawable, the texture (and thus FBO) may be non-upside-down
	assert(!surface->has_drawable);

	if(!surface->texture.tex)
		_cairo_gpu_surface__create_tex(ctx, surface);

	if(!surface->fb)
	{
		GLenum status;

		status = _cairo_gpu_surface__create_fb(ctx, surface);
		if(status && status != GL_FRAMEBUFFER_COMPLETE_EXT)
			fprintf(stderr, "surface is framebuffer incomplete: status is %x\n", status);
	}

	return 1;
}

static void
_cairo_gpu_surface__set(cairo_gpu_surface_t * surface, cairo_gpu_context_t* ctx, int mask, int i)
{
       unsigned fb, buffer;
       long long id;
       int flip_height = -1;

       if(i == SURF_FRONT)
       {
               fb = 0;
               id = 0;
               buffer = GL_FRONT;
               flip_height = surface->buffer_non_upside_down ? (int)surface->height : -1;
       }
       else if(i == SURF_BACK)
       {
               fb = 0;
               id = 0;
               buffer = GL_BACK;
               flip_height = surface->buffer_non_upside_down ? (int)surface->height : -1;
       }
       else if(i == SURF_TEX)
       {
               if(!surface->fb)
                       _cairo_gpu_surface__create_tex_fb(ctx, surface);

               fb = surface->fb;
               id = surface->texture.id;
               buffer = GL_COLOR_ATTACHMENT0;
       }
       else if(i == SURF_MSAA)
       {
               fb = surface->msaa_fb;
               id = surface->msaa_id;
               buffer = GL_COLOR_ATTACHMENT0;
       }
       else
               assert(0);

       _cairo_gl_context_set_framebuffer(ctx, mask, fb, id, buffer, flip_height);
}


static void
_cairo_gpu_blit_image(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, cairo_gpu_texture_t* texture, int dst_x, int dst_y, int src_x, int src_y, int width, int height, int zoom_x, int zoom_y);

static __attribute__((unused)) void
_cairo_gpu_fill_rect_diff(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface, cairo_rectangle_int_t * out, cairo_rectangle_int_t * in, float r, float g, float b, float a);

// may clobber read fbo and draw state
static void
_cairo_gpu_surface__update_draw(cairo_gpu_context_t* ctx, cairo_gpu_surface_t * surface, int dst, int src)
{
	cairo_content_t content;
	assert(!(surface->valid_mask & (1 << dst)));

	content = surface->base.content;
	if(surface->clear_rgba)
		surface->base.content = CAIRO_CONTENT_COLOR_ALPHA;

	if(src >= 0)
	{
		cairo_rectangle_int_t *out = &surface->bbox[dst];
		cairo_rectangle_int_t *in = &surface->bbox[src];

		//printf("update draw %i = %i [%i %i %i %i : %i %i %i %i]\n", j, i, out->x, out->y, out->width, out->height, in->x, in->y, in->width, in->height);

		_cairo_gpu_fill_rect_diff(ctx, surface, out, in, 0, 0, 0, 0);

		if(src != SURF_TEX || surface->fb)
		{
			_cairo_gpu_surface__set(surface, ctx, FB_READ, src);

			//printf("blit %i to %i: (%i, %i, %i, %i) -> (%i, %i, %i, %i)\n", i, j, in->x, in->y, in->width, in->height, out->x, out->y, out->width, out->height);
			//_cairo_gpu_context_fill_rect(ctx, out->x, out->y, out->width, out->height, 0, 0, 0, 0);

			_cairo_gpu_context_blit_same(ctx, surface, in->x, in->y, in->width, in->height);
		}
		else
		{
			// may happen if we switched between different drawables
			_cairo_gpu_blit_image(ctx, surface, &surface->texture, in->x, in->y, in->x, in->y, in->width, in->height, 1, 1);
		}

		memcpy(out, in, sizeof(*in));
	}
	else
	{
		_cairo_gpu_context__set_color_mask(ctx, 0xf);
		if(surface->base.content == CAIRO_CONTENT_COLOR)
			ctx->gl.ClearColor(0, 0, 0, 1);
		else
			ctx->gl.ClearColor(0, 0, 0, 0);
		ctx->gl.Clear(GL_COLOR_BUFFER_BIT);
	}

	surface->base.content = content;
	surface->valid_mask |= 1 << dst;
}

static inline cairo_gpu_context_t *
_cairo_gpu_surface__do_lookup_context(cairo_gpu_surface_t* surface)
{
	cairo_gpu_context_t* ctx = 0;
	cairo_gpu_space_tls_t* tls;

	tls = _cairo_gpu_space_get_tls(surface->space);

	if(likely(!surface->subspace))
		return _cairo_gpu_space_tls_lookup_context(tls);

	ctx = (cairo_gpu_context_t*)pthread_getspecific(surface->subspace->context_tls);

	if(!ctx)
		ctx = _cairo_gpu_space_tls__create_context(tls, surface->subspace);
	else
		tls->last_context = ctx;
	return ctx;
}

static inline cairo_gpu_context_t*
_cairo_gpu_surface__lookup_context(cairo_gpu_surface_t * surface, unsigned mask, int idx)
{
	int src = idx;
	if(!(surface->valid_mask & (1 << idx)))
	{
		int i;
		src = -1;
		for(i = 0; i < 4; ++i)
		{
			if(surface->valid_mask & (1 << i))
			{
				src = i;
				break;
			}
		}

		mask |= FB_DRAW;
	}

	if(!mask)
		return 0;

	if(SURF_IS_DRAWABLE(idx) || SURF_IS_DRAWABLE(src))
		return _cairo_gpu_surface__do_lookup_context(surface);
	else
		return _cairo_gpu_space_tls_lookup_context(_cairo_gpu_space_get_tls(surface->space));
}

static inline cairo_gpu_context_t*
_cairo_gpu_surface_lookup_context(cairo_gpu_surface_t * surface, unsigned mask)
{
	return _cairo_gpu_surface__lookup_context(surface, mask, _cairo_gpu_surface_destination(surface));
}

static inline void
_cairo_gpu_surface__bind_to(cairo_gpu_surface_t * surface, cairo_gpu_context_t* ctx, unsigned mask, int idx)
{
	int src = idx;
	if(!(surface->valid_mask & (1 << idx)))
	{
		int i;
		src = -1;
		for(i = 0; i < 4; ++i)
		{
			if(surface->valid_mask & (1 << i))
			{
				src = i;
				break;
			}
		}

		mask |= FB_DRAW;
	}

	if(SURF_IS_DRAWABLE(idx) || SURF_IS_DRAWABLE(src))
		_cairo_gl_context_bind_surface(ctx, surface);
	else
		_cairo_gpu_context_bind(ctx);

	if(src != idx)
	{
		mask |= FB_DRAW;
		_cairo_gpu_surface__set(surface, ctx, mask, idx);
		_cairo_gpu_surface__update_draw(ctx, surface, idx, src);
	}
	else
		_cairo_gpu_surface__set(surface, ctx, mask, idx);
}

static inline void
_cairo_gpu_surface_bind_to(cairo_gpu_surface_t * surface, cairo_gpu_context_t* ctx, unsigned mask)
{
	_cairo_gpu_surface__bind_to(surface, ctx, mask, _cairo_gpu_surface_destination(surface));
}

static inline cairo_gpu_context_t*
_cairo_gpu_surface__bind(cairo_gpu_surface_t * surface, unsigned mask, int idx)
{
	cairo_gpu_context_t* ctx;
	ctx = _cairo_gpu_surface__lookup_context(surface, mask, idx);
	if(ctx)
		_cairo_gpu_surface__bind_to(surface, ctx, mask, idx);
	return ctx;
}

static inline cairo_gpu_context_t*
_cairo_gpu_surface_bind(cairo_gpu_surface_t * surface, unsigned mask)
{
	return _cairo_gpu_surface__bind(surface, mask, _cairo_gpu_surface_destination(surface));
}

static inline cairo_gpu_context_t*
_cairo_gpu_surface_bind_drawable(cairo_gpu_surface_t * surface, unsigned mask)
{
	return _cairo_gpu_surface__bind(surface, mask, _cairo_gpu_surface_destination_drawable(surface));
}

static void
_cairo_gpu_surface__flip_drawable(cairo_gpu_surface_t* surface)
{
	surface->buffer_non_upside_down ^= 1;
	surface->bbox[SURF_FRONT].y = surface->height - surface->bbox[SURF_FRONT].height - surface->bbox[SURF_FRONT].y;
	surface->bbox[SURF_BACK].y = surface->height - surface->bbox[SURF_BACK].height - surface->bbox[SURF_BACK].y;
}

static void
_cairo_gpu_surface__flip_texture(cairo_gpu_surface_t* surface)
{
	surface->texture.non_upside_down ^= 1;
	surface->bbox[SURF_TEX].y = surface->texture.height - surface->bbox[SURF_TEX].height - surface->bbox[SURF_TEX].y;

	// padding space may also be dirty
	if(!surface->transparent_padding)
	{
		surface->bbox[SURF_TEX].height += surface->bbox[SURF_TEX].y;
		surface->bbox[SURF_TEX].y = 0;
		surface->bbox[SURF_TEX].x = 0;
		surface->bbox[SURF_TEX].width = surface->width;
	}

	// the flipped dirty area may go into the padding space. If so, fix it and invalidate the padding space.
	if(surface->bbox[SURF_TEX].y >= (int)surface->height)
	{
		surface->bbox[SURF_TEX].width = surface->bbox[SURF_TEX].height = 0;
		surface->transparent_padding = 0;
	}
	else if(surface->bbox[SURF_TEX].height > ((int)surface->height - surface->bbox[SURF_TEX].y))
	{
		surface->bbox[SURF_TEX].height = surface->height - surface->bbox[SURF_TEX].y;
		surface->transparent_padding = 0;
	}

	surface->valid_mask &=~ (1 << SURF_PSEUDO_PADDING);
}

// may clobber read fb, blend/color mask
// you must rebind your old context, if any!
static cairo_gpu_context_t*
_cairo_gpu_surface__update_tex(cairo_gpu_surface_t * surface)
{
	int j = SURF_TEX;
	int i;
	cairo_gpu_context_t* ctx = 0;

	for(i = 0; i < 4; ++i)
	{
		if(surface->valid_mask & (1 << i))
		{
retry:
			// TODO: how about always using blits?
			if(i != SURF_MSAA)
			{
				cairo_rectangle_int_t *in;
				cairo_rectangle_int_t *out;
				cairo_rectangle_int_t rect;
				int alpha_to_red;
				int idx;

				in = &surface->bbox[i];
				out = &surface->bbox[j];

				if((!out->width || !out->height) && (!in->width || !in->height))
					goto out;

				alpha_to_red = (surface->texture.unsized_format == GL_INTENSITY && !(surface->valid_mask & (1 << SURF_PSEUDO_ALPHA_TO_RED)));
				ctx = _cairo_gpu_surface__bind(surface, FB_READ | (alpha_to_red ? FB_DRAW : 0), i);

				//printf("update tex %i = %i [%i %i %i %i : %i %i %i %i]\n", j, i, out->x, out->y, out->width, out->height, in->x, in->y, in->width, in->height);

				if(!surface->texture.tex)
					_cairo_gpu_surface__create_tex(ctx, surface);

				if(surface->buffer_non_upside_down != surface->texture.non_upside_down)
					_cairo_gpu_surface__flip_texture(surface);

				if(out->width && out->height && in->width && in->height)
				{
					rect.x = MIN(in->x, out->x);
					rect.y = MIN(in->y, out->y);
					rect.width = MAX(in->x + in->width, out->x + out->width) - rect.x;
					rect.height = MAX(in->y + in->height, out->y + out->height) - rect.y;
				}
				else if(out->width && out->height)
					rect = *out;
				else if(in->width && in->height)
					rect = *in;
				else
					assert(0);

				assert(i == SURF_FRONT || i == SURF_BACK);

				if(alpha_to_red)
				{
					cairo_gpu_blend_t blend;
					blend.v = 0;
					blend.src_rgb = BLEND_DST_ALPHA;
					if(!surface->space->blend_func_separate)
						blend.src_alpha = blend.src_rgb;
					blend.color_mask = 1;

					_cairo_gpu_context_set_viewport(ctx, 0, 0, surface->width, surface->height);
					_cairo_gpu_context_set_blend(ctx, blend.v);
					_cairo_gpu_context_set_raster(ctx, 0);
					_cairo_gpu_context_set_vert_frag(ctx, 0, FRAG_CONSTANT);
					_cairo_gpu_context_set_constant_color(ctx, &_cairo_gpu_white);
					_cairo_gpu_context_set_translation(ctx, 0, 0);
					_cairo_gpu_context_draw_rect(ctx, 0, 0, surface->width, surface->height);
					surface->valid_mask |= (1 << SURF_PSEUDO_ALPHA_TO_RED);
				}

				idx = _cairo_gl_context_set_texture(ctx, -1, &surface->texture);
				_cairo_gl_context_set_active_texture(ctx, idx);
				//printf("CopyTexSubImage2D (%lx)\n", surface->glx.glxdrawable);
				ctx->gl.CopyTexSubImage2D(_cairo_gl_context_active_target(ctx), 0, rect.x, rect.y, rect.x, rect.y, rect.width, rect.height);
				memcpy(out, in, sizeof(*in));
				surface->valid_mask |= 1 << j;
				return ctx;
			}
			else
			{
				// we need to do multisample resolve via blit, just use update_draw
				return _cairo_gpu_surface__bind(surface, 0, j);
			}
		}
	}

	// there isn't any ClearTexSubImage2D...
	if(!surface->texture.tex || (surface->bbox[j].width && surface->bbox[j].height))
	{
		i = _cairo_gpu_surface_destination(surface);
		ctx = _cairo_gpu_surface__bind(surface, FB_READ | FB_DRAW, i);
		goto retry;
	}

out:
	surface->valid_mask |= 1 << j;
	return 0;
}

static cairo_gpu_context_t*
_cairo_gpu_surface_bind_tex_update(cairo_gpu_surface_t * surface)
{
	cairo_gpu_context_t* ctx = 0;
	if(!(surface->valid_mask & (1 << SURF_TEX)))
		ctx = _cairo_gpu_surface__update_tex(surface);

	if(!ctx)
	{
		ctx = _cairo_gpu_space_bind(surface->space);
		if(!surface->texture.tex)
			_cairo_gpu_surface__create_tex(ctx, surface);
	}
	return ctx;
}

// ctx may be 0 and doesn't need to be bound (and may be unbound if it is)
static inline cairo_gpu_texture_t*
_cairo_gpu_surface_begin_texture(cairo_gpu_surface_t * surface, cairo_gpu_context_t* ctx, int idx)
{
	cairo_gpu_texture_t* texture = 0;
	if(!surface->valid_mask)
		texture = &surface->space->dummy_texture;
	else if(surface->valid_mask & (1 << SURF_TEX))
	{
		if(!surface->texture.tex)
			texture = &surface->space->dummy_texture;
		else
			texture = &surface->texture;
	}
	else
	{
		// if in the update, we happen to be able to use context ctx, then also use the final texture unit, so we don't have to bind the texture twice
		if(ctx)
			ctx->preferred_texture = idx;

		API_SURFACE_(texture, begin_texture)(surface);
		if(!texture)
		{
			_cairo_gpu_surface__update_tex(surface);
			texture = &surface->texture;
		}
	}

	// TODO: test this, decide whether to do this and enable this
#if 0
	if(!(surface->valid_mask & (1 << SURF_PSEUDO_MIPMAPS)) && !surface->tex_rectangle)
	{
		_cairo_gl_context_set_active_texture(ctx, i);
		ctx->gl.GenerateMipmapEXT(GL_TEXTURE_2D);
		surface->valid_mask |= 1 << SURF_PSEUDO_MIPMAPS;
	}
#endif
	return texture;
}

static void
_cairo_gpu_surface_end_texture(cairo_gpu_context_t * ctx, cairo_gpu_surface_t* surface, cairo_gpu_texture_t* texture)
{
	API_SURFACE(end_texture)(surface, ctx, texture);
}

static void
_cairo_gpu_surface__msaa_fini(cairo_gpu_context_t* ctx, cairo_gpu_surface_t * surface)
{
	if(surface->msaa_rb)
	{
		GLenum fba = _cairo_gl_context_set_any_framebuffer(ctx, surface->msaa_fb, surface->msaa_id, 0);
		ctx->gl.FramebufferRenderbufferEXT(fba, GL_COLOR_ATTACHMENT0_EXT, GL_RENDERBUFFER_EXT, 0);
		ctx->gl.DeleteRenderbuffersEXT(1, &surface->msaa_rb);
		ctx->gl.DeleteFramebuffersEXT(1, &surface->msaa_fb);

		surface->msaa_rb = 0;
		surface->msaa_fb = 0;

		surface->bbox[SURF_MSAA].width = 0;
		surface->bbox[SURF_MSAA].height = 0;

		surface->valid_mask &=~ (1 << SURF_MSAA);
	}
}

static cairo_int_status_t
_cairo_gpu_surface__msaa_realize(void* abstract_surface, unsigned coverage_samples, unsigned color_samples)
{
	cairo_int_status_t status;
	cairo_gpu_surface_t * surface = abstract_surface;
	GLuint rb = 0;
	GLuint fb = 0;
	long long id = 0;
	GLuint format;
	cairo_gpu_context_t *ctx = 0;
	unsigned gl_coverage_samples = coverage_samples;
	unsigned gl_color_samples = color_samples;

	if(coverage_samples != color_samples && !surface->space->has_framebuffer_multisample_coverage)
		gl_coverage_samples = gl_color_samples = MAX(color_samples, coverage_samples);

	format = (surface->base.content == CAIRO_CONTENT_COLOR) ? GL_RGB : GL_RGBA;

	if(surface->valid_mask == (1 << SURF_MSAA))
	{
		int idx = _cairo_gpu_surface_destination_msaa(surface, 0);
		ctx = _cairo_gpu_surface__bind(surface, 0, idx);
	}

	if(coverage_samples >= 1 || color_samples >= 1)
	{
		GLenum fba;
		id = _cairo_id();
		if(!ctx)
			ctx = _cairo_gpu_space_bind(surface->space);
		ctx->gl.GenRenderbuffersEXT(1, &rb);
		ctx->gl.BindRenderbufferEXT(GL_RENDERBUFFER_EXT, rb);
		if(gl_coverage_samples != gl_color_samples)
			ctx->gl.RenderbufferStorageMultisampleCoverageNV(GL_RENDERBUFFER_EXT, gl_coverage_samples, gl_color_samples, format, surface->width, surface->height);
		else
			ctx->gl.RenderbufferStorageMultisampleEXT(GL_RENDERBUFFER_EXT, gl_color_samples, format, surface->width, surface->height);
		if(ctx->gl.GetError())
		{
			ctx->gl.DeleteRenderbuffersEXT(1, &rb);
			status = CAIRO_STATUS_NO_MEMORY;
			goto out;
		}

		fba = _cairo_gl_context_set_any_framebuffer(ctx, fb, id, 0);
		ctx->gl.GenFramebuffersEXT(1, &fb);
		ctx->gl.BindFramebufferEXT(fba, fb);
		ctx->gl.FramebufferRenderbufferEXT(fba, GL_COLOR_ATTACHMENT0_EXT, GL_RENDERBUFFER_EXT, rb);
		if(ctx->gl.CheckFramebufferStatusEXT(fba) != GL_FRAMEBUFFER_COMPLETE_EXT)
		{
			ctx->gl.FramebufferRenderbufferEXT(fba, GL_COLOR_ATTACHMENT0_EXT, GL_RENDERBUFFER_EXT, 0);
			ctx->gl.DeleteRenderbuffersEXT(1, &rb);
			ctx->gl.DeleteFramebuffersEXT(1, &fb);
			status = CAIRO_STATUS_NO_MEMORY;
			goto out;
		}

		// uninitialized, thus fully dirty
		surface->bbox[SURF_MSAA].x = 0;
		surface->bbox[SURF_MSAA].y = 0;
		surface->bbox[SURF_MSAA].width = surface->width;
		surface->bbox[SURF_MSAA].height = surface->height;
	}

	_cairo_gpu_surface__msaa_fini(ctx, surface);

	if(rb)
	{
		surface->msaa_rb = rb;
		surface->msaa_fb = fb;
		surface->msaa_id = id;
	}

	surface->coverage_samples = coverage_samples;
	surface->color_samples = color_samples;
	status = CAIRO_STATUS_SUCCESS;
out:
	return status;
}

static cairo_int_status_t
_cairo_gpu_surface_set_samples(void* abstract_surface, unsigned coverage_samples, unsigned color_samples)
{
	cairo_int_status_t status;
	cairo_gpu_surface_t * surface = abstract_surface;

	if(coverage_samples == surface->coverage_samples && color_samples == surface->color_samples)
		return CAIRO_STATUS_SUCCESS;

	if(!surface->space->use_fbo || !surface->space->has_framebuffer_multisample || coverage_samples >= 256 || color_samples >= 256)
		return CAIRO_STATUS_NO_MEMORY;	// TODO: cairo doesn't have a generic invalid status?!?

	_cairo_gpu_enter();
	status = _cairo_gpu_surface__msaa_realize(surface, coverage_samples, color_samples);
	_cairo_gpu_exit();
	return status;
}

static int _cairo_gpu_aa_formats[] = {
	16, 4,
	16, 8,
	16, 16,
	8, 4,
	8, 8,
	4, 4, // 4x MSAA
	0, 0
};

static void
_cairo_gpu_surface__set_size(void* abstract_surface, cairo_content_t content, unsigned width, unsigned height)
{
	cairo_gpu_surface_t* surface = abstract_surface;

	if(width == surface->width && height == surface->height && width && height)
		return;

	if(width < 1)
		width = 1;
	if(height < 1)
		height = 1;

	surface->width = width;
	surface->height = height;
	surface->base.content = content;

	_cairo_gpu_texture_init(surface->space, &surface->texture, width, height);
}

static inline void
_cairo_gpu_surface__init(cairo_gpu_surface_t* surface, cairo_gpu_space_t* space)
{
	surface->width = ~0;
	surface->height = ~0;
}

static cairo_gpu_surface_t *
_cairo_gpu_surface__create(cairo_gpu_space_t* space)
{
	cairo_gpu_surface_t *surface;

	surface = calloc(1, sizeof(cairo_gpu_surface_t));
	if(unlikely(surface == NULL))
		return (cairo_gpu_surface_t*)_cairo_surface_create_in_error(_cairo_error(CAIRO_STATUS_NO_MEMORY));

	_cairo_surface_init(&surface->base, &_cairo_gpu_surface_backend, 0);
	_cairo_gpu_surface__init(surface, space);

	return surface;
}

static void
_cairo_gpu_surface__fini(cairo_gpu_surface_t* surface)
{
	cairo_gpu_context_t* ctx = _cairo_gpu_space_bind(surface->space);

	_cairo_gpu_surface__msaa_fini(ctx, surface);

	if(surface->fb)
	{
		ctx->gl.DeleteFramebuffersEXT(1, &surface->fb);
		surface->fb = 0;
	}

	_cairo_gpu_texture_fini(ctx, &surface->texture);

	surface->valid_mask &=~ ((1 << SURF_TEX) | (1 << SURF_MSAA));
}

static void
_cairo_gpu_surface__unset_drawable(cairo_gpu_surface_t* surface)
{
	API_SURFACE(unset_drawable)(surface);
	surface->has_drawable = 0;
	surface->public_drawable = 0;
}


static cairo_status_t
_cairo_gpu_surface_finish(void *abstract_surface)
{
	cairo_gpu_surface_t *surface = abstract_surface;

	_cairo_gpu_enter();

	_cairo_gpu_surface__unset_drawable(surface);
	_cairo_gpu_surface__fini(surface);

	if(surface->space)
	{
		cairo_space_destroy(&surface->space->base);
		surface->space = 0;
	}

	_cairo_gpu_exit();

	return CAIRO_STATUS_SUCCESS;
}

static cairo_status_t
_cairo_gpu_surface__set_drawable(void* abstract_surface, cairo_content_t content, unsigned long drawable, unsigned long visualid, cairo_bool_t double_buffer, double width, double height)
{
	cairo_int_status_t status;
	cairo_gpu_surface_t* surface = abstract_surface;

	if(!drawable)
		return CAIRO_STATUS_NO_MEMORY;

	status = CAIRO_STATUS_SURFACE_TYPE_MISMATCH;
	API_SURFACE_(status, set_drawable_visualid)(surface, content, drawable, visualid, double_buffer, width, height);
	return status;
}

static cairo_status_t
_cairo_gpu_surface__set_offscreen(void* abstract_surface, cairo_content_t content, double width, double height, int color_mantissa_bits, int alpha_mantissa_bits, int exponent_bits, int shared_exponent_bits)
{
	cairo_gpu_surface_t * surface = abstract_surface;
	cairo_status_t status = CAIRO_STATUS_SUCCESS;

	API_SURFACE_(status, set_offscreen)(surface, content, width, height, color_mantissa_bits, alpha_mantissa_bits, exponent_bits, shared_exponent_bits);

	return status;
}

static cairo_surface_t *
_cairo_gpu_surface_create(void *abstract_space, void* abstract_surface, cairo_content_t content,
				    unsigned long drawable, unsigned long visualid, cairo_bool_t double_buffered,
				    double width, double height, int color_mantissa_bits, int alpha_mantissa_bits, int exponent_bits, int shared_exponent_bits)
{
	cairo_gpu_space_t* space = abstract_space;
	cairo_gpu_surface_t* surface = abstract_surface;
	cairo_bool_t same_size = 0;
	unsigned long surf_drawable = 0;

	if(surface && surface->base.backend != &_cairo_gpu_surface_backend)
		return _cairo_surface_create_in_error(CAIRO_STATUS_SURFACE_TYPE_MISMATCH);

	if(!space)
	{
		if(!surface)
			return _cairo_surface_create_in_error(CAIRO_STATUS_SURFACE_FINISHED);

		space = surface->space;
	}

	if(drawable == ~0UL)
	{
		drawable = 0;
		if(surface)
		{
			API_SURFACE_(drawable, get_drawable)(surface);
		}
	}

	if(width < 0)
	{
		if(!surface)
			return _cairo_surface_create_in_error(CAIRO_STATUS_SURFACE_FINISHED);
		width = surface->width;
	}

	if(height < 0)
	{
		if(!surface)
			return _cairo_surface_create_in_error(CAIRO_STATUS_SURFACE_FINISHED);
		height = surface->height;
	}

	_cairo_gpu_enter();
	if(space && surface && space != surface->space)
	{
		_cairo_gpu_surface__fini(surface);
		_cairo_gpu_surface__init(surface, space);
	}
	else if(!surface)
		surface = _cairo_gpu_surface__create(space);
	else
	{
		if(width != (int)surface->width || height != (int)surface->height)
			_cairo_gpu_surface__fini(surface);
		else
			same_size = 1;

		surf_drawable = 0;
		API_SURFACE_(surf_drawable, get_drawable)(surface);

		if(surf_drawable != drawable)
			_cairo_gpu_surface__unset_drawable(surface);
	}

	if(surface->space)
	{
		cairo_space_destroy(&surface->space->base);
		surface->space = 0;
	}
	surface->space = (cairo_gpu_space_t*)cairo_space_reference(&space->base);

	if(drawable)
	{
		if(surf_drawable != drawable)
			_cairo_gpu_surface__set_drawable(surface, content, drawable, visualid, double_buffered, width, height);

		// set fully dirty
		surface->bbox[SURF_FRONT].x = 0;
		surface->bbox[SURF_FRONT].y = 0;
		surface->bbox[SURF_FRONT].width = width;
		surface->bbox[SURF_FRONT].height = height;
		memcpy(&surface->bbox[SURF_BACK], &surface->bbox[SURF_FRONT], sizeof(surface->bbox[SURF_BACK]));
	}
	else
	{
		/* TODO: we currently force 8-bit formats, but we should stop this.
		 * If we fix this, change dst_alpha stealing to ensure it has enough alpha bits.
		 */

		if(content == CAIRO_CONTENT_ALPHA)
			color_mantissa_bits = 0;
		else //if(!color_mantissa_bits)
			color_mantissa_bits = 8;

		// we always need at least one bit
		if(content == CAIRO_CONTENT_COLOR)
			alpha_mantissa_bits = 1;
		else //if(!alpha_mantissa_bits)
			alpha_mantissa_bits = 8;

		_cairo_gpu_surface__set_offscreen(surface, content, width, height, color_mantissa_bits, alpha_mantissa_bits, exponent_bits, shared_exponent_bits);
	}

	if(!same_size)
	{
		_cairo_gpu_surface__set_size(surface, content, width, height);
		if(surface->color_samples)
			_cairo_gpu_surface__msaa_realize(surface, surface->coverage_samples, surface->color_samples);
	}

	_cairo_gpu_exit();

	return &surface->base;
}

static cairo_int_status_t
_cairo_gpu_surface_copy_page(void* abstract_surface)
{
	cairo_gpu_surface_t * surface = abstract_surface;
	if(!surface->draw_to_back_buffer || (surface->valid_mask & (1 << SURF_FRONT)))
		return CAIRO_STATUS_SUCCESS;

	_cairo_gpu_enter();

	_cairo_gpu_surface__bind(surface, 0, SURF_BACK);

	memcpy(&surface->bbox[SURF_FRONT], &surface->bbox[SURF_BACK], sizeof(surface->bbox[SURF_BACK]));
	surface->valid_mask &=~ (1 << SURF_BACK);
	surface->valid_mask = 1 << SURF_FRONT;

	// OpenGL defines the back buffer as becoming undefined
	surface->bbox[SURF_BACK].x = 0;
	surface->bbox[SURF_BACK].y = 0;
	surface->bbox[SURF_BACK].width = surface->width;
	surface->bbox[SURF_BACK].height = surface->height;

	API_SURFACE(swap_buffers)(surface);

	_cairo_gpu_exit();
	return CAIRO_STATUS_SUCCESS;
}

static cairo_status_t
_cairo_gpu_surface_get_image(cairo_gpu_surface_t * surface, cairo_rectangle_int_t * interest, cairo_image_surface_t ** image_out, cairo_rectangle_int_t * rect_out, void** extra)
{
	cairo_image_surface_t* image;
	cairo_rectangle_int_t extents;
	GLenum err;
	unsigned char *data;
	int y;
	unsigned int cpp;
	unsigned stride, dheight;
	int flip = 0;
	pixman_format_code_t pixman_format;
	cairo_gpu_context_t* ctx;
	GLenum format, type;
	int idx = _cairo_gpu_surface_destination(surface);

	extents.x = 0;
	extents.y = 0;
	extents.width = surface->width;
	extents.height = surface->height;

	if(interest != NULL)
	{
		if(!_cairo_rectangle_intersect(&extents, interest))
		{
			*image_out = NULL;
			return CAIRO_STATUS_SUCCESS;
		}
	}

	if(rect_out != NULL)
		*rect_out = extents;

	/* Want to use a switch statement here but the compiler gets whiny. */
	if(surface->base.content == CAIRO_CONTENT_COLOR_ALPHA)
	{
		format = GL_BGRA;
		pixman_format = PIXMAN_a8r8g8b8;
		type = GL_UNSIGNED_INT_8_8_8_8_REV;
		cpp = 4;
	}
	else if(surface->base.content == CAIRO_CONTENT_COLOR)
	{
		format = GL_BGRA;
		pixman_format = PIXMAN_x8r8g8b8;
		type = GL_UNSIGNED_INT_8_8_8_8_REV;
		cpp = 4;
	}
	else if(surface->base.content == CAIRO_CONTENT_ALPHA)
	{
		format = GL_ALPHA;
		pixman_format = PIXMAN_a8;
		type = GL_UNSIGNED_BYTE;
		cpp = 1;
	}
	else
	{
		fprintf(stderr, "get_image fallback: %d\n", surface->base.content);
		return CAIRO_INT_STATUS_UNSUPPORTED;
	}

//	if((surface->valid_mask & ((1 << idx) | (1 << SURF_TEX))) == (1 << SURF_TEX)
//			&&

	// avoid ctx->gl.GetTexImage for partial reads
	/*
	if((surface->valid_mask & (1 << SURF_TEX))
			&& !extents.x && !extents.y && extents.width == (int)surface->width && extents.height == (int)surface->height
			&& surface->texture.width == surface->width && surface->texture.height == surface->height)
	*/

	/* avoid this, as it seems to not work on bound framebuffers on nVidia */
	if(0)
	{
		stride = MAX(extents.width, (int)surface->texture.width) * cpp;
		dheight = MAX(extents.height, (int)surface->texture.height);

		data = malloc(dheight * stride * cpp);
		if(data == NULL)
			return CAIRO_STATUS_NO_MEMORY;

		if(surface->texture.non_upside_down)
			flip = 1;

		ctx = _cairo_gpu_space_bind(surface->space);
		_cairo_gl_context_set_active_texture(ctx, _cairo_gl_context_set_texture(ctx, -1, &surface->texture));
		ctx->gl.PixelStorei(GL_PACK_ALIGNMENT, 1);
		ctx->gl.GetTexImage(_cairo_gl_context_active_target(ctx), 0, format, type, data);

		if(rect_out)
		{
			rect_out->x = 0;
			rect_out->y = 0;
			rect_out->width = surface->width;
			rect_out->height = surface->height;
		}
	}
	else
	{
		if(
				(idx == SURF_FRONT || idx == SURF_BACK)
				&& surface->buffer_non_upside_down
				)
			flip = 1;

		stride = extents.width * cpp;
		dheight = extents.height;

		data = malloc(dheight * stride * cpp);
		if(data == NULL)
			return CAIRO_STATUS_NO_MEMORY;

		ctx = _cairo_gpu_surface_bind(surface, FB_READ);

		ctx->gl.PixelStorei(GL_PACK_ALIGNMENT, 1);
		ctx->gl.ReadPixels(extents.x, extents.y, extents.width, extents.height, format, type, data);
	}

	if(flip)
	{
		unsigned char* data2 = malloc(extents.width * extents.height * cpp);
		for(y = 0; y < extents.height; y++)
			memcpy((char *)data2 + (extents.height - y - 1) * stride, data + y * extents.width * cpp, extents.width * cpp);

		free(data);
		stride = extents.width;
		dheight = extents.height;
		data = data2;
	}

	image = (cairo_image_surface_t*)_cairo_image_surface_create_with_pixman_format (data, pixman_format, extents.width, extents.height, stride);
	if(image->base.status)
		return image->base.status;

	_cairo_image_surface_assume_ownership_of_data(image);

	*image_out = image;

	while((err = ctx->gl.GetError()))
		fprintf(stderr, "GL error 0x%08x\n", (int)err);

	return CAIRO_STATUS_SUCCESS;
}

static inline void
_cairo_gpu_surface_modified(cairo_gpu_surface_t* surface, int x, int y, int width, int height);

static inline void
_cairo_gpu_surface_modified_(cairo_gpu_surface_t* surface, int idx, int x, int y, int width, int height);

static cairo_status_t
_cairo_gpu_surface_draw_image(cairo_gpu_surface_t* dst, cairo_image_surface_t * src, int src_x, int src_y, int width, int height, int dst_x, int dst_y)
{
	cairo_gpu_context_t* ctx;
	cairo_gpu_space_tls_t* tls;
	int idx = _cairo_gpu_surface_destination(dst);
	int mask = ((1 << SURF_TEX) | (1 << idx));
	int valid = (dst->valid_mask & mask);

	tls = _cairo_gpu_space_get_tls(dst->space);

	if(dst_x == 0 && dst_y == 0 && width == (int)dst->width && height == (int)dst->height)
		dst->valid_mask |= 1 << SURF_TEX;

	if(!dst->texture.non_upside_down && (idx == SURF_TEX ||
		((!valid || valid == mask) && tls->in_acquire) ||
		(valid == (1 << SURF_TEX))
	))
	{
		ctx = _cairo_gpu_surface_bind_tex_update(dst);
		_cairo_gpu_context_upload_pixels(ctx, dst, -1, &dst->texture, src, src_x, src_y, width, height, dst_x, dst_y);
		_cairo_gpu_surface_modified_(dst, SURF_TEX, dst_x, dst_y, width, height);
	}
	else
	{
		ctx = _cairo_gpu_surface_bind(dst, FB_DRAW);
		_cairo_gpu_context_blit_pixels(ctx, dst, src, dst_x, dst_y, src_x, src_y, width, height);
		_cairo_gpu_surface_modified(dst, dst_x, dst_y, width, height);
	}

	return CAIRO_STATUS_SUCCESS;
}

static cairo_status_t
_cairo_gpu_surface_put_image(cairo_gpu_surface_t* dst, cairo_image_surface_t * src, int src_x, int src_y, int width, int height, int dst_x, int dst_y, int is_write, void* extra)
{
	if(!is_write)
		return CAIRO_STATUS_SUCCESS;

	return _cairo_gpu_surface_draw_image(dst, src, src_x, src_y, width, height, dst_x, dst_y);
}

unsigned
cairo_gl_surface_get_texture(cairo_surface_t * abstract_surface)
{
	cairo_gpu_surface_t* surface = (cairo_gpu_surface_t*)abstract_surface;
	if(abstract_surface->backend != &_cairo_gpu_surface_backend)
		return 0;

	if(!surface->texture.tex)
	{
		cairo_gpu_context_t* ctx;
		_cairo_gpu_enter();
		ctx = _cairo_gpu_space_bind(surface->space);
		_cairo_gpu_surface__create_tex(ctx, surface);
		_cairo_gpu_exit();
	}

	return surface->texture.tex;
}

unsigned
cairo_gl_surface_get_framebuffer(cairo_surface_t * abstract_surface)
{
	cairo_gpu_surface_t* surface = (cairo_gpu_surface_t*)abstract_surface;
	if(abstract_surface->backend != &_cairo_gpu_surface_backend)
		return 0;

	if(_cairo_gpu_surface_use_msaa(surface))
		return surface->msaa_fb;

	if(!surface->fb && surface->space->use_fbo)
	{
		cairo_gpu_context_t* ctx;
		_cairo_gpu_enter();
		ctx = _cairo_gpu_space_bind(surface->space);
		_cairo_gpu_surface__create_tex_fb(ctx, surface);
		_cairo_gpu_exit();
	}

	return surface->fb;
}

unsigned
cairo_gl_surface_get_renderbuffer(cairo_surface_t * abstract_surface)
{
	cairo_gpu_surface_t* surface = (cairo_gpu_surface_t*)abstract_surface;
	if(abstract_surface->backend != &_cairo_gpu_surface_backend)
		return 0;

	if(_cairo_gpu_surface_use_msaa(surface))
		return surface->msaa_rb;

	return 0;
}

unsigned long
cairo_gl_surface_get_drawable(cairo_surface_t * abstract_surface)
{
	cairo_gpu_surface_t* surface = (cairo_gpu_surface_t*)abstract_surface;
	unsigned long drawable = 0;
	if(abstract_surface->backend != &_cairo_gpu_surface_backend)
		return 0;

	API_SURFACE_(drawable, get_drawable)(surface);
	return drawable;
}

unsigned long
cairo_gl_surface_get_gl_drawable(cairo_surface_t * abstract_surface)
{
	cairo_gpu_surface_t* surface = (cairo_gpu_surface_t*)abstract_surface;
	unsigned long drawable = 0;
	if(abstract_surface->backend != &_cairo_gpu_surface_backend)
		return 0;

	API_SURFACE_(drawable, get_gl_drawable)(surface);
	return drawable;
}

static inline void
_cairo_gpu_surface_orient_drawable_like_texture(cairo_gpu_surface_t* draw_surf, cairo_gpu_surface_t* tex_surf)
{
	if(draw_surf->buffer_non_upside_down != tex_surf->texture.non_upside_down)
		_cairo_gpu_surface__flip_drawable(draw_surf);
}
