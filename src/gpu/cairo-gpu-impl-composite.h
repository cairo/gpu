#define A 2
#define ALPHA 2
static struct
{
	char src;
	char dst;
} _cairo_gpu_blend_factors[] =
{
	{0,		0},	/* Clear */
	{1,		0},	/* Source */
	{1,		1 | A},	/* Over */
	{A,		0},	/* In */
	{1 | A,	0},	/* Out */
	{A,		1 | A},	/* Atop */
	{0,		1},	/* Dest */
	{1 | A,	1},	/* DestOver */
	{0,		A},	/* DestIn */
	{0,		1 | A},	/* DestOut */
	{1 | A,	A},	/* DestAtop */
	{1 | A,	1 | A},	/* Xor */
	{1,		1},	/* Add */
	{1 | A,	1} /* Saturate */
};
#undef A

static inline cairo_status_t
_cairo_gpu_composite__do_init(cairo_gpu_composite_setup_t * setup, cairo_operator_t op)
{
#ifdef DEBUG_FORCE_SOURCE
	op = CAIRO_OPERATOR_SOURCE;
#endif

#ifdef DEBUG_FORCE_ADD
	op = CAIRO_OPERATOR_ADD;
#endif

	if(op > CAIRO_OPERATOR_ADD)
	{
		// either SATURATE or invalid operator
		if(op > CAIRO_OPERATOR_SATURATE)
			return CAIRO_INT_STATUS_UNSUPPORTED;

		setup->saturate = 1;
		setup->unpremultiplied = setup->dst->base.content != CAIRO_CONTENT_ALPHA;
	}
	else
		setup->saturate = setup->unpremultiplied = 0;

	setup->primary_chan = 0;
	setup->constant_chan = 0;
	setup->operands_chan = 0;
	setup->c.r = setup->c.g = setup->c.b = setup->a.r = setup->a.g = setup->a.b = setup->ka = 1.0;

	setup->blend_dst = _cairo_gpu_blend_factors[op].dst;
	setup->blend_src = _cairo_gpu_blend_factors[op].src;

	setup->smooth = 0;
	setup->dst_alpha_mask = 0;

	memset(setup->operands, 0, sizeof(setup->operands));

	return CAIRO_STATUS_SUCCESS;
}

/* dst_x and dst_y are the eye coords corresponding to object coords (0,0) */
static inline cairo_status_t
_cairo_gpu_composite_init(cairo_gpu_composite_setup_t * setup, cairo_operator_t op, cairo_gpu_surface_t * dst, int dst_x, int dst_y,  int obj_x, int obj_y, int width, int height)
{
	setup->dst = dst;
	setup->dst_x = dst_x;
	setup->dst_y = dst_y;
	setup->obj_x = obj_x;
	setup->obj_y = obj_y;
	setup->width = width;
	setup->height = height;

	return _cairo_gpu_composite__do_init(setup, op);
}

static inline cairo_status_t
_cairo_gpu_composite_init_smooth(cairo_gpu_composite_setup_t * setup, cairo_operator_t op, cairo_gpu_surface_t * dst, int dst_x, int dst_y,  int obj_x, int obj_y, int width, int height, int smooth, int dst_alpha_mask)
{
	cairo_status_t status;
	status = _cairo_gpu_composite_init(setup, op, dst, dst_x, dst_y,  obj_x, obj_y, width, height);
	if(status)
		return status;

	if(smooth && setup->blend_src && setup->dst->base.content != CAIRO_CONTENT_ALPHA)
		setup->unpremultiplied = 1;
	setup->smooth = smooth;
	setup->dst_alpha_mask = dst_alpha_mask;
	return CAIRO_STATUS_SUCCESS;
}


/* src_x and src_y are the texture coords corresponding to object coords (0,0) */
static cairo_status_t
_cairo_gpu_composite_operand(cairo_gpu_composite_setup_t * setup, const cairo_pattern_t * pattern, int src_x, int src_y, cairo_bool_t is_mask, int coords)
{
	cairo_solid_pattern_t *solid = (cairo_solid_pattern_t *) pattern;
	cairo_gradient_pattern_t* gradient = (cairo_gradient_pattern_t*)pattern;
	cairo_linear_pattern_t* linear = (cairo_linear_pattern_t*)gradient;
	cairo_radial_pattern_t* radial = (cairo_radial_pattern_t*)gradient;
	const cairo_color_t* color;
	cairo_gpu_composite_operand_t* operand;

	unsigned i;
	int chan = 0;
	cairo_bool_t multiple = 0;
	unsigned width = 0;
	unsigned denom = 0;

	i = !!setup->operands[0].src;
	operand = &setup->operands[i];

	switch (pattern->type)
	{
	case CAIRO_PATTERN_TYPE_LINEAR:
	case CAIRO_PATTERN_TYPE_RADIAL:
		for (i = 1; i < gradient->n_stops; i++)
		{
			if (! _cairo_color_equal (&gradient->stops[0].color, &gradient->stops[i].color))
			{
				break;
			}
		}

		// TODO: check for degenerate radial
		if(i >= gradient->n_stops ||
				((pattern->type == CAIRO_PATTERN_TYPE_LINEAR)
					&& linear->p1.x == linear->p2.x
					&& linear->p1.y == linear->p2.y
				) ||
				((pattern->type == CAIRO_PATTERN_TYPE_RADIAL)
					&& radial->c1.x == radial->c2.x
					&& radial->c1.y == radial->c2.y
					&& radial->r1 == radial->r2
				)
				)
		{
			if(gradient->n_stops && gradient->base.extend != CAIRO_EXTEND_NONE)
				color = &gradient->stops[0].color;
			else
				color = CAIRO_COLOR_TRANSPARENT;
			goto solid;
		}

		// TODO: use vertex programs to setup colors for 2-stop gradients
		for (i = 0; i < gradient->n_stops; i++)
		{
			if(gradient->stops[i].color.alpha != 1.0)
				chan |= CHAN_KA;
			if(gradient->stops[i].color.red != 1.0 || gradient->stops[i].color.green != 1.0 || gradient->stops[i].color.blue != 1.0)
				chan |= CHAN_C | CHAN_A;
		}

		if(setup->unpremultiplied)
			operand->unpremultiplied = 1;
		else if((operand->chan & CHAN_KA) && (operand->chan & ~CHAN_KA))
		{
			unsigned i;
			/* We interpolate premultiplied coordinates on the GPU, but we should interpolate non-premultiplied ones.
			 * So we only do it on the GPU if there is no difference.
			 */
			for(i = 1; i < gradient->n_stops; ++i)
			{
				if(gradient->stops[i - 1].color.alpha != gradient->stops[i].color.alpha)
					break;
			}

			if(i < gradient->n_stops)
			{
				for(i = 1; i < gradient->n_stops; ++i)
				{
					if((gradient->stops[i - 1].color.red != gradient->stops[i].color.red) ||
							(gradient->stops[i - 1].color.green != gradient->stops[i].color.green) ||
							(gradient->stops[i - 1].color.blue != gradient->stops[i].color.blue)
							)
						 break;
				}

				if(i < gradient->n_stops)
				{
					if(!setup->dst->space->frag_mul_alpha)
					{
						goto nogradient;
					}
					else
						operand->unpremultiplied = 1;
				}
			}
		}

		denom = _cairo_gradient_pattern_compute_stops_common_denominator(gradient, setup->dst->space->discontinuous ? &multiple : 0, 65536);
nogradient:
		if(gradient->base.extend == CAIRO_EXTEND_NONE || gradient->base.extend == CAIRO_EXTEND_PAD)
		{
			width = denom + 1;
			if(!setup->dst->space->tex_npot && !is_pow2(width))
				width = higher_pow2(width);
		}
		else
		{
			if(!setup->dst->space->tex_npot && !is_pow2(denom))
				denom = 0;
			else if(gradient->base.extend == CAIRO_EXTEND_REPEAT)
				width = denom;
			else if(gradient->base.extend == CAIRO_EXTEND_REFLECT)
				width = denom * 2;
			else
				abort();
		}

		if(setup->unpremultiplied && !setup->dst->space->frag_mul_alpha && !denom)
			return CAIRO_INT_STATUS_UNSUPPORTED;

		//printf("denominator is %u multiple is %i extend is %i\n", denom, multiple, gradient->base.extend);
		break;
	case CAIRO_PATTERN_TYPE_SOLID:
		color = &solid->color;
	solid:
		if(is_mask && !pattern->component_alpha)
		{
			setup->c.r *= color->alpha;
			setup->c.g *= color->alpha;
			setup->c.b *= color->alpha;
		}
		else
		{
			setup->c.r *= color->red * color->alpha;
			setup->c.g *= color->green * color->alpha;
			setup->c.b *= color->blue * color->alpha;
		}

		if(is_mask && pattern->component_alpha)
		{
			setup->a.r *= color->red * color->alpha;
			setup->a.g *= color->green * color->alpha;
			setup->a.b *= color->blue * color->alpha;
		}
		else
		{
			setup->a.r *= color->alpha;
			setup->a.g *= color->alpha;
			setup->a.b *= color->alpha;
		}

		setup->ka *= color->alpha;
		return CAIRO_STATUS_SUCCESS;
	default:
	case CAIRO_PATTERN_TYPE_SURFACE:
		if(((cairo_surface_pattern_t*)pattern)->surface->content == CAIRO_CONTENT_ALPHA)
			chan = CHAN_C | CHAN_KA; // CHAN_C is because alpha-only surfaces are black...
		else if(((cairo_surface_pattern_t*)pattern)->surface->content == CAIRO_CONTENT_COLOR)
			chan = CHAN_C | CHAN_A;
		else
			chan = CHAN_C | CHAN_A | CHAN_KA;
		break;
	}

	if(is_mask)
	{
		if(pattern->component_alpha)
			chan &=~ CHAN_C;
		else
			chan &=~ (CHAN_C | CHAN_A);
	}
	else
		chan &=~ CHAN_A;

	if(pattern->type == CAIRO_PATTERN_TYPE_SURFACE)
	{
		if(setup->unpremultiplied)
		{
			if((chan & (CHAN_C | CHAN_A)))
			{
				// TODO: maybe unpremultiply in software
				if(!setup->dst->space->frag_div_alpha)
					return CAIRO_INT_STATUS_UNSUPPORTED;
			}
			else
			{
				// TODO: clone to GL_ALPHA
				if(!setup->dst->space->tex_aaaa_111a)
					return CAIRO_INT_STATUS_UNSUPPORTED;
			}
		}
		else
		{
			// TODO: clone to GL_INTENSITY
			if( (((cairo_surface_pattern_t*)pattern)->surface->content != CAIRO_CONTENT_ALPHA)
					&& !(chan & (CHAN_C | CHAN_A)) && !setup->dst->space->tex_aaaa_111a)
				return CAIRO_INT_STATUS_UNSUPPORTED;
		}
	}

	/* The extended area is transparent and thus has != 1 alpha */
	if(pattern->extend == CAIRO_EXTEND_NONE)
		chan |= CHAN_KA;

	operand->surface = 0;
	operand->chan = chan;
	operand->has_coords = coords >= 0;
	operand->src = (cairo_pattern_t*)pattern;
	operand->src_x = src_x;
	operand->src_y = src_y;
	operand->gradient_width = width;
	operand->gradient_denominator = denom;
	operand->gradient_discontinuous = multiple;

	return CAIRO_STATUS_SUCCESS;
}

static inline void
_cairo_gpu_prepare_color(float* p, cairo_color_t* color, int unpremultiplied)
{
	if(unpremultiplied)
	{
		p[0] = (float)(color->red);
		p[1] = (float)(color->green);
		p[2] = (float)(color->blue);
	}
	else
	{
		p[0] = (float)(color->red * color->alpha);
		p[1] = (float)(color->green * color->alpha);
		p[2] = (float)(color->blue * color->alpha);
	}
	p[3] = color->alpha;
}

static inline void
_cairo_gpu_composite_get_constant_color(cairo_gpu_composite_setup_t* setup, cairo_gpu_color4_t* p)
{
	cairo_gpu_color4_t u;
	u.c = setup->c;
	u.ka = setup->ka;

	if(setup->unpremultiplied)
	{
		if(setup->a.r)
			u.c.r /= setup->a.r;

		if(setup->a.g)
			u.c.g /= setup->a.g;

		if(setup->a.b)
			u.c.b /= setup->a.b;
	}

	*p = u;
}

static void _cairo_gpu_acquire_repeat_reflect(cairo_gpu_surface_t** psurface, cairo_surface_attributes_t* attrs, int x, int y, unsigned width, unsigned height, int* x_off, int* y_off)
{
	cairo_gpu_surface_t* surface = *psurface;
	cairo_gpu_surface_t* dst;
	cairo_gpu_context_t* ctx;
	cairo_gpu_geometry_t* geometry;
	double x1, y1, x2, y2;
	int rx, ry, mx, my, flipx, oflipx, flipy;
	cairo_rectangle_int_t sampled_area;
	double pad;
	int quads;
	float* v;
	float* vertices;
	cairo_surface_attributes_t tex_attrs;
	cairo_gpu_texture_t* texture;

	x1 = x + attrs->x_offset;
	y1 = y + attrs->y_offset;
	x2 = x + (int) width;
	y2 = y + (int) height;

	_cairo_matrix_transform_bounding_box (&attrs->matrix,
					      &x1, &y1, &x2, &y2,
					      NULL);

	pad = (attrs->filter == CAIRO_FILTER_GOOD || attrs->filter == CAIRO_FILTER_BEST || attrs->filter == CAIRO_FILTER_BILINEAR) ? 0.5 : 0.0;

	sampled_area.x = floor (x1 - pad);
	sampled_area.y = floor (y1 - pad);
	sampled_area.width  = ceil (x2 + pad) - sampled_area.x;
	sampled_area.height = ceil (y2 + pad) - sampled_area.y;

	dst = (cairo_gpu_surface_t*)_cairo_gpu_surface_create_similar(surface, surface->base.content, sampled_area.width, sampled_area.height);

	dst->base.device_transform = surface->base.device_transform;
	dst->base.device_transform_inverse = surface->base.device_transform_inverse;

	ctx = _cairo_gpu_surface_lookup_context(surface, FB_DRAW);
	texture = _cairo_gpu_surface_begin_texture(surface, ctx, 0);
	_cairo_gpu_surface_bind_to(surface, ctx, FB_DRAW);

	_cairo_gpu_context_set_viewport(ctx, 0, 0, dst->width, dst->height);
	_cairo_gpu_context_set_translation(ctx, -sampled_area.x, -sampled_area.y);

	_cairo_gpu_context_set_vert_frag(ctx, 1 << VERT_TEX_SHIFT,
			(((texture->target_idx == TARGET_RECTANGLE) ? FRAG_TEX_RECTANGLE : 0) | FRAG_TEX_COLOR_RGBA) << FRAG_TEX_SHIFT);

	cairo_matrix_init_identity(&tex_attrs.matrix);
	_cairo_gpu_texture_adjust_matrix(texture, &tex_attrs.matrix);
	tex_attrs.extend = CAIRO_EXTEND_NONE;
	tex_attrs.filter = CAIRO_FILTER_NEAREST;
	tex_attrs.extra = 0;
	_cairo_gpu_context_set_texture_and_attributes(ctx, 0, texture, &tex_attrs);

	_cairo_gpu_context_set_blend(ctx, (surface->base.content == CAIRO_CONTENT_COLOR_ALPHA) ? BLEND_SOURCE :
		((surface->base.content == CAIRO_CONTENT_COLOR) ? BLEND_SOURCE_COLORONLY : BLEND_SOURCE_ALPHAONLY));

	_cairo_gpu_context_set_raster(ctx, 0);

	geometry = _cairo_gpu_context_geometry(ctx, GEOM_TEMP);

	rx = sampled_area.x;
	mx = rx % (int)surface->width;
	oflipx = (rx / (int)surface->width) & 1;
	if(mx < 0)
	{
	     mx += surface->width;
	     oflipx ^= 1;
	}

	ry = sampled_area.y;
	my = ry % (int)surface->height;
	flipy = (ry / (int)surface->height) & 1;
	if(my < 0)
	{
	     my += surface->height;
	     flipy ^= 1;
	}

	quads = ((sampled_area.width + mx + surface->width - 1) / surface->width)
		* ((sampled_area.height + my + surface->height - 1) / surface->height);
	v = vertices = _cairo_gpu_geometry_begin(ctx, geometry, PRIM_QUADS, quads * 4, 2, 0, 2);

	for(ry = sampled_area.y - my; ry < (sampled_area.y + sampled_area.height); ry += surface->height, flipy ^= 1)
	{
	     for(rx = sampled_area.x - mx, flipx = oflipx; rx < (sampled_area.x + sampled_area.width); rx += surface->width, flipx ^= 1)
	     {
		     int dx = MAX(sampled_area.x - rx, 0);
		     int dy = MAX(sampled_area.y - ry, 0);
		     int w = MIN((int)surface->width, sampled_area.x + sampled_area.width - rx) - dx;
		     int h = MIN((int)surface->height, sampled_area.y + sampled_area.height - ry) - dy;
		     int tx = dx;
		     int ty = dy;
		     int tw = w;
		     int th = h;
		     if(attrs->extend == CAIRO_EXTEND_REFLECT)
		     {
			     if(flipx)
			     {
				     tx = surface->width - tx;
				     tw = -tw;
			     }
			     if(flipy)
			     {
				     ty = surface->height - ty;
				     th = -th;
			     }
		     }

		     _cairo_gpu_emit_rect_tex_(&v, rx + dx, ry + dy, tx, ty, w, h, tw, th);
	     }
	}
	_cairo_gpu_geometry_end(ctx, geometry, quads * 4);
	_cairo_gpu_context_set_geometry(ctx, geometry);

	_cairo_gpu_context_draw(ctx);

	_cairo_gpu_geometry_put(ctx, geometry);

	_cairo_gpu_surface_modified(dst, 0, 0, sampled_area.width, sampled_area.height);
	_cairo_gpu_surface_end_texture(ctx, surface, texture);

	cairo_surface_destroy(&surface->base);
	*psurface = dst;

	attrs->extend = CAIRO_EXTEND_NONE;
	*x_off = -sampled_area.x;
	*y_off = -sampled_area.y;
}

#ifdef __SSE__
#include <xmmintrin.h>
#endif

static cairo_status_t _cairo_gpu_composite_prepare_operands(cairo_gpu_composite_setup_t* setup)
{
	int op;
	cairo_gpu_context_t* ctx;

	for(op = 0; op < MAX_OPERANDS; ++op)
	{
		cairo_matrix_t m;
		cairo_gpu_composite_operand_t* operand = &setup->operands[op];
		cairo_pattern_t* src = operand->src;
		cairo_surface_attributes_t* attributes = &operand->attributes;
		if(!(setup->operands[op].chan & setup->operands_chan))
			continue;

		attributes->extra = 0;
		attributes->extend = src->extend;
		attributes->filter = src->filter;
		attributes->x_offset = operand->src_x - setup->obj_x;
		attributes->y_offset = operand->src_y - setup->obj_y;

		attributes->matrix = src->matrix;

		// we do this lazily in setup_pass
		if(src->type == CAIRO_PATTERN_TYPE_SURFACE)
		{
			int tex_xdiv, tex_ydiv;
			cairo_surface_t* surface = ((cairo_surface_pattern_t*)src)->surface;
			cairo_gpu_texture_t* adjust_texture;
			if(!(setup->dst->space->extend_mask & (1 << attributes->extend)))
				goto acquire;

			if(_cairo_surface_is_image(surface))
			{
				cairo_image_surface_t* image_surface = (cairo_image_surface_t*)(((cairo_surface_pattern_t*)src)->surface);
				int width = image_surface->width;
				int height = image_surface->height;

				/* use general path for large image surfaces since it can acquire subrectangles */
				if(width > 32 || height > 32)
					goto acquire;

				tex_xdiv = width;
				tex_ydiv = height;
				if(!setup->dst->space->tex_npot && (!is_pow2(width) || !is_pow2(height)))
				{
					if(attributes->extend == CAIRO_EXTEND_REFLECT || attributes->extend == CAIRO_EXTEND_REPEAT)
						goto acquire;

					if(!setup->dst->space->tex_rectangle)
						goto acquire;

					tex_xdiv = 1;
					tex_ydiv = 1;
				}

				adjust_texture = operand->texture = (cairo_gpu_texture_t*)malloc(sizeof(cairo_gpu_texture_t));
				ctx = _cairo_gpu_space_bind(setup->dst->space);
				_cairo_gpu_texture_create(ctx, operand->texture, image_surface->width, image_surface->height);
				_cairo_gpu_context_upload_pixels(ctx, 0, op, operand->texture, image_surface, 0, 0, image_surface->width, image_surface->height, -1, -1);

				operand->owns_texture = 1;
			}
			else if(_cairo_gpu_surface_is_compatible(setup->dst, surface))
			{
				cairo_gpu_surface_t* surf = (cairo_gpu_surface_t*)surface;
				if((surf->texture.target_idx == TARGET_RECTANGLE || surf->texture.width != surf->width || surf->texture.height != surf->height) &&
						(attributes->extend == CAIRO_EXTEND_REFLECT || attributes->extend == CAIRO_EXTEND_REPEAT))
					goto acquire;

				if(surf->texture.width != surf->width || surf->texture.height != surf->height)
					_cairo_gpu_surface_fix_pad(surf, attributes->extend);

				operand->surface = surf;

				adjust_texture = &surf->texture;
			}
			else
				goto acquire;

			attributes->matrix.x0 += attributes->x_offset * attributes->matrix.xx + attributes->y_offset * attributes->matrix.xy;
			attributes->matrix.y0 += attributes->x_offset * attributes->matrix.yx + attributes->y_offset * attributes->matrix.yy;

			_cairo_gpu_texture_adjust_matrix(adjust_texture, &attributes->matrix);

			if(_cairo_matrix_is_pixel_exact(&attributes->matrix))
				attributes->filter = CAIRO_FILTER_NEAREST;
		}
		else if(src->type == CAIRO_PATTERN_TYPE_LINEAR
				|| (src->type == CAIRO_PATTERN_TYPE_RADIAL && setup->dst->space->radial))
		{
			cairo_gradient_pattern_t* gradient = (cairo_gradient_pattern_t*)src;
			unsigned denom, width, swidth;
			cairo_bool_t multiple;

#ifdef DEBUG_DISABLE_GRADIENTS
			goto acquire;
#endif

			multiple = operand->gradient_discontinuous;
			denom = operand->gradient_denominator;
			width = operand->gradient_width;
			if(!denom)
				goto acquire;

			// note that we never use texture rectangles here

			if(multiple)
				swidth = width * 2;
			else
				swidth = width;

			if(src->type == CAIRO_PATTERN_TYPE_LINEAR)
			{
				cairo_linear_pattern_t* linear = (cairo_linear_pattern_t*)gradient;
				double x0, y0, x1, y1, dx, dy;
				double l, n;

				if(linear->base.base.extend == CAIRO_EXTEND_NONE || linear->base.base.extend == CAIRO_EXTEND_PAD)
				{
					x0 = _cairo_fixed_to_double(linear->p1.x) * (1 - linear->base.stops[0].offset) + _cairo_fixed_to_double(linear->p2.x) * linear->base.stops[0].offset;
					y0 = _cairo_fixed_to_double(linear->p1.y) * (1 - linear->base.stops[0].offset) + _cairo_fixed_to_double(linear->p2.y) * linear->base.stops[0].offset;
					x1 = _cairo_fixed_to_double(linear->p1.x) * (1 - linear->base.stops[linear->base.n_stops - 1].offset) + _cairo_fixed_to_double(linear->p2.x) * linear->base.stops[linear->base.n_stops - 1].offset;
					y1 = _cairo_fixed_to_double(linear->p1.y) * (1 - linear->base.stops[linear->base.n_stops - 1].offset) + _cairo_fixed_to_double(linear->p2.y) * linear->base.stops[linear->base.n_stops - 1].offset;
				}
				else
				{
					x0 = _cairo_fixed_to_double(linear->p1.x);
					y0 = _cairo_fixed_to_double(linear->p1.y);
					x1 = _cairo_fixed_to_double(linear->p2.x);
					y1 = _cairo_fixed_to_double(linear->p2.y);
				}
				dx = x1 - x0;
				dy = y1 - y0;

				l = (dx * dx + dy * dy);

				if(l == 0.0)
					goto acquire;

				n = (double)denom / l;
				if(!multiple)
					n /= (double)swidth;

				attributes->matrix.x0 += attributes->x_offset * attributes->matrix.xx + attributes->y_offset * attributes->matrix.xy;
				attributes->matrix.y0 += attributes->x_offset * attributes->matrix.yx + attributes->y_offset * attributes->matrix.yy;

				m.xx = dx * n;
				m.xy = dy * n;
				m.x0 = (multiple ? 0.0 : 1.0 / (2 * swidth)) - (dx * x0 + dy * y0) * n;
				m.yx = 0;
				m.yy = 0;
				m.y0 = 0.5;
				cairo_matrix_multiply(&attributes->matrix, &attributes->matrix, &m);
			}
			else
			{
				// move (xc, yc) to 0 and yd to (sqrt((xd - xc)^2 + (yd - yc)^2), 0)
				cairo_radial_pattern_t* radial = (cairo_radial_pattern_t*)gradient;

				attributes->matrix.x0 += attributes->x_offset * attributes->matrix.xx + attributes->y_offset * attributes->matrix.xy
					- _cairo_fixed_to_double(radial->c1.x);
				attributes->matrix.y0 += attributes->x_offset * attributes->matrix.yx + attributes->y_offset * attributes->matrix.yy
					- _cairo_fixed_to_double(radial->c1.y);
			}

			{
				double off = 0.0;
				double scale = 1.0;
				int inc = 1;
				unsigned start = 0;
				unsigned prev_stop = 0;
				unsigned next_stop;
				float prev_color[4];
				float next_color[4];
				int stopi;
				unsigned x;
				float* p;
				float* data;

				ctx = _cairo_gpu_space_bind(setup->dst->space);

				if(gradient->base.extend == CAIRO_EXTEND_NONE || gradient->base.extend == CAIRO_EXTEND_PAD)
				{
					scale = 1.0 / (gradient->stops[gradient->n_stops - 1].offset - gradient->stops[0].offset);
					off = -gradient->stops[0].offset * scale;
				}

				next_stop = lround((gradient->stops[0].offset * scale + off) * denom);

				if(gradient->base.extend != CAIRO_EXTEND_NONE)
					_cairo_gpu_prepare_color(next_color, &gradient->stops[0].color, operand->unpremultiplied);
				else
				{
					assert(multiple);
					memset(next_color, 0, sizeof(next_color));
				}
				if(gradient->base.extend == CAIRO_EXTEND_REPEAT)
				{
					prev_stop = lround((gradient->stops[gradient->n_stops - 1].offset * scale + off) * denom) - denom;
					_cairo_gpu_prepare_color(prev_color, &gradient->stops[gradient->n_stops - 1].color, operand->unpremultiplied);
				}
				else if(gradient->base.extend == CAIRO_EXTEND_REFLECT)
				{
					prev_stop = -next_stop;
					memcpy(prev_color, next_color, sizeof(next_color));
				}

				{
					int owned;
					operand->texture = _cairo_gpu_temp_1d_image(ctx, op, &swidth, &owned);
					operand->owns_texture = owned;
				}

				p = data = alloca(swidth * sizeof(float) * 4);
				assert(width);
				//printf("denom is %u width is %u swidth is %u extend is %i\n", denom, width, swidth, gradient->base.extend);
				if(gradient->base.extend == CAIRO_EXTEND_PAD)
					assert(next_stop == 0);

				stopi = 0;
				for(;;)
				{
					unsigned end = next_stop;
					unsigned len;
					if(width < end)
						end = width;
					len = end - start;
					if(len > 0)
					{
						float c = 1.0 / (next_stop - prev_stop);

#ifdef __SSE__
// TODO: this will currently only be used for x86-64 or x86 with -msse: we should detect at runtime on i386
						__m128 cv = _mm_set1_ps(c);
						__m128 prev_b = _mm_set1_ps(next_stop - start);
						__m128 next_b = _mm_set1_ps(start - prev_stop);
						__m128 a = (*(__m128*)next_color - *(__m128*)prev_color) * cv;
						__m128 v = (prev_b * *(__m128*)prev_color + next_b * *(__m128*)next_color) * cv;

						if(!multiple)
						{
							__m128* pend = (__m128*)p + len;
							for(; (__m128*)p < pend; p += 4)
							{
								*(__m128*)p = v;
								v += a;
							}
						}
						else
						{
							__m128* pend = (__m128*)p + (len << 1);
							for(; (__m128*)p < pend; p += 8)
							{
								((__m128*)p)[0] = v;
								((__m128*)p)[1] = v;
								v += a;
							}
						}
#else
						float v[4];
						float a[4];

						v[0] = ((next_stop - start) * prev_color[0] + (start - prev_stop) * next_color[0]) * c;
						v[1] = ((next_stop - start) * prev_color[1] + (start - prev_stop) * next_color[1]) * c;
						v[2] = ((next_stop - start) * prev_color[2] + (start - prev_stop) * next_color[2]) * c;
						v[3] = ((next_stop - start) * prev_color[3] + (start - prev_stop) * next_color[3]) * c;
						a[0] = (next_color[0] - prev_color[0]) * c;
						a[1] = (next_color[1] - prev_color[1]) * c;
						a[2] = (next_color[2] - prev_color[2]) * c;
						a[3] = (next_color[3] - prev_color[3]) * c;

		//#define DO(i) p[i] = (((next_stop - x) * prev_color[i] + (x - prev_stop) * next_color[i]) / (next_stop - prev_stop))

		//#define DO(i) p[i] = (((next_stop - (end - x)) * prev_color[i] + ((end - x) - prev_stop) * next_color[i]) / (next_stop - prev_stop))
						if(!multiple)
						{
							float* pend = p + (len << 2);
							for(; p < pend; p += 4)
							{
								p[0] = v[0];
								p[1] = v[1];
								p[2] = v[2];
								p[3] = v[3];
								v[0] += a[0];
								v[1] += a[1];
								v[2] += a[2];
								v[3] += a[3];
							}
						}
						else
						{
							float* pend = p + (len << 3);
							for(; p < pend; p += 8)
							{
								p[0] = v[0];
								p[1] = v[1];
								p[2] = v[2];
								p[3] = v[3];
								p[4] = v[0];
								p[5] = v[1];
								p[6] = v[2];
								p[7] = v[3];
								v[0] += a[0];
								v[1] += a[1];
								v[2] += a[2];
								v[3] += a[3];
							}
						}
#endif
					}
					if(end == width)
						break;

					memcpy(p, next_color, sizeof(next_color));
					p += 4;

					for(;;)
					{
						prev_stop = next_stop;
						memcpy(prev_color, next_color, sizeof(next_color));

						if(stopi < 0)
						{
							stopi = 0;
							scale = 1;
						}
						else if(stopi == (int)gradient->n_stops)
						{
							if(gradient->base.extend == CAIRO_EXTEND_NONE)
							{
								unsigned len = width - (next_stop + 1);
								unsigned size = multiple ? (4 + len * 8) : len * 4;
								memset(p, 0, size);
								p += size;
								goto finish;
							}
							else if(gradient->base.extend == CAIRO_EXTEND_REPEAT)
							{
								stopi = 0;
								off += 1;
							}
							else if(gradient->base.extend == CAIRO_EXTEND_REFLECT)
							{
								--stopi;
								off += 2;
								scale = -scale;
								inc = -1;
							}
							else if(gradient->base.extend == CAIRO_EXTEND_PAD)
							{
								unsigned len = width - (next_stop + 1);
								if(multiple)
								{
									memcpy(p, next_color, sizeof(next_color));
									p += 4;
									len *= 2;
								}

								if(len > 0)
								{
									// this only happens for padded-to-power-of-two textures
									for(x = 0; x < len; ++x)
										memcpy(p + (x << 2), next_color, sizeof(next_color));
								}
								goto finish;
							}
							else
								assert(0);
						}

						//printf("stopi is %i / %i\n", stopi, gradient->n_stops);
						next_stop = lround((gradient->stops[stopi].offset * scale + off) * denom);
						_cairo_gpu_prepare_color(next_color, &gradient->stops[stopi].color, operand->unpremultiplied);
						stopi += inc;
						if(next_stop != prev_stop)
							break;
					}

					if(multiple)
					{
						memcpy(p, prev_color, sizeof(prev_color));
						p += 4;
					}

					start = prev_stop + 1;
				}

		finish:
				//printf("uploading gradient with width %u\n", width);

				// TODO: maybe we want to use a float32 or float16 texture here
				_cairo_gpu_context_upload_data(ctx, op, operand->texture, data, swidth, 1);
			}

			/* we draw REFLECT gradients on a double-sized texture */
			if(operand->attributes.extend == CAIRO_EXTEND_REFLECT)
				operand->attributes.extend = CAIRO_EXTEND_REPEAT;

			operand->attributes.filter = CAIRO_FILTER_BILINEAR;
			operand->attributes.extra = (void*)1;
			operand->gradient_width = width;
		}
		else
		{
			cairo_status_t status;
			cairo_gpu_surface_t* surface;
			int x_off, y_off;
			cairo_gpu_space_tls_t* tls;
	acquire:
			/* this recurses with the context locked (IF we locked it!)
			 */

			// we check this in composite_operand
			assert(!setup->unpremultiplied || setup->dst->space->frag_div_alpha);

			tls = _cairo_gpu_space_get_tls(setup->dst->space);
			++tls->in_acquire;
			status = _cairo_pattern_acquire_surface(
					src, &setup->dst->base,
					(operand->chan & CHAN_KA) ? ((operand->chan & (CHAN_C | CHAN_A)) ? CAIRO_CONTENT_COLOR_ALPHA : CAIRO_CONTENT_ALPHA) : CAIRO_CONTENT_COLOR,
							operand->src_x, operand->src_y, setup->width, setup->height,
							CAIRO_PATTERN_ACQUIRE_COMPONENT_ALPHA |
							(setup->dst->space->extend_mask & (1 << CAIRO_EXTEND_REFLECT)) ? 0 : CAIRO_PATTERN_ACQUIRE_NO_REFLECT,
							(cairo_surface_t **) & surface, attributes);
			--tls->in_acquire;
			if(unlikely(status))
				return status;

			x_off = y_off = 0;
			if((surface->texture.target_idx == TARGET_RECTANGLE || surface->texture.width != surface->width || surface->texture.height != surface->height)
					&& (attributes->extend == CAIRO_EXTEND_REPEAT || attributes->extend == CAIRO_EXTEND_REFLECT))
				_cairo_gpu_acquire_repeat_reflect(&surface, attributes, operand->src_x, operand->src_y, setup->width, setup->height, &x_off, &y_off);
			else if(surface->texture.width != surface->width || surface->texture.height != surface->height)
				_cairo_gpu_surface_fix_pad(surface, attributes->extend);

			operand->surface = surface;
			operand->owns_surface = 1;

			attributes->x_offset += operand->src_x - setup->obj_x;
			attributes->y_offset += operand->src_y - setup->obj_y;

			assert(surface->base.backend == &_cairo_gpu_surface_backend);

			attributes->matrix.x0 += x_off + attributes->x_offset * attributes->matrix.xx + attributes->y_offset * attributes->matrix.xy;
			attributes->matrix.y0 += y_off + attributes->x_offset * attributes->matrix.yx + attributes->y_offset * attributes->matrix.yy;

			_cairo_gpu_texture_adjust_matrix(&surface->texture, &attributes->matrix);
		}
	}
	return CAIRO_STATUS_SUCCESS;
}

static void
_cairo_gpu_composite__set_operand(cairo_gpu_context_t* ctx, int op_idx, cairo_gpu_composite_operand_t* operand)
{
	cairo_surface_attributes_t* attributes = &operand->attributes;
	cairo_pattern_t* src = operand->src;

	int idx = op_idx;

	if(operand->gradient_denominator && src->type == CAIRO_PATTERN_TYPE_RADIAL)
	{
		cairo_radial_pattern_t* radial = (cairo_radial_pattern_t*)src;
		double dx = _cairo_fixed_to_double(radial->c2.x) - _cairo_fixed_to_double(radial->c1.x);
		double dy = _cairo_fixed_to_double(radial->c2.y) - _cairo_fixed_to_double(radial->c1.y);
		double r0 = _cairo_fixed_to_double(radial->r1);
		double r1 = _cairo_fixed_to_double(radial->r2);
		double dr = r1 - r0;

		double a = dx * dy + dy * dy - dr * dr;

		// XXX: all this is likely not numerically stable.
		// XXX: we should use an algorithm like the one described in Jim Blinn's "How to Solve a Quadratic Equation"

		// TODO: this is not a good thing to do...
		if(a == 0)
			a = FLT_EPSILON;

		{
			float sign = (a > 0) ? 1 : -1;
			float mac[4] = {-a, -a, 1.0, r0 * r0 * a}; // -ac
			float so[4] = {sign * operand->gradient_denominator / a, 0.0, 0, 0.5};
			float mbd2[4] = {dx * sign, dy * sign, r0 * dr * sign, 0.0}; // -b/2

			if(!operand->gradient_discontinuous)
			{
				so[0] /= (double)operand->gradient_width;
				so[2] = 0.5 / (double)operand->gradient_width;
			}

			_cairo_gpu_context_set_texture_and_attributes_(ctx, idx, operand->texture, attributes, mbd2);
			_cairo_gpu_context_set_radial_gradient(ctx, idx, mac, so);
		}
	}
	else
		_cairo_gpu_context_set_texture_and_attributes(ctx, idx, operand->texture, attributes);

	if(operand->gradient_discontinuous)
		_cairo_gpu_context_set_discontinuous_width(ctx, idx, operand->gradient_width);
}


static cairo_bool_t
_cairo_gpu_composite__set_pass(cairo_gpu_context_t* ctx, cairo_gpu_composite_setup_t* setup, cairo_gpu_composite_pass_t* pass)
{
	unsigned schan = pass->chan;
	cairo_gpu_color4_t u;
	int i;

	if(pass->frag & FRAG_CONSTANT)
	{
		u.c = (schan & CHAN_C) ? setup->c : setup->a;
		u.ka = setup->ka;

		if(pass->unpremultiplied)
		{
			if(setup->a.r)
				u.c.r /= setup->a.r;

			if(setup->a.g)
				u.c.g /= setup->a.g;

			if(setup->a.b)
				u.c.b /= setup->a.b;

			if(pass->unpremultiplied_one_alpha)
				u.ka = 1.0;
		}
	}

	_cairo_gpu_context_set_vert_frag(ctx, pass->vert, pass->frag);

	if(pass->frag & FRAG_CONSTANT)
		_cairo_gpu_context_set_constant_color(ctx, &u);

	_cairo_gpu_context_set_blend(ctx, pass->blend.v);
	if(pass->blend_color == BLEND_COLOR_C_DIV_A)
	{
		cairo_gpu_color4_t bc;
		if(setup->a.r)
			bc.c.r = setup->c.r / setup->a.r;
		if(setup->a.g)
			bc.c.g = setup->c.g / setup->a.g;
		if(setup->a.b)
			bc.c.b = setup->c.b / setup->a.b;
		bc.ka = 1.0;
		_cairo_gpu_context_set_blend_color(ctx, &bc);
	}
	else if(pass->blend_color == BLEND_COLOR_A)
	{
		cairo_gpu_color4_t bc;
		bc.c = setup->a;
		bc.ka = setup->ka;
		_cairo_gpu_context_set_blend_color(ctx, &bc);
	}
	else if(pass->blend_color == BLEND_COLOR_1_MINUS_A)
	{
		cairo_gpu_color4_t bc;
		bc.c.r = 1.0 - setup->a.r;
		bc.c.g = 1.0 - setup->a.g;
		bc.c.b = 1.0 - setup->a.b;
		bc.ka = 1.0 - setup->ka;
		_cairo_gpu_context_set_blend_color(ctx, &bc);
	}

	for(i = 0; i < MAX_OPERANDS; ++i)
	{
		if((pass->operands[i] >= 0) && pass->operands[i] != setup->cur_operands[i])
		{
			_cairo_gpu_composite__set_operand(ctx, i, &setup->operands[(int)pass->operands[i]]);
			setup->cur_operands[i] = pass->operands[i];
		}
	}

	return pass->draw_geometry;
}

static void
_cairo_gpu_composite_fini(cairo_gpu_composite_setup_t * setup)
{
	int i;
	for(i = 0; i < MAX_OPERANDS; ++i)
	{
		cairo_gpu_composite_operand_t* operand = &setup->operands[0];
		if(operand->src)
		{
			if(operand->owns_surface)
			{
				cairo_gpu_surface_t *surface = operand->surface;

				_cairo_pattern_release_surface(operand->src, &surface->base, &operand->attributes);
				operand->owns_surface = 0;
				operand->surface = 0;
			}
		}
	}
}

static void
_cairo_gpu_composite__init_pass(cairo_gpu_composite_setup_t * setup, int i, unsigned schan)
{
	setup->operands_chan |= schan;
	setup->passes[i].chan = schan;
	setup->passes[i].blend_color = 0;
	setup->passes[i].unpremultiplied = 0;
	setup->passes[i].unpremultiplied_one_alpha = 0;
	setup->passes[i].draw_geometry = 1;
	setup->passes[i].blend.v = 0;
	setup->passes[i].component = ~0;
}

static void
_cairo_gpu_composite__make_pass(cairo_gpu_composite_setup_t * setup, int i, unsigned schan)
{
	cairo_gpu_blend_t blend;
	int blend_src = setup->blend_src;
	int blend_dst = setup->blend_dst;

	_cairo_gpu_composite__init_pass(setup, i, schan);

	blend.v = 0;

	if(blend_src == ALPHA)
		blend.src_rgb = blend.src_alpha = BLEND_DST_ALPHA;
	else if(blend_src == (1 | ALPHA))
		blend.src_rgb = blend.src_alpha = BLEND_ONE_MINUS_DST_ALPHA;
	else
		blend.src_rgb = blend.src_alpha = blend_src;

	if(blend_dst == ALPHA)
		blend.dst_rgb = blend.dst_alpha = (schan & CHAN_C) ? BLEND_SRC_ALPHA : BLEND_SRC_COLOR;
	else if(blend_dst == (1 | ALPHA))
		blend.dst_rgb = blend.dst_alpha = (schan & CHAN_C) ? BLEND_ONE_MINUS_SRC_ALPHA : BLEND_ONE_MINUS_SRC_COLOR;
	else
		blend.dst_rgb = blend.dst_alpha = blend_dst;

	if(setup->dst->base.content == CAIRO_CONTENT_COLOR)
		blend.src_alpha = blend.dst_alpha = 0;
	else if(setup->dst->base.content == CAIRO_CONTENT_ALPHA)
		blend.src_rgb = blend.dst_rgb = 0;

	if(setup->zero_chan & schan & (CHAN_C | CHAN_A))
		blend.src_rgb = 0;
	if(setup->zero_chan & CHAN_KA)
		blend.src_alpha = 0;

	if(!setup->dst->space->blend_func_separate)
	{
		if(blend.src_alpha)
		{
			if(!blend.src_rgb)
				blend.src_rgb = blend.src_alpha;
			else
				assert(blend.src_rgb == blend.src_alpha);
		}
		else
		{
			if(blend.src_rgb)
				blend.src_alpha = blend.src_rgb;
		}

		if(blend.dst_alpha)
		{
			if(!blend.dst_rgb)
				blend.dst_rgb = blend.dst_alpha;
			else
				assert(blend.dst_rgb == blend.dst_alpha);
		}
		else
		{
			if(blend.dst_rgb)
				blend.dst_alpha = blend.dst_rgb;
		}
	}

	// TODO: check this
	if(setup->saturate)
	{
		assert(blend.src_rgb == BLEND_ONE_MINUS_DST_ALPHA);

		if(schan & CHAN_C)
		{
			setup->passes[i].unpremultiplied = 1;
			blend.src_rgb = blend.src_alpha = BLEND_SRC_ALPHA_SATURATE;
		}
		else
		{
			assert(!(schan & CHAN_A));

			blend.src_rgb = blend.src_alpha = 1;
		}
	}

	if(setup->smooth)
	{
		if(schan & CHAN_C)
		{
			if(blend.src_rgb == 0)
			{}
			else if(blend.src_rgb == 1)
			{
				blend.src_rgb = BLEND_SRC_ALPHA;
				setup->passes[i].unpremultiplied = 1;
			}
			else
				assert(0);
		}
		else
			assert(!(schan & CHAN_A));
	}

	if(setup->dst->base.content == CAIRO_CONTENT_COLOR)
		blend.color_mask = 7;
	else if(setup->dst->base.content == CAIRO_CONTENT_ALPHA)
		blend.color_mask = 8;
	else
		blend.color_mask = 0xf;

	setup->passes[i].blend.v = blend.v;
}

/* We use a general component-alpha model, which is simplified down if there is no "component alpha".
 * The component alpha model is the following:
 * - Each surface is 6-channel with p = (c, a) where c and a are (r, g, b) vectors
 * - Operands are multiplied together per-channel.
 * - Blending is done this way:
 * d.c = sb(d.a) * s.c + db(s.a) * d.c
 * d.a = sb(d.a) * s.a + db(s.a) * d.a
 * where multiplication is componentwise multiplication of 3-vectors
 *
 * There are several mismatches between this model and OpenGL's one:
 * 1. OpenGL has no 6-channel textures. This can be solved by using two 3-channel textures, one for C and one for A.
 * 2. The blender only has one input, while we would need two
 *
 * Another issue is how to handle 4-channel images.
 * For inputs, we simply set ra = ga = ba = a.
 * For outputs, we need an extended model.
 *
 * In the extended model, there is an additional "ka" component, which is grayscale alpha.
 * We then have:
 * d.c = sb(d.a) * s.c + db(s.a) * d.c
 * d.a = sb(d.a) * s.a + db(s.a) * d.a
 * d.ka = sb(d.ka) * s.ka + db(s.ka) * d.ka
 *
 * We now have 7-channel images, represented as a 3-channel color image, plus a 4-channel alpha texture.
 * If the destination is 7-channel, we add a ka computation to pass 3.
 *
 * *** THIS IS THE GENERAL CASE WE IMPLEMENT BELOW ***
 * If the destination is (r, g, b, ka), we have:
 * d.c = sb(d.ka) * s.c + db(s.a) * d.c
 * d.ka = sb(d.ka) * s.ka + db(s.ka) * d.ka
 *
 * Which can be implemented like this:
 *
 * Pass 1 (CHAN_A)
 * d.c = 0 * s.a + db(s.a) * d.c
 *
 * Pass 2 (CHAN_C)
 * d.c = sb(d.ka) * s.c + 1 * d.c
 *
 * Pass 3 (CHAN_KA)
 * d.ka = sb(d.ka) * s.ka + db(s.ka) * d.ka
 *
 * If we have GL_EXT_blend_func_separate we can merge pass 2 and pass 3:
 *
 * Pass 1:
 * d.c = db(s.a) * d.c
 *
 * Pass 2:
 * d.c = sb(d.a) * s.c + 1 * d.c
 * d.ka = sb(d.ka) * s.a + db(s.ka) * d.ka
 *
 *
 * Or, if sb(d.a) == 0,
 * d.c = 0 * s.a + db(s.a) * d.c
 * d.ka = sb(d.ka) * s.a + db(s.ka) * d.ka
 *
 *
 * d.c = sb(d.ka) * s.c + db(s.a) * d.c
 * d.ka = sb(d.ka) * s.ka + db(s.ka) * d.ka
 */

static int
_cairo_gpu_composite_plan_passes(cairo_gpu_composite_setup_t * setup)
{
	/*
	 * Determine constness of source and destination channels
	 * Zero source blend if zero color, zero alpha
	 * Exit if nop
	 * Demote alpha factors if alpha is const 0 or const 1
	 * Remove dead inputs (consider non-zeroness and alpha-inclusion of blend factors and colormask here!)
	 * Separate blend factors, zero-out useless blend factors (preserving func_separate!)
	 * Generate code
	 */
	int uchan;
	int zchan;
	int vchan;
	int chan;
	int dstchan;
	int separate;

	/* 1. Determine input/output channels usefulness and constness */

	if(setup->dst->base.content == CAIRO_CONTENT_COLOR_ALPHA)
		dstchan = CHAN_C | CHAN_KA;
	else if(setup->dst->base.content == CAIRO_CONTENT_COLOR)
		dstchan = CHAN_C;
	else
		dstchan = CHAN_KA;

	if(!(dstchan & CHAN_KA) && (setup->blend_src & ALPHA))
	{
		setup->blend_src ^= ALPHA | 1;
		setup->saturate = 0;
	}

	uchan = 0;
	if(dstchan & CHAN_C)
	{
		if(setup->blend_src)
			uchan |= CHAN_C;

		if(setup->blend_dst & ALPHA)
			uchan |= CHAN_A;
	}

	if(((dstchan & CHAN_KA) && setup->blend_src) || setup->blend_dst & ALPHA)
		uchan |= CHAN_KA;

	setup->constant_chan = 0;
	zchan = 0;
	if(setup->ka == 0.0)
	{
		zchan |= CHAN_C | CHAN_A | CHAN_KA;
		setup->constant_chan |= CHAN_C | CHAN_A | CHAN_KA;
		setup->c.r = setup->c.b = setup->c.g = setup->a.r = setup->a.g = setup->a.b = setup->ka = 0;
	}
	else
	{
		if(setup->ka != 1.0)
			setup->constant_chan |= CHAN_KA;

		if(setup->a.r + setup->a.g + setup->a.b == 0.0f)
		{
			zchan |= CHAN_C | CHAN_A;
			setup->constant_chan |= CHAN_C | CHAN_A;
			setup->c.r = setup->c.b = setup->c.g = setup->a.r = setup->a.g = setup->a.b = 0;
		}
		else
		{
			if(setup->a.r != setup->a.b || setup->a.b != setup->a.g || setup->a.g != setup->ka)
				setup->constant_chan |= CHAN_A;

			if((setup->c.r + setup->c.g + setup->c.b) == 0.0f)
			{
				zchan |= CHAN_C;
				setup->constant_chan |= CHAN_C;
				setup->c.r = setup->c.b = setup->c.g = 0;
			}
			else if((setup->c.r + setup->c.g + setup->c.b) != 3.0f)
				setup->constant_chan |= CHAN_C;
		}
	}
	setup->zero_chan = zchan;

#if 0
	{
		_cairo_gpu_composite__make_pass(setup, 0, CHAN_C | CHAN_A | CHAN_KA);
		setup->npasses = 1;
		return;
	}
#endif

	vchan = setup->smooth ? CHAN_KA : 0; /* coverage alpha */
	vchan |= setup->primary_chan;
	{
		int i;
		for(i = 0; i < MAX_OPERANDS; ++i)
			vchan |= setup->operands[i].chan;
	}

	chan = vchan | setup->constant_chan | zchan;

	/* 2. Constant fold channels in blend factors */

	if(zchan & CHAN_KA)
		setup->blend_dst &= ~ALPHA;
	else if(!(chan & CHAN_KA) && (setup->blend_dst & ALPHA))
		setup->blend_dst ^= ALPHA | 1;

	// TODO: handle src_alpha == 0
	// this is done separately in blend setup
	if(((zchan | ~uchan) & (CHAN_C | CHAN_KA)) == (CHAN_C | CHAN_KA))
	{
		uchan &=~ (CHAN_C | CHAN_KA);
		setup->blend_src = 0;
		setup->saturate = 0;
	}

	/* 3. Nop exit */
	if(!setup->blend_src && setup->blend_dst == 1)
	{
		// nothing to do...
		return 0;
	}

	/* 4. Dead input elimination */

	setup->constant_chan &= uchan;

	if((zchan | ~uchan) & CHAN_KA)
		setup->smooth = 0;

	setup->primary_chan &= ~zchan | uchan;
	{
		int i;
		for(i = 0; i < MAX_OPERANDS; ++i)
			setup->operands[i].chan &= ~zchan | uchan;
	}

	vchan &= ~zchan | uchan;
	chan &= ~zchan | uchan;

	/*
	 * d.ka = (1 - s.ka * cov)
	 *
	 * d.c = sb(1) * s.c + db(1 - d.ka) * d.c
	 * d.ka = 1 * s.ka + d.ka (= 1)
	 */

	// if blend_dst == 1, we are adding, so we can just use the vanilla no-component-alpha path
	if(setup->dst_alpha_mask && (setup->blend_dst != 1))
	{
		cairo_gpu_blend_t blend;
		assert(!(chan & CHAN_A));

		// if db() = 0/1, use one pass
		_cairo_gpu_composite__init_pass(setup, 0, CHAN_KA);
		setup->passes[0].blend.v = BLEND_SUB_ALPHAONLY;

		_cairo_gpu_composite__init_pass(setup, 1, CHAN_C);
		blend.v = 0;
		if(setup->blend_src == 1)
			blend.src_rgb = BLEND_ONE_MINUS_DST_ALPHA;
		else if(!setup->blend_src)
			blend.src_rgb = 0;
		else // impossible because there is no dst alpha
			assert(0);

		if(setup->blend_dst == ALPHA)
			blend.dst_rgb = BLEND_ONE_MINUS_DST_ALPHA;
		else if(setup->blend_dst == (1 | ALPHA))
			blend.dst_rgb = BLEND_DST_ALPHA;
		else
			blend.dst_rgb = setup->blend_dst;

		if(setup->dst->space->blend_func_separate)
		{
			blend.src_alpha = 1;
			blend.dst_alpha = 1;
			blend.color_mask = 0xf;
		}
		else
		{
			blend.src_alpha = blend.src_rgb;
			blend.dst_alpha = blend.dst_rgb;
			blend.color_mask = 0x7;
		}

		setup->passes[1].blend.v = blend.v;
		setup->passes[1].unpremultiplied = 1;
		setup->passes[1].unpremultiplied_one_alpha = 1;
		setup->passes[1].draw_geometry = 0;

		if(setup->dst->space->blend_func_separate)
			return 2;
		else
		{
			_cairo_gpu_composite__init_pass(setup, 2, 0);
			setup->passes[2].blend.v = BLEND_SOURCE_ALPHAONLY;
			setup->passes[2].draw_geometry = 0;
			return 3;
		}
	}

	/* 5. No component alpha */
	if(!(chan & CHAN_A))
	{
		_cairo_gpu_composite__make_pass(setup, 0, CHAN_C | CHAN_A | CHAN_KA);
		return 1;
	}

	/* 6. Per-component 4-pass algorithm (only used and needed for SATURATE with component alpha) */
	// TODO: test this!
	if(setup->saturate)
	{
		int i = 0;
		if(!setup->dst->space->per_component)
			return -1;

		if(dstchan & CHAN_C)
		{
			for(; i < 3; ++i)
			{
				_cairo_gpu_composite__make_pass(setup, i, CHAN_C | CHAN_A | CHAN_KA);
				setup->passes[i].blend.color_mask = 1 << i;
				setup->passes[i].component = i;
				assert(setup->passes[i].unpremultiplied);
			}
		}

		if(dstchan & CHAN_KA)
		{
			_cairo_gpu_composite__make_pass(setup, i, CHAN_KA);
			setup->passes[i].blend.color_mask &= 8;
			++i;
		}
		return i;
	}

	separate = setup->dst->space->blend_func_separate || !(dstchan & CHAN_KA);

	/* 7. Zero color
	 *
	 * **** THIS IS WHAT WE USE FOR BLACK COMPONENT ALPHA TEXT ***
	 * d.c = 0 * s.a + db(s.a) * d.c
	 * d.ka = sb(d.ka) * s.ka + db(s.ka) * d.ka
	 */
	if((zchan & CHAN_C) && separate)
	{
		_cairo_gpu_composite__make_pass(setup, 0, CHAN_A | CHAN_KA);
		setup->passes[0].blend.src_rgb = 0;
		if(!(dstchan & CHAN_KA))
			setup->passes[0].blend.src_alpha = 0;
		return 1;
	}

	// d.c = 0, {1 | d.ka} * s.c, K + 0, {s.a} * d.c, K * d.c, d.c

	/* 8. Constant component alpha
	 * d.c = sb(d.ka) * s.c + K * d.c
	 * d.ka = sb(d.ka) * s.ka + db(s.ka) * d.ka
	 */
	if(!(vchan & (CHAN_A | CHAN_KA)) && setup->dst->space->blend_color && separate)
	{
		_cairo_gpu_composite__make_pass(setup, 0, CHAN_C | CHAN_A | CHAN_KA);
		if(setup->passes[0].blend.dst_rgb == BLEND_SRC_ALPHA)
		{
			setup->passes[0].blend.dst_rgb = BLEND_CONSTANT_COLOR;
			setup->passes[0].blend_color = BLEND_COLOR_A;
		}
		if(setup->passes[0].blend.dst_rgb == BLEND_ONE_MINUS_SRC_ALPHA)
		{
			setup->passes[0].blend.dst_rgb = BLEND_CONSTANT_COLOR;
			setup->passes[0].blend_color = BLEND_COLOR_1_MINUS_A;
		}
		if(!(dstchan & CHAN_KA) && setup->passes[0].blend.dst_alpha)
			setup->passes[0].blend.dst_alpha = BLEND_CONSTANT_COLOR;
		return 1;
	}

	/* 9. Constant color, one source blending
	 **** THIS IS WHAT WE USE FOR SOLID NON-BLACK COMPONENT ALPHA TEXT ***
	 * d.c = constant_color(s.const_c / s.const_a) * s.a + db(s.a) * d.c
	 * d.ka = s.ka + db(s.ka) * d.ka
	 */
	if(!(vchan & CHAN_C) && setup->blend_src == 1 && setup->dst->space->blend_color)
	{
		_cairo_gpu_composite__make_pass(setup, 0, CHAN_A | CHAN_KA);
		setup->passes[0].blend_color = BLEND_COLOR_C_DIV_A;
		setup->passes[0].blend.src_rgb = BLEND_CONSTANT_COLOR;
		setup->passes[0].blend.src_alpha = setup->dst->space->blend_func_separate ? 1 : BLEND_CONSTANT_COLOR;
		return 1;
	}

	/* 10. General multipass algorithm */
	{
		int i = 0;
		// d.c = db(s.a) * d.c
		// blend_dst is either ALPHA or 1 | ALPHA, otherwise we already hit "no component alpha"
		if(dstchan & CHAN_C)
		{
			_cairo_gpu_composite__make_pass(setup, i, CHAN_A | CHAN_KA);
			setup->passes[i].blend.src_rgb = setup->passes[i].blend.src_alpha = BLEND_ZERO;
			setup->passes[i].blend.color_mask &= ~8;
			++i;
		}

		if((dstchan & (CHAN_C | CHAN_KA)) == (CHAN_C | CHAN_KA) && setup->dst->space->blend_func_separate)
		{
			/*
			 * d.c = sb(d.ka) * s.c + 1 * d.c
			 * d.ka = sb(d.ka) * s.ka + db(s.ka) * d.ka
			 */
			_cairo_gpu_composite__make_pass(setup, i, CHAN_C | CHAN_A | CHAN_KA);
			setup->passes[i].blend.dst_rgb = 1;
			++i;
		}
		else
		{
			// d.c = sb(d.ka) * s.c + 1 * d.c
			// must be non-trivial, otherwise we already hit "zero color"
			if(dstchan & CHAN_C)
			{
				_cairo_gpu_composite__make_pass(setup, i, CHAN_C | CHAN_A | CHAN_KA);
				setup->passes[i].blend.dst_rgb = setup->passes[i].blend.dst_alpha = 1;
				setup->passes[i].blend.color_mask = 7;
				if(setup->dst->space->blend_func_separate)
					setup->passes[i].blend.dst_alpha = setup->passes[i].blend.src_alpha = 0;
				++i;
			}

			// d.ka = sb(d.ka) * s.ka + db(s.ka) * d.ka
			// must be non-trivial, because otherwise db(s.ka) == 0 and we hit "no component alpha"
			if(dstchan & CHAN_KA)
			{
				_cairo_gpu_composite__make_pass(setup, i, CHAN_KA);
				setup->passes[i].blend.color_mask = 8;
				if(setup->dst->space->blend_func_separate)
					setup->passes[i].blend.dst_rgb = setup->passes[i].blend.src_rgb = 0;
				++i;
			}
		}

		return i;
	}
}

static cairo_bool_t
_cairo_gpu_composite_prepare_passes(cairo_gpu_composite_setup_t* setup)
{
	int ipass;
	for(ipass = 0; ipass < setup->npasses; ++ipass)
	{
		cairo_gpu_composite_pass_t* pass = &setup->passes[ipass];
		unsigned frag = 0;
		unsigned vert = 0;
		int noperands = 0;
		int vert_care_about_premultiply = 0;
		unsigned operands_mask = 0;
		unsigned schan = pass->chan;
		int noperands_op;
		int i;

		// NOTE: this works because currently unpremultiplied primary color is white+alpha.
		// Otherwise, we need to add "component swizzling" to the vertex shader too
		if(setup->primary_chan & schan)
		{
			frag |= FRAG_PRIMARY;

			// We assume that the primary color is constant per-primitive. Otherwise, everything is broken!

			vert |= VERT_COLOR_POSTOP;

			if(pass->blend.color_mask & 7)
				vert_care_about_premultiply = 1;
		}
		// currently all users of primary colors must bake the constant in and reset constant_chan
		// this could be changed, but it would be problematic without ragment programs.
		else if(setup->constant_chan & schan)
			frag |= FRAG_CONSTANT;

		// should never happen with current code
		if(vert_care_about_premultiply && pass->unpremultiplied != setup->unpremultiplied)
		{
			if(!setup->dst->space->vert_op)
				return -1;

			// XXX: fix op management when we support passthru
			if(pass->unpremultiplied)
			{
				if(pass->unpremultiplied_one_alpha)
					vert |= OP_DIV_ALPHA_RGBA << VERT_OP_SHIFT;
				else
					vert |= OP_DIV_ALPHA << VERT_OP_SHIFT;
			}
			else
				// TODO: revisit this when implementing a subpixel-antialiasing scan renderer
				vert |= OP_MUL_ALPHA << VERT_OP_SHIFT;
		}

		// component alpha must come first because the fixed OpenGL pipeline needs it in the per-component case
		/*
		if(schan & CHAN_A)
		{
			for(i = 0; i < MAX_OPERANDS; ++i)
			{
				if(setup->operands[i].chan & CHAN_A)
					operands[noperands++] = &setup->operands[i];
			}
		}

		for(i = 0; i < MAX_OPERANDS; ++i)
		{
			if(setup->operands[i].chan & schan && !(setup->operands[i].chan & CHAN_A))
				operands[noperands++] = &setup->operands[i];
		}
		*/

		for(i = 0; i < MAX_OPERANDS; ++i)
		{
			cairo_gpu_composite_operand_t* operand = &setup->operands[i];
			if(!(operand->chan & schan))
				continue;

			if(pass->component < 3 && operand->chan & CHAN_A)
				operand->unpremultiplied = 1;

			if(!(operand->chan & schan & ~CHAN_KA))
				operand->unpremultiplied = setup->unpremultiplied;
		}

		if(pass->blend.color_mask & 7)
		{
			for(i = 0; i < MAX_OPERANDS; ++i)
			{
				cairo_gpu_composite_operand_t* operand = &setup->operands[i];
				if(!(operand->chan & schan))
					continue;

				if(
					!(pass->component < 3 && operand->chan & CHAN_A)
					&& (operand->chan & schan & ~CHAN_KA)
					&& (operand->unpremultiplied != pass->unpremultiplied)
					)
				{
					pass->operands[noperands++] = i;
					operands_mask |= 1 << i;
				}
			}

			noperands_op = noperands;
		}
		else
			// we don't need RGB, so we don't need to apply any operation
			noperands_op = 0;

		for(i = 0; i < MAX_OPERANDS; ++i)
		{
			cairo_gpu_composite_operand_t* operand = &setup->operands[i];
			if(!(operand->chan & schan) || (operands_mask & (1 << i)))
				continue;

			if(operand->chan & CHAN_A)
			{
				pass->operands[noperands++] = i;
				operands_mask |= 1 << i;
			}
		}

		for(i = 0; i < MAX_OPERANDS; ++i)
		{
			cairo_gpu_composite_operand_t* operand = &setup->operands[i];
			if(!(operand->chan & schan) || (operands_mask & (1 << i)))
				continue;

			pass->operands[noperands++] = i;
			operands_mask |= 1 << i;
		}

		for(i = 0; i < noperands; ++i)
		{
			cairo_gpu_composite_operand_t* operand = &setup->operands[(int)pass->operands[i]];

			vert |= (operand->has_coords ? (i + 1) : VERT_TEX_GEN) << (VERT_TEX_SHIFT + i * VERT_TEX_BITS);

			if(pass->component < 3 && operand->chan & CHAN_A)
			{
				frag |= ((1 + pass->component) << FRAG_COMPONENT_SHIFT);

				if(pass->unpremultiplied)
					frag |= ((operand->unpremultiplied ? FRAG_TEX_COLOR_111CA : FRAG_TEX_COLOR_111C) << (FRAG_TEX_SHIFT + i * FRAG_TEX_BITS));
				else
					assert(0);
			}
			else if(!setup->dst->space->tex_aaaa_111a)
			{
				// we assume we failed earlier if we have to
				frag |= FRAG_TEX_COLOR_RGBA << (FRAG_TEX_SHIFT + i * FRAG_TEX_BITS);
			}
			else if((pass->blend.color_mask & 7) && operand->chan & schan & ~CHAN_KA)
				frag |= FRAG_TEX_COLOR_RGBA << (FRAG_TEX_SHIFT + i * FRAG_TEX_BITS);
			else if(!pass->unpremultiplied)
				frag |= FRAG_TEX_COLOR_AAAA << (FRAG_TEX_SHIFT + i * FRAG_TEX_BITS);
			else
				frag |= FRAG_TEX_COLOR_111A << (FRAG_TEX_SHIFT + i * FRAG_TEX_BITS);

			if(operand->gradient_denominator && operand->src->type == CAIRO_PATTERN_TYPE_RADIAL)
				frag |= FRAG_TEX_RADIAL << (FRAG_TEX_SHIFT + i * FRAG_TEX_BITS);

			if(operand->gradient_discontinuous)
				frag |= FRAG_TEX_DISCONTINUOUS << (FRAG_TEX_SHIFT + i * FRAG_TEX_BITS);
		}

		if(noperands_op)
		{
			if(noperands_op > 1)
			{
				assert(noperands_op == 2);
				frag |= FRAG_OPPOS_TEX1;
			}

			if(pass->unpremultiplied)
			{
				if(pass->unpremultiplied_one_alpha)
					frag |= OP_DIV_ALPHA_RGBA << FRAG_OP_SHIFT;
				else
					frag |= OP_DIV_ALPHA << FRAG_OP_SHIFT;
			}
			else
				frag |= OP_MUL_ALPHA << FRAG_OP_SHIFT;
		}

		if(!_cairo_gpu_space_is_frag_supported(setup->dst->space, frag))
			return 0;

		for(i = noperands; i < MAX_OPERANDS; ++i)
			pass->operands[i] = -1;

		pass->vert = vert;
		pass->frag = frag;
	}
	return 1;
}

static cairo_status_t
_cairo_gpu_composite_plan(cairo_gpu_composite_setup_t * setup)
{
	if(0 && setup->operands[0].src && setup->operands[0].src->type == CAIRO_PATTERN_TYPE_LINEAR)
	{
		const char* blends_s_a[] = {"0", "d.c", "s.a * d.c", "(1 - s.a) * d.c"};
		const char* blends_s_ka[] = {"0", "d.ka", "s.ka * d.ka", "(1 - s.ka) * d.ka"};
		const char* blends_d_ka[] = {"0", "s.c", "d.ka * s.c", "(1 - d.ka) * s.c"};
		const char* blends_d_ka_ka[] = {"0", "s.ka", "d.ka * s.ka", "(1 - d.ka) * s.ka"};
		const char* chans[] = {"", "c", "a", "c, a", "ka", "c, ka", "a, ka", "c, a, ka"};

		printf("\nBLEND");
		if(setup->operands[0].src)
			printf("  op0 = (%s)", chans[setup->operands[0].chan]);
		if(setup->operands[0].src)
			printf("  op1 = (%s)", chans[setup->operands[0].chan]);
		if(setup->primary_chan)
			printf("  primary = (%s)", chans[setup->primary_chan]);
		if(setup->smooth)
			printf("  smooth = %i", setup->smooth);

		printf("  const = (%f %f %f) (%f %f %f) %f\n", setup->c.r, setup->c.g, setup->c.b, setup->a.r, setup->a.g, setup->a.b, setup->ka);

		if(setup->dst->base.content != CAIRO_CONTENT_ALPHA)
			printf("d.c = %s + %s\n", blends_d_ka[setup->blend_src], blends_s_a[setup->blend_dst]);
		if(setup->dst->base.content != CAIRO_CONTENT_COLOR)
			printf("d.ka = %s + %s\n", blends_d_ka_ka[setup->blend_src], blends_s_ka[setup->blend_dst]);
	}

	setup->npasses = _cairo_gpu_composite_plan_passes(setup);
	if(setup->npasses < 0)
		return CAIRO_INT_STATUS_UNSUPPORTED;

	if(!_cairo_gpu_composite_prepare_passes(setup))
		return CAIRO_INT_STATUS_UNSUPPORTED;

	return _cairo_gpu_composite_prepare_operands(setup);
}

static void
_cairo_gpu_composite_draw_prepare(cairo_gpu_context_t* ctx, cairo_gpu_composite_setup_t* setup)
{
	int i;
	int ipass;

	memset(setup->cur_operands, 0xff, sizeof(setup->cur_operands));
	for(i = 0; i < MAX_OPERANDS; ++i)
	{
		cairo_gpu_composite_operand_t* operand = &setup->operands[i];
		if(operand->surface && !operand->texture)
		{
			operand->texture = _cairo_gpu_surface_begin_texture(operand->surface, ctx, i);
		}
	}

	for(ipass = 0; ipass < setup->npasses; ++ipass)
	{
		cairo_gpu_composite_pass_t* pass = &setup->passes[ipass];
		for(i = 0; i < MAX_OPERANDS; ++i)
		{
			if(pass->operands[i] >= 0)
			{
				if(setup->operands[(int)pass->operands[i]].texture->target_idx == TARGET_RECTANGLE)
					pass->frag |= FRAG_TEX_RECTANGLE << (FRAG_TEX_SHIFT + i * FRAG_TEX_BITS);
			}
		}
	}
}

static inline cairo_gpu_context_t*
_cairo_gpu_composite_draw_prepare_bind(cairo_gpu_composite_setup_t* setup)
{
	cairo_gpu_context_t* ctx = _cairo_gpu_surface_lookup_context(setup->dst, FB_DRAW);
	_cairo_gpu_composite_draw_prepare(ctx, setup);
	_cairo_gpu_surface_bind_to(setup->dst, ctx, FB_DRAW);
	return ctx;
}

static inline cairo_gpu_context_t*
_cairo_gpu_composite_plan_prepare_bind(cairo_gpu_composite_setup_t* setup)
{
	cairo_status_t status = _cairo_gpu_composite_plan(setup);
	if(status)
		return 0;
	return _cairo_gpu_composite_draw_prepare_bind(setup);
}

static void
_cairo_gpu_composite_draw_inner(cairo_gpu_context_t* ctx, cairo_gpu_composite_setup_t* setup, cairo_gpu_geometry_t* geometry)
{
	int ipass;
	int discard_geometry = 0;
	_cairo_gpu_context_set_translation(ctx, setup->dst_x - setup->obj_x, setup->dst_y - setup->obj_y);
	if(geometry)
		_cairo_gpu_context_set_geometry(ctx, geometry);
	else if(setup->dst->has_clip)
	{
		int i, nboxes;
		float* vertices;
		float* v;
		pixman_box32_t *pboxes = pixman_region32_rectangles(&setup->dst->clip.rgn, &nboxes);
		if(nboxes == 1)
		{
			int x1 = MAX(setup->dst_x, pboxes[0].x1);
			int y1 = MAX(setup->dst_y, pboxes[0].y1);
			int x2 = MIN(setup->dst_x + setup->width, pboxes[0].x2);
			int y2 = MIN(setup->dst_y + setup->height, pboxes[0].y2);
			setup->obj_x += x1 - setup->dst_x;
			setup->obj_y += y1 - setup->dst_y;
			setup->width = x2 - x1;
			setup->height = y2 - y1;
		}
		geometry = _cairo_gpu_context_geometry(ctx, GEOM_TEMP);
		v = vertices = _cairo_gpu_geometry_begin(ctx, geometry, PRIM_QUADS, nboxes * 4, 2, 0, 0);

		for(i = 0; i < nboxes; ++i)
		{
			int x1 = MAX(setup->dst_x, pboxes[i].x1);
			int y1 = MAX(setup->dst_y, pboxes[i].y1);
			int x2 = MIN(setup->dst_x + setup->width, pboxes[i].x2);
			int y2 = MIN(setup->dst_y + setup->height, pboxes[i].y2);

			if(x1 >= x2 || y1 >= y2)
				continue;

			//printf("%i %i %i %i %i\n", i, x1, y1, x2, y2);

			_cairo_gpu_emit_rect(&v, x1, y1, x2 - x1, y2 - y1);
		}
		_cairo_gpu_geometry_end(ctx, geometry, (v - vertices) >> 1);

		_cairo_gpu_context_set_translation(ctx, setup->dst_x - setup->obj_x, setup->dst_y - setup->obj_y);
		_cairo_gpu_context_set_geometry(ctx, geometry);
		discard_geometry = 1;
	}

	for(ipass = 0; ipass < setup->npasses; ++ipass)
	{
		if(_cairo_gpu_composite__set_pass(ctx, setup, &setup->passes[ipass]) && geometry)
		{
			_cairo_gpu_context_set_raster(ctx, setup->smooth);
			_cairo_gpu_draw_clipped(ctx, setup->dst, setup->dst_x, setup->dst_y, setup->width, setup->height);
		}
		else
		{
			_cairo_gpu_context_set_raster(ctx, 0);
			_cairo_gpu_context_set_viewport(ctx, 0, 0, setup->dst->width, setup->dst->height);
			_cairo_gpu_context_draw_rect(ctx, setup->obj_x, setup->obj_y, setup->width, setup->height);
		}
	}
	if(discard_geometry)
		_cairo_gpu_geometry_put(ctx, geometry);
}

static void
_cairo_gpu_composite_end(cairo_gpu_context_t* ctx, cairo_gpu_composite_setup_t* setup)
{
	int i;

	// TODO: may want to split the function here

	for(i = 0; i < MAX_OPERANDS; ++i)
	{
		cairo_gpu_composite_operand_t* operand = &setup->operands[i];
		if(operand->chan & setup->operands_chan)
		{
			if(operand->surface)
				_cairo_gpu_surface_end_texture(ctx, operand->surface, operand->texture);

			if(operand->owns_texture)
			{
				_cairo_gpu_texture_fini(ctx, operand->texture);
				free(operand->texture);
				operand->texture = 0;
				operand->owns_texture = 0;
			}
		}
	}
}

static inline void
_cairo_gpu_composite_modified(cairo_gpu_composite_setup_t* setup)
{
	_cairo_gpu_surface_modified(setup->dst, setup->dst_x, setup->dst_y, setup->width, setup->height);
}

static inline void
_cairo_gpu_composite_draw_once_modify(cairo_gpu_context_t* ctx, cairo_gpu_composite_setup_t* setup, cairo_gpu_geometry_t* geometry)
{
	_cairo_gpu_composite_draw_inner(ctx, setup, geometry);
	_cairo_gpu_composite_modified(setup);
	_cairo_gpu_composite_end(ctx, setup);
}
