static void
_cairo_gpu_space__fini_entry(void* abstract_entry, void* closure)
{
	cairo_gpu_context_t* ctx = closure;
	cairo_gpu_ptr_entry_t* ptr_entry = (cairo_gpu_ptr_entry_t*)abstract_entry;
	switch(ptr_entry->base.hash & TABLE_MASK)
	{
	case TABLE_VERT:
		ctx->pipe->delete_vs_state(ctx->pipe, ptr_entry->v);
		break;
	case TABLE_FRAG:
		ctx->pipe->delete_fs_state(ctx->pipe, ptr_entry->v);
		break;
	case TABLE_BLEND:
		ctx->pipe->delete_blend_state(ctx->pipe, ptr_entry->v);
		break;
	case TABLE_SAMPLER:
		ctx->pipe->delete_sampler_state(ctx->pipe, ptr_entry->v);
		break;
	default:
		break;
	}

	_cairo_hash_table_remove(ctx->space->table, (cairo_hash_entry_t*)abstract_entry);
	free(abstract_entry);
}

static inline void
_cairo_gpu_enter(void)
{
}

static inline void
_cairo_gpu_exit(void)
{
}

static void
_cairo_gpu_context_init(cairo_gpu_context_t* ctx);

static cairo_gpu_space_tls_t*
_cairo_gpu_space_alloc_tls(cairo_gpu_space_t* space)
{
	cairo_gpu_space_tls_t* tls = (cairo_gpu_space_tls_t*)calloc(sizeof(cairo_gpu_space_tls_t), 1);
	tls->context.space = space;
	tls->context.tls = tls;

	_cairo_gpu_context_init(&tls->context);
	return tls;
}

static cairo_font_options_t *
_cairo_gpu_get_font_options (cairo_gpu_space_t* space)
{
    if (space->has_font_options)
	return &space->font_options;

    CAIRO_MUTEX_LOCK(space->mutex);
    if (! space->has_font_options) {
	_cairo_font_options_init_default (&space->font_options);

	space->font_options.antialias = CAIRO_ANTIALIAS_SUBPIXEL;

	space->has_font_options = TRUE;
    }
    CAIRO_MUTEX_UNLOCK(space->mutex);

    return &space->font_options;
}

static void
_cairo_gpu_context__destroy(cairo_gpu_context_t * ctx)
{
	ctx->pipe->destroy(ctx->pipe);
	ctx->pipe = 0;
}

static inline void
_cairo_gpu_space_tls_destroy_contexts(cairo_gpu_space_tls_t* tls)
{
	_cairo_gpu_context__destroy(&tls->context);
}

static inline cairo_gpu_context_t *
_cairo_gpu_space_tls_lookup_context(cairo_gpu_space_tls_t* tls)
{
	return &tls->context;
}

static void
_cairo_gpu_drm_space_destroy(cairo_gpu_space_t* space);

static void
_cairo_gpu_space_destroy(void* abstract_space)
{
	unsigned i;
	cairo_gpu_space_t * space = abstract_space;
	cairo_gpu_space_tls_t* tls;
	cairo_gpu_space_tls_t* tls_next;
	cairo_gpu_context_t* ctx;

	pthread_key_delete(space->tls);

	for(tls = (cairo_gpu_space_tls_t*)space->tls_list.next; (list_node_t*)tls != &space->tls_list; tls = tls_next)
	{
		tls_next = (cairo_gpu_space_tls_t*)tls->node.next;
		ctx = &tls->context;

		ctx->pipe->bind_blend_state(ctx->pipe, 0);
		ctx->pipe->bind_fs_state(ctx->pipe, 0);
		ctx->pipe->bind_vs_state(ctx->pipe, 0);
		ctx->pipe->bind_rasterizer_state(ctx->pipe, 0);
		ctx->pipe->bind_sampler_states(ctx->pipe, 0, 0);
		ctx->pipe->bind_depth_stencil_alpha_state(ctx->pipe, 0);
	}

	ctx = _cairo_gpu_space_bind(space);
	_cairo_hash_table_foreach(space->table, _cairo_gpu_space__fini_entry, ctx);
	_cairo_hash_table_destroy(space->table);

	for(i = 0; i < sizeof(space->rasterizer) / sizeof(space->rasterizer[0]); ++i)
	{
		if(space->rasterizer[i])
			ctx->pipe->delete_rasterizer_state(ctx->pipe, space->rasterizer[i]);
	}

	if(space->zsa)
		ctx->pipe->delete_depth_stencil_alpha_state(ctx->pipe, space->zsa);

	_cairo_gpu_texture_fini(0, &space->dummy_texture);

	for(tls = (cairo_gpu_space_tls_t*)space->tls_list.next; (list_node_t*)tls != &space->tls_list; tls = tls_next)
	{
		tls_next = (cairo_gpu_space_tls_t*)tls->node.next;

		_cairo_gpu_space_tls_destroy(tls);
	}

	if(space->owns_screen)
		space->screen->destroy(space->screen);

	space->screen = 0;

	if(space->api == API_DRM)
		_cairo_gpu_drm_space_destroy(space);
}

/* only current thread */
static cairo_status_t
_cairo_gpu_space_sync(void* abstract_space)
{
	cairo_gpu_space_t* space = abstract_space;
	cairo_gpu_context_t* ctx = _cairo_gpu_space_bind(space);

	struct pipe_fence_handle *fence = NULL;

	ctx->pipe->flush(ctx->pipe, PIPE_FLUSH_RENDER_CACHE | PIPE_FLUSH_FRAME, &fence);

	if(fence)
	{
		 ctx->space->screen->fence_finish(ctx->space->screen, fence, 0);
		 ctx->space->screen->fence_reference(ctx->space->screen, &fence, NULL);
	 }

	return CAIRO_STATUS_SUCCESS;
}

static void
_cairo_gpu_space__finish_create(cairo_gpu_space_t* space, unsigned flags)
{
	const char* env = getenv("__CAIRO_GPU_GALLIUM_FLAGS");
	if(env)
		flags |= atoi(env);

	space->base.is_software = !strcmp(space->screen->get_name(space->screen), "softpipe");

	if(flags & CAIRO_GPU_GALLIUM_TRACE)
	{
		space->real_screen = space->screen;
		space->screen = trace_screen_create(space->screen);
	}

	space->use_fbo = 1;
	space->tex_npot = !(flags & CAIRO_GPU_GALLIUM_DISABLE_TEXTURE_NON_POWER_OF_TWO)
		&& space->screen->get_param(space->screen, PIPE_CAP_NPOT_TEXTURES);
	space->tex_rectangle = 0;

	space->extend_mask = 1 << CAIRO_EXTEND_REPEAT;
	space->extend_mask |= 1 << CAIRO_EXTEND_NONE;
	space->extend_mask |= 1 << CAIRO_EXTEND_PAD;

	if(space->screen->get_param(space->screen, PIPE_CAP_TEXTURE_MIRROR_REPEAT))
		space->extend_mask |= 1 << CAIRO_EXTEND_REFLECT;

	space->vert_op = space->vert_passthru = 1;
	space->per_component = 1;
	space->radial = space->frag_div_alpha = space->discontinuous = 1;
	space->frag_mul_alpha = 1;
	space->tex_aaaa_111a = 1;
	space->frag_passthru = 1;
	space->blend_func_separate = 1;
	space->blend_color = 1;
	space->blend_subtract = 1;

	space->msaa_samples = 16; /* optimistic value, will be downgraded if necessary */

	// TODO: this is true on nVidia G70, which does the equivalent of 2x2 MSAA
	space->fastest_polygon_smooth_samples = 4;
	space->nicest_polygon_smooth_samples = 4;

	space->max_anisotropy = space->screen->get_paramf(space->screen, PIPE_CAP_MAX_TEXTURE_ANISOTROPY);
}

static inline cairo_bool_t
_cairo_gpu_space_is_frag_supported(cairo_gpu_space_t* space, unsigned frag)
{
	return 1;
}

static cairo_bool_t
_cairo_gpu_hash_keys_equal(const void *key_a, const void *key_b)
{
	return ((cairo_hash_entry_t*)key_a)->hash == ((cairo_hash_entry_t*)key_b)->hash;
}

static cairo_gpu_space_t *
_cairo_gpu_space__begin_create(void)
{
	cairo_gpu_space_t *space;

	space = calloc(1, sizeof(cairo_gpu_space_t));
	if(!space)
		return 0;

	CAIRO_REFERENCE_COUNT_INIT(&space->base.ref_count, 1);
	CAIRO_MUTEX_INIT(space->mutex);
	CAIRO_MUTEX_INIT(space->cached_mask_surface_mutex);
	space->base.backend = &_cairo_gpu_space_backend;
	space->tls_list.prev = &space->tls_list;
	space->tls_list.next = &space->tls_list;
	pthread_key_create(&space->tls, _cairo_gpu_space_tls_dtor);
	space->table = _cairo_hash_table_create(_cairo_gpu_hash_keys_equal);

	return space;
}

#include "cairo-gpu-impl-space-gallium-softpipe.h"
#include "cairo-gpu-impl-space-gallium-drm.h"


cairo_space_t*
cairo_gallium_hardpipe_space_create (unsigned flags)
{
	cairo_space_t* space;

	space = cairo_gallium_drm_space_create(0, flags);
	if(space)
	{
		if(space->is_software)
			cairo_space_destroy(space);
		else
			return space;
	}

	space = cairo_gallium_x11_space_create(0, flags);
	if(space)
	{
		if(space->is_software)
			cairo_space_destroy(space);
		else
			return space;
	}

	return 0;
}

cairo_space_t*
cairo_gallium_space_create (unsigned flags)
{
	cairo_space_t* space = cairo_gallium_hardpipe_space_create(flags);
	if(space)
		return space;

	return cairo_gallium_softpipe_space_create(flags);
}

cairo_space_t *
cairo_gallium_space_wrap(struct pipe_screen* screen, struct pipe_context* (*context_create)(void*, struct pipe_screen*), void* cookie, unsigned flags)
{
	cairo_gpu_space_t *space;
	space = _cairo_gpu_space__begin_create();
	if(!space)
		return 0;

	space->api = API_USER;
	space->screen = (struct pipe_screen*)screen;
	space->user.context_create = context_create;
	space->user.cookie = cookie;

	_cairo_gpu_space__finish_create(space, flags);
	return &space->base;
}

struct pipe_screen*
cairo_gallium_space_get_screen(cairo_space_t* abstract_space)
{
	cairo_gpu_space_t* space = (cairo_gpu_space_t*)abstract_space;
	if(abstract_space->backend != &_cairo_gpu_space_backend)
		return 0;

	return space->screen;
}

static struct pipe_context*
_cairo_gallium_space_create_context(cairo_gpu_space_t* space)
{
	struct pipe_screen* screen;

	screen = space->real_screen ? space->real_screen : space->screen;

	if(space->api == API_SOFTPIPE)
		return softpipe_create(screen);
	else if(space->api == API_USER)
		return (struct pipe_context*)space->user.context_create(space->user.cookie, screen);
	else if(space->api == API_DRM)
		return space->drm.api->create_context(space->drm.api, screen);
	else
		abort();
}

struct pipe_context*
cairo_gallium_space_create_context(cairo_space_t* abstract_space)
{
	cairo_gpu_space_t* space = (cairo_gpu_space_t*)abstract_space;

	if(abstract_space->backend != &_cairo_gpu_space_backend)
		return 0;

	return _cairo_gallium_space_create_context(space);
}
