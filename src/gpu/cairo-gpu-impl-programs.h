typedef struct
{
	char* buf;
	size_t len;
	size_t size;
} cairo_gpu_string_builder_t;

static inline void
_cairo_gpu_string_builder_enlarge(cairo_gpu_string_builder_t* b, size_t len)
{
	if(!b->size)
		b->size = 16;

	while((b->len + len) > b->size)
		b->size <<= 1;

	/*
	buf = malloc(b->size);
	if(b->buf)
	{
		memcpy(buf, b->buf, b->len);
		free(b->buf);
	}
	b->buf = buf;
	*/

	b->buf = realloc(b->buf, b->size);
}

static void
_cairo_gpu_string_builder_write(cairo_gpu_string_builder_t* b, const char* x, int len)
{
	if((b->len + len) > b->size)
		_cairo_gpu_string_builder_enlarge(b, len);

	memcpy(b->buf + b->len, x, len);
	b->len += len;
}

static  __attribute__ ((__format__ (__printf__, 2, 3))) void
_cairo_gpu_string_builder_writef(cairo_gpu_string_builder_t* b, const char* fmt, ...)
{
	int n = 0;
	for(;;)
	{
		if(b->buf)
		{
			va_list ap;

			va_start(ap, fmt);
			n = vsnprintf(b->buf + b->len, b->size - b->len, fmt, ap);
			va_end(ap);

			if((b->len + n) < b->size)
				break;
		}

		_cairo_gpu_string_builder_enlarge(b, n + 1);
	}

	b->len += n;
}

#define OUT_(x) _cairo_gpu_string_builder_write(builder, x, strlen(x))
#define OUT(x) OUT_(x L_TERMINATOR "\n")
#define OUTF(x, args...) _cairo_gpu_string_builder_writef(builder, x L_TERMINATOR "\n", ## args)

typedef struct
{
	cairo_gpu_string_builder_t main;
	cairo_gpu_string_builder_t body;

	const char* in_position;
	const char* in_color;
	const char* in_texcoord[2];

	const char* out_position;
	const char* out_color;
	const char* out_texcoord[2];

	int has_01_swizzles;
	int div_uses;
	int dp2a_uses;
	int result_color_uses;
	int tmp_uses;

	const char* color;
	const char* color_swizzle;
	const char* color_alpha_swizzle;
	int result_color;
	int result_color_a;
	size_t len;
	size_t size;

	char color_buf[64];
} cairo_gpu_program_builder_t;

static inline void
_cairo_gpu_program_builder_color_mod_insn(cairo_gpu_program_builder_t* p, unsigned tex, int component, const char* insn, const char* args)
{
	cairo_gpu_string_builder_t* builder = &p->body;
	char wmask_buf[4];
	const char* wmask;
	const char* tmp;
	const char* swizzle = "";
	const char* alpha_swizzle = ".wwww";

	const char* swizzles[] = {".111w", ".111x", ".111y", ".111z"};
	const char* alpha_swizzles[] = {".wwww", ".xxxx", ".yyyy", ".zzzz"};

	if(tex == FRAG_TEX_COLOR_111C)
	{
		wmask_buf[0] = '.';
		wmask_buf[1] = "wxyz"[component];
		wmask_buf[2] = 0;
		wmask = wmask_buf;
	}
	else if(tex == FRAG_TEX_COLOR_111CA)
	{
		wmask_buf[0] = '.';
		wmask_buf[1] = "wxyz"[component];
		wmask_buf[2] = 'w';
		wmask_buf[3] = 0;
		wmask = wmask_buf;
	}
	else if(tex == FRAG_TEX_COLOR_RGBA)
		wmask = "";
	else
		wmask = ".w";

	if(p->color)
	{
		tmp = L_TMP;
		++p->tmp_uses;
	}
	else
	{
		tmp = L_RESULT_COLOR_TEMP;
		++p->result_color_uses;
		if(tex == FRAG_TEX_COLOR_RGBA)
			p->result_color = p->result_color_a = builder->len + strlen(insn) + 1;
	}

	OUTF("%s %s%s, %s", insn, tmp, wmask,  args);

	if(tex == FRAG_TEX_COLOR_111CA || tex == FRAG_TEX_COLOR_111C)
	{
		if(tex == FRAG_TEX_COLOR_111CA)
		{
			p->result_color = p->result_color_a = builder->len + 4;
			OUTF("MUL %s.w, %s.wwww, %s%s", tmp, tmp, tmp, alpha_swizzles[component]);
		}
		if(!p->has_01_swizzles)
		{
			p->result_color = p->result_color_a = builder->len + 4;
			OUTF("SWZ %s, %s, 1, 1, 1, %c", tmp, tmp, "wxyz"[component]);
		}
		else
		{
			swizzle = swizzles[component];
			alpha_swizzle = alpha_swizzles[component];
		}
	}
	else if(tex == FRAG_TEX_COLOR_111A)
	{
		p->result_color = p->result_color_a = builder->len + 4;
		if(!p->has_01_swizzles)
		{
			p->result_color = p->result_color_a = builder->len + 4;
			OUTF("SWZ %s, %s, 1, 1, 1, w", tmp, tmp);
		}
		else
		{
			swizzle = ".111w";
		}
	}
	else if(tex == FRAG_TEX_COLOR_AAAA)
		swizzle = ".wwww";

	if(p->color)
	{
		p->result_color = p->result_color_a = builder->len + 4;
		OUTF("MUL " L_RESULT_COLOR_TEMP ", %s%s, %s%s", p->color, p->color_swizzle, tmp, swizzle);
		++p->result_color_uses;

		p->color_swizzle = "";
		p->color_alpha_swizzle = ".wwww";
	}
	else
	{
		p->color_swizzle = swizzle;
		p->color_alpha_swizzle = alpha_swizzle;
	}
	p->color = L_RESULT_COLOR_TEMP;
}

static inline void
_cairo_gpu_program_builder_color_mod(cairo_gpu_program_builder_t* p, const char* v)
{
	cairo_gpu_string_builder_t* builder = &p->body;
	if(p->color)
	{
		p->result_color = p->result_color_a = builder->len + 4;
		OUTF("MUL " L_RESULT_COLOR_TEMP ", %s%s, %s", p->color, p->color_swizzle, v);
		++p->result_color_uses;
		p->color = L_RESULT_COLOR_TEMP;
		p->color_swizzle = "";
		p->color_alpha_swizzle = ".wwww";
	}
	else
	{
		strcpy(p->color_buf, v);
		p->color = p->color_buf;
		p->color_swizzle = "";
		p->color_alpha_swizzle = ".wwww";
	}
}

static inline void
_cairo_gpu_program_builder_color_op(cairo_gpu_program_builder_t* p, unsigned op)
{
	cairo_gpu_string_builder_t* builder = &p->body;
	if(p->color)
	{
		if(op & OP_DIV_ALPHA)
		{
			if(!strcmp(p->color_swizzle, ".wwww"))
			{
				if(op == OP_DIV_ALPHA_RGBA)
				{
					assert(0); // where was the optimizer?!
					p->result_color = p->result_color_a = builder->len + 4;
					OUTF("MOV " L_RESULT_COLOR_TEMP ", " L_1);
				}
				else
				{
					if(!p->has_01_swizzles)
					{
						p->result_color = p->result_color_a = builder->len + 4;
						OUTF("SWZ " L_RESULT_COLOR_TEMP ", %s, 1, 1, 1, w", p->color);
					}
					else
					{
						p->color_swizzle = ".111w";
					}
				}
			}
			else
			{
				if(p->div_uses >= 0)
				{
					p->result_color = builder->len + 4;
					OUTF("DIV " L_RESULT_COLOR_TEMP ".xyz, %s%s, %s%s", p->color, p->color_swizzle, p->color, p->color_alpha_swizzle);
					++p->div_uses;
				}
				else
				{
					// TODO: what happens if " L_TMP ".w is zero? Does OpenGL guarantee that 0 * (1 / 0) = 0? Or should we add an epsilon value before RCP?
					OUTF("RCP " L_TMP ".w, %s%s", p->color, p->color_alpha_swizzle);
					p->result_color = builder->len + 4;
					OUTF("MUL " L_RESULT_COLOR_TEMP ".xyz, %s%s, " L_TMP, p->color, p->color_swizzle);
					++p->tmp_uses;
				}
				++p->result_color_uses;

				if(op == OP_DIV_ALPHA_RGBA)
				{
					p->result_color_a = builder->len + 4;
					++p->result_color_uses;
					OUT("MOV " L_RESULT_COLOR_TEMP ".w, " L_1);
				}
				else if(strcmp(p->color, L_RESULT_COLOR_TEMP))
				{
					p->result_color_a = builder->len + 4;
					++p->result_color_uses;
					OUTF("MOV " L_RESULT_COLOR_TEMP ".w, %s%s", p->color, p->color_alpha_swizzle);
				}
				else
					p->result_color = p->result_color_a = -1;
			}
			p->color = L_RESULT_COLOR_TEMP;
			p->color_swizzle = "";
			p->color_alpha_swizzle = ".wwww";
		}
		else if(op == OP_MUL_ALPHA)
		{
			p->result_color = builder->len + 4;
			++p->result_color_uses;
			OUTF("MUL " L_RESULT_COLOR_TEMP ".xyz, %s%s, %s.wwww", p->color, p->color_swizzle, p->color);

			if(strcmp(p->color, L_RESULT_COLOR_TEMP))
			{
				p->result_color_a = builder->len + 4;
				++p->result_color_uses;
				OUTF("MOV " L_RESULT_COLOR_TEMP ".w, %s.w", p->color);
			}
			else
				p->result_color = p->result_color_a = 0;
			p->color = L_RESULT_COLOR_TEMP;
			p->color_swizzle = "";
			p->color_alpha_swizzle = ".wwww";
		}
	}
}

static inline void
_cairo_gpu_program_builder_color_write(cairo_gpu_program_builder_t* p)
{
	cairo_gpu_string_builder_t* builder = &p->body;
	if(!p->color)
	{
		p->color = L_1;
		p->color_swizzle = "";
		p->color_alpha_swizzle = ".wwww";
	}
	else
	{
		if(p->result_color > 0)
		{
			L_SET_RESULT_COLOR(&builder->buf[p->result_color]);
			--p->result_color_uses;
		}
		if(p->result_color_a > 0 && p->result_color_a != p->result_color)
		{
			L_SET_RESULT_COLOR(&builder->buf[p->result_color_a]);
			--p->result_color_uses;
		}
	}

	if(!p->result_color)
		OUTF("MOV %s, %s%s", p->out_color, p->color, p->color_swizzle);
}

static inline char*
_cairo_gpu_program_builder_finish(cairo_gpu_program_builder_t* p)
{
	cairo_gpu_string_builder_t* builder = &p->main;
	if(p->result_color_uses)
		OUT(L_TEMP_RESULT_COLOR);
	if(p->tmp_uses)
		OUT(L_TEMP_TMP);

	if(p->body.buf)
	{
		_cairo_gpu_string_builder_write(builder, p->body.buf, p->body.len);
		free(p->body.buf);
	}

	_cairo_gpu_string_builder_write(builder, "END\n", 5); // add null terminator as well

	//printf("%s\n", builder->buf);

	return builder->buf;
}

static inline void
_cairo_gpu_write_vert_position(cairo_gpu_program_builder_t* p, unsigned vert)
{
	cairo_gpu_string_builder_t* builder = &p->body;
	if(p->has_01_swizzles)
		OUTF("MAD %s, %s, " L_PROGRAM_ENV "[0].xy00, " L_PROGRAM_ENV "[0].zw11", p->out_position, p->in_position);
	else
	{
		OUTF("MAD %s.xy, %s, " L_PROGRAM_ENV "[0], " L_PROGRAM_ENV "[0].zwxy", p->out_position, p->in_position);
		OUTF("MOV %s.zw, " L_1, p->out_position);
	}
}

static inline void
_cairo_gpu_write_vert(cairo_gpu_program_builder_t* p, unsigned vert)
{
	cairo_gpu_string_builder_t* builder = &p->body;
	int i;
	int pos;
	int passthru_tex = vert &  (VERT_PASSTHRU_PREOP * 3) ? !!(vert & VERT_PASSTHRU_TEX1) : -1;

	for(i = 0; i < MAX_OPERANDS; ++i)
	{
		int k = (vert >> (VERT_TEX_SHIFT + i * VERT_TEX_BITS)) & VERT_TEX_MASK;
		if((k & VERT_TEX_GEN) && i != passthru_tex)
		{
			const char* input;

			if(k == VERT_TEX_GEN)
				input = p->in_position;
			else
				input = p->in_texcoord[k - 1];

			++p->tmp_uses;
			OUTF("MAD " L_TMP ", %s.xxxx, " L_PROGRAM_ENV "[%i], " L_PROGRAM_ENV "[%i]",  input, VERTENV_TEX_MATRIX_X(i), VERTENV_TEX_MATRIX_W(i));
			OUTF("MAD %s, %s.yyyy, " L_PROGRAM_ENV "[%i], " L_TMP, p->out_texcoord[i], input, VERTENV_TEX_MATRIX_Y(i));
		}
	}

	for(pos = 0;; ++pos)
	{
		if(vert & (VERT_PASSTHRU_PREOP << pos))
		{
			int k;
			const char* input;
			const char* tmp;
			k = (vert >> (VERT_TEX_SHIFT + passthru_tex * VERT_TEX_BITS)) & VERT_TEX_MASK;

			if(k == VERT_TEX_GEN)
				input = p->in_position;
			else
				input = p->in_texcoord[k - 1];

			if(!p->color)
			{
				tmp = L_RESULT_COLOR_TEMP;
				p->result_color_uses += 3;
			}
			else
			{
				tmp = L_TMP;
				p->tmp_uses += 3;
			}

			OUTF("MUL %s, %s.xxxx, " L_PROGRAM_ENV "[0]", tmp, input);
			OUTF("MAD %s, %s.yyyy, " L_PROGRAM_ENV "[1], %s", tmp, input, tmp);
			if(!p->color)
				p->result_color = builder->len + 4;
			OUTF("ADD %s, %s, " L_PROGRAM_ENV "[2]", tmp, tmp);
			_cairo_gpu_program_builder_color_mod(p, tmp);
		}

		if(vert & (VERT_COLOR_PREOP << pos))
			_cairo_gpu_program_builder_color_mod(p, p->in_color);

		if(pos == 1)
			break;

		_cairo_gpu_program_builder_color_op(p, (vert & VERT_OP_MASK) >> VERT_OP_SHIFT);
	}

	if(p->color)
		_cairo_gpu_program_builder_color_write(p);
}

static inline void
_cairo_gpu_write_frag(cairo_gpu_program_builder_t* p, unsigned frag)
{
	cairo_gpu_string_builder_t* builder = &p->body;
	int i;

	for(i = 0; i < MAX_OPERANDS; ++i)
	{
		unsigned tex = frag >> (FRAG_TEX_SHIFT + i * FRAG_TEX_BITS);
		if(tex & FRAG_TEX_MASK)
		{
			char params_buf[64];
			const char* coord;

			if(tex & FRAG_TEX_RADIAL)
			{
				// x = (((x, y, 1) . p1) + sqrt(((x, y, 1) . p1)^2 + ((x^2, y^2, 1) . p2))) * p3.x + p3.z
				// y = undef * 0 + p3.w
				/*
				OUTF("MUL " L_TMP ".xyw, fragment.texcoord[%i], fragment.texcoord[%i]", i, i);
				if(GLEW_NV_fragment_program2)
				{
					OUTF("DP2A " L_TMP ".x, " L_TMP ", " L_PROGRAM_ENV "[%i], " L_PROGRAM_ENV "[%i].w", FRAGENV_TEX_RADIAL_MAC(i), FRAGENV_TEX_RADIAL_MAC(i));
					OUTF("DP2A " L_TMP ".w, fragment.texcoord[%i], " L_PROGRAM_ENV "[%i], " L_PROGRAM_ENV "[%i].z", i, FRAGENV_TEX_RADIAL_MBD2(i), FRAGENV_TEX_RADIAL_MBD2(i));
					++nv_fragment_program2_uses;
				}
				else
				{
					OUTF("DP3 " L_TMP ".x, " L_TMP ".xyww, " L_PROGRAM_ENV "[%i].xywz", FRAGENV_TEX_RADIAL_MAC(i));
					OUTF("DP3 " L_TMP ".w, fragment.texcoord[%i].xyww, " L_PROGRAM_ENV "[%i].xyzw", i, FRAGENV_TEX_RADIAL_MBD2(i));
				}
				OUT("MAD " L_TMP ".x, " L_TMP ".w, " L_TMP ".w, " L_TMP ".x");
				*/

				// x = (x, y, 1) . p1 + sqrt((x, y, (x, y, 1) . p1)^2 .H p2) * p3.x + p3.z
				// y = undef * 0 + p3.w

				OUTF("MUL " L_TMP ", %s, %s", p->in_texcoord[i], p->in_texcoord[i]);
				OUTF("DPH " L_TMP ".x, " L_TMP ", " L_PROGRAM_ENV "[%i]", FRAGENV_TEX_RADIAL_MAC(i));
				OUT("RSQ " L_TMP ".x, " L_TMP L_SCALAR_X);
				OUT("RCP " L_TMP ".x, " L_TMP L_SCALAR_X);
				// We could save an instruction here by spending an interpolated attrib and complicating the code. Avoid for now
				OUTF("ADD " L_TMP ".x, " L_TMP ", %s.zzzz", p->in_texcoord[i]);
				OUTF("MAD " L_TMP ".xy, " L_TMP ".xyxy, " L_PROGRAM_ENV "[%i].xyzw, " L_PROGRAM_ENV "[%i].zwxy", FRAGENV_TEX_RADIAL_SO(i), FRAGENV_TEX_RADIAL_SO(i));
				coord = L_TMP;
				++p->tmp_uses;
			}
			else
			{
				coord = p->in_texcoord[i];
			}

			if(tex & FRAG_TEX_DISCONTINUOUS)
			{
				// TODO: this is somewhat broken, because it results in "sharp edges" and sometimes 2x2 artifacts at discontinuities
				// This is especially visible in radial gradients, where an aliased circle will be visible
				// Note however that this matches the behavior of pixman, so we consider it OK for now.
				// Furthermore, it may be considered a feature for linear gradients, and radial gradients are seldom used.
				// To implement smoothing, we need to sample twice considering partial derivatives available (easy even without DDX/DDY)

				// TODO: on ATI FLR uses 2 instructions. Maybe we can do better.
				OUTF("FLR " L_TMP ".zw, %s.xyxy", coord);
				OUTF("ADD " L_TMP ".xy, %s.xyxy, " L_TMP ".zwzw", coord);
				OUTF("MAD " L_TMP ".x, " L_TMP ", " L_PROGRAM_ENV "[%i], " L_PROGRAM_ENV "[%i].yyyy", FRAGENV_TEX_DISCONTINUOUS_SO(i), FRAGENV_TEX_DISCONTINUOUS_SO(i));
				coord = "" L_TMP "";
				++p->tmp_uses;
			}

			sprintf(params_buf, "%s, " L_TEXTURE "[%i], %s", coord, i, (tex & FRAG_TEX_RECTANGLE) ? "RECT" : "2D");

			_cairo_gpu_program_builder_color_mod_insn(p, tex & FRAG_TEX_COLOR_MASK, (int)((tex >> FRAG_COMPONENT_SHIFT) & 3), "TEX", params_buf);
		}

		{
			unsigned op = frag & FRAG_OP_MASK;
			if(op && (!i || (frag & FRAG_OPPOS_TEX1)))
				_cairo_gpu_program_builder_color_op(p, op >> FRAG_OP_SHIFT);
		}
	}

	if(frag & FRAG_PRIMARY)
		_cairo_gpu_program_builder_color_mod(p, p->in_color);

	if(frag & FRAG_CONSTANT)
		_cairo_gpu_program_builder_color_mod(p, L_PROGRAM_ENV "[0]");

	_cairo_gpu_program_builder_color_write(p);
}
