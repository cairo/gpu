// adapted copy of state_trackers/python/st_softpipe_winsys.c

struct st_softpipe_buffer
{
   struct pipe_buffer base;
   boolean userBuffer;  /** Is this a user-space buffer? */
   void *data;
   void *mapped;
};


/** Cast wrapper */
static inline struct st_softpipe_buffer *
st_softpipe_buffer( struct pipe_buffer *buf )
{
   return (struct st_softpipe_buffer *)buf;
}


static void *
st_softpipe_buffer_map(struct pipe_winsys *winsys,
                       struct pipe_buffer *buf,
                       unsigned flags)
{
   struct st_softpipe_buffer *st_softpipe_buf = st_softpipe_buffer(buf);
   st_softpipe_buf->mapped = st_softpipe_buf->data;
   return st_softpipe_buf->mapped;
}


static void
st_softpipe_buffer_unmap(struct pipe_winsys *winsys,
                         struct pipe_buffer *buf)
{
   struct st_softpipe_buffer *st_softpipe_buf = st_softpipe_buffer(buf);
   st_softpipe_buf->mapped = NULL;
}


static void
st_softpipe_buffer_destroy(struct pipe_buffer *buf)
{
   struct st_softpipe_buffer *oldBuf = st_softpipe_buffer(buf);

   if (oldBuf->data) {
      if (!oldBuf->userBuffer)
         free(oldBuf->data);

      oldBuf->data = NULL;
   }

   free(oldBuf);
}


static void
st_softpipe_flush_frontbuffer(struct pipe_winsys *winsys,
                              struct pipe_surface *surf,
                              void *context_private)
{
}



static const char *
st_softpipe_get_name(struct pipe_winsys *winsys)
{
   return "softpipe";
}

#include <malloc.h>

static struct pipe_buffer *
st_softpipe_buffer_create(struct pipe_winsys *winsys,
                          unsigned alignment,
                          unsigned usage,
                          unsigned size)
{
   struct st_softpipe_buffer *buffer = calloc(sizeof(struct st_softpipe_buffer), 1);

   pipe_reference_init(&buffer->base.reference, 1);
   buffer->base.alignment = alignment;
   buffer->base.usage = usage;
   buffer->base.size = size;

   //if(posix_memalign(&buffer->data, alignment, size))
   if(!(buffer->data = memalign(alignment, size)))
   {
	   free(buffer);
	   return 0;
   }

   return &buffer->base;
}


/**
 * Create buffer which wraps user-space data.
 */
static struct pipe_buffer *
st_softpipe_user_buffer_create(struct pipe_winsys *winsys,
                               void *ptr,
                               unsigned bytes)
{
   struct st_softpipe_buffer *buffer;

   buffer = calloc(sizeof(struct st_softpipe_buffer), 1);
   if(!buffer)
      return NULL;

   pipe_reference_init(&buffer->base.reference, 1);
   buffer->base.size = bytes;
   buffer->userBuffer = TRUE;
   buffer->data = ptr;

   return &buffer->base;
}


/**
 * Round n up to next multiple.
 */
static inline unsigned
round_up(unsigned n, unsigned multiple)
{
   return (n + multiple - 1) & ~(multiple - 1);
}


static struct pipe_buffer *
st_softpipe_surface_buffer_create(struct pipe_winsys *winsys,
                                  unsigned width, unsigned height,
                                  enum pipe_format format,
                                  unsigned usage,
                                  unsigned *stride)
{
   const unsigned alignment = 64;
   struct pipe_format_block block;
   unsigned nblocksx, nblocksy;

   pf_get_block(format, &block);
   nblocksx = pf_get_nblocksx(&block, width);
   nblocksy = pf_get_nblocksy(&block, height);
   *stride = round_up(nblocksx * block.size, alignment);

   return winsys->buffer_create(winsys, alignment,
                                usage,
                                *stride * nblocksy);
}


static void
st_softpipe_fence_reference(struct pipe_winsys *winsys,
                            struct pipe_fence_handle **ptr,
                            struct pipe_fence_handle *fence)
{
}


static int
st_softpipe_fence_signalled(struct pipe_winsys *winsys,
                            struct pipe_fence_handle *fence,
                            unsigned flag)
{
   return 0;
}


static int
st_softpipe_fence_finish(struct pipe_winsys *winsys,
                         struct pipe_fence_handle *fence,
                         unsigned flag)
{
   return 0;
}


static void
st_softpipe_destroy(struct pipe_winsys *winsys)
{
   free(winsys);
}


static struct pipe_screen *
st_softpipe_screen_create(void)
{
   static struct pipe_winsys *winsys;
   struct pipe_screen *screen;

   winsys = calloc(sizeof(struct pipe_winsys), 1);
   if(!winsys)
      return NULL;

   winsys->destroy = st_softpipe_destroy;

   winsys->buffer_create = st_softpipe_buffer_create;
   winsys->user_buffer_create = st_softpipe_user_buffer_create;
   winsys->buffer_map = st_softpipe_buffer_map;
   winsys->buffer_unmap = st_softpipe_buffer_unmap;
   winsys->buffer_destroy = st_softpipe_buffer_destroy;

   winsys->surface_buffer_create = st_softpipe_surface_buffer_create;

   winsys->fence_reference = st_softpipe_fence_reference;
   winsys->fence_signalled = st_softpipe_fence_signalled;
   winsys->fence_finish = st_softpipe_fence_finish;

   winsys->flush_frontbuffer = st_softpipe_flush_frontbuffer;
   winsys->get_name = st_softpipe_get_name;

   screen = softpipe_create_screen(winsys);
   if(!screen)
      st_softpipe_destroy(winsys);

   return screen;
}

cairo_space_t *
cairo_gallium_softpipe_space_create(unsigned flags)
{
	cairo_gpu_space_t *space;
	space = _cairo_gpu_space__begin_create();
	if(!space)
		return 0;

	space->api = API_SOFTPIPE;
	space->screen = st_softpipe_screen_create();
	space->owns_screen = 1;

	_cairo_gpu_space__finish_create(space, flags);
	return &space->base;
}
