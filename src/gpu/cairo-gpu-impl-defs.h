/* These values are used by both OpenGL and Gallium */
#define PRIM_POINTS          0
#define PRIM_LINES           1
#define PRIM_LINE_LOOP       2
#define PRIM_LINE_STRIP      3
#define PRIM_TRIANGLES       4
#define PRIM_TRIANGLE_STRIP  5
#define PRIM_TRIANGLE_FAN    6
#define PRIM_QUADS           7
#define PRIM_QUAD_STRIP      8
#define PRIM_POLYGON         9

#define TARGET_2D 0
#define TARGET_RECTANGLE 1

#define FB_DRAW 1
#define FB_READ 2

#define MAX_SMALL_WIDTH 16
#define MAX_OPERANDS 2

// TEMP or QUAD geometry is clobbered by _cairo_gpu_setup
#define GEOM_TEMP 0
#define GEOM_QUAD 1
#define GEOM_BLIT 2

// TRAPS and GLYPHS are different because they are held across composite
#define GEOM_TRAPS 3
#define GEOM_GLYPHS 4
#define GEOM_SPANS GEOM_TEMP

#define GEOMS_COUNT 5

#define OP_MUL_ALPHA 1
#define OP_DIV_ALPHA 2
#define OP_DIV_ALPHA_RGBA 3

#define VERT_SHIFT 0
#define VERT_TEX_SHIFT VERT_SHIFT
#define VERT_TEX_BITS 2
// assert ((1 << VERT_TEX_BITS) - 2) >= MAX_OPERANDS
#define VERT_TEX_GEN ((1 << VERT_TEX_BITS) - 1)
#define VERT_TEX_MASK ((1 << VERT_TEX_BITS) - 1)

#define VERT_POST_TEX_SHIFT (VERT_TEX_SHIFT + VERT_TEX_BITS * MAX_OPERANDS)

#define VERT_COLOR_PREOP (1 << VERT_POST_TEX_SHIFT)
#define VERT_COLOR_POSTOP (2 << VERT_POST_TEX_SHIFT)
#define VERT_PASSTHRU_PREOP (4 << VERT_POST_TEX_SHIFT)
#define VERT_PASSTHRU_POSTOP (8 << VERT_POST_TEX_SHIFT)
#define VERT_PASSTHRU_TEX1 (0x10 << VERT_POST_TEX_SHIFT)

#define VERT_OP_MASK (0x60 << VERT_POST_TEX_SHIFT)
#define VERT_OP_SHIFT (VERT_POST_TEX_SHIFT + 5)
#define VERT_PROG_MASK ((0x80 << VERT_POST_TEX_SHIFT) - 1)
#define VERT_MASK ((0x80 << VERT_POST_TEX_SHIFT) - 1)

// this may result in 16K different programs! Maybe we should use "super-shaders"?
#define FRAG_TEX_COLOR_RGBA 1
#define FRAG_TEX_COLOR_111A 2
#define FRAG_TEX_COLOR_AAAA 3
#define FRAG_TEX_COLOR_111C 4
#define FRAG_TEX_COLOR_111CA 5
#define FRAG_TEX_COLOR_MASK 7
#define FRAG_TEX_RECTANGLE 8
#define FRAG_TEX_RADIAL 0x10
#define FRAG_TEX_DISCONTINUOUS 0x20

#define FRAG_TEX_BITS 6
#define FRAG_TEX_MASK ((1 << FRAG_TEX_BITS) - 1)

#define FRAG_SHIFT 0
#define FRAG_TEX_SHIFT FRAG_SHIFT

#define FRAG_POST_TEX_SHIFT (FRAG_TEX_SHIFT + FRAG_TEX_BITS * MAX_OPERANDS)
#define FRAG_PRIMARY (1 << FRAG_POST_TEX_SHIFT)
#define FRAG_CONSTANT (2 << FRAG_POST_TEX_SHIFT)
#define FRAG_OP_MASK (0xc << FRAG_POST_TEX_SHIFT)
#define FRAG_OP_SHIFT (FRAG_POST_TEX_SHIFT + 2)
#define FRAG_OPPOS_TEX1 (0x10 << FRAG_POST_TEX_SHIFT)
#define FRAG_COMPONENT_SHIFT (FRAG_POST_TEX_SHIFT + 5)
#define FRAG_PROG_MASK ((0x80 << FRAG_POST_TEX_SHIFT) - 1)
#define FRAG_MASK FRAG_PROG_MASK

#define VERTENV_BASE 0
#define VERTENV_MATRIX VERTENV_BASE
#define VERTENV_PASSTHRU_X (VERTENV_BASE + 1)
#define VERTENV_PASSTHRU_Y (VERTENV_BASE + 2)
#define VERTENV_PASSTHRU_W (VERTENV_BASE + 3)
#define VERTENV_TEX(i) (VERTENV_BASE + 4 + i * 3)
#define VERTENV_TEX_MATRIX_X(i) (VERTENV_TEX(i) + 0)
#define VERTENV_TEX_MATRIX_Y(i) (VERTENV_TEX(i) + 1)
#define VERTENV_TEX_MATRIX_W(i) (VERTENV_TEX(i) + 2)
#define VERTENV_COUNT VERTENV_TEX(MAX_OPERANDS)

#define FRAGENV_BASE 0
#define FRAGENV_CONSTANT FRAGENV_BASE
#define FRAGENV_TEX(i) (FRAGENV_BASE + 1 + i * 3)
#define FRAGENV_TEX_DISCONTINUOUS_SO(i) (FRAGENV_TEX(i) + 0)
#define FRAGENV_TEX_RADIAL_MAC(i) (FRAGENV_TEX(i) + 1)
#define FRAGENV_TEX_RADIAL_SO(i) (FRAGENV_TEX(i) + 2)
#define FRAGENV_COUNT FRAGENV_TEX(MAX_OPERANDS)

/* these must be ordered like GL constants */
enum
{
	BLEND_ZERO,
	BLEND_ONE,

	BLEND_SRC_COLOR,
	BLEND_ONE_MINUS_SRC_COLOR,
	BLEND_SRC_ALPHA,
	BLEND_ONE_MINUS_SRC_ALPHA,
	BLEND_DST_ALPHA,
	BLEND_ONE_MINUS_DST_ALPHA,
	BLEND_DST_COLOR,
	BLEND_ONE_MINUS_DST_COLOR,
	BLEND_SRC_ALPHA_SATURATE, /* OpenGL interpretation */

	BLEND_CONSTANT_COLOR,
	BLEND_ONE_MINUS_CONSTANT_COLOR,
	BLEND_CONSTANT_ALPHA,
	BLEND_ONE_MINUS_CONSTANT_ALPHA,
};

#define BLEND_EQ_ADD 0
#define BLEND_EQ_SUB 1

#ifdef WORDS_BIGENDIAN
#warning Check if the following blend constants are correct or should be byteswapped
#endif

#define BLEND_FUNC_SOURCE 0x11

#define BLEND_SOURCE 0x0f000011
#define BLEND_SOURCE_ALPHAONLY 0x08000011
#define BLEND_SOURCE_COLORONLY 0x07000011
#define BLEND_CLEAR 0x0f000000
#define BLEND_ADD 0x0f001111
#define BLEND_SUB 0x0f011111
#define BLEND_SUB_ALPHAONLY 0x08011111
#define BLEND_OVER 0x0f005511

#define SURF_TEX 0
#define SURF_MSAA 1
#define SURF_FRONT 2
#define SURF_BACK 3

#define SURF_IS_DRAWABLE(i) ((int)(i) >= SURF_FRONT)

#define SURF_PSEUDO_MIPMAPS 4
#define SURF_PSEUDO_PADDING 5
#define SURF_PSEUDO_ALPHA_TO_RED 6

struct _cairo_gpu_surface;
typedef struct _cairo_gpu_surface cairo_gpu_surface_t;
struct _cairo_gpu_space;
typedef struct _cairo_gpu_texture cairo_gpu_texture_t;
typedef struct _cairo_gpu_space cairo_gpu_space_t;
typedef struct _cairo_gpu_geometry cairo_gpu_geometry_t;
typedef struct _cairo_gpu_space_tls cairo_gpu_space_tls_t;

#define CAIRO_GPU_BASE_TEXTURE_T \
	unsigned target_idx; \
	unsigned width; \
	unsigned height;

static const cairo_space_backend_t _cairo_gpu_space_backend;
static const cairo_surface_backend_t _cairo_gpu_surface_backend;

typedef struct _list_node list_node_t;

struct _list_node
{
	list_node_t* prev;
	list_node_t* next;
};

static inline void init_node(list_node_t* node)
{
	node->next = node;
	node->prev = node;
}

static inline void unlink_node(list_node_t* node)
{
	node->prev->next = node->next;
	node->next->prev = node->prev;
}

static inline void insert_after(list_node_t* list, list_node_t* node)
{
	node->prev = list;
	node->next = list->next;
	list->next->prev = node;
	list->next = node;
}

static inline void insert_before(list_node_t* list, list_node_t* node)
{
	node->prev = list->prev;
	node->next = list;
	list->prev->next = node;
	list->prev = node;
}

// TODO: the use of fastest/nicest is quite OpenGL-specific
#define CAIRO_GPU_BASE_SPACE_T \
	cairo_space_t base; \
	cairo_mutex_t mutex; \
	cairo_mutex_t cached_mask_surface_mutex; \
	cairo_gpu_surface_t *cached_mask_surface; \
	unsigned fastest_polygon_smooth_samples; \
	unsigned nicest_polygon_smooth_samples; \
	unsigned msaa_samples; \
	unsigned use_fbo : 1; \
	unsigned tex_npot : 1; \
	unsigned tex_rectangle : 1; \
	unsigned blend_func_separate : 1; \
	unsigned blend_color : 1; \
	unsigned blend_subtract : 1; \
	unsigned frag_mul_alpha : 1; \
	unsigned frag_div_alpha : 1; \
	unsigned tex_aaaa_111a : 1; \
	unsigned radial : 1; \
	unsigned discontinuous : 1; \
	unsigned per_component : 1; \
	unsigned frag_passthru : 1; \
	unsigned vert_passthru : 1; \
	unsigned vert_op : 1; \
	unsigned char extend_mask; \
	unsigned char has_font_options; \
	cairo_font_options_t font_options; \
	float max_anisotropy; \
	pthread_key_t tls; \
	list_node_t tls_list; \
	cairo_gpu_texture_t dummy_texture;

#define CAIRO_GPU_BASE_SPACE_TLS_T \
	list_node_t node; \
	cairo_gpu_space_t* space; \
	cairo_bool_t in_acquire; \
	struct \
	{ \
		cairo_gpu_texture_t* texture[MAX_SMALL_WIDTH]; \
	} small_texture_pools[MAX_OPERANDS];


#define CAIRO_GPU_BASE_SURFACE_T \
	cairo_surface_t base; \
	cairo_gpu_space_t *space; \
	unsigned width, height; \
	unsigned char want_msaa : 1; \
	unsigned char public_drawable : 1; \
	unsigned char autoflush : 1; \
	unsigned char transparent_padding : 1; \
	unsigned char coverage_samples; \
	unsigned char color_samples; \
	unsigned char valid_mask; \
	cairo_region_t clip; \
	unsigned char has_clip;

typedef struct _cairo_gpu_base_surface
{
	CAIRO_GPU_BASE_SURFACE_T;
} cairo_gpu_base_surface_t;

typedef union
{
	struct
	{
		union
		{
			struct
			{
				unsigned char src_rgb : 4;
				unsigned char src_alpha : 4;
				unsigned char dst_rgb : 4;
				unsigned char dst_alpha : 4;
			};
			unsigned short func;
		};
		unsigned char eq;
		unsigned char color_mask;
	};
	unsigned v;
} cairo_gpu_blend_t;

typedef struct
{
	float r;
	float g;
	float b;
} cairo_gpu_color_t;

typedef struct
{
	cairo_gpu_color_t c;
	float ka;
} cairo_gpu_color4_t;

static __attribute__((unused)) cairo_gpu_color4_t _cairo_gpu_black = {{0.0, 0.0, 0.0}, 0.0};
static cairo_gpu_color4_t _cairo_gpu_white = {{1.0, 1.0, 1.0}, 1.0};

static inline cairo_bool_t
_cairo_gpu_surface_enable_multisampling(void * surface, cairo_bool_t want_msaa)
{
	((cairo_gpu_base_surface_t*)surface)->want_msaa = want_msaa;
	return 1;
}

static inline cairo_bool_t
_cairo_surface_is_gpu_any(cairo_surface_t * surface)
{
	return surface->backend->type == CAIRO_SURFACE_TYPE_GPU;
}

static inline cairo_bool_t
_cairo_gpu_space_is_gpu_this(void* space)
{
	return ((cairo_space_t *)space)->backend == &_cairo_gpu_space_backend;
}

static inline cairo_bool_t
_cairo_surface_is_gpu_this(void * surface)
{
	return ((cairo_surface_t*)surface)->backend == &_cairo_gpu_surface_backend;
}

static inline cairo_bool_t
_cairo_gpu_surface_is_compatible_ (void *surface_a,
				    void *surface_b)
{
    cairo_gpu_base_surface_t *a = (cairo_gpu_base_surface_t *) surface_a;
    cairo_gpu_base_surface_t *b = (cairo_gpu_base_surface_t *) surface_b;

    return a->space == b->space;
}

static inline cairo_bool_t
_cairo_gpu_surface_is_compatible (void *surface_a,
				    void *surface_b)
{
    cairo_gpu_base_surface_t *a = (cairo_gpu_base_surface_t *) surface_a;
    cairo_gpu_base_surface_t *b = (cairo_gpu_base_surface_t *) surface_b;

    if(a->base.backend != b->base.backend)
	    return 0;

    return a->space == b->space;
}

static inline cairo_bool_t
_cairo_gpu_surface_is_similar (void *surface_a,
				    void *surface_b,
				 cairo_content_t content)
{
    return _cairo_gpu_surface_is_compatible_(surface_a, surface_b);
}

static inline unsigned is_pow2(unsigned v)
{
	return !(v & (v - 1));
}

static inline unsigned higher_pow2(unsigned int x)
{
    x |= (x >> 1);
    x |= (x >> 2);
    x |= (x >> 4);
    x |= (x >> 8);
    x |= (x >> 16);

    return (x + 1);
}

static inline uint8_t
_cairo_color_double_to_byte(double d)
{
    uint32_t i;
    i = (uint32_t) (d * 256);
    i -= (i >> 8);
    return i;
}

static inline uint8_t
_cairo_color_float_to_byte(float f)
{
    uint32_t i;
    i = (uint32_t) (f * 256);
    i -= (i >> 8);
    return i;
}


static inline void
_cairo_gpu_emit_rect(float **vp, int x, int y, int width, int height)
{
	float *v = *vp;

	v[0] = x;
	v[1] = y;
	v[2] = x + width;
	v[3] = y;
	v[4] = x + width;
	v[5] = y + height;
	v[6] = x;
	v[7] = y + height;
	*vp += 8;
}

static inline void
_cairo_gpu_emit_rect_tex_(float **vp, int x, int y, int xt, int yt, int w, int h, int wt, int ht)
{
	float *v = *vp;

	v[0] = x;
	v[1] = y;
	v[2] = xt;
	v[3] = yt;
	v[4] = x + w;
	v[5] = y;
	v[6] = xt + wt;
	v[7] = yt;
	v[8] = x + w;
	v[9] = y + h;
	v[10] = xt + wt;
	v[11] = yt + ht;
	v[12] = x;
	v[13] = y + h;
	v[14] = xt;
	v[15] = yt + ht;
	*vp += 16;
}

static inline void
_cairo_gpu_emit_rect_tex(float **vp, int x, int y, int xt, int yt, int w, int h)
{
	_cairo_gpu_emit_rect_tex_(vp, x, y, xt, yt, w, h, w, h);
}

static float _cairo_gpu_vec4_zero[4];
