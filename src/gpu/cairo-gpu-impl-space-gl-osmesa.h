static void
_cairo_gpu_osmesa_init(cairo_osmesa_t* osmesa, PFNGLGETPROCADDRESSPROC GetProcAddress)
{
#define DO(x) osmesa->x = GetProcAddress("OSMesa" #x)
	DO(CreateContext);
	DO(DestroyContext);
	DO(GetColorBuffer);
	DO(GetCurrentContext);
	DO(GetIntegerv);
	DO(MakeCurrent);
#undef DO
}

static inline void
_cairo_osmesa_space_make_current(cairo_gpu_space_t* space, cairo_gpu_gl_state_t* user)
{
	space->osmesa.MakeCurrent(user->osmesa.ctx, user->osmesa.buffer, user->osmesa.type, user->osmesa.width, user->osmesa.height);
}

static char _cairo_osmesa_dummy_buffer[1024];

static void
_cairo_osmesa_context_do_bind(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface)
{
	ctx->space->osmesa.MakeCurrent(ctx->osmesa.ctx, _cairo_osmesa_dummy_buffer, GL_UNSIGNED_BYTE, 16, 16);
}

static void
_cairo_osmesa_space_unbind_context(cairo_gpu_space_t* space)
{
	space->osmesa.MakeCurrent(0, 0, 0, 0, 0);
}

static void
_cairo_osmesa_context_flush(cairo_gpu_context_t* ctx)
{
	ctx->gl.Flush();
}

/* we do the most old-fashioned thing possible, since it seems to be much more reliable than anything else */
static cairo_gpu_context_t *
_cairo_osmesa_space_create_context(cairo_gpu_space_t* space, cairo_gpu_subspace_t* sub)
{
	cairo_gpu_context_t *ctx = 0;

	ctx = _cairo_gpu_context__create(space, sub);

	ctx->osmesa.ctx = space->osmesa.CreateContext(GL_RGBA, space->osmesa.share);

	if(!ctx->osmesa.ctx)
	{
		free(ctx);
		return 0;
	}

	return ctx;
}

static void
_cairo_osmesa_context_do_destroy(cairo_gpu_context_t * ctx)
{
	ctx->space->osmesa.DestroyContext(ctx->osmesa.ctx);
}

static void _cairo_osmesa_space_do_destroy(cairo_gpu_space_t* space)
{
	space->osmesa.DestroyContext(space->osmesa.share);
}

static void _cairo_osmesa_space_destroy_subspace(cairo_gpu_space_t* space, cairo_gpu_subspace_t* sub)
{
}

static void
_cairo_osmesa_space_init_font_options(cairo_gpu_space_t* space)
{
}

static cairo_bool_t
_cairo_osmesa_space_has_offscreen_drawables(cairo_gpu_space_t* space)
{
	return 0;
}

static void
_cairo_osmesa_space_set_surface_subspace(cairo_gpu_space_t* space, cairo_gpu_surface_t* surface)
{
}

static cairo_bool_t
_cairo_osmesa_context_is_surface_bound(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface)
{
	return 1;
}

static void
_cairo_osmesa_context_update_state(cairo_gpu_context_t* ctx, cairo_gpu_gl_state_t* user)
{
	cairo_gpu_context_t* new_ctx = 0;
	OSMesaContext gl_ctx = ctx->space->osmesa.GetCurrentContext();
	if(gl_ctx)
	{
		if(_cairo_gl_tls.ctx && gl_ctx == _cairo_gl_tls.ctx->osmesa.ctx)
			new_ctx = _cairo_gl_tls.ctx;
		else
		{
			int format;
			user->api = CAIRO_GL_API_OSMESA;
			user->osmesa.ctx = gl_ctx;
			ctx->space->osmesa.GetIntegerv(OSMESA_TYPE, &user->osmesa.type);
			ctx->space->osmesa.GetColorBuffer(user->osmesa.ctx, &user->osmesa.width, &user->osmesa.height, &format, &user->osmesa.buffer);
		}
	}

	_cairo_gl_tls.ctx = new_ctx;
}

static void*
_cairo_osmesa_space_create_gl_context(cairo_gpu_space_t * space, cairo_surface_t* surface)
{
	return space->osmesa.CreateContext(GL_RGBA, space->osmesa.share);
}

static void
_cairo_osmesa_space_destroy_gl_context(cairo_gpu_space_t * space, void* context)
{
	space->osmesa.DestroyContext(context);
}

static cairo_bool_t
_cairo_osmesa_space_make_gl_context_current(cairo_gpu_space_t * space, void* context, cairo_gpu_surface_t* draw_surface, cairo_gpu_surface_t* read_surface)
{
	space->osmesa.MakeCurrent(context, _cairo_osmesa_dummy_buffer, GL_UNSIGNED_BYTE, 16, 16);
	return 1;
}

cairo_space_t *
cairo_osmesa_space_create(void* libgl, OSMesaContext share_context, unsigned flags)
{
	cairo_gpu_space_t *space;
	int owns_libgl = 0;

	PFNGLGETPROCADDRESSPROC GetProcAddress = (PFNGLGETPROCADDRESSPROC)dlsym(libgl, "OSMesaGetProcAddress");
	if(!GetProcAddress)
	{
		if(!libgl)
		{
			libgl = dlopen("libOSMesa.so.6", RTLD_LAZY);
			if(!libgl)
			{
				libgl = dlopen("libOSMesa32.so.6", RTLD_LAZY);
				if(!libgl)
				{
					libgl = dlopen("libOSMesa16.so.6", RTLD_LAZY);
					if(!libgl)
						return 0;
				}
			}

			owns_libgl = 1;
			GetProcAddress = (PFNGLGETPROCADDRESSPROC)dlsym(libgl, "OSMesaGetProcAddress");
			if(!GetProcAddress)
				goto fail;
		}
		else
			return 0;
	}

	space = _cairo_gpu_space__begin_create();
	if(!space)
		goto fail;

	space->GetProcAddress = GetProcAddress;
	space->libgl = libgl;
	space->owns_libgl = owns_libgl;
	_cairo_gpu_osmesa_init(&space->osmesa, GetProcAddress);

	space->api = CAIRO_GL_API_OSMESA;
	space->osmesa.share = space->osmesa.CreateContext(GL_RGBA, share_context);

	return _cairo_gpu_space__finish_create(space, flags);
fail:
	if(owns_libgl)
		dlclose(libgl);
	return 0;
}

cairo_space_t *
cairo_osmesa_space_create_from_current_context(void* libgl, unsigned flags)
{
	cairo_space_t *space;
	PFNGLGETPROCADDRESSPROC GetProcAddress;

	if((GetProcAddress = dlsym(libgl, "OSMesaGetProcAddress")))
	{
		PFNOSMESAGETCURRENTCONTEXTPROC GetCurrentContext = GetProcAddress("OSMesaGetCurrentContext");
		if(GetCurrentContext)
		{
			OSMesaContext ctx = GetCurrentContext();
			space = cairo_osmesa_space_create(libgl, ctx, flags);
			if(space)
				return space;
		}
	}
	return 0;
}
