#include <pipe/p_screen.h>
#include <pipe/p_context.h>
#include <pipe/p_state.h>
#include <pipe/p_format.h>
#include <pipe/p_context.h>
#include <pipe/p_inlines.h>
#include <pipe/internal/p_winsys_screen.h>
#include <softpipe/sp_winsys.h>
#include <trace/tr_context.h>
#include <state_tracker/drm_api.h>

struct _cairo_gpu_texture
{
	CAIRO_GPU_BASE_TEXTURE_T;

	struct pipe_texture* texture;
	struct pipe_surface* surface;
};

// only have non-MSAA and MSAA
#define MAX_SURFS 2

 struct _cairo_gpu_surface
{
	CAIRO_GPU_BASE_SURFACE_T;
	cairo_rectangle_int_t bbox[MAX_SURFS];

	cairo_gpu_texture_t texture;
};

typedef struct _cairo_gpu_context cairo_gpu_context_t;

struct _cairo_gpu_geometry
{
	unsigned mode;
	unsigned count;

	union
	{
		struct
		{
			unsigned char vert;
			unsigned char color;
			unsigned char tex;
			unsigned char velem_pad;
		};
		unsigned velem;
	};

	struct pipe_vertex_buffer vbuffer;
};

#define TABLE_FRAG (0 << 30)
#define TABLE_VERT (1 << 30)
#define TABLE_BLEND (2 << 30)
#define TABLE_SAMPLER (3 << 30)
#define TABLE_MASK (3 << 30)

typedef struct
{
	cairo_hash_entry_t base;
	void* v;
} cairo_gpu_ptr_entry_t;;

#define API_SOFTPIPE 0
#define API_USER 1
#define API_DRM 2

struct _cairo_gpu_space
{
	CAIRO_GPU_BASE_SPACE_T;

	struct pipe_screen* screen;
	struct pipe_screen* real_screen;
	char owns_screen;

	unsigned api;
	union
	{
		struct
		{
			struct pipe_context* (*context_create)(void*, struct pipe_screen*);
			void* cookie;
		} user;

		struct
		{
			struct drm_api* api;

			// currently only used in _destroy
			void* driver;
			int fd;
			Display* dpy;
		} drm;
	};

	cairo_hash_table_t* table;
	void* rasterizer[2];
	void* zsa;
};

#define MAX_SMALL_WIDTH 16
#define MAX_TEXTURE_UNITS 6

// subspace_tls
struct _cairo_gpu_context
{
	// these are redundant, but we currently keep them for simplicity
	cairo_gpu_space_t* space;
	cairo_gpu_space_tls_t* tls;

	struct pipe_context* pipe;

	unsigned velem;
	struct pipe_vertex_buffer vbuffer;

	unsigned samplers[MAX_OPERANDS];
	void* ssamplers[MAX_OPERANDS];
	struct pipe_texture* textures[MAX_OPERANDS];

	char smooth;
	void* rasterizer[2];

	int blend;

	float vert_constants[VERTENV_COUNT * 4];
	float frag_constants[FRAGENV_COUNT * 4];

	struct pipe_constant_buffer vert_cbuffer;
	struct pipe_constant_buffer frag_cbuffer;

	char ssamplers_dirty : 1;
	char textures_dirty : 1;
	char vert_constants_dirty : 1;
	char frag_constants_dirty : 1;
	char matrix_dirty : 1;

	int frag;
	int vert;

	int viewport_x;
	int viewport_y;
	int viewport_width;
	int viewport_height;

	int dx;
	int dy;

	unsigned mode;
	unsigned count;

	struct pipe_surface* surface;
	unsigned width;
	unsigned height;

	struct pipe_surface* read_surface;
	cairo_gpu_texture_t* read_texture;
};

struct _cairo_gpu_space_tls
{
	CAIRO_GPU_BASE_SPACE_TLS_T;
	cairo_gpu_context_t context;
};

static inline void*
_cairo_gpu_space__lookup_ptr_unlocked(cairo_gpu_space_t* space, unsigned hash)
{
	cairo_hash_entry_t entry;
	cairo_gpu_ptr_entry_t* p;
	entry.hash = hash;

	p = (cairo_gpu_ptr_entry_t*)_cairo_hash_table_lookup(space->table, &entry);
	if(!p)
		return 0;
	return p->v;
}

static inline void*
_cairo_gpu_space__lookup_ptr(cairo_gpu_space_t* space, unsigned hash)
{
	void* p;
	CAIRO_MUTEX_LOCK(space->mutex);
	p = _cairo_gpu_space__lookup_ptr_unlocked(space, hash);
	CAIRO_MUTEX_UNLOCK(space->mutex);
	return p;
}

static inline cairo_bool_t
_cairo_gpu_space_store_ptr_unlocked(cairo_gpu_space_t* space, unsigned hash, void* p)
{
	cairo_gpu_ptr_entry_t* e = malloc(sizeof(cairo_gpu_ptr_entry_t));
	e->base.hash = hash;
	e->v = p;
	if(_cairo_hash_table_insert(space->table, &e->base))
	{
		free(e);
		return 0;
	}
	return 1;
}

static inline void _cairo_gpu_texture_fini(cairo_gpu_context_t* ctx, cairo_gpu_texture_t* texture)
{
	if(texture->texture)
		pipe_texture_reference(&texture->texture, 0);
	if(texture->surface)
		pipe_surface_reference(&texture->surface, 0);
}

// you must fill target_idx
static inline void _cairo_gpu_texture_init(cairo_gpu_space_t* space, cairo_gpu_texture_t* texture, int width, int height)
{
	texture->texture = 0;
	texture->surface = 0;

	if(space->tex_npot || (is_pow2(width) && is_pow2(height)))
		texture->target_idx = TARGET_2D;
	else
	{
		texture->target_idx = TARGET_2D;
		width = higher_pow2(width - 1);
		height = higher_pow2(height - 1);
	}

	texture->width = width;
	texture->height = height;
}

static inline void _cairo_gpu_texture_realize(cairo_gpu_context_t* ctx, cairo_gpu_texture_t* texture)
{
	struct pipe_texture templ;
	memset(&templ, 0, sizeof(templ));
	templ.target = PIPE_TEXTURE_2D;

	// TODO: choose appropriate format
	templ.format = PIPE_FORMAT_A8R8G8B8_UNORM;

	pf_get_block(templ.format, &templ.block);
	templ.width[0] = texture->width;
	templ.height[0] = texture->height;
	templ.depth[0] = 1;
	templ.last_level = 0;
	// PIPE_TEXTURE_USAGE_DISPLAY_TARGET | PIPE_TEXTURE_USAGE_RENDER_TARGET
	templ.tex_usage = PIPE_TEXTURE_USAGE_SAMPLER;
	texture->texture = ctx->space->screen->texture_create(ctx->space->screen, &templ);
	texture->surface = ctx->space->screen->get_tex_surface(ctx->space->screen, texture->texture, 0, 0, 0, PIPE_BUFFER_USAGE_GPU_READ | PIPE_BUFFER_USAGE_GPU_WRITE);
}

// you must fill target_idx
static inline void _cairo_gpu_texture_create(cairo_gpu_context_t* ctx, cairo_gpu_texture_t* texture, int width, int height)
{
	_cairo_gpu_texture_init(ctx->space, texture, width, height);
	_cairo_gpu_texture_realize(ctx, texture);
}

static inline void
_cairo_gpu_texture_adjust_matrix(cairo_gpu_texture_t* texture, cairo_matrix_t* matrix)
{
	if(texture->target_idx == TARGET_2D)
	{
		double xm = 1.0 / texture->width;
		double ym = 1.0 / texture->height;

		matrix->xx *= xm;
		matrix->xy *= xm;
		matrix->x0 *= xm;

		matrix->yx *= ym;
		matrix->yy *= ym;
		matrix->y0 *= ym;
	}
}
