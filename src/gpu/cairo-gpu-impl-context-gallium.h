#include <tgsi/tgsi_text.h>

#define L_TERMINATOR ""
#define L_TEXTURE "SAMP"
#define L_PROGRAM_ENV "CONST"

#define L_RESULT_COLOR_TEMP "TEMP[0]"
#define L_SET_RESULT_COLOR(p) memcpy(p, " OUT", 4)

#define L_TMP "TEMP[1]"

#define L_TEMP_RESULT_COLOR "DCL TEMP[0], CONSTANT"
#define L_TEMP_TMP "DCL TEMP[1], CONSTANT"

#define L_0 "IMM[0]"
#define L_1 "IMM[1]"

#define L_SCALAR_X ".xxxx"

#include "cairo-gpu-impl-programs.h"

#define CREATE_STATE(name) do { void* todel = 0; \
			p = ctx->pipe->create_##name##_state(ctx->pipe, &name); \
			CAIRO_MUTEX_LOCK(ctx->space->mutex); \
			if(*pp) \
			{ \
				p = *pp; \
			} \
			else \
				*pp = p; \
			CAIRO_MUTEX_UNLOCK(ctx->space->mutex); \
			if(todel) ctx->pipe->delete_##name##_state(ctx->pipe, p); \
	} while(0)

#define CREATE_STATE_HASH(name) do { void* pe; void* todel = 0; \
			p = ctx->pipe->create_##name##_state(ctx->pipe, &name); \
			CAIRO_MUTEX_LOCK(ctx->space->mutex); \
			pe = _cairo_gpu_space__lookup_ptr_unlocked(ctx->space, hash); \
			if(pe) \
			{ \
				todel = p; \
				p = pe; \
			} \
			else \
				_cairo_gpu_space_store_ptr_unlocked(ctx->space, hash, p); \
			CAIRO_MUTEX_UNLOCK(ctx->space->mutex); \
			if(todel) ctx->pipe->delete_##name##_state(ctx->pipe, todel); \
	} while(0)

static inline void
_cairo_gpu_context_set_vert_param(cairo_gpu_context_t* ctx, unsigned i, float* v)
{
	if(memcmp(&ctx->vert_constants[i * 4], v, 4 * sizeof(float)))
	{
		memcpy(&ctx->vert_constants[i * 4], v, 4 * sizeof(float));
		ctx->vert_constants_dirty = 1;
	}
}

static inline void
_cairo_gpu_context_set_frag_param(cairo_gpu_context_t* ctx, unsigned i, float* v)
{
	if(memcmp(&ctx->frag_constants[i * 4], v, 4 * sizeof(float)))
	{
		memcpy(&ctx->frag_constants[i * 4], v, 4 * sizeof(float));
		ctx->frag_constants_dirty = 1;
	}
}

static inline void
_cairo_gpu_context_init(cairo_gpu_context_t * ctx)
{
	ctx->smooth = -1;
	ctx->blend = -1;
	ctx->frag = -1;
	ctx->vert = -1;
	ctx->viewport_height = -1;
	ctx->viewport_width = -1;
	memset(ctx->samplers, 0xff, sizeof(ctx->samplers));
}

static struct pipe_context*
_cairo_gallium_space_create_context(cairo_gpu_space_t* space);

static inline void
_cairo_gpu_context__create(cairo_gpu_context_t* ctx)
{
	void** pp;
	void* p;
	struct pipe_depth_stencil_alpha_state depth_stencil_alpha;

	ctx->pipe = _cairo_gallium_space_create_context(ctx->space);

	if(ctx->space->real_screen)
		ctx->pipe = trace_context_create(ctx->space->screen, ctx->pipe);

	pp = &ctx->space->zsa;
	p = *pp;
	if(!p)
	{
		memset(&depth_stencil_alpha, 0, sizeof(depth_stencil_alpha));

		CREATE_STATE(depth_stencil_alpha);
	}

	ctx->pipe->bind_depth_stencil_alpha_state(ctx->pipe, p);
}

static inline void
_cairo_gpu_context_bind(cairo_gpu_context_t* ctx)
{
	if(!ctx->pipe)
		_cairo_gpu_context__create(ctx);
}

static void
_cairo_gallium_context_set_framebuffer(cairo_gpu_context_t* ctx, struct pipe_surface* surface, unsigned width, unsigned height)
{
	if(surface != ctx->surface || width != ctx->width || height != ctx->height)
	{
		struct pipe_framebuffer_state fb;
		int i;
		fb.nr_cbufs = 1;
		fb.cbufs[0] = surface;
		for(i = 1; i < PIPE_MAX_COLOR_BUFS; ++i)
			fb.cbufs[i] = 0;
		fb.width = width;
		fb.height = height;
		fb.zsbuf = 0;

		ctx->pipe->set_framebuffer_state(ctx->pipe, &fb);

		ctx->surface = surface;
		ctx->width = width;
		ctx->height = height;
	}
}

// note that attributes->matrix must be adjusted by the caller, if necessary!!
static void
_cairo_gpu_context_set_texture_and_attributes_(cairo_gpu_context_t* ctx, int idx, cairo_gpu_texture_t* texture, cairo_surface_attributes_t* attributes, float* zm)
{
	unsigned hash = TABLE_SAMPLER | attributes->extend | ((unsigned)attributes->extra << 2) | (attributes->filter << 3);
	if(idx < 0)
		return;

	if(hash != ctx->samplers[idx])
	{
		void* p = _cairo_gpu_space__lookup_ptr(ctx->space, hash);

		if(!p)
		{
			struct pipe_sampler_state sampler;
			unsigned filter, wrap;

			memset(&sampler, 0, sizeof(sampler));
			if(!(ctx->space->extend_mask & (1 << attributes->extend)))
				wrap = PIPE_TEX_WRAP_CLAMP;
			else
			{
				switch (attributes->extend)
				{
				case CAIRO_EXTEND_NONE:
					wrap = PIPE_TEX_WRAP_CLAMP_TO_BORDER;
					break;
				case CAIRO_EXTEND_PAD:
					wrap = PIPE_TEX_WRAP_CLAMP_TO_EDGE;
					break;
				case CAIRO_EXTEND_REPEAT:
					wrap = PIPE_TEX_WRAP_REPEAT;
					break;
				case CAIRO_EXTEND_REFLECT:
					wrap = PIPE_TEX_WRAP_MIRROR_REPEAT;
					break;
				default:
					abort();
				}
			}

			if((unsigned)attributes->extra & 1)
			{
				sampler.wrap_s = wrap;
				sampler.wrap_t = PIPE_TEX_WRAP_CLAMP_TO_EDGE;
			}
			else
				sampler.wrap_t = sampler.wrap_s = wrap;

			switch (attributes->filter)
			{
			case CAIRO_FILTER_FAST:
			case CAIRO_FILTER_NEAREST:
				filter = PIPE_TEX_FILTER_NEAREST;
				break;
			case CAIRO_FILTER_BEST:
				if(ctx->space->max_anisotropy > 1.0)
				{
					sampler.max_anisotropy = ctx->space->max_anisotropy;
					filter = PIPE_TEX_FILTER_ANISO;
				}
				else
					filter = PIPE_TEX_FILTER_LINEAR;
				break;
			case CAIRO_FILTER_GOOD:
				if(ctx->space->max_anisotropy > 1.0)
				{
					sampler.max_anisotropy = MIN(4.0, ctx->space->max_anisotropy);
					filter = PIPE_TEX_FILTER_ANISO;
				}
				else
					filter = PIPE_TEX_FILTER_LINEAR;
				break;
			case CAIRO_FILTER_BILINEAR:
				filter = PIPE_TEX_FILTER_LINEAR;
				break;
			default:
			case CAIRO_FILTER_GAUSSIAN:
				abort();
			}

			sampler.min_img_filter = sampler.mag_img_filter = filter;

			// TODO: we would prefer unnormalized. Does it actually work in Gallium, also with REPEAT and REFLECT?
			sampler.normalized_coords = 1;

			CREATE_STATE_HASH(sampler);
		}

		if(p != ctx->ssamplers[idx])
		{
			ctx->ssamplers[idx] = p;
			ctx->ssamplers_dirty = 1;
		}

		ctx->samplers[idx] = hash;
	}

	if(texture->texture != ctx->textures[idx])
	{
		ctx->textures[idx] = texture->texture;
		ctx->textures_dirty = 1;
	}

	{
		cairo_matrix_t* matrix = &attributes->matrix;
		float xv[4] = {matrix->xx, matrix->yx, zm[0], 0};
		float yv[4] = {matrix->xy, matrix->yy, zm[1], 0};
		float wv[4] = {matrix->x0, matrix->y0, zm[2], 1};
		_cairo_gpu_context_set_vert_param(ctx, VERTENV_TEX_MATRIX_X(idx), xv);
		_cairo_gpu_context_set_vert_param(ctx, VERTENV_TEX_MATRIX_Y(idx), yv);
		_cairo_gpu_context_set_vert_param(ctx, VERTENV_TEX_MATRIX_W(idx), wv);
	}
}

static void
_cairo_gpu_context_set_texture_and_attributes(cairo_gpu_context_t* ctx, int idx, cairo_gpu_texture_t* texture, cairo_surface_attributes_t* attributes)
{
	_cairo_gpu_context_set_texture_and_attributes_(ctx, idx, texture, attributes, _cairo_gpu_vec4_zero);
}

static void
_cairo_gpu_surface__create_tex(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface);

static inline void
_cairo_gallium_context_upload_pixels(cairo_gpu_context_t* ctx, cairo_gpu_texture_t* texture, cairo_image_surface_t * image_surface, int src_x, int src_y, int width, int height, int dst_x, int dst_y)
{
	struct pipe_transfer* transfer;
	pixman_image_t* dst_image;
	unsigned char* dstp;

	assert(texture->texture);

	if(dst_x >= 0)
	{
		if(ctx->pipe->is_texture_referenced(ctx->pipe, texture->texture, 0, 0))
		{
			struct pipe_fence_handle* fence;
			ctx->pipe->flush(ctx->pipe, PIPE_FLUSH_RENDER_CACHE, &fence);

			if(fence)
			{
				ctx->space->screen->fence_finish(ctx->space->screen, fence, 0);
				ctx->space->screen->fence_reference(ctx->space->screen, &fence, 0);
			}
		}
	}

	transfer = ctx->space->screen->get_tex_transfer(ctx->space->screen, texture->texture, 0, 0, 0, PIPE_TRANSFER_WRITE, dst_x >= 0 ? dst_x : 0, dst_y >= 0 ? dst_y : 0, width, height);
	dstp = ctx->space->screen->transfer_map(ctx->space->screen, transfer);
	dst_image = pixman_image_create_bits(PIXMAN_a8r8g8b8, width, height, (uint32_t*)dstp, transfer->stride);

	pixman_image_composite (PIXMAN_OP_SRC,
					image_surface->pixman_image,
					NULL,
					dst_image,
					src_x,
					src_y,
					0, 0,
					0, 0,
					width, height);
	pixman_image_unref(dst_image);

	ctx->space->screen->transfer_unmap(ctx->space->screen, transfer);
	ctx->space->screen->tex_transfer_destroy(transfer);
}

#ifdef __SSE2__
#include <emmintrin.h>
#endif

#ifdef __SSSE3__
#include <tmmintrin.h>
#endif

static inline void
_cairo_gpu_context_upload_data(cairo_gpu_context_t* ctx, int idx, cairo_gpu_texture_t* texture, float* p, int width, int height)
{
	unsigned x, y;
	struct pipe_transfer* transfer;
	unsigned char* map;

	// TODO: support fp32 textures
	transfer = ctx->space->screen->get_tex_transfer(ctx->space->screen, texture->texture, 0, 0, 0, PIPE_TRANSFER_WRITE, 0, 0, width, height);
	map = ctx->space->screen->transfer_map(ctx->space->screen, transfer);

	if(transfer->format == PIPE_FORMAT_R32G32B32A32_FLOAT)
	{
		unsigned char* dstp = map;

		if(transfer->stride == width * 4 * sizeof(float))
			memcpy(dstp, p, width * height * 4 * sizeof(float));
		else
		{
			for(y = height; y; --y)
			{
				memcpy(dstp, p, width * 4 * sizeof(float));
				p += width * 4;
				dstp += transfer->stride - width * 4 * sizeof(float);
			}
		}
	}
#if 0
	else if(transfer->format == PIPE_FORMAT_R16G16B16A16_FLOAT)
	{
		unsigned short* dstp = map;
		for(y = 0; y < height; ++y)
		{
			for(x = 0; x < width * 4; ++x)
				*dstp++ = _cairo_float_to_half(*p++);
			dstp = (unsigned short*)((char*)dstp + transfer->stride - width * 4 * sizeof(unsigned short));
		}
	}
#endif
	else if(transfer->format == PIPE_FORMAT_A8R8G8B8_UNORM)
	{
#ifdef __SSE2__
		unsigned* dstp = (unsigned*)map;
		__m128* mp = (__m128*)p;
		__m128 c256me;
#ifdef __SSSE3__
		__m128i shuffle;
		unsigned char* shuffleb = (unsigned char*)&shuffle;
		shuffleb[0] = 0x80;
		shuffleb[1] = 0x84;
		shuffleb[2] = 0x88;
		shuffleb[3] = 0x8c;
		memset(shuffleb + 4, 0, 12);
#endif
		c256me = _mm_set_ps1(nextafterf(256.0f, 0.0f));

		for(y = height; y; --y)
		{
			// TODO: do 4 pixels at a time - beware of alignment issues
			for(x = width; x; --x)
			{
				__m128i v = _mm_cvttps_epi32(_mm_mul_ps(*mp++, c256me));
				__m128i s;

				// SSSE3
				// TODO: is endianness correct? it should be.
#ifdef __SSSE3__
				s = _mm_shuffle_epi8(v, shuffle);
#else
				s = v;
				s = _mm_or_si128(s, _mm_srli_si128(v, 24));
				s = _mm_or_si128(s, _mm_srli_si128(v, 16 + 32));
				s = _mm_or_si128(s, _mm_srli_si128(v, 8 + 64));
#endif
				*dstp++ = _mm_cvtsi128_si32(v);
			}

			dstp = (unsigned*)((char*)dstp + transfer->stride - width * 4 * sizeof(unsigned char));
		}
#else
		unsigned char* dstp = map;
		for(y = height; y; --y)
		{
			for(x = width * 4; x; --x)
				*dstp++ = _cairo_color_float_to_byte(*p++);

			dstp += transfer->stride - width * 4 * sizeof(unsigned char);
		}
#endif
	}
	else
		abort();

	ctx->space->screen->transfer_unmap(ctx->space->screen, transfer);
	ctx->space->screen->tex_transfer_destroy(transfer);
}

static __attribute__((unused)) cairo_status_t
_cairo_gpu_context_blit_pixels(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, cairo_image_surface_t * image_surface, int src_x, int src_y, int width, int height, int dst_x, int dst_y)
{
	_cairo_gallium_context_upload_pixels(ctx, &dst->texture, image_surface, src_x, src_y, width, height, dst_x, dst_y);
	return CAIRO_STATUS_SUCCESS;
}

static inline void
_cairo_gpu_context_upload_pixels(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, int idx, cairo_gpu_texture_t* texture, cairo_image_surface_t * image_surface, int src_x, int src_y, int width, int height, int dst_x, int dst_y)
{
	_cairo_gallium_context_upload_pixels(ctx, texture, image_surface, src_x, src_y, width, height, dst_x, dst_y);
}

#if MAX_OPERANDS > 2
#error Add more IN/OUT variables
#endif

static const char* _cairo_gpu_in[] = {"IN[0]", "IN[1]", "IN[2]", "IN[3]"};
static const char* _cairo_gpu_out[] = {"OUT[0]", "OUT[1]", "OUT[2]", "OUT[3]"};

static inline void
_cairo_gpu_context__set_vert(cairo_gpu_context_t* ctx, unsigned vert)
{
	if((int)vert != ctx->vert)
	{
		unsigned hash = TABLE_VERT | vert;
		void* p = _cairo_gpu_space__lookup_ptr(ctx->space, hash);

		if(!p)
		{
			struct pipe_shader_state vs;
			struct tgsi_token tokens[1024];
			char* ps;
			cairo_gpu_program_builder_t pb;
			cairo_gpu_program_builder_t* b = &pb;
			cairo_gpu_string_builder_t* builder = &b->body;
			int i, j;

			memset(&pb, 0, sizeof(pb));
			b->has_01_swizzles = 1;
			b->div_uses = b->dp2a_uses = 0;

			builder = &b->main;
			OUT_("VERT1.1\n");
			OUT("DCL IN[0], POSITION");
			b->in_position = _cairo_gpu_in[0];
			i = 1;
			if(vert & (VERT_COLOR_PREOP | VERT_COLOR_POSTOP))
			{
				OUT("DCL IN[1], COLOR");
				b->in_color = _cairo_gpu_in[1];
				i = 2;
			}
			{
				int have_tex = 0;
				for(j = 0; j < MAX_OPERANDS; ++j)
				{
					unsigned tex = (vert >> (VERT_TEX_SHIFT + j * VERT_TEX_BITS)) & VERT_TEX_MASK;
					if(tex && tex != VERT_TEX_GEN)
					{
						have_tex = 1;
						break;
					}
				}
				if(have_tex)
				{
					OUTF("DCL IN[%i], GENERIC", i);
					b->in_texcoord[0] = b->in_texcoord[1] = _cairo_gpu_in[i];
				}
			}

			OUT("DCL OUT[0], POSITION");
			b->out_position = _cairo_gpu_out[0];
			i = 1;
			if(vert & (VERT_COLOR_PREOP | VERT_COLOR_POSTOP))
			{
				OUT("DCL OUT[1], COLOR");
				b->out_color = _cairo_gpu_out[1];
				i = 2;
			}

			for(j = 0; j < MAX_OPERANDS; ++j)
			{
				unsigned tex = (vert >> (VERT_TEX_SHIFT + j * VERT_TEX_BITS)) & VERT_TEX_MASK;
				if(tex)
				{
					OUTF("DCL OUT[%i], GENERIC[%i]", i, j);
					b->out_texcoord[j] = _cairo_gpu_out[i];
					++i;
				}
			}

			// TODO: only emit needed ones to shut up Galliium
			OUTF("DCL CONST[0..%i]", VERTENV_COUNT - 1);
			OUT("IMM FLT32 {0.0, 0.0, 0.0, 0.0}");
			OUT("IMM FLT32 {1.0, 1.0, 1.0, 1.0}");

			_cairo_gpu_write_vert_position(b, vert);
			_cairo_gpu_write_vert(b, vert);

			ps = _cairo_gpu_program_builder_finish(b);

			printf("%s", ps);
			tgsi_text_translate(ps, tokens, sizeof(tokens));
			free(ps);
			vs.tokens = tokens;
			CREATE_STATE_HASH(vs);
		}

		ctx->pipe->bind_vs_state(ctx->pipe, p);
		ctx->vert = vert;
	}
}

static inline void
_cairo_gpu_context__set_frag(cairo_gpu_context_t* ctx, unsigned frag)
{
	if((int)frag != ctx->frag)
	{
		unsigned hash = TABLE_FRAG | frag;
		void* p = _cairo_gpu_space__lookup_ptr(ctx->space, hash);

		if(!p)
		{
			struct pipe_shader_state fs;
			struct tgsi_token tokens[1024];
			char* ps;
			cairo_gpu_program_builder_t pb;
			cairo_gpu_program_builder_t* b = &pb;
			cairo_gpu_string_builder_t* builder = &b->body;
			int i, j;

			memset(&pb, 0, sizeof(pb));
			b->has_01_swizzles = 1;
			b->div_uses = b->dp2a_uses = 0;

			builder = &b->main;
			OUT_("FRAG1.1\n");

			OUT("DCL IN[0], POSITION, LINEAR");
			b->in_position = _cairo_gpu_in[0];
			i = 1;
			if(frag & FRAG_PRIMARY)
			{
				OUT("DCL IN[1], COLOR, CONSTANT");
				b->in_color = _cairo_gpu_in[1];
				i = 2;
			}
			for(j = 0; j < MAX_OPERANDS; ++j)
			{
				unsigned tex = frag >> (FRAG_TEX_SHIFT + j * FRAG_TEX_BITS);
				if(tex)
				{
					OUTF("DCL IN[%i], GENERIC[%i], LINEAR", i, j);
					b->in_texcoord[j] = _cairo_gpu_in[i];
					++i;
				}
			}

			OUT("DCL OUT[0], COLOR, CONSTANT");
			b->out_color = _cairo_gpu_out[0];

			OUTF("DCL CONST[0..%i], CONSTANT", FRAGENV_COUNT - 1);
			// TODO: only declare used ones?

			for(j = 0; j < MAX_OPERANDS; ++j)
			{
				unsigned tex = (frag >> (FRAG_TEX_SHIFT + j * FRAG_TEX_BITS)) & FRAG_TEX_MASK;
				if(tex)
					OUTF("DCL SAMP[%i], CONSTANT", j);
			}

			// TODO: only emit needed ones to shut up Galliium
			OUT("IMM FLT32 {0.0, 0.0, 0.0, 0.0}");
			OUT("IMM FLT32 {1.0, 1.0, 1.0, 1.0}");

			_cairo_gpu_write_frag(b, frag);
 			ps = _cairo_gpu_program_builder_finish(b);

 			printf("%s", ps);
			tgsi_text_translate(ps, tokens, sizeof(tokens));
			free(ps);
			fs.tokens = tokens;
			CREATE_STATE_HASH(fs);
		}

		ctx->pipe->bind_fs_state(ctx->pipe, p);
		ctx->frag = frag;
	}
}

static inline void
_cairo_gpu_context_set_constant_color(cairo_gpu_context_t* ctx, cairo_gpu_color4_t* color)
{
	_cairo_gpu_context_set_frag_param(ctx, FRAGENV_CONSTANT, &color->c.r);
	//printf("%f %f %f %f\n", color->c.r, color->c.g, color->c.b, color->ka);
}

static void
_cairo_gpu_context_set_vert_frag(cairo_gpu_context_t* ctx, unsigned vert, unsigned frag)
{
	_cairo_gpu_context__set_vert(ctx, vert);
	_cairo_gpu_context__set_frag(ctx, frag);
}

static inline void
_cairo_gpu_context_set_translation(cairo_gpu_context_t* ctx, int dx, int dy)
{
	if(dx != ctx->dx || dy != ctx->dy)
	{
		ctx->matrix_dirty = 1;

		ctx->dx = dx;
		ctx->dy = dy;
	}
}

static void
_cairo_gpu_context_set_viewport(cairo_gpu_context_t* ctx, unsigned x, unsigned y, unsigned width, unsigned height)
{
	if(ctx->viewport_width != (int)width || ctx->viewport_height != (int)height || ctx->viewport_x != (int)x || ctx->viewport_y != (int)y)
	{
		float half_width = width / 2.0;
		float half_height = height / 2.0;

		struct pipe_viewport_state viewport;
		viewport.scale[0] = half_width;
		viewport.scale[1] = half_height;
		viewport.scale[2] = 1.0;
		viewport.scale[3] = 1.0;

		viewport.translate[0] = half_width + x;
		viewport.translate[1] = half_height + y;
		viewport.translate[2] = 1.0;
		viewport.translate[3] = 0.0;

		ctx->pipe->set_viewport_state(ctx->pipe, &viewport);

		ctx->matrix_dirty = 1;

		ctx->viewport_x = x;
		ctx->viewport_y = y;
		ctx->viewport_width = width;
		ctx->viewport_height = height;
	}
}

static unsigned _cairo_gpu_pipe_blendfactors[] =
{
	PIPE_BLENDFACTOR_ZERO,
	PIPE_BLENDFACTOR_ONE,

	PIPE_BLENDFACTOR_SRC_COLOR,
	PIPE_BLENDFACTOR_INV_SRC_COLOR,
	PIPE_BLENDFACTOR_SRC_ALPHA,
	PIPE_BLENDFACTOR_INV_SRC_ALPHA,
	PIPE_BLENDFACTOR_DST_ALPHA,
	PIPE_BLENDFACTOR_INV_DST_ALPHA,
	PIPE_BLENDFACTOR_DST_COLOR,
	PIPE_BLENDFACTOR_INV_DST_COLOR,
	PIPE_BLENDFACTOR_SRC_ALPHA_SATURATE,

	PIPE_BLENDFACTOR_CONST_COLOR,
	PIPE_BLENDFACTOR_INV_CONST_COLOR,
	PIPE_BLENDFACTOR_CONST_ALPHA,
	PIPE_BLENDFACTOR_INV_CONST_ALPHA,
};

static inline void
_cairo_gpu_context_set_blend(cairo_gpu_context_t* ctx, unsigned blendv)
{
	if((int)blendv != ctx->blend)
	{
		unsigned hash = TABLE_BLEND | blendv;
		void* p = _cairo_gpu_space__lookup_ptr(ctx->space, hash);

		if(!p)
		{
			cairo_gpu_blend_t cblend;
			struct pipe_blend_state blend;

			cblend.v = blendv;

			// XXX: fix endianness??
			blend.blend_enable = !(cblend.func == BLEND_FUNC_SOURCE && !cblend.eq);

			blend.rgb_func = cblend.eq * (PIPE_BLEND_REVERSE_SUBTRACT - PIPE_BLEND_ADD) + PIPE_BLEND_ADD;
			blend.rgb_src_factor = _cairo_gpu_pipe_blendfactors[cblend.src_rgb];
			blend.rgb_dst_factor = _cairo_gpu_pipe_blendfactors[cblend.dst_rgb];

			blend.alpha_func = cblend.eq * (PIPE_BLEND_REVERSE_SUBTRACT - PIPE_BLEND_ADD) + PIPE_BLEND_ADD;
			blend.alpha_src_factor = _cairo_gpu_pipe_blendfactors[cblend.src_alpha];
			blend.alpha_dst_factor = _cairo_gpu_pipe_blendfactors[cblend.dst_alpha];
			blend.logicop_enable = 0;

			blend.logicop_func = 0;

			blend.colormask = cblend.color_mask;
			blend.dither = 0;

			CREATE_STATE_HASH(blend);
		}

		ctx->pipe->bind_blend_state(ctx->pipe, p);
		ctx->blend = blendv;
	}
}

static inline void _cairo_gpu_context_set_blend_color(cairo_gpu_context_t* ctx, cairo_gpu_color4_t* blend_color)
{
	ctx->pipe->set_blend_color(ctx->pipe, (struct pipe_blend_color*)blend_color);
}

static inline void _cairo_gpu_context_set_raster(cairo_gpu_context_t* ctx, unsigned smooth)
{
	smooth = !!smooth;

	if((int)smooth != ctx->smooth)
	{
		void** pp;
		void* p;
		pp = &ctx->space->rasterizer[smooth];

		if(!(p = *pp))
		{
			struct pipe_rasterizer_state rasterizer;

			memset(&rasterizer, 0, sizeof(rasterizer));
			rasterizer.gl_rasterization_rules = 1;
			// TODO rasterizer.multisample;
			rasterizer.poly_smooth = !!smooth;

			CREATE_STATE(rasterizer);
		}

		ctx->pipe->bind_rasterizer_state(ctx->pipe, p);
		ctx->smooth = smooth;
	}
}

static inline void
_cairo_gpu_context_fill_rect_unbound(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface, int x, int y, int width, int height, float r, float g, float b, float a)
{
	union
	{
		unsigned value;
		struct
		{
#ifndef WORDS_BIGENDIAN
			unsigned char b, g, r, a;
#else
			unsigned char a, r, g, b;
#endif
		};
	} c;

	c.a = _cairo_color_float_to_byte(a);
	c.r = _cairo_color_float_to_byte(r);
	c.g = _cairo_color_float_to_byte(g);
	c.b = _cairo_color_float_to_byte(b);

	assert(surface->texture.texture->format == PIPE_FORMAT_A8R8G8B8_UNORM);

	ctx->pipe->surface_fill(ctx->pipe, surface->texture.surface, x, y, width, height, c.value);
}

#define _cairo_gpu_context_fill_rect _cairo_gpu_context_fill_rect_unbound

// must be destination
static inline void
_cairo_gpu_context_fill(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface, float r, float g, float b, float a)
{
	// ignores color mask, but this is probably faster than using a quad with color mask
	float color[4] = {r, g, b, a};
	ctx->pipe->clear(ctx->pipe, PIPE_CLEAR_COLOR, color, 0.0, 0);
}

static inline void
_cairo_gpu_context_fill_unbound(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface, float r, float g, float b, float a)
{
	_cairo_gpu_context_fill_rect_unbound(ctx, surface, 0, 0, surface->width, surface->height, r, g, b, a);
}

static inline void
_cairo_gpu_context_set_geometry(cairo_gpu_context_t * ctx, cairo_gpu_geometry_t * geometry)
{
	_cairo_gpu_geometry_bind(ctx, geometry);
}

static inline void
_cairo_gpu_context__emit_state(cairo_gpu_context_t* ctx)
{
	unsigned ntex;

	if(ctx->textures_dirty)
	{
		for(ntex = MAX_OPERANDS; ntex > 0; --ntex)
		{
			if(ctx->frag & (FRAG_TEX_COLOR_MASK << (FRAG_TEX_SHIFT + (ntex - 1) * FRAG_TEX_BITS)))
				break;
		}

		ctx->pipe->set_sampler_textures(ctx->pipe, ntex, ctx->textures);
		ctx->textures_dirty = 0;
	}

	if(ctx->ssamplers_dirty)
	{
		for(ntex = MAX_OPERANDS; ntex > 0; --ntex)
		{
			if(ctx->frag & (FRAG_TEX_COLOR_MASK << (FRAG_TEX_SHIFT + (ntex - 1) * FRAG_TEX_BITS)))
				break;
		}

		ctx->pipe->bind_sampler_states(ctx->pipe, ntex, ctx->ssamplers);
		ctx->ssamplers_dirty = 0;
	}

	if(ctx->matrix_dirty)
	{
		float v[4] = {2.0f / ctx->viewport_width, 2.0f / ctx->viewport_height, 2.0f * (ctx->dx - ctx->viewport_x) / ctx->viewport_width - 1.0f, 2.0f * (ctx->dy - ctx->viewport_y) / ctx->viewport_height - 1.0f};
		_cairo_gpu_context_set_vert_param(ctx, VERTENV_MATRIX, v);
		ctx->matrix_dirty = 0;
	}

	// create a new buffer each time like Mesa state tracker, to avoid render races
	if(ctx->vert_constants_dirty)
	{
		pipe_buffer_reference(&ctx->vert_cbuffer.buffer, NULL);
		ctx->vert_cbuffer.buffer = ctx->space->screen->buffer_create(ctx->space->screen, 16, PIPE_BUFFER_USAGE_CONSTANT, sizeof(ctx->vert_constants));
		pipe_buffer_write(ctx->space->screen, ctx->vert_cbuffer.buffer, 0, sizeof(ctx->vert_constants), ctx->vert_constants);
		ctx->pipe->set_constant_buffer(ctx->pipe, PIPE_SHADER_VERTEX, 0, &ctx->vert_cbuffer);
		ctx->vert_constants_dirty = 0;
	}

	if(ctx->frag_constants_dirty)
	{
		pipe_buffer_reference(&ctx->frag_cbuffer.buffer, NULL);
		ctx->frag_cbuffer.buffer = ctx->space->screen->buffer_create(ctx->space->screen, 16, PIPE_BUFFER_USAGE_CONSTANT, sizeof(ctx->frag_constants));
		pipe_buffer_write(ctx->space->screen, ctx->frag_cbuffer.buffer, 0, sizeof(ctx->frag_constants), ctx->frag_constants);
		ctx->pipe->set_constant_buffer(ctx->pipe, PIPE_SHADER_FRAGMENT, 0, &ctx->frag_cbuffer);
		ctx->frag_constants_dirty = 0;
	}
}

// must be bound
static inline void
_cairo_gpu_context_draw(cairo_gpu_context_t * ctx)
{
	_cairo_gpu_context__emit_state(ctx);
	ctx->pipe->draw_arrays(ctx->pipe, ctx->mode, 0, ctx->count);
}

static inline void
_cairo_gpu_context_blit_1to1(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, int dx, int dy, int sx, int sy, int w, int h)
{
	// surfaces must be the same format. Currently, this always hold.
	ctx->pipe->surface_copy(ctx->pipe, dst->texture.surface, dx, dy, ctx->read_surface, sx, sy, w, h);
}

static inline void
_cairo_gpu_context_blit_same(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, int x, int y, int w, int h)
{
	_cairo_gpu_context_blit_1to1(ctx, dst, x, y, x, y, w, h);
}

static void
_cairo_gpu_blit_image(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, cairo_gpu_texture_t* texture, int dst_x, int dst_y, int src_x, int src_y, int width, int height, int zoom_x, int zoom_y);

static inline void
_cairo_gpu_context_blit_zoom(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, int dx, int dy, int sx, int sy, int sw, int sh, int zx, int zy)
{
	if(!sw || !sh)
		return;

	if(zx == 1 && zy == 1)
		_cairo_gpu_context_blit_1to1(ctx, dst, dx, dy, sx, sy, sw, sh);
	else
	{
		assert(ctx->read_texture);
		_cairo_gpu_blit_image(ctx, dst, ctx->read_texture, dx, dy, sx, sy, sw, sh, zx, zy);
	}
}

#define _cairo_gpu_context_draw_rect _cairo_gpu_emulate_draw_rect

#include "cairo-gpu-impl-context-emulate.h"

static inline void
_cairo_gpu_context_read_to_texture(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, int dx, int dy, int sx, int sy, int w, int h)
{
	ctx->pipe->surface_copy(ctx->pipe, dst->texture.surface, dx, dy, ctx->read_surface, sx, sy, w, h);
}
