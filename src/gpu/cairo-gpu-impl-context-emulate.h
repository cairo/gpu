static inline void
_cairo_gpu_emulate_draw_rect(cairo_gpu_context_t * ctx, int x, int y, int width, int height)
{
	cairo_gpu_geometry_t* geometry = _cairo_gpu_context_geometry(ctx, GEOM_QUAD);
	float* v = _cairo_gpu_geometry_begin(ctx, geometry, PRIM_QUADS, 4, 2, 0, 0);
	_cairo_gpu_emit_rect(&v, x, y, width, height);
	_cairo_gpu_geometry_end(ctx, geometry, 4);

	_cairo_gpu_context_set_geometry(ctx, geometry);
	_cairo_gpu_context_draw(ctx);
	_cairo_gpu_geometry_put(ctx, geometry);
}

static __attribute__((unused)) void
_cairo_gpu_emulate_blit_pixels(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, cairo_image_surface_t* image, int dst_x, int dst_y, int src_x, int src_y, int width, int height)
{
	cairo_gpu_texture_t texture;
	cairo_surface_attributes_t attrs;
	_cairo_gpu_texture_create(ctx, &texture, width, height);
	_cairo_gpu_context_upload_pixels(ctx, dst, 0, &texture, image, src_x, src_y, width, height, -1, -1);
	cairo_matrix_init_identity(&attrs.matrix);
	attrs.extend = CAIRO_EXTEND_NONE;
	attrs.filter = CAIRO_FILTER_NEAREST;
	attrs.extra = 0;
	_cairo_gpu_context_set_texture_and_attributes(ctx, 0, &texture, &attrs);
	_cairo_gpu_context_set_viewport(ctx, 0, 0, dst->width, dst->height);
	_cairo_gpu_context_set_blend(ctx, BLEND_SOURCE);
	_cairo_gpu_context_set_raster(ctx, 0);
	_cairo_gpu_context_set_vert_frag(ctx, VERT_TEX_GEN << VERT_TEX_SHIFT, FRAG_TEX_COLOR_RGBA << FRAG_TEX_SHIFT);
	_cairo_gpu_context_set_translation(ctx, dst_x, dst_y);

	_cairo_gpu_context_draw_rect(ctx, 0, 0, width, height);
	_cairo_gpu_texture_fini(ctx, &texture);
}

static inline void
_cairo_gpu_emulate_fill_rect(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface, int x, int y, int width, int height, float r, float g, float b, float a)
{
	cairo_gpu_blend_t blend;
	cairo_bool_t clear = (r + g + b + a) == 0.0;
	if(!clear)
		blend.v = BLEND_SOURCE;
	else
		blend.v = BLEND_CLEAR;

	if(surface)
	{
		if(surface->base.content == CAIRO_CONTENT_COLOR)
			blend.color_mask = 7;
		else if(surface->base.content == CAIRO_CONTENT_ALPHA)
			blend.color_mask = 8;
	}

	_cairo_gpu_context_set_viewport(ctx, 0, 0, surface->width, surface->height);
	_cairo_gpu_context_set_blend(ctx, blend.v);
	_cairo_gpu_context_set_raster(ctx, 0);
	if(!clear)
		_cairo_gpu_context_set_vert_frag(ctx, VERT_COLOR_PREOP, FRAG_PRIMARY);
	else
		_cairo_gpu_context_set_vert_frag(ctx, 0, 0);
	_cairo_gpu_context_set_translation(ctx, x, y);

	_cairo_gpu_context_draw_rect(ctx, 0, 0, width, height);
}
