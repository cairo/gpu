/* the geometry context must always be bound before calling geometry_ functions */

/*
	_cairo_gpu_geometry_begin
	// write vertex data
	_cairo_gpu_geometry_end
	// draw

	//optionally
	{
		// do something else
		_cairo_gpu_bind
		// draw again
	}
	_cairo_gpu_geometry_put
*/

// XXX: we may want to not reuse vertex buffers, to prevent possible synchronization (or, here, corruption!)

static inline cairo_gpu_geometry_t*
_cairo_gpu_geometry_init(cairo_gpu_geometry_t * geometry)
{
	geometry->vbuffer.buffer = 0;
	geometry->vbuffer.buffer_offset = 0;
	return geometry;
}

static inline cairo_gpu_geometry_t *
_cairo_gpu_geometry_fini(cairo_gpu_context_t * ctx, cairo_gpu_geometry_t * geometry)
{
	if(geometry->vbuffer.buffer)
		pipe_buffer_reference(&geometry->vbuffer.buffer, 0);
	return geometry;
}

static void *
_cairo_gpu_geometry_begin(cairo_gpu_context_t * ctx, cairo_gpu_geometry_t * geometry, unsigned mode, int count, int vert, int color, int tex)
{
	unsigned size;

	_cairo_gpu_geometry_fini(ctx, geometry);

	if(!count)
		count = 1;

	geometry->mode = mode;
	geometry->vbuffer.stride = ((vert + tex + !!color) * sizeof(float));
	geometry->vbuffer.max_index = count;
	size = geometry->vbuffer.stride * geometry->vbuffer.max_index;

	geometry->vert = vert;
	geometry->color = color;
	geometry->tex = tex;
	geometry->velem_pad = 0;

	if(!geometry->vbuffer.buffer || size > geometry->vbuffer.buffer->size)
	{
		unsigned alloc = size;
		if(geometry->vbuffer.buffer)
		{
			pipe_buffer_reference(&geometry->vbuffer.buffer, 0);
			alloc = MAX(geometry->vbuffer.buffer->size * 2, size);
		}

		geometry->vbuffer.buffer = pipe_buffer_create(ctx->space->screen, 32, PIPE_BUFFER_USAGE_VERTEX, alloc);
	}

	return pipe_buffer_map_range(ctx->space->screen, geometry->vbuffer.buffer,
			0, size, PIPE_BUFFER_USAGE_CPU_WRITE | PIPE_BUFFER_USAGE_FLUSH_EXPLICIT);
}

static void
_cairo_gpu_geometry_end(cairo_gpu_context_t * ctx, cairo_gpu_geometry_t * geometry, int count)
{
	int size = count * ((geometry->vert + !!geometry->color + geometry->tex) * sizeof(float));

	geometry->count = count;

	pipe_buffer_flush_mapped_range(ctx->space->screen, geometry->vbuffer.buffer, 0, size);
	pipe_buffer_unmap(ctx->space->screen, geometry->vbuffer.buffer);
}

static inline void
_cairo_gpu_geometry_bind(cairo_gpu_context_t * ctx, cairo_gpu_geometry_t * geometry)
{
	if(geometry->velem != ctx->velem)
	{
		struct pipe_vertex_element ve[3];
		int i = 0;
		int off = 0;

		if(geometry->vert)
		{
			ve[i].nr_components = geometry->vert;
			if(geometry->vert == 2)
				ve[i].src_format = PIPE_FORMAT_R32G32_FLOAT;
			else
				assert(0);
			ve[i].src_offset = 0;
			ve[i].vertex_buffer_index = 0;
			off += geometry->vert * sizeof(float);
			++i;
		}

		if(geometry->color)
		{
			ve[i].nr_components = geometry->color;
			if(geometry->color == 4)
				ve[i].src_format = PIPE_FORMAT_R8G8B8A8_UNORM;
			else
				assert(0);
			ve[i].src_offset = off;
			ve[i].vertex_buffer_index = 0;
			off += !!geometry->color * sizeof(float);
			++i;
		}

		if(geometry->tex)
		{
			ve[i].nr_components = geometry->tex;
			if(geometry->tex == 2)
				ve[i].src_format = PIPE_FORMAT_R32G32_FLOAT;
			else
				assert(0);
			ve[i].src_offset = off;
			ve[i].vertex_buffer_index = 0;
			off += geometry->tex * sizeof(float);
			++i;
		}
		ctx->pipe->set_vertex_elements(ctx->pipe, i, ve);
		ctx->velem = geometry->velem;
	}

	if(memcmp(&ctx->vbuffer, &geometry->vbuffer, sizeof(ctx->vbuffer)))
	{
		memcpy(&ctx->vbuffer, &geometry->vbuffer, sizeof(ctx->vbuffer));
		ctx->pipe->set_vertex_buffers(ctx->pipe, 1, &ctx->vbuffer);
	}

	ctx->mode = geometry->mode;
	ctx->count = geometry->count;
}

/*
static inline cairo_geometry_t*
_cairo_gpu_space_create_geometry(cairo_gpu_space_t* space)
{
	cairo_gpu_user_geometry_t* geometry = (cairo_gpu_geometry_t*)malloc(sizeof(cairo_gpu_user_geometry_t));
	geometry->space = space;
	_cairo_gpu_geometry_init(&geometry->geometry, space);
	return &geometry->base;
}

static inline void
_cairo_gpu_geometry_destroy(cairo_gpu_user_geometry_t* geometry)
{
	_cairo_gpu_geometry_fini(0, &geometry->geometry);
	free(geometry);
}
*/
