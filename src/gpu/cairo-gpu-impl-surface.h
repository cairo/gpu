static inline void
_cairo_gpu_surface_modified_(cairo_gpu_surface_t* surface, int i, int x, int y, int width, int height)
{
	if(!surface->bbox[i].width)
	{
		surface->bbox[i].x = x;
		surface->bbox[i].y = y;
		surface->bbox[i].width = width;
		surface->bbox[i].height = height;
	}
	else if(width)
	{
		int x2 = MAX(surface->bbox[i].x + surface->bbox[i].width, x + width);
		int y2 = MAX(surface->bbox[i].y + surface->bbox[i].height, y + height);

		surface->bbox[i].x = MIN(x, surface->bbox[i].x);
		surface->bbox[i].y = MIN(y, surface->bbox[i].y);
		surface->bbox[i].width = x2 - surface->bbox[i].x;
		surface->bbox[i].height = y2 - surface->bbox[i].y;
	}

	surface->valid_mask = 1 << i;
}

static inline void
_cairo_gpu_surface_modified(cairo_gpu_surface_t* surface, int x, int y, int width, int height)
{
	_cairo_gpu_surface_modified_(surface, _cairo_gpu_surface_destination(surface), x, y, width, height);
}

static inline void
_cairo_gpu_surface_modified_drawable(cairo_gpu_surface_t* surface, int x, int y, int width, int height)
{
	_cairo_gpu_surface_modified_(surface, _cairo_gpu_surface_destination_drawable(surface), x, y, width, height);
}

static inline void
_cairo_gpu_surface_cleared_(cairo_gpu_surface_t* surface, int idx)
{
	surface->bbox[idx].width = 0;
	surface->bbox[idx].height = 0;

	surface->valid_mask = 1 << idx;
}

static inline void
_cairo_gpu_surface_cleared(cairo_gpu_surface_t* surface)
{
	_cairo_gpu_surface_cleared_(surface, _cairo_gpu_surface_destination(surface));
}

static void
_cairo_gpu_context_set_discontinuous_width(cairo_gpu_context_t* ctx, int idx, unsigned width)
{
	float so[4] = {0.5 / (double)width, 0.75 / (double)width, 0.0, 0.0};
	_cairo_gpu_context_set_frag_param(ctx, FRAGENV_TEX_DISCONTINUOUS_SO(idx), so);
}

static void
_cairo_gpu_context_set_radial_gradient(cairo_gpu_context_t* ctx, int idx, float* mac, float* so)
{
	//printf("a = %f\n", a);
	//printf("-b/2: %f %f %f\n", mbd2[0], mbd2[1], mbd2[2]);
	//printf("-ac: %f %f %f\n", mac[0], mac[1], mac[2]);
	//printf("* %f + %f\n", so[0], so[2]);
	_cairo_gpu_context_set_frag_param(ctx, FRAGENV_TEX_RADIAL_MAC(idx), mac);
	_cairo_gpu_context_set_frag_param(ctx, FRAGENV_TEX_RADIAL_SO(idx), so);
}

static __attribute__((unused)) void
_cairo_gpu_emit_rect_diff(float **vp, cairo_rectangle_int_t * out, cairo_rectangle_int_t * in)
{
	if(!out->width)
	{}
	else if(!in->width)
	{
		_cairo_gpu_emit_rect(vp, out->x, out->y, out->width, out->height);
	}
	else
	{
		int y = MAX(in->y, out->y);
		int h = MIN(in->y + in->height, out->y + out->height) - y;

		int t;
		int yb;

		// over(in) & out
		yb = MIN(in->y, out->y + out->height);
		t = yb - out->y;
		if(t > 0)
			_cairo_gpu_emit_rect(vp, out->x, out->y, out->width, t);

		if(h > 0)
		{
			// directly_left(in) & out
			t = in->x - out->x;
			if(t > 0)
				_cairo_gpu_emit_rect(vp, out->x, y, t, h);

			// directly_right(in) & out
			t = (out->x + out->width) - (in->y + in->width);;
			if(t > 0)
				_cairo_gpu_emit_rect(vp, in->x + in->width, y, t, h);
		}

		// under(in) & out
		yb = MAX(in->y + in->height, out->y);
		t = (out->y + out->height) - yb;
		if(t > 0)
			_cairo_gpu_emit_rect(vp, out->x, yb, out->width, t);
	}
}

static __attribute__((unused)) void
_cairo_gpu_fill_rect_diff(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface, cairo_rectangle_int_t * out, cairo_rectangle_int_t * in, float r, float g, float b, float a)
{
	if(!out->width)
	{}
	else if(!in->width)
	{
		_cairo_gpu_context_fill_rect(ctx, surface, out->x, out->y, out->width, out->height, r, g, b, a);
	}
	else
	{
		int y = MAX(in->y, out->y);
		int h = MIN(in->y + in->height, out->y + out->height) - y;

		int t;
		int yb;

		// over(in) & out
		yb = MIN(in->y, out->y + out->height);
		t = yb - out->y;
		if(t > 0)
			_cairo_gpu_context_fill_rect(ctx, surface, out->x, out->y, out->width, t, r, g, b, a);

		if(h > 0)
		{
			// directly_left(in) & out
			t = in->x - out->x;
			if(t > 0)
				_cairo_gpu_context_fill_rect(ctx, surface, out->x, y, t, h, r, g, b, a);

			// directly_right(in) & out
			t = (out->x + out->width) - (in->y + in->width);;
			if(t > 0)
				_cairo_gpu_context_fill_rect(ctx, surface, in->x + in->width, y, t, h, r, g, b, a);
		}

		// under(in) & out
		yb = MAX(in->y + in->height, out->y);
		t = (out->y + out->height) - yb;
		if(t > 0)
			_cairo_gpu_context_fill_rect(ctx, surface, out->x, yb, out->width, t, r, g, b, a);
	}
}

/* ignores clipping, use only when that is appropriate */
static inline void
_cairo_gpu_solid_rect(cairo_gpu_context_t* ctx, cairo_gpu_surface_t * surface, int x, int y, int width, int height, float r, float g, float b, float a)
{
	int i = _cairo_gpu_surface_destination(surface);
	cairo_rectangle_int_t *bbox = &surface->bbox[i];

	/* if clearing, intersect with the bounding box and try to subtract the rect from the box */
	if(!r && !g && !b && !a)
	{
		int x2, y2;

		int c;

		if(!bbox->width || !bbox->height)
			return;

		x = MAX(x, bbox->x);
		y = MAX(y, bbox->y);
		x2 = MIN(x + width, bbox->x + (int)bbox->width);
		y2 = MIN(y + height, bbox->y + (int)bbox->height);

		c = (bbox->x == x) + (bbox->y == y) + ((int)(bbox->x + bbox->width) == x2) + ((int)(bbox->y + bbox->height) == y2);
		if(c >= 3)
		{
			if(c == 4)
			{
				bbox->width = 0;
				bbox->height = 0;
			}
			else if(bbox->x != x)
				bbox->width = x - bbox->x;
			else if(bbox->y != y)
				bbox->height = y - bbox->y;
			else if((bbox->y + (int)bbox->height) != y2)
			{
				bbox->height -= y2 - bbox->y;
				bbox->y = y2;
			}
			else if((bbox->x + (int)bbox->width) != x2)
			{
				bbox->width -= x2 - bbox->x;
				bbox->x = x2;
			}
		}

		width = x2 - x;
		height = y2 - y;
	}
	else
		_cairo_gpu_surface_modified(surface, x, y, width, height);

	if(!width || !height)
		return;

	_cairo_gpu_context_fill_rect(ctx, surface, x, y, width, height, r, g, b, a);
}

static void
_cairo_gpu_prepare_blit_image(cairo_gpu_context_t* ctx, cairo_gpu_texture_t* texture, int src_x, int src_y, int zoom_x, int zoom_y)
{
	cairo_surface_attributes_t attrs;

	attrs.matrix.xx = 1.0 / zoom_x;
	attrs.matrix.xy = 0;
	attrs.matrix.x0 = src_x;

	attrs.matrix.yx = 0;
	attrs.matrix.yy = 1.0 / zoom_y;
	attrs.matrix.y0 = src_y;
	attrs.filter = CAIRO_FILTER_NEAREST;
	attrs.extend = CAIRO_EXTEND_NONE;
	attrs.extra = 0;

	_cairo_gpu_texture_adjust_matrix(texture, &attrs.matrix);

	_cairo_gpu_context_set_blend(ctx, BLEND_SOURCE);
	_cairo_gpu_context_set_raster(ctx, 0);
	_cairo_gpu_context_set_vert_frag(ctx, VERT_TEX_GEN << VERT_TEX_SHIFT,
			((FRAG_TEX_COLOR_RGBA | (texture->target_idx == TARGET_RECTANGLE ? FRAG_TEX_RECTANGLE : 0)) << FRAG_TEX_SHIFT));
	_cairo_gpu_context_set_texture_and_attributes(ctx, 0, texture, &attrs);
}

static void
_cairo_gpu_finish_blit_image(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, int dst_x, int dst_y, int width, int height)
{
	_cairo_gpu_context_set_viewport(ctx, 0, 0, dst->width, dst->height);
	_cairo_gpu_context_set_translation(ctx, dst_x, dst_y);
	_cairo_gpu_context_draw_rect(ctx, 0, 0, width, height);
}

static void
_cairo_gpu_blit_image(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, cairo_gpu_texture_t* texture, int dst_x, int dst_y, int src_x, int src_y, int width, int height, int zoom_x, int zoom_y)
{
	_cairo_gpu_prepare_blit_image(ctx, texture, src_x, src_y, zoom_x, zoom_y);
	_cairo_gpu_finish_blit_image(ctx, dst, dst_x, dst_y, width, height);
}

static cairo_gpu_texture_t*
_cairo_gpu_temp_1d_image(cairo_gpu_context_t* ctx, unsigned idx, unsigned* pwidth, int* owned)
{
	unsigned width = *pwidth;
	cairo_gpu_texture_t** texture_pp;
	cairo_gpu_texture_t* texture = 0;

	if(!ctx->space->tex_npot && !ctx->space->tex_rectangle && !is_pow2(width))
		*pwidth = width = higher_pow2(width);

	if(width <= MAX_SMALL_WIDTH)
	{
		texture_pp = &ctx->tls->small_texture_pools[idx].texture[width - 1];
		*owned = 0;
	}
	else
	{
		texture_pp = &texture;
		*owned = 1;
	}

	if(!*texture_pp)
	{
		texture = *texture_pp = (cairo_gpu_texture_t*)malloc(sizeof(cairo_gpu_texture_t));
		_cairo_gpu_texture_create(ctx, texture, width, 1);
	}
	else
		texture = *texture_pp;

	return texture;
}

static void
_cairo_gpu_draw_clipped(cairo_gpu_context_t* ctx, cairo_gpu_surface_t * dst, int dst_x, int dst_y, int width, int height)
{
	if(!dst->has_clip)
	{
		_cairo_gpu_context_set_viewport(ctx, 0, 0, dst->width, dst->height);
		_cairo_gpu_context_draw(ctx);
	}
	else
	{
		int i, nboxes;
		pixman_box32_t *pboxes;

		pboxes = pixman_region32_rectangles(&dst->clip.rgn, &nboxes);
		for(i = 0; i < nboxes; ++i)
		{
			int x1 = MAX(dst_x, pboxes[i].x1);

			int y1 = MAX(dst_y, pboxes[i].y1);

			int x2 = MIN(dst_x + width, pboxes[i].x2);

			int y2 = MIN(dst_y + height, pboxes[i].y2);

			if(x1 >= x2 || y1 >= y2)
				continue;

			//printf("%i %i %i %i %i\n", i, x1, y1, x2, y2);

			_cairo_gpu_context_set_viewport(ctx, x1, y1, x2 - x1, y2 - y1);
			_cairo_gpu_context_draw(ctx);
		}
	}
}


static cairo_surface_t *
_cairo_gpu_surface_create_similar(void *abstract_surface, cairo_content_t content, int width, int height)
{
	cairo_gpu_surface_t *surface = abstract_surface;

	// TODO: specify mantissa/exponent correctly
	return _cairo_gpu_surface_create(surface->space, 0, content, 0, 0, 0, width, height, 0, 0, 0, 0);
}

static cairo_gpu_surface_t *
_cairo_gpu_space_get_cached_mask_surface(cairo_gpu_space_t * space, unsigned width, unsigned height, int aa)
{
	cairo_gpu_surface_t *surf;

	CAIRO_MUTEX_LOCK(space->cached_mask_surface_mutex);
	if(!space->cached_mask_surface || space->cached_mask_surface->width < width || space->cached_mask_surface->height < width)
	{
		unsigned nwidth = space->cached_mask_surface ? space->cached_mask_surface->width : 16;

		unsigned nheight = space->cached_mask_surface ? space->cached_mask_surface->height : 16;

		while(nwidth < width)
			nwidth <<= 1;
		while(nheight < width)
			nheight <<= 1;

		surf = (cairo_gpu_surface_t *) cairo_surface_create(&space->base, &space->cached_mask_surface->base, CAIRO_CONTENT_ALPHA, nwidth, nheight, 0, 8, 0, 0);
	}
	else
		surf = space->cached_mask_surface;

	if(aa && !surf->color_samples && !surf->coverage_samples)
	{
		int *aap;

		for(aap = _cairo_gpu_aa_formats; *aap; aap += 2)
		{
			if(!_cairo_gpu_surface_set_samples(surf, aap[0], aap[1]))
				break;
		}

		printf("set mask msaa to %i %i\n", aap[0], aap[1]);

		if(!*aap)
		{
			if(!space->cached_mask_surface)
				cairo_surface_destroy(&surf->base);
			space->msaa_samples = 1;
			CAIRO_MUTEX_UNLOCK(space->cached_mask_surface_mutex);
			return 0;
		}
		else
		{
			assert(aap[0] == surf->coverage_samples);
			space->msaa_samples = surf->coverage_samples;
		}
	}
	space->cached_mask_surface = surf;

	return surf;
}

static void
_cairo_gpu_space_put_cached_mask_surface(cairo_gpu_space_t * space, cairo_gpu_surface_t * surf)
{
	CAIRO_MUTEX_UNLOCK(space->cached_mask_surface_mutex);
}

static cairo_status_t
_cairo_gpu_surface_acquire_source_image(void *abstract_surface, cairo_image_surface_t ** image_out, void **image_extra)
{
	cairo_status_t status;
	cairo_gpu_surface_t *surface = abstract_surface;

	_cairo_gpu_enter();
	status = _cairo_gpu_surface_get_image(surface, NULL, image_out, NULL, image_extra);
	_cairo_gpu_exit();

	return status;
}

static void
_cairo_gpu_surface_release_source_image(void *abstract_surface, cairo_image_surface_t * image, void *image_extra)
{
	cairo_gpu_surface_t *surface = abstract_surface;
	_cairo_gpu_enter();
	_cairo_gpu_surface_put_image(surface, image, 0, 0, image->width, image->height, 0, 0, 1, image_extra);
	_cairo_gpu_exit();
	cairo_surface_destroy(&image->base);
}

static cairo_status_t
_cairo_gpu_surface_acquire_dest_image(void *abstract_surface, cairo_rectangle_int_t * interest_rect, cairo_image_surface_t ** image_out, cairo_rectangle_int_t * image_rect_out, void **image_extra)
{
	cairo_status_t status;
	cairo_gpu_surface_t *surface = abstract_surface;

	_cairo_gpu_enter();
	status = _cairo_gpu_surface_get_image(surface, interest_rect, image_out, image_rect_out, image_extra);
	_cairo_gpu_exit();

	return status;
}

static void
_cairo_gpu_surface_release_dest_image(void *abstract_surface, cairo_rectangle_int_t * interest_rect, cairo_image_surface_t * image, cairo_rectangle_int_t * image_rect, void *image_extra)
{
	cairo_status_t status;
	cairo_gpu_surface_t* dst = abstract_surface;

	_cairo_gpu_enter();
	status = _cairo_gpu_surface_put_image(dst, image, 0, 0, image->width, image->height, image_rect->x, image_rect->y, 1, image_extra);

	if(status)
		status = _cairo_surface_set_error(abstract_surface, status);
	_cairo_gpu_exit();

	cairo_surface_destroy(&image->base);
}

static inline cairo_status_t
_cairo_gpu_surface_flush(void *abstract_surface)
{
	cairo_gpu_surface_t *surface = abstract_surface;
	int i = _cairo_gpu_surface_destination_drawable(surface);

	if(i < 0 || !surface->valid_mask || surface->valid_mask & (1 << i) || !surface->public_drawable)
		return CAIRO_STATUS_SUCCESS;

	_cairo_gpu_enter();

	// this makes it valid, thus flushing
	_cairo_gpu_surface_bind_drawable(surface, 0);

	_cairo_gpu_exit();
	return CAIRO_STATUS_SUCCESS;
}

static inline cairo_status_t
_cairo_gpu_surface_mark_dirty_rectangle(void *abstract_surface, int x, int y, int width, int height)
{
	cairo_gpu_surface_t *surface = abstract_surface;
	_cairo_gpu_surface_modified_drawable(surface, x, y, width, height);
	return CAIRO_STATUS_SUCCESS;
}

static cairo_status_t
_cairo_gpu_surface_clone_similar(void *abstract_surface, cairo_surface_t * src, cairo_content_t content, int src_x, int src_y, int width, int height, int *clone_offset_x, int *clone_offset_y, cairo_surface_t ** clone_out)
{
	cairo_gpu_surface_t *surface = abstract_surface;
	cairo_gpu_surface_t *clone;
	cairo_image_surface_t *image_src = (cairo_image_surface_t *) src;
	cairo_status_t status;

	if(src->backend == surface->base.backend)
	{
		*clone_offset_x = 0;
		*clone_offset_y = 0;
		*clone_out = cairo_surface_reference(src);

		return CAIRO_STATUS_SUCCESS;
	}

	if(!_cairo_surface_is_image(src))
		return CAIRO_INT_STATUS_UNSUPPORTED;

	_cairo_gpu_enter();

	clone = (cairo_gpu_surface_t *) _cairo_gpu_surface_create_similar(&surface->base, content, width, height);
	if(clone == NULL)
	{
		status = CAIRO_INT_STATUS_UNSUPPORTED;
		goto out;
	}
	if(clone->base.status)
	{
		status = clone->base.status;
		goto out;
	}

	status = _cairo_gpu_surface_draw_image(clone, image_src, src_x, src_y, width, height, 0, 0);

	if(status)
	{
		cairo_surface_destroy(&clone->base);
		goto out;
	}

	*clone_out = &clone->base;
	*clone_offset_x = src_x;
	*clone_offset_y = src_y;

out:
	_cairo_gpu_exit();
	return status;
}

static cairo_int_status_t
_cairo_gpu_surface_get_extents(void *abstract_surface, cairo_rectangle_int_t * rectangle)
{
	cairo_gpu_surface_t *surface = abstract_surface;

	rectangle->x = 0;
	rectangle->y = 0;
	rectangle->width = surface->width;
	rectangle->height = surface->height;

	return CAIRO_STATUS_SUCCESS;
}


static cairo_int_status_t
_cairo_gpu_surface_show_page(void *abstract_surface)
{
	cairo_gpu_surface_t *surface = (cairo_gpu_surface_t *) abstract_surface;
	cairo_int_status_t status = _cairo_gpu_surface_copy_page(surface);
	if(!status)
		surface->valid_mask = 0;
	return status;
}

static void
_cairo_gpu_surface_get_font_options (void		    *abstract_surface,
				      cairo_font_options_t  *options)
{
    cairo_gpu_surface_t *surface = abstract_surface;

    *options = *_cairo_gpu_get_font_options (surface->space);
}


static void
_cairo_gpu_surface_fix_pad(cairo_gpu_surface_t* surface, cairo_extend_t extend)
{
	cairo_gpu_context_t *ctx;

	if(extend == CAIRO_EXTEND_NONE)
	{
		if(surface->transparent_padding)
			return;

		if(surface->space->use_fbo)
		{
			ctx = _cairo_gpu_surface_bind(surface, FB_DRAW);

			if(surface->width < surface->texture.width)
				_cairo_gpu_context_fill_rect(ctx, surface, surface->width, 0, surface->texture.width, surface->height, 0, 0, 0, 0);

			if(surface->height < surface->texture.height)
				_cairo_gpu_context_fill_rect(ctx, surface, 0, surface->height, surface->texture.width, surface->texture.height - surface->height, 0, 0, 0, 0);
		}
		else
		{
			cairo_gpu_surface_t* temp;
			unsigned w = surface->texture.width - surface->width;
			unsigned h = surface->texture.height - surface->height;

			if(surface->width < surface->texture.width)
				h = MAX(h, surface->height);

			if(surface->height < surface->texture.height)
				w = MAX(w, surface->width);

			temp = _cairo_gpu_space_get_cached_mask_surface(surface->space, w, h, 0);
			temp->base.content = CAIRO_CONTENT_COLOR_ALPHA;
			//temp->clear_rgba = 1;

			_cairo_gpu_surface_orient_drawable_like_texture(temp, surface);

			ctx = _cairo_gpu_surface_bind(temp, FB_READ | FB_DRAW);

			if(surface->width < surface->texture.width)
			{
				_cairo_gpu_context_fill_rect(ctx, surface, 0, 0, surface->texture.width - surface->width, surface->height, 0, 0, 0, 0);
				_cairo_gpu_context_read_to_texture(ctx, surface, surface->width, 0, 0, 0, surface->texture.width - surface->width, surface->height);
			}

			if(surface->height < surface->texture.height)
			{
				_cairo_gpu_context_fill_rect(ctx, surface, 0, 0, surface->width, surface->texture.height - surface->height, 0, 0, 0, 0);
				_cairo_gpu_context_read_to_texture(ctx, surface, 0, surface->height, 0, 0, surface->width, surface->texture.height - surface->height);

				if(surface->width < surface->texture.width)
					_cairo_gpu_context_read_to_texture(ctx, surface, surface->width, surface->height, 0, 0, surface->texture.width - surface->width, surface->texture.height - surface->height);
			}

			_cairo_gpu_surface_modified(temp, 0, 0, w, h);

			temp->base.content = CAIRO_CONTENT_ALPHA;
			_cairo_gpu_space_put_cached_mask_surface(surface->space, temp);
		}

		surface->valid_mask &= ~(1 << SURF_PSEUDO_PADDING);
		surface->transparent_padding = 1;
	}
	else
	{
		if(surface->valid_mask & (1 << SURF_PSEUDO_PADDING))
			return;

		if(surface->space->use_fbo)
		{
			// only with FBO we have destination as large as the texture
			ctx = _cairo_gpu_surface_bind(surface, FB_READ | FB_DRAW);

			if(surface->width < surface->texture.width)
				_cairo_gpu_context_blit_zoom(ctx, surface, surface->width, 0, surface->width - 1, 0, 1, surface->height, surface->texture.width - surface->width, 1);

			if(surface->height < surface->texture.height)
			{
				_cairo_gpu_context_blit_zoom(ctx, surface, 0, surface->height, 0, surface->height - 1, surface->width, 1, 1, surface->texture.height - surface->height);

				if(surface->width < surface->texture.width)
				{
					_cairo_gpu_context_blit_zoom(ctx, surface, surface->width, surface->height, surface->width - 1, surface->height - 1, 1, 1, surface->texture.width - surface->width, surface->texture.height - surface->height);
				}
			}
		}
		else
		{
			cairo_gpu_surface_t* temp;
			unsigned w = surface->texture.width - surface->width;
			unsigned h = surface->texture.height - surface->height;

			if(surface->width < surface->texture.width)
				h = MAX(h, surface->height);

			if(surface->height < surface->texture.height)
				w = MAX(w, surface->width);

			// Clever hack here that allows us to reuse the cached mask surface!
			// here we don't have FBOs, so the cached mask surface is not MSAA, hence we can safely use it!
			// not having FBO means the card can only render to RGBA even if we only have an alpha surface.
			// the texture may be alpha-only but we don't care
			// But! We must clear all the channels, so we use the "clear RGBA" hack
			// clear_rgba should be unnecessary since alpha-only use will ignore color channels and we clear the parts we use ourselves
			temp = _cairo_gpu_space_get_cached_mask_surface(surface->space, w, h, 0);
			temp->base.content = CAIRO_CONTENT_COLOR_ALPHA;
			//temp->clear_rgba = 1;

			_cairo_gpu_surface_orient_drawable_like_texture(temp, surface);

			ctx = _cairo_gpu_surface_bind(temp, FB_DRAW);

			if(surface->width < surface->texture.width)
			{
				_cairo_gpu_blit_image(ctx, temp, &surface->texture, 0, 0,
						surface->width - 1, 0, surface->texture.width - surface->width, surface->height, surface->texture.width - surface->width, 1);
				_cairo_gpu_context_read_to_texture(ctx, surface, surface->width, 0, 0, 0, surface->texture.width - surface->width, surface->height);
			}

			if(surface->height < surface->texture.height)
			{
				_cairo_gpu_blit_image(ctx, temp, &surface->texture, 0, 0,
						0, surface->height - 1, surface->width, surface->texture.height - surface->height, 1, surface->texture.height - surface->height);
				_cairo_gpu_context_read_to_texture(ctx, surface, 0, surface->height, 0, 0, surface->width, surface->texture.height - surface->height);

				if(surface->width < surface->texture.width)
				{
					_cairo_gpu_blit_image(ctx, temp, &surface->texture, 0, 0,
							surface->width - 1, surface->height - 1, surface->texture.width - surface->width, surface->texture.height - surface->height, surface->width, surface->height);
					_cairo_gpu_context_read_to_texture(ctx, surface, surface->width, surface->height, 0, 0, surface->texture.width - surface->width, surface->texture.height - surface->height);
				}
			}

			_cairo_gpu_surface_modified(temp, 0, 0, w, h);

			temp->base.content = CAIRO_CONTENT_ALPHA;
			_cairo_gpu_space_put_cached_mask_surface(surface->space, temp);
		}

		surface->valid_mask |= (1 << SURF_PSEUDO_PADDING);
		surface->transparent_padding = 0;
	}
}
