#define L_TERMINATOR ";"
#define L_TEXTURE "texture"
#define L_PROGRAM_ENV "program.env"

#define L_RESULT_COLOR_TEMP "result_color"
#define L_SET_RESULT_COLOR(p) (p)[6] = '.'

#define L_TMP "tmp"

#define L_TEMP_TMP "TEMP tmp"
#define L_TEMP_RESULT_COLOR "TEMP result_color"

#define L_0 "0"
#define L_1 "1"

#define L_SCALAR_X ".x"

#include "cairo-gpu-impl-programs.h"

static inline void
_cairo_gpu_context_set_vert_param(cairo_gpu_context_t* ctx, unsigned i, float* v)
{
	ctx->gl.ProgramEnvParameter4fvARB(GL_VERTEX_PROGRAM_ARB, i, v);;
}

static inline void
_cairo_gpu_context_set_frag_param(cairo_gpu_context_t* ctx, unsigned i, float* v)
{
	ctx->gl.ProgramEnvParameter4fvARB(GL_FRAGMENT_PROGRAM_ARB, i, v);
}

static inline unsigned
_cairo_gl_context_active_target(cairo_gpu_context_t* ctx)
{
	return _cairo_gl_target(ctx->textures[ctx->active_texture].active_target_idx);
}

static inline cairo_gpu_texture_t*
_cairo_gpu_context__active_texture(cairo_gpu_context_t* ctx, int idx)
{
	return ctx->textures[idx].targets[ctx->textures[idx].active_target_idx].texture;
}

static inline void
_cairo_gpu_context__do_set_active_texture(cairo_gpu_context_t* ctx, unsigned i)
{
	ctx->gl.ActiveTextureARB(GL_TEXTURE0_ARB + i);
	ctx->active_texture = i;
}

static inline void
_cairo_gl_context_set_active_texture(cairo_gpu_context_t* ctx, unsigned i)
{
	if(ctx->active_texture != i)
		_cairo_gpu_context__do_set_active_texture(ctx, i);
}

static void
_cairo_gl_context_set_texture_target(cairo_gpu_context_t* ctx, int i, unsigned target)
{
	if(target != ctx->textures[i].enabled_target)
	{
		_cairo_gl_context_set_active_texture(ctx, i);
		if(ctx->textures[i].enabled_target)
			ctx->gl.Disable(ctx->textures[i].enabled_target);
		if(target)
			ctx->gl.Enable(target);
		ctx->textures[i].enabled_target = target;
	}
}

static void
_cairo_gpu_context__set_nv_texture_shader(cairo_gpu_context_t* ctx, int i, unsigned nv_op)
{
	if(ctx->nv_texture_shader)
	{
		if(nv_op != ctx->textures[i].nv_texture_shader)
		{
			_cairo_gl_context_set_active_texture(ctx, i);
			ctx->gl.TexEnvi(GL_TEXTURE_SHADER_NV, GL_SHADER_OPERATION_NV, nv_op);
			ctx->textures[i].nv_texture_shader = nv_op;
		}
	}
}

static void
_cairo_gpu_context__set_mipmap_hint(cairo_gpu_context_t* ctx)
{
	if(_cairo_gpu_context__active_texture(ctx, ctx->active_texture)->filter != GL_NEAREST)
	{
		ctx->gl.TexParameteri(_cairo_gl_context_active_target(ctx), GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		ctx->gl.TexParameteri(_cairo_gl_context_active_target(ctx), GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		_cairo_gpu_context__active_texture(ctx, ctx->active_texture)->filter = GL_NEAREST;
	}
}

// We use "generations" for textures because if we delete a texture which is shared among context and the number is reused, we have a problem that can only be solved this way
// NOT guaranteed to make target texture unit active
static int
_cairo_gl_context_set_texture(cairo_gpu_context_t* ctx, int i, cairo_gpu_texture_t* texture)
{
	int j;
	if(i < 0)
		i = ctx->preferred_texture;
	else
		ctx->preferred_texture = i;
	j = texture->target_idx;

	ctx->textures[i].active_target_idx = j;

	if(ctx->textures[i].targets[j].tex != texture->tex || !texture->id || ctx->textures[i].targets[j].id != texture->id)
	{
		_cairo_gl_context_set_active_texture(ctx, i);
		ctx->gl.BindTextureEXT(_cairo_gl_context_active_target(ctx), texture->tex);
		ctx->textures[i].targets[j].tex = texture->tex;
		ctx->textures[i].targets[j].id = texture->id;
		ctx->textures[i].targets[j].texture = texture;

		/*
		j ^= 1;
		if(ctx->textures[i].targets[j].tex)
		{
			ctx->gl.BindTextureEXT(_cairo_gl_target(j), 0);
			ctx->textures[i].targets[j].tex = 0;
			ctx->textures[i].targets[j].id = 0;
			ctx->textures[i].targets[j].texture = 0;
		}
		*/
	}
	return i;
}

static void
_cairo_gl_context_set_framebuffer(cairo_gpu_context_t* ctx, int mask, GLuint fb, long long id, unsigned buffer, int flip_height)
{
	if(!mask)
		return;

	if(!ctx->space->fb_blit)
		mask = FB_DRAW | FB_READ;

	if(mask == (FB_DRAW | FB_READ))
	{
		if(ctx->draw_fb != fb || ctx->draw_fb_id != id || ctx->read_fb != fb || ctx->read_fb_id != id)
		{
			ctx->gl.BindFramebufferEXT(GL_FRAMEBUFFER_EXT, fb);

			ctx->draw_fb = fb;
			ctx->draw_fb_id = id;
			ctx->draw_buffer = 0;
			ctx->read_fb = fb;
			ctx->read_fb_id = id;
			ctx->read_buffer = 0;
		}
		ctx->draw_flip_height = flip_height;
		ctx->read_flip_height = flip_height;
	}
	else if(mask == FB_DRAW)
	{
		if(ctx->draw_fb != fb || ctx->draw_fb_id != id)
		{
			ctx->gl.BindFramebufferEXT(GL_DRAW_FRAMEBUFFER, fb);
			ctx->draw_fb = fb;
			ctx->draw_fb_id = id;
			ctx->draw_buffer = 0;
		}
		ctx->draw_flip_height = flip_height;
	}
	else if(mask == FB_READ)
	{
		if(ctx->read_fb != fb || ctx->read_fb_id != id)
		{
			ctx->gl.BindFramebufferEXT(GL_READ_FRAMEBUFFER, fb);

			ctx->read_fb = fb;
			ctx->read_fb_id = id;
			ctx->read_buffer = 0;
		}
		ctx->read_flip_height = flip_height;
	}

	if(buffer)
	{
		if((mask & FB_DRAW) && ctx->draw_buffer != buffer)
		{
			ctx->gl.DrawBuffer(buffer);
			ctx->draw_buffer = buffer;
		}

		if((mask & FB_READ) && ctx->read_buffer != buffer)
		{
			ctx->gl.ReadBuffer(buffer);
			ctx->read_buffer = buffer;
		}
	}
}

static GLenum
_cairo_gl_context_set_any_framebuffer(cairo_gpu_context_t* ctx, GLuint fb, long long id, int buffer)
{
	if(!ctx->space->fb_blit)
	{
		_cairo_gl_context_set_framebuffer(ctx, FB_READ | FB_DRAW, fb, id, buffer, -1);
		return GL_FRAMEBUFFER_EXT;
	}

	_cairo_gl_context_set_framebuffer(ctx, FB_READ, fb, id, buffer, -1);
	return GL_READ_FRAMEBUFFER;
}

static inline void
_cairo_gl_context_set_texture_extend(cairo_gpu_context_t* ctx, int idx, cairo_extend_t extend, int is_1d)
{
	unsigned wrap_s, wrap_t;
	if(!(ctx->space->extend_mask & (1 << extend)))
		wrap_s = GL_CLAMP;
	else
	{
		switch (extend)
		{
		case CAIRO_EXTEND_NONE:
			wrap_s = GL_CLAMP_TO_BORDER;
			break;
		case CAIRO_EXTEND_PAD:
			wrap_s = GL_CLAMP_TO_EDGE;
			break;
		case CAIRO_EXTEND_REPEAT:
			wrap_s = GL_REPEAT;
			break;
		case CAIRO_EXTEND_REFLECT:
			wrap_s = GL_MIRRORED_REPEAT;
			break;
		}
	}

	if(is_1d)
		wrap_t = GL_CLAMP_TO_EDGE;
	else
		wrap_t = wrap_s;

	if(_cairo_gpu_context__active_texture(ctx, idx)->wrap_s != wrap_s)
	{
		_cairo_gl_context_set_active_texture(ctx, idx);
		ctx->gl.TexParameteri(_cairo_gl_context_active_target(ctx), GL_TEXTURE_WRAP_S, wrap_s);
		_cairo_gpu_context__active_texture(ctx, idx)->wrap_s = wrap_s;
	}

	if(_cairo_gpu_context__active_texture(ctx, idx)->wrap_t != wrap_t)
	{
		_cairo_gl_context_set_active_texture(ctx, idx);
		ctx->gl.TexParameteri(_cairo_gl_context_active_target(ctx), GL_TEXTURE_WRAP_T, wrap_t);
		_cairo_gpu_context__active_texture(ctx, idx)->wrap_t = wrap_t;
	}
}

// TODO: are we sure we don't want to use mipmaps?
static inline void
_cairo_gl_context_set_texture_filter(cairo_gpu_context_t* ctx, int idx, cairo_filter_t filter)
{
	float anisotropy = 1.0;
	unsigned gl_filter = GL_NEAREST;

	switch (filter)
	{
	case CAIRO_FILTER_FAST:
	case CAIRO_FILTER_NEAREST:
		break;
	case CAIRO_FILTER_BEST:
		anisotropy = ctx->space->max_anisotropy;
		gl_filter = GL_LINEAR;
		break;
	case CAIRO_FILTER_GOOD:
		// TODO: is 4x anisotropic on GOOD, which it the default, a good idea? Should we do 0? Maybe max?
		anisotropy = 4.0;
		if(anisotropy > ctx->space->max_anisotropy)
			anisotropy = ctx->space->max_anisotropy;
		gl_filter = GL_LINEAR;
		break;
	case CAIRO_FILTER_BILINEAR:
		gl_filter = GL_LINEAR;
		break;
	default:
	case CAIRO_FILTER_GAUSSIAN:
		ASSERT_NOT_REACHED;
	}

	if(_cairo_gpu_context__active_texture(ctx, idx)->filter != gl_filter)
	{
		_cairo_gl_context_set_active_texture(ctx, idx);
		ctx->gl.TexParameteri(_cairo_gl_context_active_target(ctx), GL_TEXTURE_MIN_FILTER, gl_filter);
		ctx->gl.TexParameteri(_cairo_gl_context_active_target(ctx), GL_TEXTURE_MAG_FILTER, gl_filter);
		_cairo_gpu_context__active_texture(ctx, idx)->filter = gl_filter;
	}

#ifndef DEBUG_DISABLE_ANISOTROPY
	if(_cairo_gpu_context__active_texture(ctx, idx)->anisotropy != anisotropy)
	{
		_cairo_gl_context_set_active_texture(ctx, idx);
		ctx->gl.TexParameteri(_cairo_gl_context_active_target(ctx), GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropy);
		_cairo_gpu_context__active_texture(ctx, idx)->anisotropy = anisotropy;
	}
#endif
}

static inline void
_cairo_gl_context_set_texture_matrix(cairo_gpu_context_t* ctx, int idx, cairo_matrix_t * matrix, float* zm)
{
	if(!ctx->vertp_enabled)
	{
		GLdouble m[16];
		_cairo_gl_context_set_active_texture(ctx, idx);
		ctx->gl.MatrixMode(GL_TEXTURE);
		m[0] = matrix->xx;
		m[1] = matrix->yx;
		m[2] = zm[0];
		m[3] = 0;
		m[4] = matrix->xy;
		m[5] = matrix->yy;
		m[6] = zm[1];
		m[7] = 0;
		m[8] = 0;
		m[9] = 0;
		m[10] = 0;
		m[11] = 0;
		m[12] = matrix->x0;
		m[13] = matrix->y0;
		m[14] = zm[2];
		m[15] = 1;
		ctx->gl.LoadMatrixd(m);
	}
	else
	{
		float xv[4] = {matrix->xx, matrix->yx, zm[0], 0};
		float yv[4] = {matrix->xy, matrix->yy, zm[1], 0};
		float wv[4] = {matrix->x0, matrix->y0, zm[2], 1};

		_cairo_gpu_context_set_vert_param(ctx, VERTENV_TEX_MATRIX_X(idx), xv);
		_cairo_gpu_context_set_vert_param(ctx, VERTENV_TEX_MATRIX_Y(idx), yv);
		_cairo_gpu_context_set_vert_param(ctx, VERTENV_TEX_MATRIX_W(idx), wv);
	}
}

// note that attributes->matrix must be adjusted by the caller, if necessary!!
static void
_cairo_gpu_context_set_texture_and_attributes_(cairo_gpu_context_t* ctx, int idx, cairo_gpu_texture_t* texture, cairo_surface_attributes_t* attributes, float* zm)
{
	_cairo_gl_context_set_texture(ctx, idx, texture);
	_cairo_gl_context_set_texture_matrix(ctx, idx, &attributes->matrix, zm);
	_cairo_gl_context_set_texture_extend(ctx, idx, attributes->extend, (unsigned)attributes->extra & 1);
	_cairo_gl_context_set_texture_filter(ctx, idx, attributes->filter);
}

static void
_cairo_gpu_context_set_texture_and_attributes(cairo_gpu_context_t* ctx, int idx, cairo_gpu_texture_t* texture, cairo_surface_attributes_t* attributes)
{
	_cairo_gpu_context_set_texture_and_attributes_(ctx, idx, texture, attributes, _cairo_gpu_vec4_zero);
}

static int
cairo_gpu_gl_format(cairo_gpu_space_t* space, pixman_format_code_t f, int* internal_format, int* format)
{
	*format = 0;

	if(PIXMAN_FORMAT_RGB(f))
	{
		if(PIXMAN_FORMAT_TYPE(f) == PIXMAN_TYPE_BGRA)
		{
			*format =  GL_BGRA;
			return GL_UNSIGNED_INT_8_8_8_8;
		}
		else if(format)
		{
			if(PIXMAN_FORMAT_TYPE(f) == PIXMAN_TYPE_ABGR)
				*format = GL_RGBA;
			else if(PIXMAN_FORMAT_TYPE(f) == PIXMAN_TYPE_ARGB)
				*format = GL_BGRA;
		}

		if(!*internal_format)
			*internal_format = PIXMAN_FORMAT_A(f) ? GL_RGBA : GL_RGB;

		switch(PIXMAN_FORMAT_G(f))
		{
		case 10:
			return GL_UNSIGNED_INT_2_10_10_10_REV;
		case 8:
			return PIXMAN_FORMAT_BPP(f) == 24 ? GL_UNSIGNED_BYTE : GL_UNSIGNED_INT_8_8_8_8_REV;
		case 6:
			return GL_UNSIGNED_SHORT_5_6_5;
		case 5:
			return GL_UNSIGNED_SHORT_1_5_5_5_REV;
		case 4:
			return GL_UNSIGNED_SHORT_4_4_4_4_REV;
		case 3:
			return GL_UNSIGNED_BYTE_2_3_3_REV;
		default:
			return 0;
		}
	}

	if(PIXMAN_FORMAT_A(f))
	{
		if(PIXMAN_FORMAT_A(f) == 8)
		{
			if(!*internal_format)
				*internal_format = space->use_intensity ? GL_INTENSITY : GL_ALPHA;

			if(*internal_format == GL_INTENSITY)
				*format = GL_LUMINANCE;
			else if(*internal_format == GL_ALPHA)
				*format = GL_ALPHA;
			else // let Pixman do the conversion. Doing it with GL is impossible because we can't give GL_INTENSITY input...
				return 0;
			return GL_UNSIGNED_BYTE;
		}
		else if(PIXMAN_FORMAT_A(f) == 1)
		{
			if(!*internal_format)
				*internal_format = space->use_intensity ? GL_INTENSITY : GL_ALPHA;
			*format = GL_COLOR_INDEX;
			return GL_BITMAP;
		}
	}

	return 0;
}

/* GL_FLOAT appears to be supported in all OpenGL versions */
static inline void
_cairo_gpu_context__upload_data(cairo_gpu_context_t* ctx, int idx, int internal_format, float* data, int width, int height)
{
	ctx->gl.PixelStorei(GL_UNPACK_ALIGNMENT, 1);
	ctx->gl.PixelStorei(GL_UNPACK_ROW_LENGTH, width);

	_cairo_gl_context_set_active_texture(ctx, idx);
	if(internal_format)
	{
		_cairo_gpu_context__set_mipmap_hint(ctx);
		ctx->gl.TexImage2D(_cairo_gl_context_active_target(ctx), 0, internal_format, width, height, 0, GL_RGBA, GL_FLOAT, data);
	}
	else
		ctx->gl.TexSubImage2D(_cairo_gl_context_active_target(ctx), 0, 0, 0, width, height, GL_RGBA, GL_FLOAT, data);
}

static inline void
_cairo_gpu_context_upload_data(cairo_gpu_context_t* ctx, int idx, cairo_gpu_texture_t* texture, float* data, int width, int height)
{
	_cairo_gl_context_set_texture(ctx, idx, texture);
	_cairo_gpu_context__upload_data(ctx, idx, GL_RGBA, data, width, height);
}

static void
_cairo_gpu_surface__flip_texture(cairo_gpu_surface_t* surface);

static cairo_status_t
_cairo_gpu_context__upload_pixels(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, int idx, cairo_image_surface_t * image_surface, int src_x, int src_y, int width, int height, int dst_x, int dst_y)
{
	unsigned char* p;
	unsigned char* buf = 0;
	int f = image_surface->pixman_format;
	int internal_format;
	int format;
	int i;
	int type;
	float i_to_x[2] = {0.0, 1.0};

	internal_format = (idx >= 0) ? _cairo_gpu_context__active_texture(ctx, idx)->unsized_format : 0;

	type = cairo_gpu_gl_format(ctx->space, f, &internal_format, &format);
	if(!type)
		goto fallback;

	if(PIXMAN_FORMAT_BPP(f) == 1)
	{
		p = image_surface->data + src_y * image_surface->stride;

		ctx->gl.PixelStorei(GL_UNPACK_LSB_FIRST, 1);
		ctx->gl.PixelStorei(GL_UNPACK_ALIGNMENT, 1);
		ctx->gl.PixelStorei(GL_UNPACK_ROW_LENGTH, image_surface->stride * 8);
		ctx->gl.PixelStorei(GL_UNPACK_SKIP_PIXELS, src_x);
	}
	else
	{
		int cpp = PIXMAN_FORMAT_BPP(f) >> 3;
		int stride = image_surface->stride;
		int stride_pixels = stride / cpp;
		int stride_red = stride_pixels * cpp;

		p = image_surface->data + src_x * cpp + src_y * image_surface->stride;

		if(stride_red == stride)
		{
			ctx->gl.PixelStorei(GL_UNPACK_ALIGNMENT, 1);
			ctx->gl.PixelStorei(GL_UNPACK_ROW_LENGTH, stride_pixels);
		}
		else if(((stride_red + 1) & ~1) == stride)
		{
			ctx->gl.PixelStorei(GL_UNPACK_ALIGNMENT, 2);
			ctx->gl.PixelStorei(GL_UNPACK_ROW_LENGTH, stride_pixels);
		}
		else if(((stride_red + 3) & ~3) == stride)
		{
			ctx->gl.PixelStorei(GL_UNPACK_ALIGNMENT, 4);
			ctx->gl.PixelStorei(GL_UNPACK_ROW_LENGTH, stride_pixels);
		}
		else
		{
			buf = (unsigned char*)malloc(cpp * width * height);
			for(i = 0; i < height; ++i)
				memcpy(buf + i * cpp * width, p + i * image_surface->stride, cpp * width);
			ctx->gl.PixelStorei(GL_UNPACK_ALIGNMENT, 1);
			ctx->gl.PixelStorei(GL_UNPACK_ROW_LENGTH, 0);
			p = buf;
		}
	}

	if(format == GL_COLOR_INDEX)
	{
		if(!ctx->init_pixel_map)
		{
			ctx->gl.PixelMapfv(GL_PIXEL_MAP_I_TO_R, 2, i_to_x);
			ctx->gl.PixelMapfv(GL_PIXEL_MAP_I_TO_G, 2, i_to_x);
			ctx->gl.PixelMapfv(GL_PIXEL_MAP_I_TO_B, 2, i_to_x);
			ctx->gl.PixelMapfv(GL_PIXEL_MAP_I_TO_A, 2, i_to_x);
			ctx->init_pixel_map = 1;
		}
	}

	/* For GL_RGB textures, alpha values are always sampled as 1, even if CLAMP_TO_BORDER is set.
	   This causes the device-offset-fractional test to fail.
	   It seems unfixable without fragment programs (and even then it would probably be inefficient to implement)

	   TODO: when fragment programs are supported, maybe conditionally eliminate this
	 */

	// we also do this so that we can steal the alpha channel to draw trapezoid masks
	if(internal_format == GL_RGB)
		internal_format = GL_RGBA;

	if(!PIXMAN_FORMAT_A(f) && PIXMAN_FORMAT_BPP(f) == 32)
	{
		ctx->gl.PixelTransferf(GL_ALPHA_SCALE, 0.0);
		ctx->gl.PixelTransferf(GL_ALPHA_BIAS, 1.0);
	}

	if(idx < 0)
	{
		ctx->gl.PixelZoom(1.0, ctx->draw_flip_height >= 0 ? -1.0 : 1.0);
		if(ctx->space->has_window_pos)
		{
			if(ctx->draw_flip_height >= 0)
				dst_y = ctx->draw_flip_height - dst_y;
			ctx->gl.WindowPos2i(dst_x, dst_y);
		}
		else
			ctx->gl.RasterPos2i(dst_x, dst_y);
		ctx->gl.DrawPixels(width, height, format, type, p);
	}
	else
	{
		if(dst)
		{
			if(dst->texture.non_upside_down)
			{
				// if we want to relax this, we must flip the data if necessary
				assert(dst_x <= 0 && dst_y <= 0 && width >= (int)dst->width && height >= (int)dst->height);
				_cairo_gpu_surface__flip_texture(dst);
			}
		}
		else // texture must be upside-down
			assert(dst_x < 0 && dst_y < 0);

		_cairo_gl_context_set_active_texture(ctx, idx);
		if(dst_x < 0)
		{
			_cairo_gpu_context__set_mipmap_hint(ctx);
			if(_cairo_gl_context_active_target(ctx) == GL_TEXTURE_2D && !ctx->space->tex_npot && (!is_pow2(width) || !is_pow2(height)))
			{
				ctx->gl.TexImage2D(_cairo_gl_context_active_target(ctx), 0, internal_format, higher_pow2(width), higher_pow2(height), 0, format, type, p);
				ctx->gl.TexSubImage2D(_cairo_gl_context_active_target(ctx), 0, 0, 0, width, height, format, type, p);
			}
			else
				ctx->gl.TexImage2D(_cairo_gl_context_active_target(ctx), 0, internal_format, width, height, 0, format, type, p);

			_cairo_gpu_context__active_texture(ctx, idx)->unsized_format = internal_format;
		}
		else
			ctx->gl.TexSubImage2D(_cairo_gl_context_active_target(ctx), 0, dst_x, dst_y, width, height, format, type, p);
	}

	if(!PIXMAN_FORMAT_A(f) && PIXMAN_FORMAT_BPP(f) == 32)
	{
		ctx->gl.PixelTransferf(GL_ALPHA_SCALE, 1.0);
		ctx->gl.PixelTransferf(GL_ALPHA_BIAS, 0.0);
	}

	if(PIXMAN_FORMAT_BPP(f) == 1)
		ctx->gl.PixelStorei(GL_UNPACK_SKIP_PIXELS, 0);

	if(buf)
		free(buf);

	if(ctx->gl.GetError())
	{
		cairo_surface_t* converted;
		cairo_pattern_t* pattern;
		cairo_format_t fallback_format;
		cairo_int_status_t status;

		while(ctx->gl.GetError())
		{}

fallback:
		// TODO: use pixman directly
		fallback_format = _cairo_format_from_content(image_surface->base.content);

		if(fallback_format == image_surface->format)
		{
			fallback_format = CAIRO_FORMAT_ARGB32;
			if(fallback_format == image_surface->format)
				return CAIRO_INT_STATUS_UNSUPPORTED;
		}

		// this does not recurse into the GL backend
		// TODO: maybe use pixman directly?
		converted = cairo_image_surface_create(fallback_format, width, height);
		pattern = cairo_pattern_create_for_surface(&image_surface->base);
		status = _cairo_surface_composite(CAIRO_OPERATOR_SOURCE, pattern, 0, converted, src_x, src_y, 0, 0, 0, 0, width, height);

		_cairo_gpu_context__upload_pixels(ctx, dst, idx, (cairo_image_surface_t*)converted, 0, 0, width, height, dst_x, dst_y);
		cairo_pattern_destroy(pattern);
		cairo_surface_destroy(converted);
		return status;
	}

	return CAIRO_STATUS_SUCCESS;
}

static inline void
_cairo_gpu_context_upload_pixels(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, int idx, cairo_gpu_texture_t* texture, cairo_image_surface_t * image_surface, int src_x, int src_y, int width, int height, int dst_x, int dst_y)
{
	idx = _cairo_gl_context_set_texture(ctx, idx, texture);
	_cairo_gpu_context__upload_pixels(ctx, dst, idx, image_surface, src_x, src_y, width, height, dst_x, dst_y);
}

static inline void
_cairo_gpu_context__set_prog(cairo_gpu_context_t* ctx, unsigned target, unsigned table, unsigned value, char* (*writef)(cairo_gpu_space_t*, unsigned))
{
	unsigned p;
	cairo_gpu_int_entry_t* entry = _cairo_gpu_space__lookup(ctx->space, table | value);
	if(entry)
		p = entry->v;
	else
	{
		char* ps;
		ps = writef(ctx->space, value);
		ctx->gl.GenProgramsARB(1, &p);
		ctx->gl.BindProgramARB(target, p);
		ctx->gl.ProgramStringARB(target, GL_PROGRAM_FORMAT_ASCII_ARB, strlen(ps), ps);
		free(ps);

		{
			unsigned todel = 0;
			cairo_hash_entry_t lookup;
			lookup.hash = table | value;

			CAIRO_MUTEX_LOCK(ctx->space->mutex);
			entry = (cairo_gpu_int_entry_t*)_cairo_hash_table_lookup(ctx->space->table, &lookup);
			if(entry)
			{
				todel = p;
				p = entry->v;
			}
			else
			{
				entry = (cairo_gpu_int_entry_t*)malloc(sizeof(cairo_gpu_int_entry_t));
				entry->base.hash = table | value;
				entry->v = p;

				if(_cairo_hash_table_insert(ctx->space->table, &entry->base))
				{
					// XXX: this will leak the program
					free(entry);
				}
			}
			CAIRO_MUTEX_UNLOCK(ctx->space->mutex);
			if(todel)
					ctx->gl.DeleteProgramsARB(1, &todel);
		}
	}

	ctx->gl.BindProgramARB(target, p);
}

static void
_cairo_gpu_context__init_vert_fixed(cairo_gpu_context_t* ctx)
{
	GLfloat tex_gen_s[] = { 1.0f, 0.0f, 0.0f, 0.0f };
	GLfloat tex_gen_t[] = { 0.0f, 1.0f, 0.0f, 0.0f };
	int i;

	for(i = 0; i < 2; ++i)
	{
		_cairo_gl_context_set_active_texture(ctx, i);
		ctx->gl.TexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
		ctx->gl.TexGenfv(GL_S, GL_EYE_PLANE, tex_gen_s);

		ctx->gl.TexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
		ctx->gl.TexGenfv(GL_T, GL_EYE_PLANE, tex_gen_t);
	}

	_cairo_gl_context_set_active_texture(ctx, 0);

	ctx->init_vert_fixed = 1;
}

static inline void
_cairo_gl_context_set_vert_fixed(cairo_gpu_context_t* ctx, unsigned vert)
{
	unsigned diff_vert = ctx->vert ^ vert;
	int i;

	if(ctx->vertp_enabled)
	{
		ctx->gl.Disable(GL_VERTEX_PROGRAM_ARB);
		ctx->vertp_enabled = 0;
	}

	if(!ctx->init_vert_fixed)
		_cairo_gpu_context__init_vert_fixed(ctx);

	for(i = 0; i < MAX_OPERANDS; ++i)
	{
		if((diff_vert >> (VERT_TEX_SHIFT + i * VERT_TEX_BITS)) & VERT_TEX_MASK)
		{
			int k = (vert >> (VERT_TEX_SHIFT + i * VERT_TEX_BITS)) & VERT_TEX_MASK;
			if(k == VERT_TEX_GEN)
			{
				_cairo_gl_context_set_active_texture(ctx, i);
				ctx->gl.Enable(GL_TEXTURE_GEN_S);
				ctx->gl.Enable(GL_TEXTURE_GEN_T);
			}
			else
			{
				// need vertex programs for k != i
				assert(!k || ((k - 1) == i));
				_cairo_gl_context_set_active_texture(ctx, i);
				ctx->gl.Disable(GL_TEXTURE_GEN_S);
				ctx->gl.Disable(GL_TEXTURE_GEN_T);
			}
		}
	}

	ctx->vert = vert;
}

static inline char*
_cairo_gpu_context__write_vert(cairo_gpu_space_t* space, unsigned vert)
{
	cairo_gpu_program_builder_t pb;
	cairo_gpu_program_builder_t* p = &pb;
	cairo_gpu_string_builder_t* builder = &p->body;

	memset(&pb, 0, sizeof(pb));
	p->div_uses = p->dp2a_uses = space->has_gpu_program4 ? 0 : -1;

	p->in_position ="vertex.position";
	p->in_color ="vertex.color";
	p->in_texcoord[0] = p->in_texcoord[1] ="vertex.texcoord[0]";
	p->out_position = "result.position";
	p->out_color = "result.color";
	p->out_texcoord[0] = "result.texcoord[0]";
	p->out_texcoord[1] = "result.texcoord[1]";

	_cairo_gpu_write_vert(p, vert);

	builder = &p->main;
	if(p->dp2a_uses > 0 || p->div_uses > 0)
		OUT_("!!NVvp4.0\n");
	else
		OUT_("!!ARBvp1.0\n");

	// only way to use clipping
	OUT("OPTION ARB_position_invariant");
	return _cairo_gpu_program_builder_finish(p);
}

static inline void
_cairo_gl_context_set_vert_prog(cairo_gpu_context_t* ctx, unsigned vert)
{
	unsigned vertp = vert & VERT_PROG_MASK;

	if(!ctx->vertp_enabled)
	{
		ctx->gl.Enable(GL_VERTEX_PROGRAM_ARB);
		ctx->vertp_enabled = 1;
	}

	if(ctx->vertp != vertp)
	{
		_cairo_gpu_context__set_prog(ctx, GL_VERTEX_PROGRAM_ARB, TABLE_VERT, vertp, _cairo_gpu_context__write_vert);
		ctx->vertp = vertp;
	}

	ctx->vert = (ctx->vert & VERT_PROG_MASK) | (vert & ~VERT_PROG_MASK);
}

static inline void
_cairo_gl_context_set_vert(cairo_gpu_context_t* ctx, unsigned vert)
{
	if(!ctx->space->use_vert_prog)
		_cairo_gl_context_set_vert_fixed(ctx, vert);
	else
		_cairo_gl_context_set_vert_prog(ctx, vert);
}

typedef struct
{
	unsigned slot;
	unsigned rgb_slot_mask;
	unsigned alpha_slot_mask;
} cairo_gpu_combine_setup_t;

static inline void
_cairo_gpu_combine_operand_at(cairo_gpu_context_t* ctx, cairo_gpu_combine_setup_t* combs, unsigned slot, unsigned src_rgb, unsigned src_alpha, unsigned operand_rgb)
{
	int op = slot ? 1 : 0;
	int unit = slot ? (slot - 1) : 0;
	_cairo_gl_context_set_active_texture(ctx, unit);
	if(src_rgb)
	{
		ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_SRC0_RGB + op, src_rgb);
		ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_OPERAND0_RGB + op, operand_rgb);
		combs->rgb_slot_mask |= 1 << slot;
	}
	if(src_alpha)
	{
		ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_SRC0_ALPHA + op, src_alpha);
		combs->alpha_slot_mask |= 1 << slot;
	}
}

static inline int
_cairo_gpu_combine_slot(cairo_gpu_combine_setup_t* combs)
{
	while((1 << combs->slot) & (combs->rgb_slot_mask | combs->alpha_slot_mask))
		++combs->slot;

	return combs->slot;
}

static inline void
_cairo_gpu_combine_operand(cairo_gpu_context_t* ctx, cairo_gpu_combine_setup_t* combs, unsigned src_rgb, unsigned src_alpha, unsigned operand_rgb)
{
	_cairo_gpu_combine_operand_at(ctx, combs, _cairo_gpu_combine_slot(combs), src_rgb, src_alpha, operand_rgb);
}

static void
_cairo_gpu_context__init_frag_combine(cairo_gpu_context_t* ctx)
{
	int i;

	// avoid for now
	//if(0 && GLEW_NV_texture_shader)
	if(0)
	{
		ctx->nv_texture_shader = 1;
		ctx->gl.Enable(GL_TEXTURE_SHADER_NV);
	}

	for(i = 0; i < ctx->space->tex_units; ++i)
	{
		_cairo_gl_context_set_active_texture(ctx, i);
		ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);

		ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_SRC0_RGB, GL_PREVIOUS);
		ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_SRC0_ALPHA, GL_PREVIOUS);

		// this is clobbered in unit 0
		ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR);

		ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, GL_SRC_ALPHA);
		ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_OPERAND1_ALPHA, GL_SRC_ALPHA);

		ctx->textures[i].nv_texture_shader = GL_NONE;
	}

	_cairo_gl_context_set_active_texture(ctx, 0);
	ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_SRC2_RGB, GL_CONSTANT);
	ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_OPERAND2_RGB, GL_SRC_ALPHA);

	ctx->init_frag_modulate = 0;
	ctx->init_frag_combine = 1;
}

static inline void
_cairo_gpu_context_set_constant_color(cairo_gpu_context_t* ctx, cairo_gpu_color4_t* color)
{
	if(!ctx->fragp_enabled)
	{
		if(ctx->constant_unit == -2)
			ctx->gl.Color4fv(&color->c.r);
		else if(ctx->constant_unit >= 0)
		{
			_cairo_gl_context_set_active_texture(ctx, ctx->constant_unit);
			ctx->gl.TexEnvfv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, &color->c.r);
		}
	}
	else
	{
		_cairo_gpu_context_set_frag_param(ctx, FRAGENV_CONSTANT, &color->c.r);
	}
}

// XXX: we may fail due to lack of enough texture units, and this will result in silent wrong rendering!!!
// pre-NV30: 2
// NV30+: 4 (but we use fragment programs)
// R300+, Intel: 8

static inline void
_cairo_gl_context_set_frag_combine(cairo_gpu_context_t* ctx, unsigned frag)
{
	unsigned tex0 = (frag >> (FRAG_TEX_SHIFT)) & FRAG_TEX_MASK;
	unsigned tex1 = (frag >> (FRAG_TEX_SHIFT + FRAG_TEX_BITS)) & FRAG_TEX_MASK;
	unsigned tex_2d_mask = 0;
	unsigned tex_rect_mask = 0;
	unsigned cfrag = frag;
	unsigned tex2_src0_rgb_constant = 0;
	cairo_gpu_combine_setup_t combs;
	combs.slot = 0;
	combs.rgb_slot_mask = 0;
	combs.alpha_slot_mask = 0;

	if(ctx->fragp_enabled)
	{
		ctx->gl.Disable(GL_FRAGMENT_PROGRAM_ARB);
		ctx->fragp_enabled = 0;
	}

	if(cfrag & (3 << FRAG_COMPONENT_SHIFT))
	{
		unsigned c = (cfrag >> FRAG_COMPONENT_SHIFT) & 3;
		cfrag |= 3 << FRAG_COMPONENT_SHIFT;

		if(c != ctx->component)
		{
			float v[4];
			v[0] = v[1] = v[2] = v[3] = 0.5;
			v[c - 1] = 1.0;

			_cairo_gl_context_set_active_texture(ctx, 0);
			ctx->gl.TexEnvfv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, v);
			ctx->component = c;
		}
	}
	else
		ctx->component = 0;

	if(ctx->frag == cfrag)
		return;

	ctx->constant_unit = -1;

	if(!ctx->init_frag_combine)
		_cairo_gpu_context__init_frag_combine(ctx);
	else if(ctx->init_frag_modulate)
	{
		int i;
		for(i = 0; i < ctx->space->tex_units; ++i)
		{
			_cairo_gl_context_set_active_texture(ctx, i);
			ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
		}
		ctx->init_frag_modulate = 0;
	}

	if(tex0)
	{
		assert(tex0 & FRAG_TEX_COLOR_MASK);
		if((tex0 & FRAG_TEX_COLOR_MASK) == FRAG_TEX_COLOR_RGBA)
			_cairo_gpu_combine_operand_at(ctx, &combs, 0, GL_TEXTURE, GL_TEXTURE, GL_SRC_COLOR);
		else if((tex0 & FRAG_TEX_COLOR_MASK) == FRAG_TEX_COLOR_AAAA)
			_cairo_gpu_combine_operand_at(ctx, &combs, 0, GL_TEXTURE, GL_TEXTURE, GL_SRC_ALPHA);
		else if((tex0 & FRAG_TEX_COLOR_MASK) == FRAG_TEX_COLOR_111C)
		{
			_cairo_gpu_combine_operand_at(ctx, &combs, 1, GL_CONSTANT, 0, GL_SRC_COLOR);
			_cairo_gpu_combine_operand_at(ctx, &combs, 2, GL_CONSTANT, 0, GL_SRC_COLOR);

			if((frag & (FRAG_OP_MASK | FRAG_OPPOS_TEX1)) == (OP_MUL_ALPHA << FRAG_OP_SHIFT))
			{}
		}
		else
			assert(0);

		if(!((tex0 & FRAG_TEX_COLOR_MASK) == FRAG_TEX_COLOR_111C)
			&& ((frag & (FRAG_OP_MASK | FRAG_OPPOS_TEX1)) == (OP_MUL_ALPHA << FRAG_OP_SHIFT)))
			_cairo_gpu_combine_operand_at(ctx, &combs, 1, GL_TEXTURE, 0, GL_SRC_ALPHA);

		if(tex0 & FRAG_TEX_RECTANGLE)
			tex_rect_mask |= 1;
		else
			tex_2d_mask |= 1;
	}

	if(tex1)
	{
		int slot = _cairo_gpu_combine_slot(&combs);
		assert(tex0 & FRAG_TEX_COLOR_MASK);
		assert(tex1 & FRAG_TEX_COLOR_MASK);

		// avoid having tex0 * tex1 in the first stage and nothing in the second stage
		if(!ctx->space->crossbar || !(frag & (FRAG_OPPOS_TEX1 | FRAG_PRIMARY | FRAG_CONSTANT)))
		{
			if(slot < 2)
			{
				slot = 2;

				if(frag & FRAG_OPPOS_TEX1)
				{
					combs.rgb_slot_mask |= slot - 1;
					combs.alpha_slot_mask |= slot - 1;
				}
			}
		}

		if((tex1 & FRAG_TEX_COLOR_MASK) == FRAG_TEX_COLOR_RGBA)
			_cairo_gpu_combine_operand_at(ctx, &combs, slot, slot == 2 ? GL_TEXTURE : GL_TEXTURE1_ARB, slot == 2 ? GL_TEXTURE : GL_TEXTURE1_ARB, GL_SRC_COLOR);
		else if((tex1 & FRAG_TEX_COLOR_MASK) == FRAG_TEX_COLOR_AAAA)
			_cairo_gpu_combine_operand_at(ctx, &combs, slot, slot == 2 ? GL_TEXTURE : GL_TEXTURE1_ARB, slot == 2 ? GL_TEXTURE : GL_TEXTURE1_ARB, GL_SRC_ALPHA);
		else
			assert(0);

		if(tex1 & FRAG_TEX_RECTANGLE)
			tex_rect_mask |= 2;
		else
			tex_2d_mask |= 2;
	}

	if((frag & (FRAG_OP_MASK | FRAG_OPPOS_TEX1)) == ((OP_MUL_ALPHA << FRAG_OP_SHIFT) | FRAG_OPPOS_TEX1))
	{
		assert(tex1 & FRAG_TEX_COLOR_MASK);
		_cairo_gpu_combine_operand(ctx, &combs, GL_PREVIOUS, 0, GL_SRC_ALPHA);
	}

	if((frag & FRAG_CONSTANT) || !frag)
	{
		_cairo_gpu_combine_operand(ctx, &combs, GL_CONSTANT, GL_CONSTANT, GL_SRC_COLOR);
		ctx->constant_unit = combs.slot ? (combs.slot - 1) : 0;
		if(!frag)
			_cairo_gpu_context_set_constant_color(ctx, &_cairo_gpu_white);
	}

	if(frag & FRAG_PRIMARY)
		_cairo_gpu_combine_operand(ctx, &combs, GL_PRIMARY_COLOR, GL_PRIMARY_COLOR, GL_SRC_COLOR);

	{
		int i;
		for(i = 0; i < ctx->space->tex_units; ++i)
		{
			unsigned combine_rgb = !(combs.rgb_slot_mask & (1 << (i + 1))) ? GL_REPLACE : GL_MODULATE;
			unsigned combine_alpha = !(combs.alpha_slot_mask & (1 << (i + 1))) ? GL_REPLACE : GL_MODULATE;

			if((tex0 & FRAG_TEX_COLOR_MASK) == FRAG_TEX_COLOR_111C)
			{
				if(i == 0)
					combine_rgb = GL_INTERPOLATE;
				else if(i == 1)
					combine_rgb = GL_DOT3_RGBA;
				else if(i == 2)
				{
					if((frag & (FRAG_OP_MASK | FRAG_OPPOS_TEX1)) == (OP_MUL_ALPHA << FRAG_OP_SHIFT))
					{}
					else
					{
						tex2_src0_rgb_constant = 1;

						if(combine_rgb == GL_MODULATE && i == ctx->constant_unit)
							combine_rgb = GL_REPLACE;
						else
						{
							float v[4] = {1, 1, 1, 1};
							_cairo_gl_context_set_active_texture(ctx, i);
							ctx->gl.TexEnvfv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, v);
						}
					}
				}
			}

			if(i && combine_rgb == GL_REPLACE && combine_alpha == GL_REPLACE)
			{
				_cairo_gl_context_set_texture_target(ctx, i, 0);
				_cairo_gpu_context__set_nv_texture_shader(ctx, i, GL_NONE);
			}
			else
			{
				if((1 << i) & tex_2d_mask)
				{
					_cairo_gl_context_set_texture_target(ctx, i, GL_TEXTURE_2D);
					_cairo_gpu_context__set_nv_texture_shader(ctx, i, GL_TEXTURE_2D);
				}
				else if((1 << i) & tex_rect_mask)
				{
					_cairo_gl_context_set_texture_target(ctx, i, GL_TEXTURE_RECTANGLE_ARB);
					_cairo_gpu_context__set_nv_texture_shader(ctx, i, GL_TEXTURE_RECTANGLE_ARB);
				}
				else
				{
					_cairo_gl_context_set_texture(ctx, i, &ctx->space->dummy_texture);
					_cairo_gl_context_set_texture_target(ctx, i, GL_TEXTURE_2D);
					_cairo_gpu_context__set_nv_texture_shader(ctx, i, GL_PASS_THROUGH_NV);
				}

				_cairo_gl_context_set_active_texture(ctx, i);
				ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB, combine_rgb);
				ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_COMBINE_ALPHA, combine_alpha);
			}
		}

		if(tex2_src0_rgb_constant != ctx->tex2_src0_rgb_constant)
		{
			_cairo_gl_context_set_active_texture(ctx, 2);
			ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_SRC0_RGB, tex2_src0_rgb_constant ? GL_CONSTANT : GL_PREVIOUS);
			ctx->tex2_src0_rgb_constant = tex2_src0_rgb_constant;
		}
	}
	ctx->frag = cfrag;
}

static inline void
_cairo_gl_context_set_frag_primary(cairo_gpu_context_t* ctx)
{
	int i;

	if(ctx->fragp_enabled)
	{
		ctx->gl.Disable(GL_FRAGMENT_PROGRAM_ARB);
		ctx->fragp_enabled = 0;
	}

	if(ctx->frag == FRAG_PRIMARY)
		return;

	for(i = 0; i < ctx->space->tex_units; ++i)
	{
		_cairo_gl_context_set_texture_target(ctx, i, 0);
		_cairo_gpu_context__set_nv_texture_shader(ctx, i, GL_NONE);
	}

	ctx->frag = FRAG_PRIMARY;
}


// TODO: when using this, we need to copy non-component-alpha but RGBA masks to an ALPHA texture
// TODO: when using this, the span renderer AND onepass trapezoid geometry must embed the constant value in the primary color(s)

// Vanilla path without ARB_texture_env_combine. Proof of concept. Currently unfinished.
static inline void
_cairo_gl_context_set_frag_modulate(cairo_gpu_context_t* ctx, unsigned frag)
{
	unsigned textures = 0;
	unsigned tex_2d_mask = 0;
	unsigned tex_rect_mask = 0;

	ctx->constant_unit = -1;

	if(!ctx->init_frag_modulate)
	{
		int i;
		for(i = 1; i < ctx->space->tex_units; ++i)
		{
			_cairo_gl_context_set_active_texture(ctx, i);
			ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		}
		ctx->init_frag_modulate = 1;
	}

	assert(!(frag & FRAG_OP_MASK));

	if(frag & FRAG_CONSTANT)
	{
		assert(!(frag & FRAG_PRIMARY)); // must be baked in if so
		ctx->constant_unit = -2; // put in primary color
	}

	if(frag & (FRAG_TEX_MASK << FRAG_TEX_SHIFT))
	{
		if(frag & (FRAG_TEX_RECTANGLE << FRAG_TEX_SHIFT))
			tex_rect_mask |= 1;
		else
			tex_2d_mask |= 1;

		_cairo_gl_context_set_active_texture(ctx, 0);
		ctx->gl.TexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, (frag & (FRAG_PRIMARY | FRAG_CONSTANT)) ? GL_MODULATE : GL_REPLACE);
		++textures;
	}

	if(frag & (FRAG_TEX_MASK << (FRAG_TEX_SHIFT + FRAG_TEX_BITS)))
	{
		if(frag & (FRAG_TEX_RECTANGLE << (FRAG_TEX_SHIFT + FRAG_TEX_BITS)))
			tex_rect_mask |= 2;
		else
			tex_2d_mask |= 2;
		++textures;
	}

	{
		int i;
		for(i = 0; i < ctx->space->tex_units; ++i)
		{
			if((1 << i) & tex_2d_mask)
			{
				_cairo_gl_context_set_texture_target(ctx, i, GL_TEXTURE_2D);
				_cairo_gpu_context__set_nv_texture_shader(ctx, i, GL_TEXTURE_2D);
			}
			else if((1 << i) & tex_rect_mask)
			{
				_cairo_gl_context_set_texture_target(ctx, i, GL_TEXTURE_RECTANGLE_ARB);
				_cairo_gpu_context__set_nv_texture_shader(ctx, i, GL_TEXTURE_RECTANGLE_ARB);
			}
			else
			{
				_cairo_gl_context_set_texture_target(ctx, i, 0);
				_cairo_gpu_context__set_nv_texture_shader(ctx, i, GL_NONE);
			}
		}
	}

	ctx->frag = frag;
}

static inline char*
_cairo_gpu_context__write_frag(cairo_gpu_space_t* space, unsigned frag)
{
	cairo_gpu_program_builder_t pb;
	cairo_gpu_program_builder_t* p = &pb;
	cairo_gpu_string_builder_t* builder = &p->body;

	memset(&pb, 0, sizeof(pb));
	p->div_uses = p->dp2a_uses = space->has_fragment_program2 ? 0 : -1;

	p->in_position ="fragment.position";
	p->in_color ="fragment.color";
	p->in_texcoord[0] = "fragment.texcoord[0]";
	p->in_texcoord[1] = "fragment.texcoord[1]";
	p->out_color = "result.color";

	_cairo_gpu_write_frag(p, frag);

	builder = &p->main;
	OUT_("!!ARBfp1.0\n");
	if(p->div_uses > 0 || p->dp2a_uses > 0)
		OUT("OPTION NV_fragment_program2");
	OUT("OPTION ARB_precision_hint_nicest");
	return _cairo_gpu_program_builder_finish(p);
}

static inline void
_cairo_gl_context_set_frag_prog(cairo_gpu_context_t* ctx, unsigned frag)
{
	if(!ctx->fragp_enabled)
	{
		ctx->gl.Enable(GL_FRAGMENT_PROGRAM_ARB);
		ctx->fragp_enabled = 1;
	}

	if(ctx->fragp != frag)
	{
		_cairo_gpu_context__set_prog(ctx, GL_FRAGMENT_PROGRAM_ARB, TABLE_FRAG, frag, _cairo_gpu_context__write_frag);
		ctx->fragp = frag;
	}
}

static void
_cairo_gl_context_set_frag(cairo_gpu_context_t* ctx, unsigned frag)
{
	if(!ctx->space->use_frag_prog)
	{
		if(frag == FRAG_PRIMARY)
			_cairo_gl_context_set_frag_primary(ctx);
		else if(ctx->space->has_combine)
			_cairo_gl_context_set_frag_combine(ctx, frag);
		else
			_cairo_gl_context_set_frag_modulate(ctx, frag);
	}
	else
		_cairo_gl_context_set_frag_prog(ctx, frag);
}

static void
_cairo_gpu_context_set_vert_frag(cairo_gpu_context_t* ctx, unsigned vert, unsigned frag)
{
	if(frag & FRAG_CONSTANT && !ctx->space->use_frag_prog && !ctx->space->has_combine)
		vert |= VERT_COLOR_POSTOP;

	_cairo_gl_context_set_vert(ctx, vert);
	_cairo_gl_context_set_frag(ctx, frag);
}

static inline void
_cairo_gpu_context_set_translation(cairo_gpu_context_t* ctx, int dx, int dy)
{
	dx -= ctx->x_offset;
	dy -= ctx->y_offset;
	if(dx || dy)
	{
		ctx->gl.MatrixMode(GL_MODELVIEW);
		ctx->gl.Translatef(dx, dy, 0);
		ctx->x_offset += dx;
		ctx->y_offset += dy;
	}
}

static void
_cairo_gpu_context_set_viewport(cairo_gpu_context_t* ctx, int x, int y, int width, int height)
{
	int proj_sign = ctx->draw_flip_height < 0;
	if(proj_sign != ctx->proj_sign || ctx->viewport_width != width || ctx->viewport_height != height || ctx->viewport_x != x || ctx->viewport_y != y)
	{
		ctx->gl.Viewport(x, (ctx->draw_flip_height >= 0) ? (ctx->draw_flip_height - y - height) : y, width, height);

		ctx->gl.MatrixMode(GL_PROJECTION);
		ctx->gl.LoadIdentity();
		if(proj_sign)
			ctx->gl.Ortho(0, width, 0, height, -1.0, 1.0);
		else
			ctx->gl.Ortho(0, width, height, 0, -1.0, 1.0);

		// we apparently need an epsilon to make a1-image-sample and a1-traps-sample pass
		// TODO: find out why
	//              ctx->gl.Translated(1.0 / (1 << 13) - 1.0 / (1 << 21) + 1.0 / (1 << 26), 1.0 / (1 << 13) - 1.0 / (1 << 21) + 1.0 / (1 << 26), 0.0);
		ctx->gl.Translated(1.0 / (1 << 13) - x, 1.0 / (1 << 13) - y, 0.0);

		ctx->proj_sign = proj_sign;
		ctx->viewport_x = x;
		ctx->viewport_y = y;
		ctx->viewport_width = width;
		ctx->viewport_height = height;
	}
}

static inline GLenum
_cairo_gl_blend_factor(int v)
{
	if(v < BLEND_SRC_COLOR)
		return (GLenum)v;
	else if(v < BLEND_CONSTANT_COLOR)
		return GL_SRC_COLOR - BLEND_SRC_COLOR + v;
	else
		return GL_CONSTANT_COLOR - BLEND_CONSTANT_COLOR + v;
}

static inline void
_cairo_gpu_context__set_color_mask(cairo_gpu_context_t* ctx, unsigned color_mask)
{
	if(color_mask != ctx->color_mask)
	{
		ctx->gl.ColorMask((color_mask) & 1, (color_mask >> 1) & 1, (color_mask >> 2) & 1, (color_mask >> 3) & 1);
		ctx->color_mask = color_mask;
	}
}

static inline void
_cairo_gpu_context_set_blend(cairo_gpu_context_t* ctx, unsigned blendv)
{
	cairo_gpu_blend_t blend;
	blend.v = blendv;

	_cairo_gpu_context__set_color_mask(ctx, blend.color_mask);

	// XXX: fix endianness??
	if(blend.func == BLEND_FUNC_SOURCE && !blend.eq)
	{
		if(ctx->blend)
		{
			ctx->gl.Disable(GL_BLEND);
			ctx->blend = 0;
		}
	}
	else
	{
		if(!ctx->blend)
		{
			ctx->gl.Enable(GL_BLEND);
			ctx->blend = 1;
		}

		if(blend.func != ctx->blend_func)
		{
			if(blend.src_rgb != blend.src_alpha || blend.dst_rgb != blend.dst_alpha)
				ctx->gl.BlendFuncSeparateEXT(_cairo_gl_blend_factor(blend.src_rgb), _cairo_gl_blend_factor(blend.dst_rgb),
						_cairo_gl_blend_factor(blend.src_alpha), _cairo_gl_blend_factor(blend.dst_alpha));
			else
				ctx->gl.BlendFunc(_cairo_gl_blend_factor(blend.src_rgb), _cairo_gl_blend_factor(blend.dst_rgb));
			ctx->blend_func = blend.func;
		}
	}

	if(blend.eq != ctx->blend_eq)
	{
		ctx->gl.BlendEquation(blend.eq ? GL_FUNC_REVERSE_SUBTRACT : GL_FUNC_ADD);
		ctx->blend_eq = blend.eq;
	}
}

static inline void _cairo_gpu_context_set_blend_color(cairo_gpu_context_t* ctx, cairo_gpu_color4_t* blend_color)
{
	ctx->gl.BlendColorEXT(blend_color->c.r, blend_color->c.g, blend_color->c.b, blend_color->ka);
}

static inline void _cairo_gpu_context_set_raster(cairo_gpu_context_t* ctx, unsigned smooth)
{
	int enable = !!smooth;
	if(ctx->smooth != enable)
	{
		if(enable)
			ctx->gl.Enable(GL_POLYGON_SMOOTH);
		else
			ctx->gl.Disable(GL_POLYGON_SMOOTH);
		ctx->smooth = enable;
	}

	if(smooth && smooth != ctx->smooth_hint)
	{
		ctx->gl.Hint(GL_POLYGON_SMOOTH_HINT, (smooth > 1) ? GL_NICEST : GL_FASTEST);
		ctx->smooth_hint = smooth;
	}
}

static inline void
_cairo_gpu_context__gl_clear(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface)
{
	unsigned color_mask = 0xf;

	if(surface)
	{
		if(surface->base.content == CAIRO_CONTENT_COLOR)
			color_mask = 7;
		else if(surface->base.content == CAIRO_CONTENT_ALPHA)
			color_mask = 8;
	}

	_cairo_gpu_context__set_color_mask(ctx, color_mask);
	ctx->gl.Clear(GL_COLOR_BUFFER_BIT);
}

// TODO: some (most?) GL implementations may turn this into a quad render.
// If so, we may want to avoid this and do the quad render ourselves.
static inline void
_cairo_gpu_context_fill_rect(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface, int x, int y, int width, int height, float r, float g, float b, float a)
{
	_cairo_gpu_flip_draw_y(ctx, &y, height);

	//printf("fill rect %i %i %i %i\n", x, y, width, height);

	ctx->gl.ClearColor(r, g, b, a);
	ctx->gl.Scissor(x, y, width, height);
	ctx->gl.Enable(GL_SCISSOR_TEST);
	_cairo_gpu_context__gl_clear(ctx, surface);
	ctx->gl.Disable(GL_SCISSOR_TEST);
}

static inline void
_cairo_gpu_context_fill(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface, float r, float g, float b, float a)
{
	ctx->gl.ClearColor(r, g, b, a);
	_cairo_gpu_context__gl_clear(ctx, surface);
}

static inline void
_cairo_gpu_context_set_geometry(cairo_gpu_context_t * ctx, cairo_gpu_geometry_t * geometry)
{
	_cairo_gpu_geometry_bind(ctx, geometry);
}

// must be bound
static inline void
_cairo_gpu_context_draw(cairo_gpu_context_t * ctx)
{
	ctx->gl.DrawArraysEXT(ctx->mode, 0, ctx->count);
}

static inline void
_cairo_gpu_context_draw_rect(cairo_gpu_context_t * ctx, int x, int y, int width, int height)
{
#if DEBUG_DISABLE_GL_RECTI
	{
		cairo_gpu_geometry_t* geometry = &ctx->tls->geometries[GEOM_TEMP];
		float* v = _cairo_gpu_geometry_begin(ctx, geometry, PRIM_QUADS, 4, 2, 0, 0);
		_cairo_gpu_emit_rect(&v, x, y, width, height);
		_cairo_gpu_geometry__do_bind(ctx, geometry);
		_cairo_gpu_geometry_end(ctx, geometry, 4);

		_cairo_gpu_context_set_geometry(ctx, geometry);
		_cairo_gpu_context_draw(ctx);
	}
#else
	ctx->gl.Recti(x, y, x + width, y + height);
#endif
}

static void
_cairo_gpu_context_blit_pixels(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, cairo_image_surface_t* image, int dst_x, int dst_y, int src_x, int src_y, int width, int height)
{
	_cairo_gpu_context_set_viewport(ctx, 0, 0, dst->width, dst->height);
	_cairo_gpu_context_set_blend(ctx, BLEND_SOURCE);
	_cairo_gpu_context_set_raster(ctx, 0);
	// vertex contextline is bypassed by upload_pixels

	/* nVidia crashes with SIGFPE when fragment programs are enabled with a pbuffer target.
	 * Workaround by not using fragment programs here.
	 */

	_cairo_gl_context_set_frag_primary(ctx);

	if(!ctx->space->has_window_pos)
	{
		_cairo_gl_context_set_vert(ctx, 0);
		_cairo_gpu_context_set_translation(ctx, 0, 0);
	}

	_cairo_gpu_context__upload_pixels(ctx, dst, -1, image, src_x, src_y, width, height, dst_x, dst_y);
}



static inline void
_cairo_gpu_context_blit_zoom(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, int dx, int dy, int sx, int sy, int sw, int sh, int zx, int zy)
{
	if(!sw || !sh)
		return;

	if(ctx->space->fb_blit)
	{
		int dw = sw * zx;
		int dh = sh * zy;

		dh = _cairo_gpu_flip_draw_y(ctx, &dy, dh);
		sh = _cairo_gpu_flip_read_y(ctx, &sy, sh);

		ctx->gl.BlitFramebufferEXT(sx, sy, sx + sw, sy + sh, dx, dy, dx + dw, dy + dh, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	}
	else
	{
		_cairo_gpu_context_set_blend(ctx, BLEND_SOURCE);
		_cairo_gpu_context_set_raster(ctx, 0);

		_cairo_gl_context_set_frag_primary(ctx);

		// vertex contextline is bypassed by upload_pixels
		if(ctx->space->has_window_pos)
		{
			if(_cairo_gpu_flip_read_y(ctx, &sy, sh) < 0)
				sy += sh;
		}
		else
		{
			_cairo_gpu_context_set_viewport(ctx, 0, 0, dst->width, dst->height);
			_cairo_gl_context_set_vert(ctx, 0);
			_cairo_gpu_context_set_translation(ctx, 0, 0);
		}

		if(zy < 0)
			dy -= sh * zy;

		// TODO: this may well be unaccelerated, and we should have a texture-mapping fallback.
		// Hopefully drivers will fall back to texture mapping themselves like Mesa/Gallium does
		ctx->gl.PixelZoom(zx, zy);
		if(ctx->space->has_window_pos)
			ctx->gl.WindowPos2i(dx, dy);
		else
			ctx->gl.RasterPos2i(dx, dy);

		// XXX: this SIGFPEs on nVidia for unknown reasons
		ctx->gl.CopyPixels(sx, sy, sw, sh, GL_COLOR);
	}
}

static inline void
_cairo_gpu_context_blit_same(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, int x, int y, int w, int h)
{
	_cairo_gpu_context_blit_zoom(ctx, dst, x, y, x, y, w, h, 1, 1);
}

// only use for padding as it doesn't update dirty parts
static inline void
_cairo_gpu_context_read_to_texture(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* dst, int dx, int dy, int sx, int sy, int w, int h)
{
	// this should be OK with GL_INTENSITY
	_cairo_gl_context_set_active_texture(ctx, _cairo_gl_context_set_texture(ctx, -1, &dst->texture));
	ctx->gl.CopyTexSubImage2D(_cairo_gl_target(dst->texture.target_idx), 0, dx, dy, sx, sy, w, h);
}

#if 0
static inline void
_cairo_gl_context_flush_surface(cairo_gpu_context_t* ctx, cairo_gpu_surface_t* surface)
{
	int mask = ((ctx->draw_fb == surface->fb) ? FB_DRAW : 0) | ((ctx->read_fb == surface->fb) ? FB_READ : 0);
	if(mask)
		_cairo_gl_context_set_framebuffer(ctx, mask, 0, 0, 0, surface->buffer_non_upside_down ? (int)surface->height : -1);
}
#endif
