/** Creates a cairo-gl pattern surface for the given trapezoids */
static cairo_status_t
_cairo_gpu_get_traps_pattern_pixman(cairo_gpu_surface_t * dst, int dst_x, int dst_y, int width, int height, cairo_trapezoid_t * traps, int num_traps, cairo_antialias_t antialias, cairo_pattern_t ** pattern)
{
	pixman_trapezoid_t stack_traps[CAIRO_STACK_ARRAY_LENGTH(pixman_trapezoid_t)];
	pixman_trapezoid_t *pixman_traps;
	cairo_image_surface_t *image_surface;
	int i;

	/* Convert traps to pixman traps */
	pixman_traps = stack_traps;
	if(num_traps > ARRAY_LENGTH(stack_traps))
	{
		pixman_traps = _cairo_malloc_ab(num_traps, sizeof(pixman_trapezoid_t));
		if(unlikely(pixman_traps == NULL))
			return _cairo_error(CAIRO_STATUS_NO_MEMORY);
	}

	for(i = 0; i < num_traps; i++)
	{
		pixman_traps[i].top = _cairo_fixed_to_16_16(traps[i].top);
		pixman_traps[i].bottom = _cairo_fixed_to_16_16(traps[i].bottom);
		pixman_traps[i].left.p1.x = _cairo_fixed_to_16_16(traps[i].left.p1.x);
		pixman_traps[i].left.p1.y = _cairo_fixed_to_16_16(traps[i].left.p1.y);
		pixman_traps[i].left.p2.x = _cairo_fixed_to_16_16(traps[i].left.p2.x);
		pixman_traps[i].left.p2.y = _cairo_fixed_to_16_16(traps[i].left.p2.y);
		pixman_traps[i].right.p1.x = _cairo_fixed_to_16_16(traps[i].right.p1.x);
		pixman_traps[i].right.p1.y = _cairo_fixed_to_16_16(traps[i].right.p1.y);
		pixman_traps[i].right.p2.x = _cairo_fixed_to_16_16(traps[i].right.p2.x);
		pixman_traps[i].right.p2.y = _cairo_fixed_to_16_16(traps[i].right.p2.y);
	}

	if(antialias != CAIRO_ANTIALIAS_NONE)
	{
		image_surface = (cairo_image_surface_t *) cairo_image_surface_create(CAIRO_FORMAT_A8, width, height);
	}
	else
	{
		image_surface = (cairo_image_surface_t *) cairo_image_surface_create(CAIRO_FORMAT_A1, width, height);
	}

	if(image_surface->base.status)
	{
		if(pixman_traps != stack_traps)
			free(pixman_traps);
		return image_surface->base.status;
	}

	pixman_add_trapezoids(image_surface->pixman_image, -dst_x, -dst_y, num_traps, pixman_traps);

	if(pixman_traps != stack_traps)
		free(pixman_traps);

	*pattern = cairo_pattern_create_for_surface(&image_surface->base);
	cairo_surface_destroy(&image_surface->base);

	return CAIRO_STATUS_SUCCESS;
}

static void
_cairo_gpu_traps_geometry(cairo_gpu_context_t* ctx, cairo_gpu_geometry_t * geometry, cairo_trapezoid_t * traps, int num_traps, char **base)
{
	int i;
	float *vertices;
	float* v;

	/*
	float colorf;

	if(color)
	{
		unsigned char c[4];
		c[0] = _cairo_color_float_to_byte(color->c.r);
		c[1] = _cairo_color_float_to_byte(color->c.g);
		c[2] = _cairo_color_float_to_byte(color->c.b);
		c[3] = _cairo_color_float_to_byte(color->ka);
		colorf = *(float*)c;
	}
	*/

	v = vertices = _cairo_gpu_geometry_begin(ctx, geometry, PRIM_QUADS, num_traps * 4, 2, 0 /*color ? 4 : 0*/, 0);
	for(i = 0; i < num_traps; ++i)
	{
		cairo_trapezoid_t *t = traps + i;

		float top = _cairo_fixed_to_float(t->top);

		float bottom = _cairo_fixed_to_float(t->bottom);

		/* TODO: we can do this with a vertex shader, but that would require 20 floats vs 8 floats per trap.
		 * Alternatively a geometry shader may do, but the hardware may not support them. */
#define DO(lr, tb, p) \
    if (t->lr.p.y != t->tb) \
	 v[0] = _cairo_fixed_to_float(t->lr.p.x) + ((float)(_cairo_fixed_to_double (t->lr.p1.x - t->lr.p2.x) * _cairo_fixed_to_double(t->tb - t->lr.p.y) / _cairo_fixed_to_double(t->lr.p1.y - t->lr.p2.y))); \
    else \
	 v[0] = _cairo_fixed_to_float(t->lr.p.x); \
    v[1] = tb; \
    v += 2
//    if(color) {v[2] = colorf; v += 3;} else v += 2

		DO(left, top, p1);
		DO(left, bottom, p2);
		DO(right, bottom, p2);
		DO(right, top, p1);
	}
	_cairo_gpu_geometry_end(ctx, geometry, num_traps * 4);
}

/* It is probably faster to subtract the traps rather than clear the whole surface.
    We rely on the GPU behaving in a deterministic fashion.
    This function must only be called for antialiased traps
 */
static cairo_status_t
_cairo_gpu_getput_traps_pattern_gpu(cairo_gpu_surface_t * dst, int dst_x, int dst_y, int width, int height, cairo_trapezoid_t * traps, int num_traps, cairo_antialias_t antialias, cairo_pattern_t ** pattern, cairo_gpu_geometry_t** geometry, int smooth)
{
	cairo_gpu_surface_t *mask;
	cairo_gpu_context_t *ctx;
	char *base;
	int sub_traps = smooth && dst->space->blend_subtract;

	if(*pattern)
		mask = (cairo_gpu_surface_t *) (((cairo_surface_pattern_t *) * pattern)->surface);
	else
	{
		mask = _cairo_gpu_space_get_cached_mask_surface(dst->space, width, height, !smooth);
		if(!smooth)
		{
			if(((antialias & CAIRO_ANTIALIAS_SAMPLES_MASK) >> CAIRO_ANTIALIAS_SAMPLES_SHIFT) > dst->space->msaa_samples)
			{
				_cairo_gpu_space_put_cached_mask_surface(dst->space, mask);
				return CAIRO_INT_STATUS_UNSUPPORTED;
			}
		}

		if(!mask)
			return CAIRO_INT_STATUS_UNSUPPORTED;
	}

	/* if we are not using MSAA, we clear the traps by subtracting, otherwise we clear the surface.
	 * TODO: benchmark and find out whether this is a good strategy
	 */
	if(!*pattern || sub_traps)
	{
		_cairo_gpu_surface_enable_multisampling(mask, !smooth);

		ctx = _cairo_gpu_surface_bind(mask, FB_DRAW);

		if(!sub_traps)
			*geometry = _cairo_gpu_context_geometry(ctx, GEOM_TRAPS);
		else if(!*pattern)
			*geometry = _cairo_gpu_context_geometry_long(ctx, GEOM_TRAPS);

		if(!*pattern)
			_cairo_gpu_traps_geometry(ctx, *geometry, traps, num_traps, &base);

		_cairo_gpu_context_set_viewport(ctx, 0, 0, mask->width, mask->height);
		_cairo_gpu_context_set_blend(ctx, *pattern ? BLEND_SUB : BLEND_ADD);
		_cairo_gpu_context_set_raster(ctx, smooth);
		_cairo_gpu_context_set_vert_frag(ctx, 0, FRAG_CONSTANT);
		_cairo_gpu_context_set_constant_color(ctx, &_cairo_gpu_white);
		_cairo_gpu_context_set_translation(ctx, -dst_x, -dst_y);
		_cairo_gpu_context_set_geometry(ctx, *geometry);

		_cairo_gpu_context_draw(ctx);

		if(!sub_traps)
			_cairo_gpu_geometry_put(ctx, *geometry);
		else if(*pattern)
			_cairo_gpu_geometry_put_long(ctx, *geometry);

		if(*pattern)
			_cairo_gpu_surface_cleared(mask);
		else
			_cairo_gpu_surface_modified(mask, 0, 0, width, height);

		_cairo_gpu_surface_enable_multisampling(mask, 0);
	}
	else
		mask->valid_mask = 0;

	if(*pattern)
	{
		cairo_pattern_destroy(*pattern);
		_cairo_gpu_space_put_cached_mask_surface(dst->space, mask);
	}
	else
		*pattern = cairo_pattern_create_for_surface((cairo_surface_t *) mask);

	return CAIRO_STATUS_SUCCESS;
}

static cairo_int_status_t
_cairo_gpu_surface_composite(cairo_operator_t op, const cairo_pattern_t * src, const cairo_pattern_t * mask, void *abstract_dst, int src_x, int src_y, int mask_x, int mask_y, int dst_x, int dst_y, unsigned int width, unsigned int height)
{
	cairo_gpu_surface_t *dst = abstract_dst;
	cairo_gpu_context_t* ctx;
	cairo_status_t status;
	cairo_gpu_composite_setup_t setup;

	if(op == CAIRO_OPERATOR_DEST)
		return CAIRO_STATUS_SUCCESS;

	// TODO: maybe use DrawPixels if drawing an untransformed image surface with source-bounded operator
	status = _cairo_gpu_composite_init(&setup, op, dst, dst_x, dst_y, 0, 0, width, height);
	if(unlikely(status))
		return status;

	status = _cairo_gpu_composite_operand(&setup, src, src_x, src_y, FALSE, -1);
	if(unlikely(status))
		return status;

	if(mask != NULL && !_cairo_pattern_is_opaque(mask))
	{
		// TODO: we cannot do component alpha (i.e. add CHAN_A here) automatically as it would break Cairo: how do we extend the API?
		status = _cairo_gpu_composite_operand(&setup, mask, mask_x, mask_y, TRUE, -1);
		if(unlikely(status))
			return status;
	}

	_cairo_gpu_enter();

	ctx = _cairo_gpu_composite_plan_prepare_bind(&setup);
	if(!ctx)
	{
		status = CAIRO_INT_STATUS_UNSUPPORTED;
		goto out;
	}

	_cairo_gpu_composite_draw_once_modify(ctx, &setup, 0);

	_cairo_gpu_composite_fini(&setup);

	// TODO: do we need to fixup unbounded? It seems we don't.

	/*
	if(!_cairo_operator_bounded_by_mask(op))
	{
		status = _cairo_surface_composite_fixup_unbounded (&dst->base,
								   &src_attr.base,
								   src_width, src_height,
								   mask ? &mask_attr.base : NULL,
								   mask_width, mask_height,
								   src_x, src_y,
								   mask_x, mask_y,
								   dst_x, dst_y, width, height);
	}
	*/

	status = CAIRO_STATUS_SUCCESS;
out:
	_cairo_gpu_exit();
	return status;
}

static cairo_int_status_t
_cairo_gpu_surface_composite_trapezoids_onepass(cairo_operator_t op, const cairo_pattern_t * pattern, void *abstract_dst, cairo_antialias_t antialias, int src_x, int src_y, int dst_x, int dst_y, unsigned int width, unsigned int height, cairo_trapezoid_t * traps, int num_traps, unsigned smooth, unsigned dst_alpha_mask)
{
	cairo_gpu_surface_t *dst = abstract_dst;
	cairo_gpu_context_t* ctx;
	cairo_gpu_geometry_t* geometry;
	cairo_status_t status;
	char *base;
	cairo_gpu_composite_setup_t setup;
	cairo_gpu_color4_t color;

	status = _cairo_gpu_composite_init_smooth(&setup, op, dst, dst_x, dst_y, dst_x, dst_y, width, height, smooth, dst_alpha_mask);
	if(unlikely(status))
		return status;

	status = _cairo_gpu_composite_operand(&setup, pattern, src_x, src_y, FALSE, -1);
	if(unlikely(status))
		return status;

	ctx = _cairo_gpu_composite_plan_prepare_bind(&setup);
	if(!ctx)
		return CAIRO_INT_STATUS_UNSUPPORTED;

	geometry = _cairo_gpu_context_geometry(ctx, GEOM_TRAPS);

	_cairo_gpu_composite_get_constant_color(&setup, &color);
	_cairo_gpu_traps_geometry(ctx, geometry, traps, num_traps, &base);

	_cairo_gpu_composite_draw_once_modify(ctx, &setup, geometry);

	_cairo_gpu_geometry_put(ctx, geometry);

	_cairo_gpu_composite_fini(&setup);

	return CAIRO_STATUS_SUCCESS;
}

static cairo_int_status_t
_cairo_gpu_surface_composite_trapezoids(cairo_operator_t op, const cairo_pattern_t * pattern, void *abstract_dst, cairo_antialias_t antialias, int src_x, int src_y, int dst_x, int dst_y, unsigned int width, unsigned int height, cairo_trapezoid_t * traps, int num_traps)
{
/* Composing multiple trapezoids with operators different than ADD is NOT equivalent to compositing them in sequence, which is what GL hardware does.
    So we render them with ADD on a temporary surface and composite them in a second pass.
    Glitz does exactly the same thing.(but does not support using GL_POLYGON_SMOOTH or MSAA and instead always renders traps in software with pixman)
*/
	cairo_int_status_t status;
	cairo_gpu_surface_t *dst = abstract_dst;
	unsigned aa_samples = ((antialias & CAIRO_ANTIALIAS_SAMPLES_MASK) >> CAIRO_ANTIALIAS_SAMPLES_SHIFT);

	int onepass = 0;

	if(num_traps == 0 || op == CAIRO_OPERATOR_DEST)
		return CAIRO_STATUS_SUCCESS;

	_cairo_gpu_enter();

	// TODO: we assume here that traps are not overlapping. Is this true?

	// TODO: maybe do onepass, then invert the traps region in software and clear that
	if(_cairo_operator_bounded_by_mask(op))
	{
		// For onepass with polygon smoothing, we need to unpremultiply the alpha and the remultiply it with the blender.
		// This results in the complicated restrictions below.
		if(antialias == CAIRO_ANTIALIAS_NONE
				|| (_cairo_gpu_surface_use_msaa(dst) ? (dst->coverage_samples >= aa_samples) :
					(((op == CAIRO_OPERATOR_ADD || (dst->base.content == CAIRO_CONTENT_COLOR && _cairo_gpu_blend_factors[op].dst == 1)) || num_traps == 1)
						&& (!(_cairo_gpu_blend_factors[op].src & BLEND_ALPHA) || dst->base.content == CAIRO_CONTENT_COLOR)
						&& (dst->space->frag_div_alpha || !_cairo_gpu_blend_factors[op].src || _cairo_gpu_blend_factors[op].src == (1 | BLEND_ALPHA))
						&& (dst->space->fastest_polygon_smooth_samples >= aa_samples
									|| dst->space->nicest_polygon_smooth_samples >= aa_samples))))
			onepass = 1;
		else
		{
			int i;

			onepass = 1;
			for(i = 0; i < num_traps; i++)
			{
				if(traps[i].left.p1.x != traps[i].left.p2.x || traps[i].right.p1.x != traps[i].right.p2.x || !_cairo_fixed_is_integer(traps[i].top) || !_cairo_fixed_is_integer(traps[i].bottom) || !_cairo_fixed_is_integer(traps[i].left.p1.x) || !_cairo_fixed_is_integer(traps[i].right.p1.x))
				{
					onepass = 0;
					break;
				}
			}
			/* here we have onepass == 1 iff traps are all aligned rectangles, so no need for antialiasing */
			if(onepass)
				antialias = CAIRO_ANTIALIAS_NONE;
		}
	}

#ifdef DEBUG_FORCE_ONEPASS
	onepass = 1;
#endif
#ifdef DEBUG_FORCE_TWOPASS
	onepass = 0;
#endif

#ifdef DEBUG_FORCE_GPU_TRAPS_MASK
	if(antialias != CAIRO_ANTIALIAS_NONE)
	{
		aa_samples = dst->space->nicest_polygon_smooth_samples;
		antialias = CAIRO_ANTIALIAS_GRAY | (aa_samples << CAIRO_ANTIALIAS_SAMPLES_SHIFT);
	}
#endif

	if(onepass)
		status = _cairo_gpu_surface_composite_trapezoids_onepass(op, pattern, abstract_dst, antialias, src_x, src_y, dst_x, dst_y, width, height, traps, num_traps,
			(antialias != CAIRO_ANTIALIAS_NONE && !_cairo_gpu_surface_use_msaa(dst))
			? ((dst->space->fastest_polygon_smooth_samples >= aa_samples) ? 1 : 2) : 0, 0
		);
	else
	{
		// currently fills will always get handled by spans or onepass-traps, but strokes may get there
		cairo_gpu_surface_t *dst = abstract_dst;
		cairo_gpu_geometry_t* geometry;

		cairo_pattern_t *traps_pattern = 0;

		int smooth = -2;	/* -2 = sw, -1 = noaa, 0 = msaa, 1 = polygon_smooth */

		if(antialias == CAIRO_ANTIALIAS_NONE)
			smooth = -1;
		else
		{
			if(dst->space->fastest_polygon_smooth_samples >= aa_samples)
				smooth = 1;
			else if(dst->space->nicest_polygon_smooth_samples >= aa_samples)
				smooth = 2;
			else if(dst->space->msaa_samples >= aa_samples)
				smooth = 0;
		}

		if(smooth > 0
				&& dst->base.content == CAIRO_CONTENT_COLOR && _cairo_gpu_surface_has_physical_alpha(dst)
				&& dst->space->blend_func_separate
				&& (dst->space->frag_div_alpha || !_cairo_gpu_blend_factors[op].src || _cairo_gpu_blend_factors[op].src == (1 | BLEND_ALPHA))
		)
			status = _cairo_gpu_surface_composite_trapezoids_onepass(op, pattern, abstract_dst, antialias, src_x, src_y, dst_x, dst_y, width, height, traps, num_traps, smooth, 1);
		else
		{
	#ifdef DEBUG_DISABLE_MSAA
			smooth = 2;
	#endif

	#ifdef DEBUG_DISABLE_GPU_TRAPS_MASK
			smooth = -2;
	#endif

	#if 0
			{
				const char* names[] = {"cpu", "direct", "msaa", "fastest", "nicest"};
				static cairo_antialias_t old_aa = -1;
				if(antialias != old_aa)
				{
					printf("drawing to mask using %s\n", names[smooth + 2]);
					old_aa = antialias;
				}
			}
	#endif

			if(smooth == -2 || _cairo_gpu_getput_traps_pattern_gpu(dst, dst_x, dst_y, width, height, traps, num_traps, antialias, &traps_pattern, &geometry, smooth))
			{
			      smooth = -2;

	#ifdef DEBUG_FORCE_GPU_TRAPS_MASK
			      assert(0);
	#endif
			      status = _cairo_gpu_get_traps_pattern_pixman(dst, dst_x, dst_y, width, height, traps, num_traps, antialias, &traps_pattern);
			}

			status = _cairo_gpu_surface_composite(op, pattern, traps_pattern, dst, src_x, src_y, 0, 0, dst_x, dst_y, width, height);

			if(smooth > -2)
				_cairo_gpu_getput_traps_pattern_gpu(dst, dst_x, dst_y, width, height, traps, num_traps, antialias, &traps_pattern, &geometry, smooth);
			else
				cairo_pattern_destroy(traps_pattern);
		}
	}

	_cairo_gpu_exit();

	return status;
}

static cairo_int_status_t
_cairo_region_init_rects(cairo_region_t * region, cairo_rectangle_int_t * boxes, int count)
{
	pixman_box32_t stack_pboxes[CAIRO_STACK_ARRAY_LENGTH(pixman_box32_t)];
	pixman_box32_t *pboxes = stack_pboxes;
	cairo_int_status_t status = CAIRO_STATUS_SUCCESS;
	int i;

	region->status = CAIRO_STATUS_SUCCESS;


	if(count > ARRAY_LENGTH(stack_pboxes))
	{
		pboxes = _cairo_malloc_ab(count, sizeof(pixman_box32_t));
		if(unlikely(pboxes == NULL))
			return _cairo_error(CAIRO_STATUS_NO_MEMORY);
	}

	for(i = 0; i < count; i++)
	{
		pboxes[i].x1 = boxes[i].x;
		pboxes[i].y1 = boxes[i].y;
		pboxes[i].x2 = boxes[i].x + boxes[i].width;
		pboxes[i].y2 = boxes[i].y + boxes[i].height;
	}

	if(!pixman_region32_init_rects(&region->rgn, pboxes, count))
		status = _cairo_error(CAIRO_STATUS_NO_MEMORY);

	if(pboxes != stack_pboxes)
		free(pboxes);

	return status;
}

static cairo_int_status_t
_cairo_region_get_rects(cairo_region_t * region, int *num_boxes, cairo_rectangle_int_t ** boxes)
{
	int nboxes;

	pixman_box32_t *pboxes;

	cairo_rectangle_int_t *cboxes;

	int i;

	pboxes = pixman_region32_rectangles(&region->rgn, &nboxes);
	if(nboxes == 0)
	{
		*num_boxes = 0;
		return CAIRO_STATUS_SUCCESS;
	}

	if(nboxes > *num_boxes)
	{
		cboxes = _cairo_malloc_ab(nboxes, sizeof(cairo_rectangle_int_t));
		if(unlikely(cboxes == NULL))
			return _cairo_error(CAIRO_STATUS_NO_MEMORY);
	}
	else
		cboxes = *boxes;

	for(i = 0; i < nboxes; i++)
	{
		cboxes[i].x = pboxes[i].x1;
		cboxes[i].y = pboxes[i].y1;
		cboxes[i].width = pboxes[i].x2 - pboxes[i].x1;
		cboxes[i].height = pboxes[i].y2 - pboxes[i].y1;
	}

	*num_boxes = nboxes;
	*boxes = cboxes;

	return CAIRO_STATUS_SUCCESS;
}

static cairo_int_status_t
_cairo_gpu_surface_fill_rectangles(void *abstract_surface, cairo_operator_t op, const cairo_color_t * color, cairo_rectangle_int_t * rects, int num_rects)
{
	cairo_gpu_surface_t *surface = abstract_surface;
	cairo_gpu_context_t* ctx;
	cairo_int_status_t status;
	int i;
	float *vertices;
	float* v;
	int save_clip = surface->has_clip;
	int free_rects = 0;
	cairo_gpu_composite_setup_t setup;
	cairo_gpu_geometry_t* geometry;

	if(num_rects == 0 || op == CAIRO_OPERATOR_DEST)
		return CAIRO_STATUS_SUCCESS;

	/* this is probably faster than the other way */
	// TODO: this only works if the rectangles are disjoint, which seems to be guaranteed
	if(save_clip)
	{
		cairo_rectangle_int_t *new_rects = 0;

		int new_num_rects = 0;

		cairo_region_t rgn;
		memset(&rgn, 0, sizeof(rgn));

		_cairo_region_init_rects(&rgn, rects, num_rects);
		if(cairo_region_intersect(&rgn, &surface->clip) == CAIRO_STATUS_SUCCESS)
		{
			if(_cairo_region_get_rects(&rgn, &new_num_rects, &new_rects) == CAIRO_STATUS_SUCCESS)
			{
				status = CAIRO_STATUS_SUCCESS;
				if(!new_num_rects)
					return CAIRO_STATUS_SUCCESS;
				rects = new_rects;
				num_rects = new_num_rects;
				free_rects = 1;
			}
		}
		_cairo_region_fini(&rgn);

		surface->has_clip = 0;
	}

	_cairo_gpu_enter();

	if((op == CAIRO_OPERATOR_SOURCE || op == CAIRO_OPERATOR_CLEAR) && (num_rects <= 16))
	{
		cairo_gpu_context_t *ctx;
		ctx = _cairo_gpu_surface_bind(surface, FB_DRAW);
		if(op == CAIRO_OPERATOR_CLEAR)
			color = _cairo_stock_color(CAIRO_STOCK_TRANSPARENT);

		for(i = 0; i < num_rects; ++i) // calls surface_modified
			_cairo_gpu_solid_rect(ctx, surface, rects[i].x, rects[i].y, rects[i].width, rects[i].height, color->red * color->alpha, color->green * color->alpha, color->blue * color->alpha, color->alpha);
		status = CAIRO_STATUS_SUCCESS;
		goto out;
	}

	if(num_rects > 1)
		status = _cairo_gpu_composite_init(&setup, op, surface, 0, 0, 0, 0, surface->width, surface->height);
	else
		status = _cairo_gpu_composite_init(&setup, op, surface, rects[0].x, rects[0].y, 0, 0, rects[0].width, rects[0].height);

	if(unlikely(status))
		goto out;

	setup.c.r = color->red * color->alpha;
	setup.c.g = color->green * color->alpha;
	setup.c.b = color->blue * color->alpha;
	setup.a.r = setup.a.g = setup.a.b = setup.ka = color->alpha;

	ctx = _cairo_gpu_composite_plan_prepare_bind(&setup);
	if(!ctx)
	{
		status = CAIRO_INT_STATUS_UNSUPPORTED;
		goto out;
	}

	if(num_rects > 1)
	{
		geometry = _cairo_gpu_context_geometry(ctx, GEOM_TEMP);
		v = vertices = _cairo_gpu_geometry_begin(ctx, geometry, PRIM_QUADS, num_rects * 4, 2, 0, 0);
		for(i = 0; i < num_rects; i++)
			_cairo_gpu_emit_rect(&v, rects[i].x, rects[i].y, rects[i].width, rects[i].height);
		_cairo_gpu_geometry_end(ctx, geometry, num_rects * 4);

		_cairo_gpu_composite_draw_inner(ctx, &setup, geometry);

		_cairo_gpu_geometry_put(ctx, geometry);
	}
	else
		_cairo_gpu_composite_draw_inner(ctx, &setup, 0);

	_cairo_gpu_composite_end(ctx, &setup);

	for(i = 0; i < num_rects; i++)
		_cairo_gpu_surface_modified(surface, rects[i].x, rects[i].y, rects[i].width, rects[i].height);

	_cairo_gpu_composite_fini(&setup);

	status = CAIRO_STATUS_SUCCESS;
out:
	_cairo_gpu_exit();
	surface->has_clip = save_clip;
	if(free_rects)
		free(rects);

	return status;
}

typedef struct _cairo_gpu_surface_span_renderer
{
	cairo_span_renderer_t base;

	cairo_gpu_context_t* ctx;
	cairo_gpu_composite_setup_t setup;
	cairo_gpu_geometry_t* geometry;

	cairo_antialias_t antialias;
	cairo_composite_rectangles_t composite_rectangles;
	int bounded_by_mask;
	int ynext;

	void *vertices;
	unsigned int vertex_size;
	unsigned int size;
	int offset;

	cairo_gpu_color4_t color;
} cairo_gpu_surface_span_renderer_t;

static inline void
_cairo_gpu_span_renderer_end(cairo_gpu_surface_span_renderer_t * renderer)
{
	if(renderer->offset >= 0)
		_cairo_gpu_geometry_end(renderer->ctx, renderer->geometry, renderer->offset);

	if(renderer->offset > 0)
		_cairo_gpu_composite_draw_inner(renderer->ctx, &renderer->setup, renderer->geometry);

	renderer->offset = -1;
}

static inline void
_cairo_gpu_span_renderer_start(cairo_gpu_surface_span_renderer_t * renderer)
{
	renderer->vertices = _cairo_gpu_geometry_begin(renderer->ctx, renderer->geometry, PRIM_LINES, renderer->size, 2, (renderer->vertex_size == 3) ? 4 : 0, 0);
	renderer->offset = 0;
}

static void *
_cairo_gpu_span_renderer_get_vbo(cairo_gpu_surface_span_renderer_t * renderer, unsigned int num_vertices)
{
	unsigned int offset;

	offset = renderer->offset;
	renderer->offset += num_vertices;

	if(renderer->offset > (int)renderer->size)
	{
		renderer->offset -= num_vertices;
		_cairo_gpu_span_renderer_end(renderer);
		_cairo_gpu_span_renderer_start(renderer);

		renderer->offset += num_vertices;
		return renderer->vertices;
	}
	return (char *)renderer->vertices + offset * renderer->vertex_size * sizeof(float);
}

static inline void
_cairo_gpu_emit_span(cairo_gpu_surface_span_renderer_t * renderer, int x1, int x2, int y, uint8_t alphab)
{
	float *v = _cairo_gpu_span_renderer_get_vbo(renderer, 2);

	//printf("%i: %i %i: %i\n", y, x1, x2, alpha);

	if(renderer->vertex_size == 3)
	{
		float colorf;
		unsigned char c[4];

		//if(renderer->setup->bake_constant)
		{
			float alpha = alphab / 255.0;
			if(!renderer->setup.unpremultiplied)
			{
				c[0] = _cairo_color_float_to_byte(alpha * renderer->color.c.r);
				c[1] = _cairo_color_float_to_byte(alpha * renderer->color.c.g);
				c[2] = _cairo_color_float_to_byte(alpha * renderer->color.c.b);
				c[3] = _cairo_color_float_to_byte(alpha * renderer->color.ka);
			}
			else
			{
				c[0] = _cairo_color_float_to_byte(renderer->color.c.r);
				c[1] = _cairo_color_float_to_byte(renderer->color.c.g);
				c[2] = _cairo_color_float_to_byte(renderer->color.c.b);
				c[3] = _cairo_color_float_to_byte(alpha * renderer->color.ka);
			}
		}
		/*
		else
		{
			if(!setup->unpremultiplied)
			{
				c[0] = alphab;
				c[1] = alphab;
				c[2] = alphab;
				c[3] = alphab;
			}
			else
			{
				c[0] = 0xff;
				c[1] = 0xff;
				c[2] = 0xff;
				c[3] = alphab;
			}
		}
		*/
		colorf = *(float*)c;

		v[0] = x1 + 0.5f;
		v[1] = y + 0.5f;
		v[2] = colorf;
		v[3] = x2 + 0.5f;
		v[4] = y + 0.5f;
		v[5] = colorf;
	}
	else
	{
		v[0] = x1 + 0.5f;
		v[1] = y + 0.5f;
		v[2] = x2 + 0.5f;
		v[3] = y + 0.5f;
	}
}


/* Emits the contents of the span renderer rows as PRIM_LINES with the span's
 * alpha.
 *
 * Unlike the image surface, which is compositing into a temporary, we emit
 * coverage even for alpha == 0, in case we're using an unbounded operator.
 * But it means we avoid having to do the fixup.
 */
static cairo_status_t
_cairo_gpu_surface_span_renderer_render_row(void *abstract_renderer, int y, const cairo_half_open_span_t * spans, unsigned num_spans)
{
	cairo_gpu_surface_span_renderer_t *renderer = abstract_renderer;
	int xmin = renderer->composite_rectangles.mask.x;
	int xmax = xmin + renderer->composite_rectangles.width;
	int prev_x = xmin;
	int prev_alpha = 0;
	unsigned i;

	/* Make sure we're within y-range. */
	if(y < renderer->composite_rectangles.mask.y || y >= renderer->composite_rectangles.mask.y + renderer->composite_rectangles.height)
		return CAIRO_STATUS_SUCCESS;

	if(!renderer->bounded_by_mask)
	{
		// TODO: we may want to emit a rect instead - that will require an additional geometry for the recs
		int yh;
		for(yh = renderer->ynext; yh < y; ++yh)
			_cairo_gpu_emit_span(renderer, xmin, xmax, yh, 0);
	}

	/* Find the first span within x-range. */
	for(i = 0; i < num_spans && spans[i].x < xmin; i++)
	{
	}
	if(i > 0)
		prev_alpha = spans[i - 1].coverage;

	/* Set the intermediate spans. */
	for(; i < num_spans; i++)
	{
		int x = spans[i].x;

		if(x >= xmax)
			break;

		if(prev_alpha != 0 || !renderer->bounded_by_mask)
			_cairo_gpu_emit_span(renderer, prev_x, x, y, prev_alpha);

		prev_x = x;
		prev_alpha = spans[i].coverage;
	}

	if(prev_x < xmax)
	{
		if(prev_alpha != 0 || !renderer->bounded_by_mask)
			_cairo_gpu_emit_span(renderer, prev_x, xmax, y, prev_alpha);
	}

	renderer->ynext = y + 1;

	return CAIRO_STATUS_SUCCESS;
}

static void
_cairo_gpu_surface_span_renderer_destroy(void *abstract_renderer)
{
	cairo_gpu_surface_span_renderer_t *renderer = abstract_renderer;

	if(!renderer)
		return;

	if(renderer->offset > 0)
	{
		renderer->offset = 0;

		_cairo_gpu_span_renderer_end(renderer);
	}

	_cairo_gpu_geometry_put_long(renderer->ctx, renderer->geometry);

	_cairo_gpu_composite_end(renderer->ctx, &renderer->setup);

	_cairo_gpu_composite_fini(&renderer->setup);

	free(renderer);

	_cairo_gpu_exit();
}

static cairo_status_t
_cairo_gpu_surface_span_renderer_finish(void *abstract_renderer)
{
	cairo_gpu_surface_span_renderer_t *renderer = abstract_renderer;

	if(!renderer->bounded_by_mask)
	{
		// TODO: we may want to emit a rect instead - that will require an additional geometry
		int yh;
		int ymax = renderer->composite_rectangles.mask.y + renderer->composite_rectangles.height;
		int xmin = renderer->composite_rectangles.mask.x;
		int xmax = xmin + renderer->composite_rectangles.width;
		for(yh = renderer->ynext; yh < ymax; ++yh)
			_cairo_gpu_emit_span(renderer, xmin, xmax, yh, 0);
	}

	_cairo_gpu_span_renderer_end(renderer);

	return CAIRO_STATUS_SUCCESS;
}

static cairo_bool_t
_cairo_gpu_surface_check_span_renderer(cairo_operator_t op, const cairo_pattern_t * pattern, void *abstract_dst, cairo_antialias_t antialias, const cairo_composite_rectangles_t * rects)
{
	cairo_gpu_surface_t *dst = abstract_dst;

	(void)op;
	(void)pattern;
	(void)abstract_dst;
	(void)antialias;
	(void)rects;

#ifdef DEBUG_DISABLE_SPANS
	return 0;
#endif
	// TODO: we should possibly modify the span renderer so that it can output "subpixel" spans for MSAA
	return !_cairo_gpu_surface_use_msaa(dst);
}

static cairo_status_t
_cairo_gpu_surface_nop_span_renderer_render_row(void *abstract_renderer, int y, const cairo_half_open_span_t * spans, unsigned num_spans)
{
	return CAIRO_STATUS_SUCCESS;
}

static cairo_status_t
_cairo_gpu_surface_nop_span_renderer_finish(void *abstract_renderer)
{
	return CAIRO_STATUS_SUCCESS;
}

static void
_cairo_gpu_surface_nop_span_renderer_destroy(void *abstract_renderer)
{
	if(abstract_renderer)
		free(abstract_renderer);
}

static cairo_span_renderer_t *
_cairo_gpu_surface_create_span_renderer(cairo_operator_t op, const cairo_pattern_t * src, void *abstract_dst, cairo_antialias_t antialias, const cairo_composite_rectangles_t * rects)
{
	cairo_gpu_surface_t *dst = abstract_dst;
	cairo_gpu_surface_span_renderer_t *renderer = calloc(1, sizeof(*renderer));
	cairo_status_t status;

	if(renderer == NULL)
		return _cairo_span_renderer_create_in_error(CAIRO_STATUS_NO_MEMORY);

	if(op == CAIRO_OPERATOR_DEST)
	{
		renderer->base.destroy = _cairo_gpu_surface_nop_span_renderer_destroy;
		renderer->base.finish = _cairo_gpu_surface_nop_span_renderer_finish;
		renderer->base.render_row = _cairo_gpu_surface_nop_span_renderer_render_row;
		return &renderer->base;
	}

	_cairo_gpu_enter();

	status = _cairo_gpu_composite_init(&renderer->setup, op, dst, rects->dst.x, rects->dst.y, rects->mask.x, rects->mask.y, rects->width, rects->height);
	if(unlikely(status))
		goto fail;

	status = _cairo_gpu_composite_operand(&renderer->setup, src, rects->src.x, rects->src.y, FALSE, -1);
	if(unlikely(status))
		goto fail;

	// TODO: support component alpha here once span renderer starts supporting it.
	if(antialias != CAIRO_ANTIALIAS_NONE)
		renderer->setup.primary_chan = CHAN_KA;

	renderer->base.destroy = _cairo_gpu_surface_span_renderer_destroy;
	renderer->base.finish = _cairo_gpu_surface_span_renderer_finish;
	renderer->base.render_row = _cairo_gpu_surface_span_renderer_render_row;

	renderer->composite_rectangles = *rects;
	renderer->ynext = rects->mask.y;
	renderer->bounded_by_mask = _cairo_operator_bounded_by_mask(op);

	renderer->ctx = _cairo_gpu_composite_plan_prepare_bind(&renderer->setup);
	if(!renderer->ctx)
	{
		status = CAIRO_INT_STATUS_UNSUPPORTED;
		goto fail;
	}

	renderer->vertex_size = (antialias == CAIRO_ANTIALIAS_NONE) ? 2 : 3;
	_cairo_gpu_composite_get_constant_color(&renderer->setup, &renderer->color);

	renderer->size = 4096; //rects->height * 6; // assume 2 "pieces" for each line, 3 spans each
	renderer->offset = 0;
	renderer->geometry = _cairo_gpu_context_geometry_long(renderer->ctx, GEOM_SPANS);

	_cairo_gpu_span_renderer_start(renderer);

	return &renderer->base;

fail:
	_cairo_gpu_surface_span_renderer_destroy(renderer);
	return _cairo_span_renderer_create_in_error(status);
}

static cairo_int_status_t
_cairo_gpu_surface_set_clip_region(void *abstract_surface, cairo_region_t * region)
{
	cairo_gpu_surface_t *surface = abstract_surface;

	if(region)
	{
		if(!surface->has_clip)
			_cairo_region_init(&surface->clip);

		if(!pixman_region32_copy(&surface->clip.rgn, &region->rgn))
		{
			_cairo_region_fini(&surface->clip);
			surface->has_clip = 0;
			return CAIRO_STATUS_NO_MEMORY;
		}
		surface->has_clip = 1;
	}
	else
	{
		if(surface->has_clip)
			_cairo_region_fini(&surface->clip);
		surface->has_clip = 0;
	}

	return CAIRO_STATUS_SUCCESS;
}

//static cairo_int_status_t
//_cairo_gpu_surface_show_glyphs(void *abstract_dst, cairo_operator_t op, const cairo_pattern_t * source, cairo_glyph_t * glyphs, int num_glyphs, cairo_scaled_font_t * scaled_font, int *remaining_glyphs, cairo_rectangle_int_t * extents)

// TODO: support using multiple font cache textures, either by multiple calls or texture stacks/arrays if available

// we need to use old_show_glyphs so that the generic Cairo code will only give us bounding-independent operators
static cairo_int_status_t
_cairo_gpu_surface_old_show_glyphs (cairo_scaled_font_t *scaled_font,
					   cairo_operator_t     op,
					   const cairo_pattern_t *pattern,
					   void		  *abstract_dst,
					   int		    src_x,
					   int		    src_y,
					   int		    dst_x,
					   int		    dst_y,
					   unsigned int	  width,
					   unsigned int	  height,
					   cairo_glyph_t	*glyphs,
					   int		    num_glyphs)

{
	cairo_gpu_surface_t *dst = abstract_dst;
	cairo_gpu_surface_glyph_private_t *locked_list = (cairo_gpu_surface_glyph_private_t*) 1;
	cairo_gpu_surface_font_private_t* font_private;
	cairo_gpu_context_t* ctx = 0;
	cairo_gpu_composite_setup_t setup;
	cairo_rectangle_int_t glyph_extents;
	cairo_int_status_t status;
	cairo_gpu_geometry_t* geometry = 0;
	cairo_region_t clear_region;
	float *vertices = 0;
	float *v = 0;
	int i;
	int j = 0;

	if(scaled_font->surface_backend != NULL && scaled_font->surface_backend != &_cairo_gpu_surface_backend)
		return CAIRO_INT_STATUS_UNSUPPORTED;

	font_private = (cairo_gpu_surface_font_private_t*)scaled_font->surface_private;

	// XXX: this causes rgb24 test to fallback to the generic rendering path. We should not need this!
	if(font_private && ((cairo_gpu_surface_t*)font_private->surface)->space != dst->space)
		return CAIRO_INT_STATUS_UNSUPPORTED;

	if(!_cairo_operator_bounded_by_mask(op))
	{
		cairo_rectangle_int_t rect;
		rect.x = dst_x;
		rect.y = dst_y;
		rect.width = width;
		rect.height = height;
		_cairo_region_init_rectangle(&clear_region, &rect);
		//printf("init %i %i %i %i\n", dst_x, dst_y, width, height);
	}

	//if(source->type != CAIRO_PATTERN_TYPE_SOLID)
	if(1)
	{
		status = _cairo_scaled_font_glyph_device_extents(scaled_font, glyphs, num_glyphs, &glyph_extents);
		if(unlikely(status))
			return status;
	}

	status = _cairo_gpu_composite_init(&setup, op, dst, glyph_extents.x, glyph_extents.y, glyph_extents.x, glyph_extents.y, glyph_extents.width, glyph_extents.height);
	if(unlikely(status))
		return status;

	/*
	else if(!extents)
		memcpy(&glyph_extents, extents, sizeof(glyph_extents));
	else
	 */

	status = _cairo_gpu_composite_operand(&setup, pattern, glyph_extents.x, glyph_extents.y, FALSE, -1);
	if(unlikely(status))
		return status;

	_cairo_gpu_enter();

	_cairo_scaled_font_freeze_cache(scaled_font);

	for(i = 0; i < num_glyphs; i++)
	{
		cairo_scaled_glyph_t *scaled_glyph;

		cairo_gpu_surface_glyph_private_t *glyph_private;

		status = _cairo_scaled_glyph_lookup(scaled_font, glyphs[i].index, CAIRO_SCALED_GLYPH_INFO_SURFACE, &scaled_glyph);
		if(status != CAIRO_STATUS_SUCCESS)
			goto skip;

		if(!_cairo_operator_bounded_by_mask(op))
		{
			cairo_region_t rect_region;
			cairo_rectangle_int_t rect;
			rect.x = glyphs[i].x - scaled_glyph->surface->base.device_transform.x0;
			rect.y = glyphs[i].y - scaled_glyph->surface->base.device_transform.y0;
			rect.width = scaled_glyph->surface->width;
			rect.height = scaled_glyph->surface->height;

			_cairo_region_init_rectangle(&rect_region, &rect);
			if(cairo_region_subtract(&clear_region, &rect_region) != CAIRO_STATUS_SUCCESS)
				goto skip;
		}

		glyph_private = scaled_glyph->surface_private;
		if(!glyph_private || !glyph_private->area)
		{
			status = _cairo_gpu_surface_add_glyph(&dst->base, scaled_font, scaled_glyph);
			if(status != CAIRO_STATUS_SUCCESS)
				goto skip;
		}
		glyph_private = scaled_glyph->surface_private;
		if(glyph_private && glyph_private->area)
		{
			if(glyph_private->area->width)
			{
				if(!ctx)
				{
					status = _cairo_gpu_composite_operand(&setup, ((cairo_gpu_surface_font_private_t*)scaled_font->surface_private)->pattern, glyph_extents.x, glyph_extents.y, TRUE, 0);
					if(unlikely(status))
						goto out;

					ctx = _cairo_gpu_composite_plan_prepare_bind(&setup);
					if(!ctx)
					{
						status = CAIRO_INT_STATUS_UNSUPPORTED;
						goto out;
					}

					geometry = _cairo_gpu_context_geometry(ctx, GEOM_GLYPHS);
					v = vertices = _cairo_gpu_geometry_begin(ctx, geometry, PRIM_QUADS, num_glyphs * 4, 2, 0, 2);
				}

				_cairo_gpu_emit_rect_tex(&v, _cairo_lround(glyphs[i].x - scaled_glyph->surface->base.device_transform.x0), _cairo_lround(glyphs[i].y - scaled_glyph->surface->base.device_transform.y0), glyph_private->area->x, glyph_private->area->y, glyph_private->area->width, glyph_private->area->height);

				if(!glyph_private->next_locked)
				{
					glyph_private->next_locked = locked_list;
					locked_list = glyph_private;
				}
			}
		}

		continue;
	      skip:
		glyphs[j++] = glyphs[i];
	}

	while(locked_list != (cairo_gpu_surface_glyph_private_t *) 1)
	{
		cairo_gpu_surface_glyph_private_t *next = locked_list->next_locked;

		locked_list->next_locked = 0;
		locked_list = next;
	}

	_cairo_scaled_font_thaw_cache(scaled_font);

	if(ctx)
	{
		_cairo_gpu_geometry_end(ctx, geometry, ((float*)v - (float*)vertices) >> 2);

		_cairo_gpu_composite_draw_once_modify(ctx, &setup, geometry);

		_cairo_gpu_geometry_put(ctx, geometry);

		_cairo_gpu_composite_fini(&setup);
	}

	if(!_cairo_operator_bounded_by_mask(op))
	{
		int num_rects = 0;
		cairo_rectangle_int_t* rects = 0;
		status = _cairo_region_get_rects(&clear_region, &num_rects, &rects);
		if(status)
			return status;

		if(num_rects)
		{
			_cairo_gpu_surface_fill_rectangles(dst, CAIRO_OPERATOR_CLEAR, _cairo_stock_color(CAIRO_STOCK_TRANSPARENT), rects, num_rects);
			free(rects);
		}
	}

	status = _cairo_scaled_font_show_glyphs(scaled_font, op, pattern, abstract_dst, src_x, src_y, dst_x, dst_y, width, height, glyphs, j);

out:
	_cairo_gpu_exit();
	return status;
}
