#define OSMESA_WIDTH		0x20
#define OSMESA_HEIGHT		0x21
#define OSMESA_FORMAT		0x22
#define OSMESA_TYPE		0x23
#define OSMESA_MAX_WIDTH	0x24
#define OSMESA_MAX_HEIGHT	0x25

#include <GL/glx.h>
#include <GL/glxext.h>

typedef void (* PFNGLXDESTROYGLXPIXMAPPROC) (Display *dpy, GLXPixmap pix);
typedef void (* PFNGLXDESTROYCONTEXTPROC) (Display *dpy, GLXContext ctx);
typedef Bool (* PFNGLXISDIRECTPROC) (Display *dpy, GLXContext ctx);
typedef Bool (* PFNGLXMAKECONTEXTCURRENTPROC) (Display *dpy, GLXDrawable drawable1, GLXDrawable drawable2, GLXContext ctx);
typedef Bool (* PFNGLXMAKECURRENTPROC) (Display *dpy, GLXDrawable drawable, GLXContext ctx);
typedef GLXContext (* PFNGLXGETCURRENTCONTEXTPROC) (void);
typedef GLXDrawable (* PFNGLXGETCURRENTDRAWABLEPROC) (void);
typedef void (* PFNGLXWAITGLPROC) (void);
typedef void (* PFNGLXWAITXPROC) (void);
typedef void (* PFNGLXSWAPBUFFERSPROC) (Display *dpy, GLXDrawable drawable);

typedef struct
{
	PFNGLXBINDTEXIMAGEEXTPROC BindTexImageEXT;
	PFNGLXCHOOSEFBCONFIGSGIXPROC ChooseFBConfigSGIX;
	PFNGLXCREATENEWCONTEXTPROC CreateNewContext;
	PFNGLXCREATEPBUFFERPROC CreatePbuffer;
	PFNGLXCREATEPIXMAPPROC CreatePixmap;
	PFNGLXDESTROYCONTEXTPROC DestroyContext;
	PFNGLXDESTROYGLXPIXMAPPROC DestroyGLXPixmap;
	PFNGLXDESTROYPBUFFERPROC DestroyPbuffer;
	PFNGLXDESTROYPIXMAPPROC DestroyPixmap;
	PFNGLXGETCURRENTCONTEXTPROC GetCurrentContext;
	PFNGLXGETCURRENTDISPLAYPROC GetCurrentDisplay;
	PFNGLXGETCURRENTDRAWABLEPROC GetCurrentDrawable;
	PFNGLXGETCURRENTREADDRAWABLEPROC GetCurrentReadDrawable;
	PFNGLXGETFBCONFIGATTRIBSGIXPROC GetFBConfigAttribSGIX;
	PFNGLXGETFBCONFIGSPROC GetFBConfigs;
	PFNGLXGETVISUALFROMFBCONFIGSGIXPROC GetVisualFromFBConfigSGIX;
	PFNGLXISDIRECTPROC IsDirect;
	PFNGLXMAKECONTEXTCURRENTPROC MakeContextCurrent;
	PFNGLXMAKECURRENTPROC MakeCurrent;
	PFNGLXQUERYDRAWABLEPROC QueryDrawable;
	PFNGLXRELEASETEXIMAGEEXTPROC ReleaseTexImageEXT;
	PFNGLXSWAPBUFFERSPROC SwapBuffers;
	PFNGLXWAITGLPROC WaitGL;
	PFNGLXWAITXPROC WaitX;

	Display *dpy;
	GLXContext share;
	int direct;
	unsigned char owns_dpy:1;
	unsigned char waitx : 1;
	unsigned char waitgl : 1;
} cairo_glx_t;

typedef struct
{
	Display* dpy;
	GLXContext ctx;
	GLXDrawable read_drawable;
	GLXDrawable draw_drawable;
} cairo_glx_state_t;
