static inline int
_cairo_glx_surface_get_destination_idx_drawable(cairo_gpu_surface_t * surface)
{
	if(surface->has_drawable)
	{
		if(surface->draw_to_back_buffer)
			return SURF_BACK;
		else
			return SURF_FRONT;
	}
	else
		return -1;
}

static inline cairo_bool_t
_cairo_glx_surface_has_physical_alpha(cairo_gpu_surface_t* surface)
{
	// We assume that the unused alpha channel is set to 1
	// TODO: due to this, we don't try to use the alpha channels of drawables; check whether we can do something about this
	return !surface->glx.fbconfig;
}

static inline cairo_gpu_texture_t*
_cairo_glx_surface_begin_texture(cairo_gpu_surface_t * surface)
{
	cairo_gpu_texture_t* texture = 0;
	if(surface->glx.bound_texture.tex && (surface->valid_mask & (1 << (SURF_FRONT | SURF_BACK))))
	{
		texture = &surface->glx.bound_texture;
		if(!surface->glx.bound_image_ref++)
		{
			cairo_gpu_context_t* ctx = _cairo_gpu_space_bind(surface->space);
			int i = _cairo_gl_context_set_texture(ctx, -1, &surface->glx.bound_texture);
			_cairo_gl_context_set_active_texture(ctx, i);
			ctx->space->glx.BindTexImageEXT(ctx->space->glx.dpy, surface->glx.glxdrawable, GLX_FRONT_LEFT_EXT, NULL);
		}
	}
	return texture;
}

static inline void
_cairo_glx_surface_end_texture(cairo_gpu_surface_t * surface, cairo_gpu_context_t* ctx, cairo_gpu_texture_t* texture)
{
	if(texture == &surface->glx.bound_texture)
	{
		if(!--surface->glx.bound_image_ref)
			ctx->space->glx.ReleaseTexImageEXT(surface->space->glx.dpy, surface->glx.glxdrawable, GLX_FRONT_LEFT_EXT);
	}
}

static void
_cairo_glx_surface_unset_drawable(cairo_gpu_surface_t* surface)
{
	surface->valid_mask &=~ ((1 << SURF_FRONT) | (1 << SURF_BACK));

	if(surface->glx.owns_pbuffer)
	{
		surface->space->glx.DestroyPbuffer(surface->space->glx.dpy, surface->glx.glxdrawable);
		surface->glx.owns_pbuffer = 0;
	}

	if(surface->glx.owns_glx_pixmap)
	{
		surface->space->glx.DestroyGLXPixmap(surface->space->glx.dpy, surface->glx.glxdrawable);
		surface->glx.owns_glx_pixmap = 0;
	}

	surface->glx.glxdrawable = 0;
	surface->glx.xdrawable = 0;
	surface->glx.fbconfig = 0;
	surface->subspace = 0;
	surface->draw_to_back_buffer = 0;
	surface->buffer_non_upside_down = 0;

	if(surface->glx.bound_texture.tex)
	{
		cairo_gpu_context_t* ctx = _cairo_gpu_space_bind(surface->space);
		_cairo_gpu_texture_fini(ctx, &surface->glx.bound_texture);
	}
}

static cairo_int_status_t
_cairo_glx_surface_set_drawable(void* abstract_surface, cairo_content_t content, Drawable xdrawable, GLXFBConfig fbconfig, cairo_bool_t double_buffer, int width, int height)
{
	cairo_gpu_surface_t* surface = abstract_surface;
	int is_db;
	int is_pixmap = 0;
	Display* dpy = surface->space->glx.dpy;

	if(xdrawable != surface->glx.xdrawable || !xdrawable)
	{
		if(xdrawable)
		{
			XWindowAttributes attr;
			int (*handler)(Display *, XErrorEvent *);

			handler = XSetErrorHandler(_cairo_gpu_x11_error_handler);
			XGetWindowAttributes(dpy, xdrawable, &attr);
			XSetErrorHandler(handler);
			if(_cairo_gpu_x11_error)
			{
				is_pixmap = 1;
				_cairo_gpu_x11_error = 0;
			}
			else
			{
				if(!fbconfig)
				{
					unsigned long visualid = attr.visual->visualid;

					fbconfig = _cairo_glxfbconfig_for_visualid(surface->space, dpy, visualid);
				}

				if(width < 0)
					width = attr.width;

				if(height < 0)
					height = attr.height;
			}

			if(!fbconfig)
				fbconfig = _cairo_glxfbconfig_for_visualid(surface->space, dpy, XVisualIDFromVisual(DefaultVisual(dpy, DefaultScreen(dpy))));

			if(!fbconfig)
				return CAIRO_STATUS_SURFACE_TYPE_MISMATCH;
		}

		_cairo_glx_surface_unset_drawable(surface);

		surface->public_drawable = surface->buffer_non_upside_down = !!xdrawable;
		surface->glx.xdrawable = surface->glx.glxdrawable = xdrawable;
		surface->has_drawable = 1;

		if(is_pixmap && (surface->space->tex_npot || surface->space->tex_rectangle)) // this is a pixmap
		{
			int (*handler)(Display *, XErrorEvent *);
			cairo_gpu_context_t* ctx;

			int alpha_size = 0;

			int attribs[] = { GLX_TEXTURE_TARGET_EXT, GLX_TEXTURE_2D_EXT,
							 GLX_TEXTURE_FORMAT_EXT, 0,
						      None, None };
			//int attribs[] = {0, 0};

			if(!surface->space->tex_npot)
				attribs[1] = GLX_TEXTURE_RECTANGLE_EXT;

	retry:
			surface->space->glx.GetFBConfigAttribSGIX(dpy, surface->glx.fbconfig, GLX_ALPHA_SIZE, &alpha_size);
			attribs[3] = alpha_size ? GLX_TEXTURE_FORMAT_RGBA_EXT : GLX_TEXTURE_FORMAT_RGB_EXT;

			ctx = _cairo_gpu_space_bind(surface->space);

			surface->glx.glxdrawable = surface->space->glx.CreatePixmap (dpy, surface->glx.fbconfig, xdrawable, attribs);
			surface->glx.owns_glx_pixmap = 1;

			handler = XSetErrorHandler(_cairo_gpu_x11_error_handler);
			surface->space->glx.QueryDrawable(dpy, surface->glx.glxdrawable, GLX_WIDTH, (unsigned*)&width);
			surface->space->glx.QueryDrawable(dpy, surface->glx.glxdrawable, GLX_HEIGHT, (unsigned*)&height);
			XSetErrorHandler(handler);
			_cairo_gpu_x11_error = 0;

			/* Unfortunately, we can't find this out earlier... */
			if(attribs[1] == GLX_TEXTURE_RECTANGLE_EXT && is_pow2(width) && is_pow2(height))
			{
				attribs[1] = GLX_TEXTURE_2D_EXT;
				surface->space->glx.DestroyPixmap(dpy, surface->glx.glxdrawable);
				surface->glx.glxdrawable = 0;
				surface->glx.owns_glx_pixmap = 0;
				goto retry;
			}

			_cairo_gpu_texture_create(ctx, &surface->glx.bound_texture, width, height);
		}
	}
	else if(!fbconfig)
		fbconfig = surface->glx.fbconfig;

	surface->space->glx.GetFBConfigAttribSGIX(dpy, fbconfig, GLX_DOUBLEBUFFER, &is_db);

	surface->draw_to_back_buffer = is_db && double_buffer;

	surface->glx.fbconfig = fbconfig;
	if(!fbconfig)
	{
		/* maybe drawable is color-indexed, which we do not support */
		free(surface);
		goto out;
	}

	{
		int alpha_size;
		surface->space->glx.GetFBConfigAttribSGIX(dpy, surface->glx.fbconfig, GLX_ALPHA_SIZE, &alpha_size);
		surface->glx.fbconfig_alpha_size = alpha_size;
	}

	{
		int color_size = 0;
		int v;
		surface->space->glx.GetFBConfigAttribSGIX(dpy, surface->glx.fbconfig, GLX_RED_SIZE, &v);
		color_size += v;
		surface->space->glx.GetFBConfigAttribSGIX(dpy, surface->glx.fbconfig, GLX_GREEN_SIZE, &v);
		color_size += v;
		surface->space->glx.GetFBConfigAttribSGIX(dpy, surface->glx.fbconfig, GLX_BLUE_SIZE, &v);
		color_size += v;
		surface->glx.fbconfig_color_size = color_size;
	}

	if(!content)
		content = surface->glx.fbconfig_color_size ?
			(surface->glx.fbconfig_alpha_size ? CAIRO_CONTENT_COLOR_ALPHA : CAIRO_CONTENT_COLOR)
			: CAIRO_CONTENT_ALPHA;

	_cairo_gpu_set_surface_subspace(surface);

	if(surface->glx.glxdrawable)
		surface->valid_mask = 1 << _cairo_glx_surface_get_destination_idx_drawable(surface);

out:;
	return CAIRO_STATUS_SUCCESS;
}

static cairo_int_status_t
_cairo_glx_surface_set_drawable_visualid(cairo_gpu_surface_t* surface, cairo_content_t content, unsigned long drawable, unsigned long visualid, cairo_bool_t double_buffer, int width, int height)
{
	GLXFBConfig fbconfig = 0;

	if(visualid)
		fbconfig = _cairo_glxfbconfig_for_visualid(surface->space, surface->space->glx.dpy, visualid);

	return _cairo_glx_surface_set_drawable(surface, content, drawable, fbconfig, double_buffer, (int)width, (int)height);
}

static cairo_int_status_t
_cairo_glx_surface_set_offscreen(cairo_gpu_surface_t* surface, cairo_content_t content, double width, double height, int color_mantissa_bits, int alpha_mantissa_bits, int exponent_bits, int shared_exponent_bits)
{
	cairo_status_t status = CAIRO_STATUS_SUCCESS;

	if(surface->glx.xdrawable || width != surface->width || height != surface->height)
	{
		if(!surface->space->use_fbo)
		{
			cairo_hash_entry_t lookup;
			cairo_gpu_ptr_entry_t* entry;
			GLXFBConfig fbconfig;

			if(color_mantissa_bits > 256)
				color_mantissa_bits = 256;
			if(alpha_mantissa_bits > 256)
				alpha_mantissa_bits = 256;

			CAIRO_MUTEX_LOCK(surface->space->mutex);
			lookup.hash = TABLE_FBCONFIG | (color_mantissa_bits << 15) | alpha_mantissa_bits;
			entry = (cairo_gpu_ptr_entry_t*)_cairo_hash_table_lookup(surface->space->table, &lookup);

			if(entry)
				fbconfig = (GLXFBConfig)entry->v;
			else
			{
				fbconfig = _cairo_glxfbconfig_for_pbuffer(surface->space, surface->space->glx.dpy, color_mantissa_bits, alpha_mantissa_bits);
				entry = (cairo_gpu_ptr_entry_t*)malloc(sizeof(cairo_gpu_ptr_entry_t));
				entry->base.hash = lookup.hash;
				entry->v = fbconfig;
				if(_cairo_hash_table_insert(surface->space->table, &entry->base))
					free(entry);
			}
			CAIRO_MUTEX_UNLOCK(surface->space->mutex);

			status = _cairo_glx_surface_set_drawable(surface, content, 0, fbconfig, 0, width, height);
		}
	}
	return status;
}

static void
_cairo_glx_surface_swap_buffers(cairo_gpu_surface_t* surface)
{
	if(surface->glx.glxdrawable)
		surface->space->glx.SwapBuffers(surface->space->glx.dpy, surface->glx.glxdrawable);
}

static unsigned long
_cairo_glx_surface_get_drawable(cairo_gpu_surface_t* surface)
{
	return surface->glx.xdrawable;
}

static unsigned long
_cairo_glx_surface_get_gl_drawable(cairo_gpu_surface_t* surface)
{
	return surface->glx.glxdrawable;
}
