typedef OSMesaContext (*PFNOSMESACREATECONTEXTPROC)( GLenum format, OSMesaContext sharelist );
typedef GLboolean (*PFNOSMESAMAKECURRENTPROC)( OSMesaContext ctx, void *buffer, GLenum type, GLsizei width, GLsizei height );
typedef OSMesaContext (*PFNOSMESAGETCURRENTCONTEXTPROC)(void);
typedef void (*PFNOSMESADESTROYCONTEXTPROC)( OSMesaContext ctx );
typedef GLboolean (*PFNOSMESAGETCOLORBUFFERPROC) ( OSMesaContext c, GLint *width, GLint *height, GLint *format, void **buffer );
typedef void (*PFNOSMESAGETINTEGERVPROC) ( GLint pname, GLint *value );

typedef struct
{
	PFNOSMESACREATECONTEXTPROC CreateContext;
	PFNOSMESADESTROYCONTEXTPROC DestroyContext;
	PFNOSMESAGETCOLORBUFFERPROC GetColorBuffer;
	PFNOSMESAGETCURRENTCONTEXTPROC GetCurrentContext;
	PFNOSMESAGETINTEGERVPROC GetIntegerv;
	PFNOSMESAMAKECURRENTPROC MakeCurrent;

	OSMesaContext share;
} cairo_osmesa_t;

typedef struct
{
	OSMesaContext ctx;
	void* buffer;
	int type;
	int width;
	int height;
} cairo_osmesa_state_t;
