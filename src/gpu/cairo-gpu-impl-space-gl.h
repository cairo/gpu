#include <dlfcn.h>

typedef struct
{
	cairo_gpu_gl_state_t user;
	cairo_gpu_context_t* ctx;
	int depth;
} cairo_gpu_gl_tls_t;

__thread cairo_gpu_gl_tls_t _cairo_gl_tls;

static cairo_gpu_context_t*
_cairo_gpu_context__create(cairo_gpu_space_t *space, cairo_gpu_subspace_t *sub)
{
	cairo_gpu_context_t* ctx = calloc(1, sizeof(cairo_gpu_context_t));

	ctx->space = space;
	ctx->subspace = sub;

	ctx->color_mask = 0xf;
	ctx->blend_func = ~0;
	ctx->frag = ~0;
	ctx->vert = 0;
	ctx->fragp = ~0;
	ctx->vertp = ~0;
	ctx->constant_unit = -1;
	ctx->viewport_width = -1;
	ctx->viewport_height = -1;
	return ctx;
}

static cairo_bool_t
_cairo_gpu_hash_keys_equal(const void *key_a, const void *key_b)
{
	return ((cairo_hash_entry_t*)key_a)->hash == ((cairo_hash_entry_t*)key_b)->hash;
}

static cairo_gpu_space_t *
_cairo_gpu_space__begin_create(void)
{
	cairo_gpu_space_t *space;

	space = calloc(1, sizeof(cairo_gpu_space_t));
	if(!space)
		return 0;

	CAIRO_REFERENCE_COUNT_INIT(&space->base.ref_count, 1);
	CAIRO_MUTEX_INIT(space->mutex);
	CAIRO_MUTEX_INIT(space->cached_mask_surface_mutex);
	space->base.backend = &_cairo_gpu_space_backend;
	space->tls_list.prev = &space->tls_list;
	space->tls_list.next = &space->tls_list;
	pthread_key_create(&space->tls, _cairo_gpu_space_tls_dtor);
	pthread_key_create(&space->subspace.context_tls, 0);
	space->table = _cairo_hash_table_create(_cairo_gpu_hash_keys_equal);

	return space;
}

static cairo_space_t*
_cairo_gpu_space__finish_create(cairo_gpu_space_t* space, unsigned flags);

#include "cairo-gpu-impl-space-gl-osmesa.h"
#include "cairo-gpu-impl-space-gl-glx.h"

static inline unsigned
_cairo_gl_error(cairo_gpu_context_t* ctx)
{
	unsigned err;
	if((err = ctx->gl.GetError()))
	{
		while(ctx->gl.GetError())
		{}
	}
	return err;
}

static inline void
_cairo_gpu_assert_no_gl_error(cairo_gpu_context_t* ctx)
{
	static int errors = 0;
	unsigned err;
	while((err = ctx->gl.GetError()))
	{
		// race condition here but we don't really care
		int n = errors;
		if(n == 10)
		{
			printf("WARNING: cairo-gpu: not showing any more OpenGL errors\n");
			errors = n +1;
		}
		else if(n < 10)
		{
			printf("WARNING: cairo-gpu: unexpected OpenGL error: %x\n", err);
			errors = n + 1;
		}
	}
}

#define GL_ERROR(x) (_cairo_gpu_assert_no_gl_error(ctx), x, _cairo_gl_error(ctx))

static void
_cairo_gpu_gl_init(cairo_gpu_gl_t* gl, PFNGLGETPROCADDRESSPROC GetProcAddress)
{
#define DO(x) gl->x = GetProcAddress("gl" #x)
	DO(ActiveTextureARB);
	DO(BindBufferARB);
	DO(BindFramebufferEXT);
	DO(BindProgramARB);
	DO(BindRenderbufferEXT);
	DO(BindTextureEXT);
	DO(BindVertexArray);
	DO(BlendColorEXT);
	DO(BlendEquation);
	DO(BlendFunc);
	DO(BlendFuncSeparateEXT);
	DO(BlitFramebufferEXT);
	DO(BufferDataARB);
	DO(CheckFramebufferStatusEXT);
	DO(Clear);
	DO(ClearColor);
	DO(ClientActiveTextureARB);
	DO(Color4fv);
	DO(ColorMask);
	DO(ColorPointer);
	DO(CopyPixels);
	DO(CopyTexSubImage2D);
	DO(DeleteBuffersARB);
	DO(DeleteFramebuffersEXT);
	DO(DeleteProgramsARB);
	DO(DeleteRenderbuffersEXT);
	DO(DeleteTexturesEXT);
	DO(DeleteVertexArrays);
	DO(Disable);
	DO(DisableClientState);
	DO(DrawArraysEXT);
	DO(DrawBuffer);
	DO(DrawPixels);
	DO(Enable);
	DO(EnableClientState);
	DO(EnableVertexAttribArrayARB);
	DO(Finish);
	DO(Flush);
	DO(FramebufferRenderbufferEXT);
	DO(FramebufferTexture2DEXT);
	DO(GenBuffersARB);
	DO(GenerateMipmapEXT);
	DO(GenFramebuffersEXT);
	DO(GenProgramsARB);
	DO(GenRenderbuffersEXT);
	DO(GenTexturesEXT);
	DO(GenVertexArrays);
	DO(GetError);
	DO(GetFloatv);
	DO(GetIntegerv);
	DO(GetString);
	DO(GetTexImage);
	DO(Hint);
	DO(LoadIdentity);
	DO(LoadMatrixd);
	DO(LoadMatrixf);
	DO(MapBufferARB);
	DO(MapBufferRange);
	DO(MatrixMode);
	DO(Ortho);
	DO(PixelMapfv);
	DO(PixelStorei);
	DO(PixelTransferf);
	DO(PixelZoom);
	DO(ProgramEnvParameter4fvARB);
	DO(ProgramStringARB);
	DO(RasterPos2i);
	DO(ReadBuffer);
	DO(ReadPixels);
	DO(Recti);
	DO(RenderbufferStorageMultisampleCoverageNV);
	DO(RenderbufferStorageMultisampleEXT);
	DO(Scissor);
	DO(TexCoordPointer);
	DO(TexEnvfv);
	DO(TexEnvi);
	DO(TexGenfv);
	DO(TexGeni);
	DO(TexImage2D);
	DO(TexParameteri);
	DO(TexSubImage2D);
	DO(Translated);
	DO(Translatef);
	DO(UnmapBufferARB);
	DO(VertexPointer);
	DO(Viewport);
	DO(WindowPos2i);
#undef DO
}

static void
_cairo_gpu_space__fini_entry(void* abstract_entry, void* closure)
{
	cairo_gpu_context_t* ctx = closure;
	cairo_gpu_int_entry_t* int_entry = (cairo_gpu_int_entry_t*)abstract_entry;
	//cairo_gpu_ptr_entry* ptr_entry = (cairo_gpu_ptr_entry_t*)abstract_entry;
	switch(int_entry->base.hash & TABLE_MASK)
	{
	case TABLE_FRAG:
	case TABLE_VERT:
		ctx->gl.DeleteProgramsARB(1, &int_entry->v);
		break;
	default:
		break;
	}

	_cairo_hash_table_remove(ctx->space->table, (cairo_hash_entry_t*)abstract_entry);
	free(abstract_entry);
}

static void
_cairo_gpu_space__fini(cairo_gpu_context_t * ctx)
{
	cairo_gpu_space_t* space = ctx->space;

	_cairo_gpu_texture_fini(ctx, &space->dummy_texture);

	_cairo_hash_table_foreach (space->table, _cairo_gpu_space__fini_entry, ctx);
	_cairo_hash_table_destroy(space->table);
}

// ctx == 0, user.api < 0 => we have an user context bound, or nothing
// ctx != 0, user.api < 0 => we have either an user context bound, or nothing, or ctx
// ctx != 0, user_api >= 0 => we have ctx bound, and may have an user context saved
// ctx == 0, user.api > 0 => we have an user context bound, and we have an user context saved
// ctx == 0, user.api == 0 => we have no context bound, and we have no user context saved

static inline void
_cairo_gpu_enter(void)
{
	++_cairo_gl_tls.depth;
}

static inline void
_cairo_gl_make_current(cairo_gpu_space_t* space, cairo_gpu_gl_state_t* user)
{
	API_SPACE(make_current)(space, user);
}

static void
_cairo_gpu_space__do_destroy(cairo_gpu_space_t* space)
{
	API_SPACE(do_destroy)(space);

	if(space->owns_libgl)
		dlclose(space->libgl);
	free(space);
}

static void
_cairo_gpu_context__do_destroy(cairo_gpu_context_t * ctx)
{
	cairo_gpu_space_t* space = ctx->space;

	API_CTX(do_destroy)(ctx);

	free(ctx);

	if(space->destroy_on_unbind)
		_cairo_gpu_space__do_destroy(space);
}

static cairo_gpu_context_t*
_cairo_gpu_context__unbind_internal(void)
{
	cairo_gpu_context_t* ctx = _cairo_gl_tls.ctx;
	API_CTX(flush)(ctx);

	_cairo_gpu_assert_no_gl_error(ctx);

	if(ctx->destroy_on_unbind)
		return ctx;

	return 0;
}

static void
_cairo_gpu_do_exit(void)
{
	cairo_gpu_context_t* ctx = _cairo_gl_tls.ctx;
	cairo_gpu_context_t* destroy_ctx = 0;
	if(ctx)
	{
		destroy_ctx = _cairo_gpu_context__unbind_internal();

		if(destroy_ctx && !_cairo_gl_tls.user.api)
		{
			cairo_gpu_space_t* space = ctx->space;
			API_SPACE(unbind_context)(space);

			_cairo_gl_tls.ctx = 0;
		}
	}

	if(_cairo_gl_tls.user.api)
	{
		_cairo_gl_make_current(ctx->space, &_cairo_gl_tls.user);
		_cairo_gl_tls.user.api = 0;
		_cairo_gl_tls.ctx = 0;
	}

	_cairo_gl_tls.user.api = -1;

	if(destroy_ctx)
		_cairo_gpu_context__do_destroy(destroy_ctx);
}

/*
 * NOTE: if no OpenGL context was set, we leak our current context.
 * This allows to run many Cairo commands without rebinding the context every time.
 * The risk is that the user may accidentally mess up our GL context.
 */
static inline void
_cairo_gpu_exit(void)
{
	if(!--_cairo_gl_tls.depth && _cairo_gl_tls.user.api >= 0)
		_cairo_gpu_do_exit();
}

// you must bind a context after calling this
static void
_cairo_gpu_context__update_state(cairo_gpu_context_t* ctx, unsigned api)
{
	cairo_gpu_gl_state_t* user = &_cairo_gl_tls.user;
	user->api = 0;
	API_CTX(update_state)(ctx, user);
}

static void
_cairo_gpu_context__do_bind(cairo_gpu_context_t* ctx, cairo_gpu_surface_t * surface)
{
	cairo_gpu_context_t* destroy_ctx = 0;
	if(_cairo_gl_tls.ctx)
		destroy_ctx = _cairo_gpu_context__unbind_internal();

	API_CTX(do_bind)(ctx, surface);

	if(!ctx->gl_inited)
	{
		_cairo_gpu_gl_init(&ctx->gl, ctx->space->GetProcAddress);
		ctx->gl_inited = 1;
	}
	_cairo_gl_tls.ctx = ctx;

	if(destroy_ctx)
		_cairo_gpu_context__do_destroy(destroy_ctx);
}

static inline void
_cairo_gl_context_bind_surface(cairo_gpu_context_t* ctx, cairo_gpu_surface_t * surface)
{
	 assert(_cairo_gl_tls.depth);

	if(_cairo_gl_tls.user.api < 0)
		_cairo_gpu_context__update_state(ctx, ctx->space->api);

	if(_cairo_gl_tls.ctx == ctx)
	{
		if(surface && surface->has_drawable)
		{
		// lenient one-way version
			if(!API_CTX(is_surface_bound)(ctx, surface))
				goto bind;
		}
	}
	else
	{
bind:
		_cairo_gpu_context__do_bind(ctx, surface);
	}
}

static inline void
_cairo_gpu_context_bind(cairo_gpu_context_t* ctx)
{
	assert(_cairo_gl_tls.depth);

	if(_cairo_gl_tls.user.api < 0)
		_cairo_gpu_context__update_state(ctx, ctx->space->api);

	if(_cairo_gl_tls.ctx == ctx)
		return;
	else
		_cairo_gpu_context__do_bind(ctx, 0);
}

static void
_cairo_gpu_context__destroy(cairo_gpu_context_t * ctx)
{
	assert(_cairo_gl_tls.depth);

	if(_cairo_gl_tls.user.api < 0)
		_cairo_gpu_context__update_state(ctx, ctx->space->api);

	if(ctx == _cairo_gl_tls.ctx)
	{
		ctx->destroy_on_unbind = 1;
		return;
	}

	_cairo_gpu_context__do_destroy(ctx);
}

cairo_public cairo_space_t *
cairo_gl_hardware_space_create(void* libgl, unsigned flags)
{
	cairo_space_t *space;

	space = cairo_glx_space_create(libgl, 0, 1, 0, flags);
	if(space)
	{
		if(space->is_software)
			cairo_space_destroy(space);
		else
			return space;
	}

	return 0;
}

cairo_public cairo_space_t *
cairo_gl_space_create(void* libgl, unsigned flags)
{
	cairo_space_t *space;

	space = cairo_glx_space_create(libgl, 0, 1, 0, flags);
	if(space)
		return space;

	space = cairo_osmesa_space_create(libgl, 0, flags);
	if(space)
		return space;

	return 0;
}

static inline cairo_gpu_space_tls_t*
_cairo_gpu_space_alloc_tls(cairo_gpu_space_t* space)
{
	return (cairo_gpu_space_tls_t*)calloc(sizeof(cairo_gpu_space_tls_t), 1);
}

static cairo_font_options_t *
_cairo_gpu_get_font_options (cairo_gpu_space_t* space)
{
    if (space->has_font_options)
	return &space->font_options;

    CAIRO_MUTEX_LOCK(space->mutex);
    if (! space->has_font_options) {
	_cairo_font_options_init_default (&space->font_options);

	space->font_options.antialias = CAIRO_ANTIALIAS_SUBPIXEL;

	API_SPACE(init_font_options)(space);

	space->has_font_options = TRUE;
    }
    CAIRO_MUTEX_UNLOCK(space->mutex);

    return &space->font_options;
}

static void
_cairo_gpu_set_surface_subspace(cairo_gpu_surface_t* surface)
{
	cairo_gpu_space_t* space = surface->space;
	API_SPACE(set_surface_subspace)(space, surface);
}

static cairo_gpu_context_t *
_cairo_gpu_space_tls__create_context(cairo_gpu_space_tls_t* tls, cairo_gpu_subspace_t* sub)
{
	cairo_gpu_space_t* space = tls->space;
	cairo_gpu_context_t* ctx;

	if(!sub)
	{
		// use the last subspace so that we hopefully minimize the number of created context
		sub = &space->subspace;
		while(sub->next)
			sub = sub->next;
	}

	ctx = API_SPACE(create_context)(space, sub);

	ctx->tls = tls;
	ctx->next = tls->contexts;
	tls->contexts = ctx;

	pthread_setspecific(sub->context_tls, ctx);
	tls->last_context = ctx;
	return ctx;
}

static inline void
_cairo_gpu_space_tls_destroy_contexts(cairo_gpu_space_tls_t* tls)
{
	cairo_gpu_context_t* ctx;
	cairo_gpu_context_t* ctx_next;
	for(ctx = tls->contexts; ctx; ctx = ctx_next)
	{
		ctx_next = ctx->next;

		_cairo_gpu_context__destroy(ctx);
	}
}

static inline cairo_gpu_context_t *
_cairo_gpu_space_tls_lookup_context(cairo_gpu_space_tls_t* tls)
{
	cairo_gpu_context_t* ctx = 0;

	ctx = tls->last_context;

	if(!ctx)
		ctx = _cairo_gpu_space_tls__create_context(tls, 0);
	return ctx;
}

static void
_cairo_gpu_space__destroy(cairo_gpu_space_t * space)
{
	cairo_gpu_subspace_t * sub;
	cairo_gpu_subspace_t * sub_next;
	cairo_gpu_space_tls_t* tls;
	cairo_gpu_space_tls_t* tls_next;

	pthread_key_delete(space->tls);

	{
		cairo_gpu_context_t* ctx = _cairo_gpu_space_bind(space);
		_cairo_gpu_space__fini(ctx);
	}

	pthread_key_delete(space->subspace.context_tls);

	for(tls = (cairo_gpu_space_tls_t*)space->tls_list.next; (list_node_t*)tls != &space->tls_list; tls = tls_next)
	{
		tls_next = (cairo_gpu_space_tls_t*)tls->node.next;

		_cairo_gpu_space_tls_destroy(tls);
	}

	for(sub = space->subspace.next; sub; sub = sub_next)
	{
		sub_next = sub->next;

		pthread_key_delete(sub->context_tls);

		API_SPACE(destroy_subspace)(space, sub);

		free(sub);
	}

	space->destroy_on_unbind = 1;
}

static void
_cairo_gpu_space_destroy(void* abstract_space)
{
	cairo_gpu_space_t * space = abstract_space;

	_cairo_gpu_enter();
	_cairo_gpu_space__destroy(space);
	_cairo_gpu_exit();
}

/* only current thread */
static cairo_status_t
_cairo_gpu_space_sync(void* abstract_space)
{
	cairo_gpu_space_t* space = abstract_space;
	cairo_gpu_space_tls_t* tls = (cairo_gpu_space_tls_t*)_cairo_gpu_space_get_tls(space);
	cairo_gpu_context_t* ctx;

	_cairo_gpu_enter();
	for(ctx = tls->contexts; ctx; ctx = ctx->next)
	{
		_cairo_gpu_context_bind(ctx);
		ctx->gl.Finish();
	}
	_cairo_gpu_exit();
	return CAIRO_STATUS_SUCCESS;
}

static int
_cairo_gpu_space__init(cairo_gpu_context_t *ctx, unsigned flags)
{
	cairo_gpu_space_t* space = ctx->space;
	char *vendor;
	char *renderer;
	char* extensions;
	unsigned dummy_tex_data = 0;

	// TODO: this assumes that GL extensions don't change between context
	extensions = (char *)ctx->gl.GetString(GL_EXTENSIONS);
	vendor = (char *)ctx->gl.GetString(GL_VENDOR);
 	renderer = (char *)ctx->gl.GetString(GL_RENDERER);

 	// TODO: any better solutions?
 	space->base.is_software = strstr(renderer, "software") || strstr(renderer, "Software");

 	// these can be implemented on any hardware
 	if(!strstr(extensions, "EXT_texture_object") || !strstr(extensions, "EXT_vertex_array"))
	{
		fprintf(stderr, "ERROR: the current OpenGL driver is severely deficient: it is missing EXT_texture_object or EXT_vertex_array!\n");
		goto fail;
	}

 	space->tex_units = 1;
 	if(!!strstr(extensions, "ARB_multitexture"))
 	{
 		int tex_units;
 		int limit;
 		ctx->gl.GetIntegerv(GL_MAX_TEXTURE_UNITS_ARB, &tex_units);

 		// 4 is enough for us
 		limit = 4 - ((flags >> CAIRO_GPU_GL_FORCE_TEX_UNITS_SHIFT) & 3);

 		if(tex_units > limit)
 			tex_units = limit;
 		space->tex_units = tex_units;
 	}

	space->use_fbo = !(flags & CAIRO_GPU_GL_DISABLE_FRAMEBUFFER_OBJECT) && strstr(extensions, "EXT_framebuffer_object");

	if(!space->use_fbo)
	{
		if(!API_SPACE(has_offscreen_drawables)(space))
			goto fail;
	}

	space->fb_blit = !(flags & CAIRO_GPU_GL_DISABLE_FRAMEBUFFER_BLIT) && strstr(extensions, "EXT_framebuffer_blit");

	ctx->space->use_vbo = !!strstr(extensions, "EXT_vertex_buffer_object");

	if(!(flags & CAIRO_GPU_GL_DISABLE_TEXTURE_NON_POWER_OF_TWO) &&
			strstr(extensions, "ARB_texture_non_power_of_two"))
		space->tex_npot = 1;

	if(!(flags & CAIRO_GPU_GL_DISABLE_TEXTURE_RECTANGLE)
			&& (strstr(extensions, "ARB_texture_rectangle") || strstr(extensions, "EXT_texture_rectangle") || strstr(extensions, "NV_texture_rectangle")))
		space->tex_rectangle = 1;

	space->extend_mask = 1 << CAIRO_EXTEND_REPEAT;

	/* These are pretty much guaranteed */
	if(strstr(extensions, "ARB_texture_border_clamp") || strstr(extensions, "SGIS_texture_border_clamp"))
		space->extend_mask |= 1 << CAIRO_EXTEND_NONE;

	if(strstr(extensions, "EXT_texture_edge_clamp") || strstr(extensions, "SGIS_texture_edge_clamp"))
		space->extend_mask |= 1 << CAIRO_EXTEND_PAD;

	if(strstr(extensions, "ARB_texture_mirrored_repeat"))
		space->extend_mask |= 1 << CAIRO_EXTEND_REFLECT;

	space->use_vert_prog = !(flags & CAIRO_GPU_GL_DISABLE_VERTEX_PROGRAM) && strstr(extensions, "ARB_vertex_program");

	space->vert_op = space->vert_passthru = space->use_vert_prog;

	space->use_frag_prog = !(flags & CAIRO_GPU_GL_DISABLE_FRAGMENT_PROGRAM) && strstr(extensions, "ARB_fragment_program");

 	space->has_combine = !(flags & CAIRO_GPU_GL_DISABLE_TEX_ENV_COMBINE) && !!strstr(extensions, "ARB_texture_env_combine");

 	space->use_intensity = !space->use_frag_prog && !space->has_combine;

 	// we do this because we need GL_INTENSITY textures for GL_MODULATE, that wouldn't be renderable and in addition hardware/drivers without ARB_texture_env_combine are less likely to support FBOs
 	// TODO: don't do this, but add new SURF_ALPHA / SURF_INTENSITY texture "aliases"
 	if(!space->use_frag_prog && !space->has_combine)
 		space->use_fbo = 0;

	space->crossbar = strstr(extensions, "ARB_texture_env_crossbar") || strstr(extensions, "NV_texture_env_combine4");

	space->per_component = (space->use_frag_prog || (space->has_combine && space->crossbar && strstr(extensions, "ARB_texture_env_dot3")));
	space->radial = space->frag_div_alpha = space->discontinuous = space->use_frag_prog;
	space->tex_aaaa_111a = space->frag_mul_alpha = space->use_frag_prog || space->has_combine;

	space->frag_passthru = space->use_frag_prog || strstr(extensions, "NV_texture_shader");

	space->blend_func_separate = !(flags & CAIRO_GPU_GL_DISABLE_BLEND_FUNC_SEPARATE) && strstr(extensions, "EXT_blend_func_separate");
	space->blend_color = !!strstr(extensions, "EXT_blend_color");
	space->blend_subtract = !!strstr(extensions, "EXT_blend_subtract"),
	space->has_window_pos = !!strstr(extensions, "ARB_window_pos");
	space->has_framebuffer_multisample = !!strstr(extensions, "EXT_framebuffer_multisample");
	space->has_framebuffer_multisample_coverage = !!strstr(extensions, "NV_framebuffer_multisample_coverage");
	space->has_fragment_program2 = !!strstr(extensions, "NV_fragment_program2");
	space->has_gpu_program4 = !!strstr(extensions, "NV_gpu_program4");

	space->msaa_samples = 16; /* optimistic value, will be downgraded if necessary */

	// TODO: this is true on nVidia G70, which does the equivalent of 2x2 MSAA
	space->fastest_polygon_smooth_samples = 4;
	space->nicest_polygon_smooth_samples = 4;

	space->max_anisotropy = 1.0;
	if(strstr(extensions, "EXT_texture_filter_anisotropic"))
		ctx->gl.GetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &space->max_anisotropy);

	/* Set up the dummy texture for tex_env_combine with constant color. */
	_cairo_gpu_texture_create(ctx, &space->dummy_texture, 1, 1);
	_cairo_gl_context_set_active_texture(ctx, _cairo_gl_context_set_texture(ctx, -1, &space->dummy_texture));
	ctx->gl.TexImage2D(_cairo_gl_context_active_target(ctx), 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, &dummy_tex_data);
	return 0;

fail:
	return -1;
}

static inline cairo_bool_t
_cairo_gpu_space_is_frag_supported(cairo_gpu_space_t* space, unsigned frag)
{
	if(space->use_frag_prog)
		return 1;
	else
	{
		unsigned tex0 = (frag >> (FRAG_TEX_SHIFT)) & FRAG_TEX_MASK;
		unsigned tex1 = (frag >> (FRAG_TEX_SHIFT + FRAG_TEX_BITS)) & FRAG_TEX_MASK;
		unsigned tex01 = tex0 | tex1;
		unsigned op = frag & FRAG_OP_MASK;

		if(
			(op && op != (OP_MUL_ALPHA << FRAG_OP_SHIFT))
			|| (tex01 & (FRAG_TEX_RADIAL | FRAG_TEX_DISCONTINUOUS))
			|| (tex01 & FRAG_TEX_COLOR_MASK) == FRAG_TEX_COLOR_111CA
			|| (tex1 & FRAG_TEX_COLOR_MASK) == FRAG_TEX_COLOR_111C
			|| (!space->tex_aaaa_111a && (
					((tex01 & FRAG_TEX_COLOR_MASK) == FRAG_TEX_COLOR_AAAA)
					|| ((tex01 & FRAG_TEX_COLOR_MASK) == FRAG_TEX_COLOR_111A) ))
					)
			return 0;

		// 4 is enough for everyone
		if(space->tex_units < 4)
		{
			int n = -1; // first unit has two slots
			if(tex1 && space->tex_units < 2)
				return 0;

			if(tex0)
			{
				++n;

				if((tex0 & FRAG_TEX_COLOR_MASK) == FRAG_TEX_COLOR_111C)
					n += 2;
				else
				{
					if((frag & (FRAG_OP_MASK | FRAG_OPPOS_TEX1)) == (OP_MUL_ALPHA << FRAG_OP_SHIFT))
						++n;
				}
			}

			// slot #1 is unused
			if(!space->crossbar && (frag & FRAG_OPPOS_TEX1))
				++n;

			if((frag & (FRAG_OP_MASK | FRAG_OPPOS_TEX1)) == ((OP_MUL_ALPHA << FRAG_OP_SHIFT) | FRAG_OPPOS_TEX1))
				++n;

			if(tex1)
				++n;

			if(frag & (FRAG_CONSTANT | FRAG_PRIMARY))
				++n;

			if(n > space->tex_units)
				return 0;
		}
	}
	return 1;
}

static cairo_space_t*
_cairo_gpu_space__finish_create(cairo_gpu_space_t* space, unsigned flags)
{
	cairo_gpu_context_t* ctx;
	int init;
	const char* env = getenv("__CAIRO_GPU_GL_FLAGS");
	if(env)
		flags |= atoi(env);

	_cairo_gpu_enter();

	// this calls _cairo_gpu_space__init
	ctx = _cairo_gpu_space_bind(space);
	init = _cairo_gpu_space__init(ctx, flags);

	if(init)
	{
		_cairo_gpu_space__destroy(space);
		space = 0;
	}
	_cairo_gpu_exit();
	return (cairo_space_t*)space;
}

cairo_space_t *
cairo_gl_space_create_from_current_context(void* libgl, unsigned flags)
{
	cairo_space_t* space;

	space = cairo_glx_space_create_from_current_context(libgl, flags);
	if(space)
		return space;

	space = cairo_osmesa_space_create_from_current_context(libgl, flags);
	if(space)
		return space;

	return 0;
}

cairo_public cairo_gl_api_t
cairo_gl_space_get_api(cairo_space_t * abstract_space)
{
	cairo_gpu_space_t *space = (cairo_gpu_space_t*)abstract_space;

	if(abstract_space->backend != &_cairo_gpu_space_backend)
		return CAIRO_GL_API_NONE;

	return space->api;
}

cairo_public void*
cairo_gl_space_get_libgl(cairo_space_t * abstract_space)
{
	cairo_gpu_space_t *space = (cairo_gpu_space_t*)abstract_space;

	if(abstract_space->backend != &_cairo_gpu_space_backend)
		return 0;

	return space->libgl;
}

void*
cairo_gl_space_get_proc_address(cairo_space_t * abstract_space, const char* name)
{
	cairo_gpu_space_t *space = (cairo_gpu_space_t*)abstract_space;

	if(abstract_space->backend != &_cairo_gpu_space_backend)
		return 0;

	return space->GetProcAddress(name);
}

void*
cairo_gl_space_create_context(cairo_space_t * abstract_space, cairo_surface_t* surface)
{
	cairo_gpu_space_t *space = (cairo_gpu_space_t*)abstract_space;

	if(abstract_space->backend != &_cairo_gpu_space_backend)
		return 0;

	if(surface && surface->backend != &_cairo_gpu_surface_backend)
		return 0;

	return API_SPACE(create_gl_context)(space, surface);
}

void
cairo_gl_space_destroy_context(cairo_space_t * abstract_space, void* context)
{
	cairo_gpu_space_t *space = (cairo_gpu_space_t*)abstract_space;

	if(abstract_space->backend != &_cairo_gpu_space_backend)
		return;

	API_SPACE(destroy_gl_context)(space, context);
}

cairo_bool_t
cairo_gl_space_make_current(cairo_space_t* abstract_space, void* context, cairo_surface_t* abstract_draw_surface, cairo_surface_t* abstract_read_surface)
{
	cairo_gpu_space_t* space = (cairo_gpu_space_t*)abstract_space;
	cairo_gpu_surface_t* read_surface = (cairo_gpu_surface_t*)abstract_read_surface;
	cairo_gpu_surface_t* draw_surface = (cairo_gpu_surface_t*)abstract_draw_surface;
	int api = 0;

	if(read_surface)
	{
		if(read_surface->base.backend != &_cairo_gpu_surface_backend)
			return 0;
		api = read_surface->space->api;
	}

	if(draw_surface)
	{
		if(draw_surface->base.backend != &_cairo_gpu_surface_backend)
			return 0;
		if(api && api != draw_surface->space->api)
			return 0;

		api = draw_surface->space->api;
	}

	if(!api)
	{
		if(!space)
			return 0;
		if(space->base.backend != &_cairo_gpu_space_backend)
			return 0;
		api = space->api;
	}

	return API_SPACE(make_gl_context_current)(space, context, draw_surface, read_surface);
}
