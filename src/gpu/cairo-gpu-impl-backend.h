static const cairo_surface_backend_t _cairo_gpu_surface_backend =
{
	CAIRO_SURFACE_TYPE_GPU,
	_cairo_gpu_surface_create_similar,
	_cairo_gpu_surface_finish,
	_cairo_gpu_surface_acquire_source_image,
	_cairo_gpu_surface_release_source_image,
	_cairo_gpu_surface_acquire_dest_image,
	_cairo_gpu_surface_release_dest_image,
	_cairo_gpu_surface_clone_similar,
	_cairo_gpu_surface_composite,
	_cairo_gpu_surface_fill_rectangles,
	_cairo_gpu_surface_composite_trapezoids,
	_cairo_gpu_surface_create_span_renderer,
	_cairo_gpu_surface_check_span_renderer,
	_cairo_gpu_surface_copy_page,
	_cairo_gpu_surface_show_page,
#ifndef DEBUG_DISABLE_CLIP
	_cairo_gpu_surface_set_clip_region,
#else
	NULL,
#endif
	NULL,	/* intersect_clip_path */
	_cairo_gpu_surface_get_extents,
#ifndef DEBUG_DISABLE_TEXT
	_cairo_gpu_surface_old_show_glyphs,	/* old_show_glyphs */
#else
	NULL,
#endif
	_cairo_gpu_surface_get_font_options,
	_cairo_gpu_surface_flush,
	_cairo_gpu_surface_mark_dirty_rectangle,
	_cairo_gpu_surface_scaled_font_fini,
	_cairo_gpu_surface_scaled_glyph_fini,
	NULL,	/* paint */
	NULL,	/* mask */
	NULL,	/* stroke */
	NULL,	/* fill */
	NULL,	/* show_glyphs */
	NULL,/* snapshot */
	_cairo_gpu_surface_is_similar,
	NULL, /* reset */
	NULL, /* fill_stroke */
	NULL, /* create_solid_pattern_surface */
	NULL, /* can_repaint_solid_pattern_surface */
	NULL, /* has_show_text_glyphs */
	NULL, /* show_text_glyphs */
	_cairo_gpu_surface_set_samples,
	_cairo_gpu_surface_enable_multisampling,
};

static const cairo_space_backend_t _cairo_gpu_space_backend =
{
	_cairo_gpu_space_destroy,
	_cairo_gpu_surface_create,
	_cairo_gpu_space_sync
};
