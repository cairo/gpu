/* cairo - a vector graphics library with display and print output
 *
 * Copyright © 2009 Luca Barbieri
 * Copyright © 2009 Eric Anholt
 * Copyright © 2009 Chris Wilson
 * Copyright © 2005 Red Hat, Inc
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation
 * (the "LGPL") or, at your option, under the terms of the Mozilla
 * Public License Version 1.1 (the "MPL"). If you do not alter this
 * notice, a recipient may use your version of this file under either
 * the MPL or the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-2.1; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * You should have received a copy of the MPL along with this library
 * in the file COPYING-MPL-1.1
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL or the MPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the cairo graphics library.
 *
 * The Initial Developer of the Original Code is Red Hat, Inc.
 *
 * Contributor(s):
 *	Carl Worth <cworth@cworth.org>
 */

#define _ISOC99_SOURCE
#include <alloca.h>
#include <float.h>

#include "cairoint.h"

#include "cairo-gpu-gl.h"

//#define DEBUG_DISABLE_SPANS
//#define DEBUG_FORCE_ONEPASS
//#define DEBUG_FORCE_TWOPASS
//#define DEBUG_FORCE_GPU_TRAPS_MASK
//#define DEBUG_DISABLE_GPU_TRAPS_MASK
//#define DEBUG_DISABLE_CLIP
//#define DEBUG_DISABLE_MSAA
//#define DEBUG_MASK_BOX
//#define DEBUG_FORCE_SOURCE
//#define DEBUG_FORCE_ADD
//#define DEBUG_DISABLE_TEXT
//#define DEBUG_DISABLE_GRADIENTS
//#define DEBUG_DISABLE_ANISOTROPY

#include "gpu/cairo-gpu-impl-defs.h"
#include "gpu/cairo-gpu-impl-defs-gl.h"
#include "gpu/cairo-gpu-impl-base.h"
#include "gpu/cairo-gpu-impl-geometry-gl.h"
#include "gpu/cairo-gpu-impl-context-gl.h"
#include "gpu/cairo-gpu-impl-space-gl.h"
#include "gpu/cairo-gpu-impl-surface-gl.h"
#include "gpu/cairo-gpu-impl-surface.h"
#include "gpu/cairo-gpu-impl-composite.h"
#include "gpu/cairo-gpu-impl-font.h"
#include "gpu/cairo-gpu-impl-draw.h"
#include "gpu/cairo-gpu-impl-backend.h"
