/* cairo - a vector graphics library with display and print output
 *
 * Copyright © 2009 Luca Barbieri
 * Copyright © 2009 Eric Anholt
 * Copyright © 2009 Chris Wilson
 * Copyright © 2005 Red Hat, Inc
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation
 * (the "LGPL") or, at your option, under the terms of the Mozilla
 * Public License Version 1.1 (the "MPL"). If you do not alter this
 * notice, a recipient may use your version of this file under either
 * the MPL or the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-2.1; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * You should have received a copy of the MPL along with this library
 * in the file COPYING-MPL-1.1
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL or the MPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the cairo graphics library.
 *
 * The Initial Developer of the Original Code is Red Hat, Inc.
 *
 * Contributor(s):
 *	Carl Worth <cworth@cworth.org>
 */

#include <float.h>
#include <X11/Xlib.h>
#include "cairoint.h"

#if CAIRO_HAS_GPU_GALLIUM_SURFACE
#include "cairo-gpu-gallium.h"
#endif

#if CAIRO_HAS_GPU_GL_SURFACE
#include "cairo-gpu-gl.h"
#endif

#if CAIRO_HAS_XLIB_SURFACE
#include "cairo-xlib.h"
#endif

#if CAIRO_HAS_XCB_SURFACE
#include "cairo-xcb.h"
#endif

cairo_public cairo_space_t *
cairo_xproto_space_create (const char* name)
{
	cairo_space_t* space;
#if CAIRO_HAS_XCB_SURFACE
	space = cairo_xcb_space_create(name);
	if(space)
		return space;
#endif

#if CAIRO_HAS_XLIB_SURFACE
	space = cairo_xlib_space_create(name);
	if(space)
		return space;
#endif
	return 0;
}

cairo_public cairo_space_t *
cairo_space_wrap_xlib(Display* dpy, int screen, int owns_dpy)
{
	cairo_space_t* space;
#if CAIRO_HAS_GPU_GALLIUM_SURFACE
	space = cairo_gallium_x11_space_wrap(dpy, screen, owns_dpy, 0);
	if(space)
	{
		if(space->is_software)
			cairo_space_destroy(space);
		else
			return space;
	}
#endif
#if CAIRO_HAS_GPU_GL_SURFACE
	space = cairo_glx_space_wrap(0, dpy, owns_dpy, 1, 0, 0);
	if(space)
	{
		if(space->is_software)
			cairo_space_destroy(space);
		else
			return space;
	}
#endif
#if CAIRO_HAS_XCB_SURFACE
	space = cairo_xcb_space_wrap_xlib(dpy, screen, owns_dpy);
	if(space)
		return space;
#endif
#if CAIRO_HAS_XLIB_SURFACE
	space = cairo_xlib_space_wrap(dpy, screen, owns_dpy);
	if(space)
		return space;
#endif
	return 0;
}

cairo_public cairo_space_t *
cairo_space_wrap_xcb(xcb_connection_t* conn, int screen, int owns_dpy)
{
	cairo_space_t* space;
#if CAIRO_HAS_XCB_SURFACE
	space = cairo_xcb_space_wrap(conn, screen, owns_dpy);
	if(space)
		return space;
#endif
	return 0;
}

cairo_public cairo_space_t *
cairo_space_create_for_display(const char* name)
{
	cairo_space_t* space;
	Display* dpy;
	dpy = XOpenDisplay(name);
	if(!dpy)
		return 0;

	space = cairo_space_wrap_xlib(dpy, DefaultScreen(dpy), 1);
	if(!space)
	{
		XCloseDisplay(dpy);
		return 0;
	}

	return space;
}

/* Try in order:
 * 1. Gallium, standalone
 * 2. Gallium via X11/WGL
 * 3. EGL (not implemented)
 * 4. GLX/WGL
 * 5. X11/XRender
 */

cairo_space_t *
cairo_space_create(void)
{
	cairo_space_t *space;

#if CAIRO_HAS_GPU_GALLIUM_SURFACE
	space = cairo_gallium_drm_space_create(0, 0);
	if(space)
	{
		if(space->is_software)
			cairo_space_destroy(space);
		else
			return space;
	}
#endif

	space = cairo_space_create_for_display(0);
	if(space)
		return space;

	return 0;
}
slim_hidden_def (cairo_space_create);

void
cairo_space_destroy(cairo_space_t * space)
{
	if(!space)
		return;

	if(!_cairo_reference_count_dec_and_test(&space->ref_count))
		return;

	space->backend->destroy(space);
}
slim_hidden_def (cairo_space_destroy);

cairo_space_t *
cairo_space_reference(cairo_space_t * space)
{
	if(space == NULL || CAIRO_REFERENCE_COUNT_IS_INVALID(&space->ref_count))
	{
		return space;
	}

	assert(CAIRO_REFERENCE_COUNT_HAS_REFERENCE(&space->ref_count));
	_cairo_reference_count_inc(&space->ref_count);

	return space;
}
slim_hidden_def (cairo_space_reference);

/* This may only synchronize drawing to the space done from the current thread */
cairo_status_t
cairo_space_sync(cairo_space_t * space)
{
	if(space && space->backend->sync)
		return space->backend->sync(space);

	return CAIRO_STATUS_SUCCESS;
}
slim_hidden_def (cairo_space_sync);

cairo_bool_t
cairo_space_is_software(cairo_space_t* space)
{
	return !space || space->is_software;
}
