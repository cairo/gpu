/* Cairo - a vector graphics library with display and print output
 *
 * Copyright © 2009 Eric Anholt
 * Copyright © 2009 Chris Wilson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation
 * (the "LGPL") or, at your option, under the terms of the Mozilla
 * Public License Version 1.1 (the "MPL"). If you do not alter this
 * notice, a recipient may use your version of this file under either
 * the MPL or the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-2.1; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * You should have received a copy of the MPL along with this library
 * in the file COPYING-MPL-1.1
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL or the MPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the cairo graphics library.
 *
 * The Initial Developer of the Original Code is Eric Anholt.
 */

#ifndef CAIRO_GPU_GL_H
#define CAIRO_GPU_GL_H

#include "cairo.h"

#if CAIRO_HAS_GPU_GL_SURFACE

CAIRO_BEGIN_DECLS

typedef enum
{
	CAIRO_GL_API_NONE = 0,
	CAIRO_GL_API_OSMESA = 1,
	CAIRO_GL_API_EGL = 2,
	CAIRO_GL_API_GLX = 3,
	CAIRO_GL_API_AGL = 4,
	CAIRO_GL_API_WGL = 5
} cairo_gl_api_t;

struct __GLXcontextRec;
typedef struct __GLXcontextRec *GLXContext;
typedef XID GLXDrawable;

struct osmesa_context;
typedef struct osmesa_context *OSMesaContext;

#define CAIRO_GPU_GL_DISABLE_FRAMEBUFFER_OBJECT 1
#define CAIRO_GPU_GL_DISABLE_FRAMEBUFFER_BLIT 2
#define CAIRO_GPU_GL_DISABLE_TEXTURE_NON_POWER_OF_TWO 4
#define CAIRO_GPU_GL_DISABLE_TEXTURE_RECTANGLE 8
#define CAIRO_GPU_GL_DISABLE_FRAGMENT_PROGRAM 0x10
#define CAIRO_GPU_GL_DISABLE_VERTEX_PROGRAM 0x20
#define CAIRO_GPU_GL_DISABLE_BLEND_FUNC_SEPARATE 0x40
#define CAIRO_GPU_GL_DISABLE_TEX_ENV_COMBINE 0x80
#define CAIRO_GPU_GL_FORCE_TEX_UNITS_SHIFT 8
#define CAIRO_GPU_GL_FORCE_TEX_UNITS_MASK 0x300

#define CAIRO_GPU_GL_DISABLE_EVERYTHING 0x1ff


cairo_public cairo_space_t *
cairo_glx_space_create (void* libgl, const char* name, int direct, GLXContext share_context, unsigned flags);

cairo_public cairo_space_t *
cairo_glx_space_wrap (void* libgl, Display* dpy, int owns_dpy, int direct, GLXContext share_context, unsigned flags);

cairo_public void
cairo_glx_space_use_waitgl(cairo_space_t* space, int value);

cairo_public void
cairo_glx_space_use_waitx(cairo_space_t* space, int value);

cairo_public cairo_space_t *
cairo_osmesa_space_create (void* libgl, OSMesaContext share_context, unsigned flags);

cairo_public cairo_space_t *
cairo_gl_hardware_space_create(void* libgl, unsigned flags);

cairo_public cairo_space_t *
cairo_gl_space_create (void* libgl, unsigned flags);

cairo_public cairo_space_t *
cairo_gl_space_create_from_current_context (void* libgl, unsigned flags);

cairo_public cairo_space_t *
cairo_glx_space_create_from_current_context(void* libgl, unsigned flags);

cairo_public cairo_space_t *
cairo_osmesa_space_create_from_current_context(void* libgl, unsigned flags);

/* These functions will return multisampled or non-multisampled objects depending on whether multisampling is enabled.
    If you enable/disable multisampling (except only at the beginning), you MUST use cairo_surface_flush and cairo_surface_mark_dirty around GL drawing.
    You must call flush and surface_mark_dirty using the multisample enable mode that you will use for OpenGL drawing.
    The reason of this is that the backend internally copies data lazily between multisampled and non-multisampled objects.
    flush and surface_mark_dirty are very cheap if there is no work to do.

    EXAMPLE:
    gl_create_surface
    set_msaa
    enable_msaa(1)
    draw
    enable_msaa(0)
    flush
    get_texture
    read with OpenGL
    get_framebuffer
    write with OpenGL, no multisampling
    mark_dirty_rectangle
    enable_msaa(1)
    maybe draw
    flush // even if you haven't drawn anything!
    get_framebuffer
    write with OpenGL, with multisampling
    mark_dirty_rectangle
 */

cairo_public cairo_gl_api_t
cairo_gl_space_get_api(cairo_space_t * space);

cairo_public void*
cairo_gl_space_get_libgl(cairo_space_t * space);

/* void* is GLXContext, OSMesaContext, EGLContext, ... */
cairo_public void*
cairo_gl_space_create_context(cairo_space_t * space, cairo_surface_t* surface);

cairo_public cairo_bool_t
cairo_gl_space_make_current(cairo_space_t * space, void* context, cairo_surface_t* draw_surface, cairo_surface_t* read_surface);

/* you must bind a created context before calling this! */
cairo_public void*
cairo_gl_space_get_proc_address(cairo_space_t * space, const char* name);

cairo_public void
cairo_gl_space_destroy_context(cairo_space_t * space, void* context);

cairo_public unsigned
cairo_gl_surface_get_texture(cairo_surface_t * surface);

cairo_public unsigned
cairo_gl_surface_get_framebuffer(cairo_surface_t * surface);

cairo_public unsigned
cairo_gl_surface_get_renderbuffer(cairo_surface_t * surface);

cairo_public unsigned long
cairo_gl_surface_get_drawable(cairo_surface_t * surface);

cairo_public unsigned long
cairo_gl_surface_get_gl_drawable(cairo_surface_t * surface);

CAIRO_END_DECLS

#else  /* CAIRO_HAS_GL_SURFACE */
# error Cairo was not compiled with support for the GPU OpenGL backend
#endif /* CAIRO_HAS_GL_SURFACE */

#endif /* CAIRO_GPU_H */

