/*
 * Copyright � 2004 David Reveman, Peter Nilsson
 *
 * Permission to use, copy, modify, distribute, and sell this software
 * and its documentation for any purpose is hereby granted without
 * fee, provided that the above copyright notice appear in all copies
 * and that both that copyright notice and this permission notice
 * appear in supporting documentation, and that the names of
 * David Reveman and Peter Nilsson not be used in advertising or
 * publicity pertaining to distribution of the software without
 * specific, written prior permission. David Reveman and Peter Nilsson
 * makes no representations about the suitability of this software for
 * any purpose. It is provided "as is" without express or implied warranty.
 *
 * DAVID REVEMAN AND PETER NILSSON DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL DAVID REVEMAN AND
 * PETER NILSSON BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 *
 * Authors: David Reveman <c99drn@cs.umu.se>
 *          Peter Nilsson <c99pnn@cs.umu.se>
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cairo.h>

void
radial_setup (cairo_surface_t *surface, int w, int h)
{
}

void
radial_render (cairo_t *cr, int w, int h)
{
	cairo_pattern_t* pattern;
	cairo_matrix_t matrix;
	
	cairo_set_operator (cr, CAIRO_OPERATOR_OVER);
	pattern = cairo_pattern_create_radial(32, 32, 0, 0, 0, w / 2);
	cairo_pattern_add_color_stop_rgba(pattern, 1.0 / 6.0, 1, 0, 0, 1);
	cairo_pattern_add_color_stop_rgba(pattern, 3.0 / 6.0, 1, 1, 0, 1);
	cairo_pattern_add_color_stop_rgba(pattern, 3.0 / 6.0, 0, 1, 1, 1);
	cairo_pattern_add_color_stop_rgba(pattern, 5.0 / 6.0, 0, 0, 1, 1);
	
	cairo_set_source(cr, pattern);
	
	cairo_pattern_set_extend(pattern, CAIRO_EXTEND_REPEAT);
	cairo_rectangle(cr, 0, 0, w, h);
	cairo_fill(cr);

	cairo_pattern_destroy(pattern);
}

