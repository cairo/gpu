/*
 * Copyright � 2004 David Reveman, Peter Nilsson
 *
 * Permission to use, copy, modify, distribute, and sell this software
 * and its documentation for any purpose is hereby granted without
 * fee, provided that the above copyright notice appear in all copies
 * and that both that copyright notice and this permission notice
 * appear in supporting documentation, and that the names of
 * David Reveman and Peter Nilsson not be used in advertising or
 * publicity pertaining to distribution of the software without
 * specific, written prior permission. David Reveman and Peter Nilsson
 * makes no representations about the suitability of this software for
 * any purpose. It is provided "as is" without express or implied warranty.
 *
 * DAVID REVEMAN AND PETER NILSSON DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL DAVID REVEMAN AND
 * PETER NILSSON BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 *
 * Authors: David Reveman <c99drn@cs.umu.se>
 *          Peter Nilsson <c99pnn@cs.umu.se>
 */

#include <cairo.h>
#include <cairo-xlib.h>
#include <cairo-gpu-gl.h>
#include <cairo-gpu-gallium.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <GL/glx.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <math.h>
#include <strings.h>

#define PACKAGE "cairogears"

#define WINDOW_WIDTH  512
#define WINDOW_HEIGHT 512

void trap_setup (cairo_surface_t *target, int w, int h);
void trap_render (cairo_t *cr, int w, int h);

void comp_setup (cairo_surface_t *target, int w, int h);
void comp_render (cairo_t *cr, int w, int h);

void text_setup (cairo_surface_t *target, int w, int h);
void text_render (cairo_t *cr, int w, int h);

void text2_setup (cairo_surface_t *target, int w, int h);
void text2_render (cairo_t *cr, int w, int h);
extern cairo_operator_t text2_operator;

void text3_setup (cairo_surface_t *target, int w, int h);
void text3_render (cairo_t *cr, int w, int h);

void shadow_setup (cairo_surface_t *target, int w, int h);
void shadow_render (cairo_t *cr, int w, int h);

void grads_setup (cairo_surface_t *target, int w, int h);
void grads_render (cairo_t *cr, int w, int h);

void radial_setup (cairo_surface_t *target, int w, int h);
void radial_render (cairo_t *cr, int w, int h);

static void (*setup) (cairo_surface_t *surface, int w, int h);
static void (*render) (cairo_t *cr, int w, int h);

static const char *backend_names[] = {"image", "x11", "osmesa", "glx", "gallium-softpipe", "gallium-hardpipe"};
enum
{
	BACKEND_IMAGE = 0,
	BACKEND_X11 = 1,
	BACKEND_OSMESA = 2,
	BACKEND_GLX = 3,
	BACKEND_GALLIUM_SOFTPIPE = 4,
	BACKEND_GALLIUM_HARDPIPE = 5
} backend;

static const char *test;

int fast = 0;
int fill_gradient = 0;
int texture_gears = 0;
int clip_rects = 0;

extern unsigned int glyph_cnt;
static unsigned int frame_cnt;
static int show_gps;

static unsigned dst_alpha;

static void
usage (const char *program_name)
{
    printf ("Usage: %s [-invisible | -direct] [-image | -x11 | -osmesa | -glx | -gallium-softpipe | -gallium-hardpipe] TEST\n\n"
	    "\tThe following tests are available:\n\n"
	    "\tTRAP   Trapezoid fill test\n"
	    "\tTEX    Trapezoid texture test\n"
	    "\tCLIP   Trapezoid texture test with clipping\n"
	    "\tGRAD   Trapezoid gradient test\n"
	    "\tCOMP   Composite and transform test\n"
	    "\tTEXT   Text path test\n"
	    "\tTEXT2  Text bitmap test\n"
	    "\tTEXT2SAT  Text bitmap test with saturate\n"
            "\tTEXT3  Fancy text to test multipass component alpha\n"
	    "\tSHADOW Composite with mask and convolution filter test\n"
	    "\tGRADS  Several gradients\n"
	    "\tRADIAL  Radial gradients\n\n",
	program_name);
}

static void
alarmhandler (int sig)
{
    if (sig == SIGALRM) {
	if (show_gps) {
	    printf ("%s - %s: %d glyphs in 5.0 seconds = %.3f GPS\n",
		    backend_names[backend], test,
		    glyph_cnt, glyph_cnt / 5.0);
	    glyph_cnt = 0;
	}
	printf ("%s - %s: %d frames in 5.0 seconds = %.3f FPS\n",
		backend_names[backend], test,
		frame_cnt, frame_cnt / 5.0);
	frame_cnt = 0;
    }
    signal (SIGALRM, alarmhandler);
    alarm(5);
}

int
main (int argc, char **argv)
{
    int invisible = 0;

    int direct = 0;

    Display *dpy;
    Window win;
    XVisualInfo* vi = 0;
    XWMHints xwmh = {
	(InputHint|StateHint),
	False,
	NormalState,
	0,
	0,
	0, 0,
	0,
	0,
    };
    XEvent event;
    XSizeHints xsh;
    XSetWindowAttributes xswa;
    unsigned int width, height;
    int aa = 4;
    cairo_surface_t *win_surface = 0;
    cairo_surface_t *surface = 0;
    cairo_space_t *win_space = 0;
    cairo_space_t *space = 0;
    cairo_t *cr;
    int i;

    for (i = 1; i < argc; i++) {
	if (!strcasecmp ("-direct", argv[i])) {
	    direct = 1;
	} else if (!strcasecmp ("-image", argv[i])) {
	    backend = BACKEND_IMAGE;
	} else if (!strcasecmp ("-x11", argv[i])) {
	    backend = BACKEND_X11;
	} else if (!strcasecmp ("-glx", argv[i])) {
	    backend = BACKEND_GLX;
	} else if (!strcasecmp ("-osmesa", argv[i])) {
	    backend = BACKEND_OSMESA;
	} else if (!strcasecmp ("-gallium-softpipe", argv[i])) {
	    backend = BACKEND_GALLIUM_SOFTPIPE;
	} else if (!strcasecmp ("-gallium-hardpipe", argv[i])) {
	    backend = BACKEND_GALLIUM_HARDPIPE;
	} else if (!strcasecmp ("TRAP", argv[i])) {
	    setup = trap_setup;
	    render = trap_render;
	    test = "trapezoids";
	} else if (!strcasecmp ("GRAD", argv[i])) {
	    setup = trap_setup;
	    render = trap_render;
	    fill_gradient = 1;
	    test = "gradient";
	} else if (!strcasecmp ("TEX", argv[i])) {
	    setup = trap_setup;
	    render = trap_render;
	    texture_gears = 1;
	    test = "texture";
	} else if (!strcasecmp ("CLIP", argv[i])) {
	    setup = trap_setup;
	    render = trap_render;
	    texture_gears = 1;
	    clip_rects = 1;
	    test = "texture";
	} else if (!strcasecmp ("COMP", argv[i])) {
	    setup = comp_setup;
	    render = comp_render;
	    test = "composite";
	} else if (!strcasecmp ("TEXT", argv[i])) {
	    setup = text_setup;
	    render = text_render;
	    test = "text";
	} else if (!strcasecmp ("TEXT2", argv[i])) {
	    setup = text2_setup;
	    render = text2_render;
	    show_gps = 1;
	    test = "text2";
	} else if (!strcasecmp ("TEXT2SAT", argv[i])) {
	    setup = text2_setup;
	    render = text2_render;
	    show_gps = 1;
	    text2_operator = CAIRO_OPERATOR_SATURATE;
	    dst_alpha = 1;
	    test = "text2sat";
	} else if (!strcasecmp ("TEXT3", argv[i])) {
	    setup = text3_setup;
	    render = text3_render;
	    test = "text3";
	} else if (!strcasecmp ("SHADOW", argv[i])) {
	    setup = shadow_setup;
	    render = shadow_render;
	    test = "shadow";
	} else if (!strcasecmp ("GRADS", argv[i])) {
	    setup = grads_setup;
	    render = grads_render;
	    test = "gradients";
	} else if (!strcasecmp ("RADIAL", argv[i])) {
	    setup = radial_setup;
	    render = radial_render;
	    test = "radial";
	} else if (!strcasecmp ("-invisible", argv[i])) {
	    invisible = 1;
	} else {
	    fprintf (stderr, "%s: unrecognized option `%s'\n",
		    argv[0], argv[i]);
	    usage (argv[0]);
	    exit (1);
	}
    }

    if (!render) {
	usage (argv[0]);
	exit (1);
    }

    width = WINDOW_WIDTH;
    height = WINDOW_HEIGHT;

    if ((dpy = XOpenDisplay (NULL)) == NULL) {
	fprintf(stderr, "%s: can't open display: %s\n", argv[0],
		XDisplayName (NULL));
	exit(1);
    }

    xsh.flags = PSize;
    xsh.width = width;
    xsh.height = height;
    xsh.x = 0;
    xsh.y = 0;

    if(backend == BACKEND_GLX)
    {
        int attribs[] = { GLX_RGBA,
		      GLX_RED_SIZE, 1,
		      GLX_GREEN_SIZE, 1,
		      GLX_BLUE_SIZE, 1,
		      GLX_DOUBLEBUFFER,
//		      GLX_DEPTH_SIZE, 1,
		      None };

    vi = glXChooseVisual (dpy, DefaultScreen (dpy), attribs);
    if (vi == NULL) {
	fprintf(stderr, "Failed to create RGB, double-buffered visual\n");
	exit(1);
    }
    }
    else
    {
    	XVisualInfo template;
    	int found;
    	template.screen = XDefaultScreen(dpy);
	template.visualid = XVisualIDFromVisual(DefaultVisualOfScreen(DefaultScreenOfDisplay(dpy)));

	vi = XGetVisualInfo(dpy, VisualIDMask | VisualScreenMask, &template, &found);
    }

    memset(&xswa, 0, sizeof(xswa));
    xswa.colormap = XCreateColormap(dpy, RootWindow (dpy, vi->screen), vi->visual, AllocNone);
  printf("%x\n", vi->visualid);
    win = XCreateWindow (dpy, RootWindow (dpy, vi->screen),
	    xsh.x, xsh.y, xsh.width, xsh.height,
	    0, vi->depth, CopyFromParent,
	    vi->visual, CWColormap, &xswa);
    printf("%x\n", win);
    XFree(vi);

    XSetStandardProperties (dpy, win, PACKAGE, PACKAGE, None,
	    argv, argc, &xsh);
    XSetWMHints (dpy, win, &xwmh);

    XSelectInput (dpy, win, StructureNotifyMask | ButtonPressMask | ButtonReleaseMask);

    if(backend == BACKEND_IMAGE)
    	space = 0;
    else if(backend == BACKEND_X11)
    	space = cairo_xlib_space_wrap(dpy, DefaultScreen (dpy), 0);
    else if(backend == BACKEND_OSMESA)
    	space = cairo_osmesa_space_create(0, 0, 0);
    else if(backend == BACKEND_GLX)
    	space = cairo_glx_space_wrap(0, dpy, 0, 1, 0, 0);
    else if(backend == BACKEND_GALLIUM_SOFTPIPE)
	space = cairo_gallium_softpipe_space_create(0);
    else if(backend == BACKEND_GALLIUM_HARDPIPE)
    	space = cairo_gallium_hardpipe_space_create(0);
    else
    {
        usage(argv[0]);
        exit(1);
    }

    if(invisible)
        win_space = 0;
    else if(backend == BACKEND_X11 || (backend == BACKEND_GLX && direct))
	win_space = space;
    else
    	win_space = cairo_xlib_space_wrap(dpy, DefaultScreen (dpy), 0);
    
    if(win_space)
        win_surface = cairo_surface_create_for_drawable(win_space, 0, CAIRO_CONTENT_COLOR, win, 0, 1, width, height);
    else
        win_surface = 0;

    if(direct && space == win_space && win_surface)
        surface = win_surface;
    else
        surface = cairo_surface_create(space, 0, dst_alpha ? CAIRO_CONTENT_COLOR_ALPHA : CAIRO_CONTENT_COLOR, width, height, 8, dst_alpha ? 8 : 0, 0, 0);

    if (cairo_surface_status (surface)) {
	fprintf (stderr, "error constructing surface: %s\n",
		 cairo_status_to_string (cairo_surface_status (surface)));
	exit (1);
    }

    if(setup)
	setup(surface, width, height);

    XMapWindow (dpy, win);
    XSync (dpy, True);

    if (show_gps) {
	XSync (dpy, False);
	usleep (200000);
	XSync (dpy, False);
    }

    signal (SIGALRM, alarmhandler);
    alarm (5);

    for (;;) {
	if (XPending (dpy)) {
	    while (XPending (dpy)) {
		XNextEvent (dpy, &event);
		if (event.type == ButtonRelease)
		{
			if(aa == 16)
				aa = 255 * 255;
			else if(aa == 255 * 255)
				aa = 1;
			else
				aa = aa << 1;

			fprintf(stderr, "Setting antialiasing samples to %i\n", aa);
		}
		if (event.type == ConfigureNotify && (width != event.xconfigure.width || height != event.xconfigure.height)) {
		    width = event.xconfigure.width;
		    height = event.xconfigure.height;

                    if(win_surface)
	                 cairo_surface_create_for_drawable(0, win_surface, 0, 0, 0, 1, width, height);

		    if(surface != win_surface)
		    {
			    surface = cairo_surface_create(space, surface, dst_alpha ? CAIRO_CONTENT_COLOR_ALPHA : CAIRO_CONTENT_COLOR, width, height, 8, dst_alpha ? 8 : 0, 0, 0);
		    }
		}
	    }
	} else {
	    cr = cairo_create (surface);
	    cairo_set_tolerance(cr, 0.5);
	    cairo_set_antialias(cr, CAIRO_ANTIALIAS_GRAY | (aa << CAIRO_ANTIALIAS_SAMPLES_SHIFT));
	    render(cr, width, height);
	    cairo_destroy (cr);

            if(win_surface)
            {
                if(win_surface != surface)
                {
		    cairo_t *cr2 = cairo_create (win_surface);
		    cairo_set_source_surface (cr2, surface, 0, 0);
		    cairo_set_operator(cr2, CAIRO_OPERATOR_SOURCE);
		    cairo_paint (cr2);
		    cairo_destroy (cr2);
		}

		cairo_surface_show_page(win_surface);
            }

	    frame_cnt++;
	}
    }

    exit (1);
}
