/*
 * Copyright � 2004 David Reveman, Peter Nilsson
 *
 * Permission to use, copy, modify, distribute, and sell this software
 * and its documentation for any purpose is hereby granted without
 * fee, provided that the above copyright notice appear in all copies
 * and that both that copyright notice and this permission notice
 * appear in supporting documentation, and that the names of
 * David Reveman and Peter Nilsson not be used in advertising or
 * publicity pertaining to distribution of the software without
 * specific, written prior permission. David Reveman and Peter Nilsson
 * makes no representations about the suitability of this software for
 * any purpose. It is provided "as is" without express or implied warranty.
 *
 * DAVID REVEMAN AND PETER NILSSON DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL DAVID REVEMAN AND
 * PETER NILSSON BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 *
 * Authors: David Reveman <c99drn@cs.umu.se>
 *          Peter Nilsson <c99pnn@cs.umu.se>
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cairo.h>
#include <cairo-ft.h>

static cairo_font_face_t *ft_face;

static char *str = "CAIRO";

void
text3_setup (cairo_surface_t *surface, int w, int h)
{
    FcResult result;
    FcPattern *pattern, *resolved;

    pattern = FcPatternCreate ();
    FcPatternAddString (pattern, FC_FAMILY, "Sans");
    FcPatternAddInteger (pattern, FC_SLANT, FC_SLANT_ROMAN);
    FcPatternAddInteger (pattern, FC_WEIGHT, FC_WEIGHT_MEDIUM);
    FcPatternAddInteger (pattern, FC_PIXEL_SIZE, 100);

    FcConfigSubstitute (NULL, pattern, FcMatchPattern);
    FcDefaultSubstitute (pattern);

    resolved = FcFontMatch (NULL, pattern, &result);
    if (!resolved)
    {
	printf ("FcFontMatch: failed\n");
	exit (1);
    }

    ft_face = cairo_ft_font_face_create_for_pattern (resolved);
    if (!ft_face)
    {
	printf ("cairo_ft_font_face_create_for_pattern: failed\n");
	exit (1);
    }

    FcPatternDestroy (resolved);
    FcPatternDestroy (pattern);
}

#define N_GLYPHS 5

void
text3_render (cairo_t *cr, int w, int h)
{
    cairo_matrix_t scale, ctm;
    cairo_scaled_font_t *ft_font;
    cairo_font_options_t *options;
    cairo_glyph_t glyphs[N_GLYPHS];
    int           ucs4[N_GLYPHS];
    int x, y, i, slen;
    FT_Face face;
    cairo_pattern_t* gradient;

    options =  cairo_font_options_create ();

    cairo_matrix_init_scale (&scale, 100.0, 100.0);
    cairo_set_font_face (cr, ft_face);
    cairo_set_font_matrix (cr, &scale);
    cairo_get_matrix (cr, &ctm);
    ft_font = cairo_scaled_font_create (ft_face, &scale, &ctm, options);

    cairo_font_options_destroy (options);

    slen = strlen (str);
    for (i = 0; i < N_GLYPHS; i++)
	FcUtf8ToUcs4 (&str[i % slen], &ucs4[i], 1);

    face = cairo_ft_scaled_font_lock_face (ft_font);

    for (i = 0; i < N_GLYPHS; i++) {
	glyphs[i].index = FT_Get_Char_Index (face, ucs4[i]);
	glyphs[i].x = 100 * i;
	glyphs[i].y = 0;
    }

    cairo_ft_scaled_font_unlock_face (ft_font);

    cairo_save(cr);

    gradient = cairo_pattern_create_linear(0, 0, w, 0);
    cairo_pattern_add_color_stop_rgba(gradient, 0, 1, 0, 0, 0.5);
    cairo_pattern_add_color_stop_rgba(gradient, 1, 0, 0, 1, 0.5);

    cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
    cairo_rectangle (cr, 0, 0, w, h);
    cairo_set_source_rgba (cr, 1.0, 1.0, 1.0, 1.0);
    cairo_fill (cr);

    /*
    cairo_push_group(cr);

    cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
    cairo_rectangle (cr, 0, 0, w, h);
    cairo_set_source_rgba (cr, 0.0, 0.0, 0.0, 0.0);
    cairo_fill (cr);
    */

    cairo_translate(cr, 0, 100);
    cairo_set_operator (cr, CAIRO_OPERATOR_OVER);
    cairo_set_source_rgba (cr, 0.0, 0.0, 0.0, 0.5);
    cairo_show_glyphs (cr, glyphs, N_GLYPHS);

    cairo_translate(cr, 0, 100);
    cairo_set_operator (cr, CAIRO_OPERATOR_OVER);
    cairo_set_source(cr, gradient);
    cairo_show_glyphs (cr, glyphs, N_GLYPHS);

    cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
    cairo_set_source_rgba (cr, 0.0, 1.0, 0.0, 0.5);
//    cairo_rectangle(cr, 30, 40, w - 100, 40);
//    cairo_clip(cr);
    cairo_paint(cr);
    cairo_paint(cr);
    cairo_reset_clip(cr);

    cairo_translate(cr, 0, 100);

//    cairo_set_operator (cr, CAIRO_OPERATOR_DEST_OVER);
//    cairo_set_source(cr, gradient);
//    cairo_show_glyphs (cr, glyphs, N_GLYPHS);

    /*
    cairo_pop_group_to_source(cr);
    cairo_set_operator(cr, CAIRO_OPERATOR_OVER);
    cairo_paint(cr);
    */

    cairo_restore(cr);
    cairo_pattern_destroy(gradient);
}
